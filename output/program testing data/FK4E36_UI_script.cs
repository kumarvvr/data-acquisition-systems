﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;
using System.Windows.Forms;
using BHEL.PUMPSDAS.Datatypes;


namespace userscript
{
    namespace FK4E36_DESIGN
    {
        // Note This will be an external script file.
        public class FK4E36_DESIGN_UIConfig : UIConfigurator
		{
            List<Label> UILabelList;
            List<DataGridView> UIGridList;
            List<AlarmStatus> chAlarmListPrevious;
            List<AlarmColors> alarmColors;

            ResultParamsBase rp, rpAvg;
            MeasurementProfileBase mp, mpAvg;
            SpecifiedParamsBase sp;
            TestSetupParamsBase tsp;
            MachineDetailsBase md;

            List<MeasurementProfileDetails> mpDetails;
            Dictionary<Styles,List<DataGridViewCellStyle>> cellStyles;


            public FK4E36_DESIGN_UIConfig() : base()
            {             
                
            }
			
			public virtual void InitializeUIConfig(ref List<Label> UILabelList,
                                                            ref List<DataGridView> UIGridList,
                                                            List<AlarmColors> alarmColors,
                                                            ref ResultParamsBase rp,
                                                            ref ResultParamsBase rpAvg,
                                                            ref MeasurementProfileBase mp,
                                                            ref MeasurementProfileBase mpAvg,
                                                            ref SpecifiedParamsBase sp,
                                                            ref TestSetupParamsBase tsp,
                                                            ref MachineDetailsBase md,
                                                            List<MeasurementProfileDetails> mpDetails,
                                                            Dictionary<Styles, List<DataGridViewCellStyle>> cellStyles)
                {
					this.UILabelList = UILabelList;
					this.UIGridList = UIGridList;
					this.alarmColors = alarmColors;
					this.rp = rp;
					this.rpAvg = rpAvg;
					this.mp = mp;
					this.sp = sp;
					this.tsp = tsp;
					this.md = md;
					this.mpDetails = mpDetails;
					this.cellStyles = cellStyles;
                }

            public override void UpdateStaticUIControls()
            {
                UILabelList[0].Text = "Test 4";
                UILabelList[1].Text = "Test yahoo";
                UILabelList[2].Text = "Test 3";
                UILabelList[3].Text = "Test 4";
                UILabelList[4].Text = "Test 5";
                UILabelList[5].Text = "Test 6";
            }

            public override void UpdateDynamicUIControls(List<AlarmStatus> chAlarmListCurrent)
            {			
                UIGridList[0].Rows[0].Cells[0].Value = "Hello There.. it's alive !!";//mpDetails[0].paramdescription;
            }

		}

    }
}
