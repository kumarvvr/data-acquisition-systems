﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;
using BHEL.PUMPSDAS.Datatypes;



namespace userscript
{
    namespace FK4E36_DESIGN
    {
        // Note This will be an external script file.
        public class MachineDetails : MachineDetailsBase
        {
            public string frameName { get; set; }

            public string frameType { get; set; }

            public string frameDesc { get; set; }

            public MachineDetails()
                : base()
            {
            }
        }
        public class MeasurementProfile : MeasurementProfileBase
        {
            public double pumpspeed { get; set; }
            public double suctiontemp { get; set; }
            public double suctionflow { get; set; }
            public double balancingleakflow { get; set; }
            public double suctionpressure { get; set; }
            public double dischargepressure { get; set; }
            public double motorspeed { get; set; }
            public double motorvoltage { get; set; }
            public double motorcurrent { get; set; }
            public double motorfieldcurrent { get; set; }
            public double motorinputpower { get; set; }
            public double vib_de_h { get; set; }
            public double vib_de_v { get; set; }
            public double vib_nde_h { get; set; }
            public double vib_nde_v { get; set; }
            public double vib_nde_a { get; set; }
            public double inletoiltemp { get; set; }
            public double journal_de { get; set; }
            public double journal_nde { get; set; }
            public double thrust_active1 { get; set; }
            public double thrust_active2 { get; set; }
            public double thrust_nonactive1 { get; set; }
            public double thrust_nonactive2 { get; set; }
            public double sealoutlettemp_de { get; set; }
            public double sealoutlettemp_nde { get; set; }
            
            public MeasurementProfile() : base() { }
        }
        public class ResultParameters : ResultParamsBase
        {
            public double percentageflow { get; set; }
            public double pumptestspeed { get; set; }
            public double suctiontemp { get; set; }
            public double suctionspecificweight { get; set; }
            public double suctionflow { get; set; }
            public double suctionpressure { get; set; }
            public double dischargepressure { get; set; }
            public double suctionvelocity { get; set; }
            public double dischargevelocity { get; set; }
            public double dischargevelhead { get; set; }
            public double dischargehead { get; set; }
            public double motorspeed { get; set; }
            public double motorvoltage { get; set; }
            public double motorcurrent { get; set; }
            public double motorfieldcurrent { get; set; }
            public double motorinputpower { get; set; }
            public double motorlosses { get; set; }
            public double motoroutputpower { get; set; }
            public double hcmechanicallosses { get; set; }
            public double hcsliplosses { get; set; }
            public double pumpinputpower { get; set; }

            public double ss_suctionflow { get; set; }
            public double ss_dischargehead { get; set; }
            public double ss_pumpinputpower { get; set; }
            public double ss_efficiency { get; set; }

            public double st_pumpinputpower { get; set; }
            public double st_efficiency { get; set; }
            
            public ResultParameters() : base() { }
        }
        public class SpecifiedParameters : SpecifiedParamsBase
        {
            public double suctionflowrate { get; set; }
            public double dischargehead { get; set; }
            public double speed { get; set; }
            public double power { get; set; }
            public double efficiency { get; set; }
            public double temperature { get; set; }
            public double specificweight { get; set; }
            public double npsh { get; set; }

            public LookupData designflowhead { get; set; }
            public LookupData designfloweff { get; set; }

            public string testprocedure { get; set; }
            public string teststandard { get; set; }
            public string testdescription { get; set; }


            public SpecifiedParameters() : base() { }
        }
        public class TestSetupParameters : TestSetupParamsBase
        {
            #region Plant parameters
            public double suctionPipeID { get; set; }
            public double dischargePipeID { get; set; }

            public string sucflowmeterref { get; set; }
            #endregion

            #region Motor details

            public string motortype { get; set; }
            public string motorserial { get; set; }
            public string motorpower { get; set; }
            public string motorvoltage { get; set; }
            public string motorcurrent { get; set; }
            public string motorspeed { get; set; }

            public LookupData motormechanicalloss { get; set; }
            public LookupData motorironloss { get; set; }
            public LookupData motorcopperloss { get; set; }
            public LookupData motorfieldloss { get; set; }

            #endregion

            #region Hydraulic coupling details

            public string hctype { get; set; }
            public string hcserial { get; set; }
            public string hcinputratio { get; set; }
            public string hcoutputratio { get; set; }

            public LookupData hcmechanicalloss { get; set; }

            #endregion

            public string testtitle { get; set; }

            public TestSetupParameters() : base() { }
        }
        public class Machine : MachineDescriptor
        {

            //md - Machine details
            //mp - Measurement Parameters
            //rp - Result Parameters
            //sp - SpecifiedParamaters
            //tsp - TestSetupParameters

            // The framework creates the above objects and call this class
            // with the objects in the constructor.
            // We can override the compute function to do the necessary stuff.

            // Constructor
            public Machine(MachineDetailsBase d1, MeasurementProfileBase d2, ResultParamsBase d3, SpecifiedParamsBase d4, TestSetupParamsBase d5) 
                : base(d1, d2, d3, d4, d5) 
            {
                
            }

            public override bool Compute()
            {
                // Modify the objects here
                rp.SetDoubleValue("ss_pumpinputpower", sp.GetDoubleValue("suctionflowrate") * 200.5);
                rp.SetDoubleValue("st_efficiency", 250.2);

                return base.Compute();
            }

        }
    }
}
