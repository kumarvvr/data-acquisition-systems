﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;
using System.Windows.Forms;
using BHEL.PUMPSDAS.Datatypes;


namespace userscript
{
    namespace FA1B75
    {
        // Note This will be an external script file.

        public struct paramUILocation
        {
            public DataGridViewRow dgvRow;
        }


        public class FA1B75_UIConfig : UIConfigurator
		{
            List<Label> UILabelList;
            List<DataGridView> UIGridList;
            List<AlarmStatus> chAlarmListPrevious;
			List<AlarmStatus> chAlarmListCurrent;

            ResultParamsBase rp, rpAvg;
            MeasurementProfileBase mp, mpAvg;
            SpecifiedParamsBase sp;
            TestSetupParamsBase tsp;
            MachineDetailsBase md;
			MachineDescriptor mcd;

            List<MeasurementProfileDetails> mpDetails;
			List<ResultParamsDetails> rpDetails;
			
            AlarmColors alarmColors;

            Dictionary<string, DataGridViewCell> mp_uiDescMap,mp_uiUnitsMap,rp_uiDescMap,rp_uiUnitsMap;
			Dictionary<string, DataGridViewCell> mp_uiRealMap,mp_uiAvgMap,rp_uiRealMap,rp_uiAvgMap;

            public FA1B75_UIConfig() : base()
            {             
                
            }
			
			public override void InitializeUIConfig(ref List<Label> UILabelList,
                                                            ref List<DataGridView> UIGridList,
                                                            ref ResultParamsBase rp,
                                                            ref ResultParamsBase rpAvg,
                                                            ref MeasurementProfileBase mp,
                                                            ref MeasurementProfileBase mpAvg,
                                                            ref SpecifiedParamsBase sp,
                                                            ref TestSetupParamsBase tsp,
                                                            ref MachineDetailsBase md,
															ref MachineDescriptor mcd,
                                                            List<MeasurementProfileDetails> mpDetails,
															List<ResultParamsDetails> rpDetails,
                                                            AlarmColors alarmColors)
                {
					this.UILabelList = UILabelList;
					this.UIGridList = UIGridList;
					this.rp = rp;
					this.rpAvg = rpAvg;
					this.mp = mp;
					this.mpAvg = mpAvg;
					this.sp = sp;
					this.tsp = tsp;
					this.md = md;
					this.mpDetails = mpDetails;
					this.alarmColors = alarmColors;
					this.mcd = mcd;
					this.rpDetails = rpDetails;
					
					mp_uiDescMap = new Dictionary<string,DataGridViewCell>();
					mp_uiUnitsMap = new Dictionary<string,DataGridViewCell>();
					rp_uiDescMap = new Dictionary<string,DataGridViewCell>();
					rp_uiUnitsMap = new Dictionary<string,DataGridViewCell>();
					
					mp_uiRealMap = new Dictionary<string,DataGridViewCell>();
					mp_uiAvgMap = new Dictionary<string,DataGridViewCell>();
					rp_uiRealMap = new Dictionary<string,DataGridViewCell>();
					rp_uiAvgMap = new Dictionary<string,DataGridViewCell>();
					
					for(int i = 0;i<17;i++)
					{
						UIGridList[0].Rows.Add(new Object[]{"","","",""});
						UIGridList[2].Rows.Add(new Object[]{"","","",""});
					}
					for(int i = 0;i<5;i++)
					{
						UIGridList[1].Rows.Add(new Object[]{"","","",""});
						UIGridList[3].Rows.Add(new Object[]{"","","",""});
					}
					
					chAlarmListCurrent = new List<AlarmStatus>();
					chAlarmListPrevious = new List<AlarmStatus>();
                } 

            public override void UpdateStaticUIControls()
            {
			
				// Add all the mapping code here....
				rp_uiDescMap.Add("_percentageflow",UIGridList[0].Rows[0].Cells[0]);
				mp_uiDescMap.Add("_pumpspeed",UIGridList[0].Rows[1].Cells[0]);
				mp_uiDescMap.Add("_suctionwatertemp",UIGridList[0].Rows[2].Cells[0]);
				mp_uiDescMap.Add("_suctionflow",UIGridList[0].Rows[3].Cells[0]);
				mp_uiDescMap.Add("_suctionpressure",UIGridList[0].Rows[4].Cells[0]);
				mp_uiDescMap.Add("_dischargepressure",UIGridList[0].Rows[5].Cells[0]);
				mp_uiDescMap.Add("_motorvoltage",UIGridList[1].Rows[0].Cells[0]);
				mp_uiDescMap.Add("_motorcurrent",UIGridList[1].Rows[1].Cells[0]);
				mp_uiDescMap.Add("_motorinputpower",UIGridList[1].Rows[2].Cells[0]);
				mp_uiDescMap.Add("_inletoiltemp",UIGridList[2].Rows[0].Cells[0]);
				mp_uiDescMap.Add("_journal_de",UIGridList[2].Rows[1].Cells[0]);
				mp_uiDescMap.Add("_journal_nde",UIGridList[2].Rows[2].Cells[0]);
				mp_uiDescMap.Add("_vib_de_h",UIGridList[3].Rows[0].Cells[0]);
				mp_uiDescMap.Add("_vib_de_v",UIGridList[3].Rows[1].Cells[0]);
				mp_uiDescMap.Add("_vib_nde_h",UIGridList[3].Rows[2].Cells[0]);
				mp_uiDescMap.Add("_vib_nde_v",UIGridList[3].Rows[3].Cells[0]);
				mp_uiDescMap.Add("_vib_nde_a",UIGridList[3].Rows[4].Cells[0]);
				rp_uiUnitsMap.Add("_percentageflow",UIGridList[0].Rows[0].Cells[1]);
				mp_uiUnitsMap.Add("_pumpspeed",UIGridList[0].Rows[1].Cells[1]);
				mp_uiUnitsMap.Add("_suctionwatertemp",UIGridList[0].Rows[2].Cells[1]);
				mp_uiUnitsMap.Add("_suctionflow",UIGridList[0].Rows[3].Cells[1]);
				mp_uiUnitsMap.Add("_suctionpressure",UIGridList[0].Rows[4].Cells[1]);
				mp_uiUnitsMap.Add("_dischargepressure",UIGridList[0].Rows[5].Cells[1]);
				mp_uiUnitsMap.Add("_motorvoltage",UIGridList[1].Rows[0].Cells[1]);
				mp_uiUnitsMap.Add("_motorcurrent",UIGridList[1].Rows[1].Cells[1]);
				mp_uiUnitsMap.Add("_motorinputpower",UIGridList[1].Rows[2].Cells[1]);
				mp_uiUnitsMap.Add("_inletoiltemp",UIGridList[2].Rows[0].Cells[1]);
				mp_uiUnitsMap.Add("_journal_de",UIGridList[2].Rows[1].Cells[1]);
				mp_uiUnitsMap.Add("_journal_nde",UIGridList[2].Rows[2].Cells[1]);
				mp_uiUnitsMap.Add("_vib_de_h",UIGridList[3].Rows[0].Cells[1]);
				mp_uiUnitsMap.Add("_vib_de_v",UIGridList[3].Rows[1].Cells[1]);
				mp_uiUnitsMap.Add("_vib_nde_h",UIGridList[3].Rows[2].Cells[1]);
				mp_uiUnitsMap.Add("_vib_nde_v",UIGridList[3].Rows[3].Cells[1]);
				mp_uiUnitsMap.Add("_vib_nde_a",UIGridList[3].Rows[4].Cells[1]);
			
				for (int i = 0; i < mpDetails.Count; i++)
				{
					MeasurementProfileDetails mpd = mpDetails[i];
					string pname = mpd.paramname;
					try
					{
						mp_uiDescMap[pname].Value = mpd.paramdescription;
						mp_uiUnitsMap[pname].Value = mpd.paramunits;
					}
					catch(Exception ex)
					{
						// Do Nothing...
					}

				}
				for (int i = 0; i < rpDetails.Count; i++)
				{
					ResultParamsDetails rpd = rpDetails[i];
					string pname = rpd.paramname;
					try
					{
						rp_uiDescMap[pname].Value = rpd.paramdescription;
						rp_uiUnitsMap[pname].Value = rpd.paramunits;
					}
					catch(Exception ex)
					{
						// Ignore the error...
					}

				}

                ResetAlarmColors(true);
				
				UILabelList[0].Text += sp.GetProjectName();
                UILabelList[1].Text += mcd.MachineUniqueCode;
				ToolTip tooltipmucode = new ToolTip();
				tooltipmucode.SetToolTip(UILabelList[1], mcd.MachineDesc);
                UILabelList[2].Text += md.GetTestReference();
                UILabelList[3].Text = "";
                UILabelList[4].Text = "";
                UILabelList[5].Text = "";
            }

            private void ResetAlarmColors(bool isResetAll)
            {
				string pname;
				if(isResetAll)
				{
					for (int i = 0; i < UIGridList.Count; i++)
					{
						for(int j=0;j<UIGridList[i].Rows.Count;j++)
						{
							UIGridList[i].Rows[j].Cells[3].Style.ForeColor = alarmColors.defaultColor;
							UIGridList[i].Rows[j].Cells[3].Style.BackColor = alarmColors.defaultBackColor;
						}
					} 
					return;
				}
				else
				{
					if(chAlarmListPrevious != null)
					{
					for(int i = 0;i<chAlarmListPrevious.Count;i++)
					{
						pname = chAlarmListPrevious[i].pname;
						mp_uiRealMap[pname].Style.ForeColor = alarmColors.defaultColor;
					}
					}
				}
				return;
            }

            public override void UpdateDynamicUIControls(List<AlarmStatus> chAlarmListCurrent, bool isAverageUpdateRequired)
            {
				string pname;
				
				// Add the value update codes here...
				// Real Values
				UIGridList[0].Rows[0].Cells[3].Value=rp.GetValue("_percentageflow");
				UIGridList[0].Rows[1].Cells[3].Value=mp.GetValue("_pumpspeed");
				UIGridList[0].Rows[2].Cells[3].Value=mp.GetValue("_suctionwatertemp");
				UIGridList[0].Rows[3].Cells[3].Value=mp.GetValue("_suctionflow");
				UIGridList[0].Rows[4].Cells[3].Value=mp.GetValue("_suctionpressure");
				UIGridList[0].Rows[5].Cells[3].Value=mp.GetValue("_dischargepressure");
				UIGridList[1].Rows[0].Cells[3].Value=mp.GetValue("_motorvoltage");
				UIGridList[1].Rows[1].Cells[3].Value=mp.GetValue("_motorcurrent");
				UIGridList[1].Rows[2].Cells[3].Value=mp.GetValue("_motorinputpower");
				UIGridList[2].Rows[0].Cells[3].Value=mp.GetValue("_inletoiltemp");
				UIGridList[2].Rows[1].Cells[3].Value=mp.GetValue("_journal_de");
				UIGridList[2].Rows[2].Cells[3].Value=mp.GetValue("_journal_nde");
				UIGridList[3].Rows[0].Cells[3].Value=mp.GetValue("_vib_de_h");
				UIGridList[3].Rows[1].Cells[3].Value=mp.GetValue("_vib_de_v");
				UIGridList[3].Rows[2].Cells[3].Value=mp.GetValue("_vib_nde_h");
				UIGridList[3].Rows[3].Cells[3].Value=mp.GetValue("_vib_nde_v");
				UIGridList[3].Rows[4].Cells[3].Value=mp.GetValue("_vib_nde_a");

				if(isAverageUpdateRequired)
				{
					UIGridList[0].Rows[0].Cells[2].Value=rpAvg.GetValue("_percentageflow");
					UIGridList[0].Rows[1].Cells[2].Value=mpAvg.GetValue("_pumpspeed");
					UIGridList[0].Rows[2].Cells[2].Value=mpAvg.GetValue("_suctionwatertemp");
					UIGridList[0].Rows[3].Cells[2].Value=mpAvg.GetValue("_suctionflow");
					UIGridList[0].Rows[4].Cells[2].Value=mpAvg.GetValue("_suctionpressure");
					UIGridList[0].Rows[5].Cells[2].Value=mpAvg.GetValue("_dischargepressure");
					UIGridList[1].Rows[0].Cells[2].Value=mpAvg.GetValue("_motorvoltage");
					UIGridList[1].Rows[1].Cells[2].Value=mpAvg.GetValue("_motorcurrent");
					UIGridList[1].Rows[2].Cells[2].Value=mpAvg.GetValue("_motorinputpower");
					UIGridList[2].Rows[0].Cells[2].Value=mpAvg.GetValue("_inletoiltemp");
					UIGridList[2].Rows[1].Cells[2].Value=mpAvg.GetValue("_journal_de");
					UIGridList[2].Rows[2].Cells[2].Value=mpAvg.GetValue("_journal_nde");
					UIGridList[3].Rows[0].Cells[2].Value=mpAvg.GetValue("_vib_de_h");
					UIGridList[3].Rows[1].Cells[2].Value=mpAvg.GetValue("_vib_de_v");
					UIGridList[3].Rows[2].Cells[2].Value=mpAvg.GetValue("_vib_nde_h");
					UIGridList[3].Rows[3].Cells[2].Value=mpAvg.GetValue("_vib_nde_v");
					UIGridList[3].Rows[4].Cells[2].Value=mpAvg.GetValue("_vib_nde_a");
				}
				this.chAlarmListCurrent = chAlarmListCurrent;
				ResetAlarmColors(false);
				//this.chAlarmListPrevious = null;
				this.chAlarmListPrevious = this.chAlarmListCurrent;
				if(chAlarmListCurrent !=null)
				{
				for(int i = 0;i<this.chAlarmListCurrent.Count;i++)
				{
					pname = this.chAlarmListCurrent[i].pname;
					if(this.chAlarmListCurrent[i].alarmType == AlarmType.HIGH_ALARM)
					mp_uiRealMap[pname].Style.ForeColor = alarmColors.highAlarmColor;
					else
					mp_uiRealMap[pname].Style.ForeColor = alarmColors.lowAlarmColor;
				}
				}

            }

		}

    }
}
