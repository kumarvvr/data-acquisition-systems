﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;
using BHEL.PUMPSDAS.Datatypes;



namespace userscript
{
    namespace <##MUCODE##>
    {
       public class <##MUCODE##>_ErrorCorrector : ErrorCorrector
	   {
			ResultParamsBase rp;
			MeasurementProfileBase mp;
			SpecifiedParamsBase sp;
			TestSetupParamsBase tsp;
			MachineDetailsBase md;
			MachineDescriptor mcd;
			
			public <##MUCODE##>_ErrorCorrector(){}
			
			public override void InitializeErrorCorrector(
                                    ref ResultParamsBase rp,
                                    ref MeasurementProfileBase mp,
                                    ref SpecifiedParamsBase sp,
                                    ref TestSetupParamsBase tsp,
                                    ref MachineDetailsBase md,
                                    ref MachineDescriptor mcd)
			{
				this.rp = rp;
				this.mp = mp;
				this.sp = sp;
				this.tsp = tsp;
				this.md = md;
				this.mcd = mcd;

			}
			
			public override void ErrorCorrect(int PointNumber)
			{
				// Note : Only modifications in the "Measurement Profile" mp object will be considered and incorporated.
			}
				
	   }

        
    }
}
