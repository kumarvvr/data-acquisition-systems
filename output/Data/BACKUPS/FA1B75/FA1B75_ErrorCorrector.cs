using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;
using BHEL.PUMPSDAS.Datatypes;



namespace userscript
{
    namespace FA1B75
    {
       public class FA1B75_ErrorCorrector : ErrorCorrector
	   {
			ResultParamsBase rp;
			MeasurementProfileBase mp;
			SpecifiedParamsBase sp;
			TestSetupParamsBase tsp;
			MachineDetailsBase md;
			MachineDescriptor mcd;
			
			double dischargepressure,flow,mf,pc,flowStretch,motorcurrent,motorpower,de_journal,nde_journal,inletoiltemp;
			
			public FA1B75_ErrorCorrector(){}
			
			public override void InitializeErrorCorrector(
                                    ref ResultParamsBase rp,
                                    ref MeasurementProfileBase mp,
                                    ref SpecifiedParamsBase sp,
                                    ref TestSetupParamsBase tsp,
                                    ref MachineDetailsBase md,
                                    ref MachineDescriptor mcd)
			{
				this.rp = rp;
				this.mp = mp;
				this.sp = sp;
				this.tsp = tsp;
				this.md = md;
				this.mcd = mcd;
				
				// Also initialize internal data stores....
				
				mf=1.0;
				pc = 0.0;

			}
			
			private void ExtractData()
			{
				//_inletoiltemp
				//_journal_de
				//_journal_nde

				flow = mp.GetValue("_suctionflow");
				dischargepressure = mp.GetValue("_dischargepressure");
				motorcurrent = mp.GetValue("_motorcurrent");
				motorpower = mp.GetValue("_motorinputpower");
				de_journal = mp.GetValue("_journal_de");
				nde_journal = mp.GetValue("_journal_nde");
				inletoiltemp = mp.GetValue("_inletoiltemp");
				
				
			}
						
			private void UpdateData()
			{
				/*
				_percentageflow
				_pumpspeed
				_suctionwatertemp
				_suctionflow
				_suctionpressure
				_dischargepressure
				_motorspeed
				_motorvoltage
				_motorcurrent
				_motorinputpower
				_inletoiltemp
				_journal_de
				_journal_nde

				*/
				
				mp.SetValue("_suctionflow",flow);
				mp.SetValue("_dischargepressure",dischargepressure);
				mp.SetValue("_motorcurrent",motorcurrent);
				mp.SetValue("_motorinputpower",motorpower);
				mp.SetValue("_journal_de",de_journal);
				mp.SetValue("_journal_nde",nde_journal);
				mp.SetValue("_inletoiltemp",_inletoiltemp);
			}
			public override void ErrorCorrect(int PointNumber, bool isAuto)
			{
				ExtractData();

				mf = 1;
				pc = 1;
				flowStretch = 1.0;
				
				dischargepressure = dischargepressure + pc;
				motorcurrent = motorcurrent * mf;
				motorpower = motorpower * mf;
				flow = flow * flowStretch;
				
				
				if(PointNumber == 1)
				{
					// Modify Mf and Pc for various points here...
				}
				
				if(PointNumber == 1)
				{
					// Modify Mf and Pc for various points here...
				}
				
				UpdateData();
			}
				
	   }

        
    }
}
