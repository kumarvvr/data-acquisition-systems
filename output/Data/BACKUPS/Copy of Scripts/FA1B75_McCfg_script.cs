﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;
using BHEL.PUMPSDAS.Datatypes;

/*
                public void SetDoubleValue(string propertyName, double val)
                public void SetValue(string propertyName, double val) // Only sets a double value...
                public void SetStringValue(string propertyName, string val)
                public void SetLookupValue(string propertyName, LookupData val)
                public double GetDoubleValue(string propertyName)
                public double GetValue(string propertyName) // Also returns a default double value.
                public string GetStringValue(string propertyName)
                public LookupData GetLookupValue(string propertyName)
                public List<DynamicDataProperty> GetDynamicPropertyList()


*/

namespace userscript
{
    namespace FA1B75
    {
        // Note This will be an external script file.
        public class MachineDetails : MachineDetailsBase
        {		
			public MachineDetails()  : base()  {}
			
			public string _frameName {get;set;}
			public string _frameType {get;set;}
			public string _frameDesc {get;set;}

        }
        public class MeasurementProfile : MeasurementProfileBase
        {			
			public double _pumpspeed {get;set;}
			public double _suctionwatertemp {get;set;}
			public double _suctionflow {get;set;}
			public double _suctionpressure {get;set;}
			public double _dischargepressure {get;set;}
			public double _motorspeed {get;set;}
			public double _motorvoltage {get;set;}
			public double _motorcurrent {get;set;}
			public double _motorinputpower {get;set;}
			public double _vib_de_h {get;set;}
			public double _vib_de_v {get;set;}
			public double _vib_nde_h {get;set;}
			public double _vib_nde_v {get;set;}
			public double _vib_nde_a {get;set;}
			public double _inletoiltemp {get;set;}
			public double _journal_de {get;set;}
			public double _journal_nde {get;set;}

            
            public MeasurementProfile() : base() { }
        }
        public class ResultParameters : ResultParamsBase
        {
			public double _percentageflow {get;set;}
			public double _suctiontemp {get;set;}
			public double _suctionspecificweight {get;set;}
			public double _pumptestspeed {get;set;}
			public double _suctionflow {get;set;}
			public double _dischargepressure {get;set;}
			public double _suctionpressure {get;set;}
			public double _dischargehead {get;set;}
			public double _motorvoltage {get;set;}
			public double _motorcurrent {get;set;}
			public double _motorinputpower {get;set;}
			public double _motorefficiency {get;set;}
			public double _motoroutputpower {get;set;}
			public double _pumpinputpower {get;set;}
			public double _pumpefficiency {get;set;}
			public double _ss_suctionflow {get;set;}
			public double _ss_dischargehead {get;set;}
			public double _ss_pumpinputpower {get;set;}
			public double _ss_efficiency {get;set;}
			public double _st_pumpinputpower {get;set;}
			public double _st_efficiency {get;set;}

            public ResultParameters() : base() { }
        }
        public class SpecifiedParameters : SpecifiedParamsBase
        {
			public double _suctionflowrate {get;set;}
			public double _dischargehead {get;set;}
			public double _speed {get;set;}
			public double _power {get;set;}
			public double _efficiency {get;set;}
			public double _temperature {get;set;}
			public double _specificweight {get;set;}
			public double _npsh {get;set;}
			public string _testprocedure {get;set;}
			public string _teststandard {get;set;}
			public string _testdescription {get;set;}



            public SpecifiedParameters() : base() { }
        }
        public class TestSetupParameters : TestSetupParamsBase
        {

			public double _suctionPipeID {get;set;}
			public double _dischargePipeID {get;set;}
			public string _sucflowmeterref {get;set;}
			public LookupData _specificWeight {get;set;}
			public LookupData _kinematicViscosity {get;set;}
			public string _motortype {get;set;}
			public string _motorserial {get;set;}
			public string _motorpower {get;set;}
			public string _motorvoltage {get;set;}
			public string _motorcurrent {get;set;}
			public string _motorspeed {get;set;}
			public LookupData _motorloss {get;set;}
			public string _testtitle {get;set;}


            public TestSetupParameters() : base() { }
        }
        public class Machine : MachineDescriptor
        {

            //md - Machine details
            //mp - Measurement Parameters
            //rp - Result Parameters
            //sp - SpecifiedParamaters
            //tsp - TestSetupParameters

            // The framework creates the above objects and call this class
            // with the objects in the constructor.
            // We can override the compute function to do the necessary stuff.

            // Constructor
			
			// Internal variables...
			// For efficiency
			
			LookupData motorloss,specificWeight,kinematicViscosity;
			double dischargePipeID,suctionPipeID;
			
			double pumpspeed;
			double suctionwatertemp;
			double suctionflow;
			double suctionpressure;
			double dischargepressure;
			double motorvoltage;
			double motorcurrent;
			double motorinputpower;

			double result_percentageflow;
			double result_suctiontemp;
			double result_suctionspecificweight;
			double result_pumptestspeed;
			double result_suctionflow;
			double result_dischargepressure;
			double result_suctionpressure;
			double result_dischargehead;
			double result_motorvoltage;
			double result_motorcurrent;
			double result_motorinputpower;
			double result_motorefficiency;
			double result_motoroutputpower;
			double result_pumpinputpower;
			double result_pumpefficiency;
			double result_ss_suctionflow;
			double result_ss_dischargehead;
			double result_ss_pumpinputpower;
			double result_ss_efficiency;
			double result_st_pumpinputpower;
			double result_st_efficiency;
			
			// Specified parameters....
			double pumpSpecifiedSpeed;
			double pumpSpecifiedFlow;
			double pumpSpecifiedHead;
			double pumpSpecifiedPower;
			double pumpSpecifiedEfficiency;
			double pumpSpecifiedTemp;
			double pumpSpecifiedNPSH;
			
			double kvSpecified;
			double spwtSpecified;
			

			
            public Machine(MachineDetailsBase d1, MeasurementProfileBase d2, ResultParamsBase d3, SpecifiedParamsBase d4, TestSetupParamsBase d5) 
                : base(d1, d2, d3, d4, d5) 
            {
				
			
            }
			
			public override void InitializeInternalData()
			{
			// Pre fetch unchanging values....
				
				// From Test Setup Parameters
                motorloss = tsp.GetLookupValue("_motorloss");
				specificWeight=tsp.GetLookupValue("_specificWeight");
				kinematicViscosity=tsp.GetLookupValue("_kinematicViscosity");
				dischargePipeID = tsp.GetValue("_suctionPipeID");
				suctionPipeID = tsp.GetValue("_dischargePipeID");
				
				// From Specified Parameters
				pumpSpecifiedSpeed = sp.GetValue("_speed");
				pumpSpecifiedFlow = sp.GetValue("_suctionflowrate");
				pumpSpecifiedHead = sp.GetValue("_dischargehead");
				pumpSpecifiedPower = sp.GetValue("_power");
				pumpSpecifiedEfficiency = sp.GetValue("_efficiency");
				pumpSpecifiedTemp = sp.GetValue("_temperature");
				pumpSpecifiedNPSH = sp.GetValue("_npsh");
				
				spwtSpecified = specificWeight.getValueAt(pumpSpecifiedTemp);
				kvSpecified = kinematicViscosity.getValueAt(pumpSpecifiedTemp);

				pumpspeed=-1;
				suctionwatertemp=-1;
				suctionflow=-1;
				suctionpressure=-1;
				dischargepressure=-1;
				motorvoltage=-1;
				motorcurrent=-1;
				motorinputpower=-1;

				result_percentageflow=-1;
				result_suctiontemp=-1;
				result_suctionspecificweight=-1;
				result_pumptestspeed=-1;
				result_suctionflow=-1;
				result_dischargepressure=-1;
				result_suctionpressure=-1;
				result_dischargehead=-1;
				result_motorvoltage=-1;
				result_motorcurrent=-1;
				result_motorinputpower=-1;
				result_motorefficiency=-1;
				result_motoroutputpower=-1;
				result_pumpinputpower=-1;
				result_ss_suctionflow=-1;
				result_ss_dischargehead=-1;
				result_ss_pumpinputpower=-1;
				result_ss_efficiency=-1;
				result_st_pumpinputpower=-1;
				result_st_efficiency=-1;

			}
			
			private void ExtractData()
			{
				pumpspeed = mp.GetValue("_pumpspeed");
				suctionwatertemp = mp.GetValue("_suctionwatertemp");
				suctionflow = mp.GetValue("_suctionflow");
				suctionpressure = mp.GetValue("_suctionpressure");
				dischargepressure = mp.GetValue("_dischargepressure");
				motorvoltage = mp.GetValue("_motorvoltage");
				motorcurrent = mp.GetValue("_motorcurrent");
				motorinputpower = mp.GetValue("_motorinputpower");
			}
			
			private void UpdateData()
			{
				rp.SetValue("_percentageflow",result_percentageflow);
				rp.SetValue("_suctiontemp",result_suctiontemp);
				rp.SetValue("_suctionspecificweight",result_suctionspecificweight);
				rp.SetValue("_pumptestspeed",result_pumptestspeed);
				rp.SetValue("_suctionflow",result_suctionflow);
				rp.SetValue("_dischargepressure",result_dischargepressure);
				rp.SetValue("_suctionpressure",result_suctionpressure);
				rp.SetValue("_dischargehead",result_dischargehead);
				rp.SetValue("_motorvoltage",result_motorvoltage);
				rp.SetValue("_motorcurrent",result_motorcurrent);
				rp.SetValue("_motorinputpower",result_motorinputpower);
				rp.SetValue("_motorefficiency",result_motorefficiency);
				rp.SetValue("_motoroutputpower",result_motoroutputpower);
				rp.SetValue("_pumpinputpower",result_pumpinputpower);				
				rp.SetValue("_pumpefficiency",result_pumpefficiency);
				rp.SetValue("_ss_suctionflow",result_ss_suctionflow);
				rp.SetValue("_ss_dischargehead",result_ss_dischargehead);
				rp.SetValue("_ss_pumpinputpower",result_ss_pumpinputpower);
				rp.SetValue("_ss_efficiency",result_ss_efficiency);
				rp.SetValue("_st_pumpinputpower",result_st_pumpinputpower);
				rp.SetValue("_st_efficiency",result_st_efficiency);
			}

            public override bool Compute()
            {
				ExtractData();	

				// Do the computation here....
				double speedRatio = (pumpSpecifiedSpeed/pumpspeed);
				
				result_percentageflow = suctionflow / pumpSpecifiedFlow * 100;
				result_suctiontemp = suctionwatertemp;
				result_suctionspecificweight = specificWeight.getValueAt(suctionwatertemp);
				result_pumptestspeed = pumpspeed;
				result_suctionflow = suctionflow;
				result_dischargepressure = dischargepressure;
				result_suctionpressure = suctionpressure;
				result_dischargehead = (dischargepressure-suctionpressure)*10/result_suctionspecificweight;
				result_motorvoltage = motorvoltage;
				result_motorcurrent = motorcurrent;
				result_motorinputpower = motorinputpower;
				result_motorefficiency = motorloss.getValueAt(motorinputpower);
				result_motoroutputpower = motorinputpower * result_motorefficiency;
				result_pumpinputpower = result_motoroutputpower;
				result_pumpefficiency = ((suctionflow * result_dischargehead * result_suctionspecificweight)/(367.2 * result_pumpinputpower));
				result_ss_suctionflow = suctionflow * (pumpSpecifiedSpeed/pumpspeed);
				result_ss_dischargehead = result_dischargehead * speedRatio * speedRatio;
				result_ss_pumpinputpower = result_pumpinputpower * speedRatio * speedRatio * speedRatio;
				result_ss_efficiency = result_pumpefficiency;
				result_st_efficiency = 1-((1-result_pumpefficiency) * Math.Pow((kvSpecified/kinematicViscosity.getValueAt(result_suctiontemp)),0.05));
				result_st_pumpinputpower = result_ss_pumpinputpower * (spwtSpecified/result_suctionspecificweight) * (result_ss_efficiency/result_st_efficiency);
				
				result_pumpefficiency = result_pumpefficiency *100;
				result_ss_efficiency = result_ss_efficiency *100;
				result_st_efficiency = result_st_efficiency *100;

				UpdateData();
                return base.Compute();
            }

        }
    }
}
