using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;
using System.Windows.Forms;
using BHEL.PUMPSDAS.Datatypes;


namespace userscript
{
    namespace FK4E36_DESIGN
    {
        // Note This will be an external script file.
		#region DO NOT EDIT
        public struct paramUILocation
        {
            public DataGridViewRow dgvRow;
        }
		#endregion
        
		
		public class FK4E36_DESIGN_UIConfig : UIConfigurator
		{
            List<Label> UILabelList;
            List<DataGridView> UIGridList;
            List<AlarmStatus> chAlarmListPrevious;
			List<AlarmStatus> chAlarmListCurrent;

            ResultParamsBase rp, rpAvg;
            MeasurementProfileBase mp, mpAvg;
            SpecifiedParamsBase sp;
            TestSetupParamsBase tsp;
            MachineDetailsBase md;
			MachineDescriptor mcd;

            List<MeasurementProfileDetails> mpDetails;
			List<ResultParamsDetails> rpDetails;
			
            AlarmColors alarmColors;

            Dictionary<string, DataGridViewCell> mp_uiDescMap,mp_uiUnitsMap,rp_uiDescMap,rp_uiUnitsMap;
			Dictionary<string, DataGridViewCell> mp_uiRealMap,mp_uiAvgMap,rp_uiRealMap,rp_uiAvgMap;

            public FK4E36_DESIGN_UIConfig() : base()
            {             
                
            }
			
			public override void InitializeUIConfig(ref List<Label> UILabelList,
                                                            ref List<DataGridView> UIGridList,
                                                            ref ResultParamsBase rp,
                                                            ref ResultParamsBase rpAvg,
                                                            ref MeasurementProfileBase mp,
                                                            ref MeasurementProfileBase mpAvg,
                                                            ref SpecifiedParamsBase sp,
                                                            ref TestSetupParamsBase tsp,
                                                            ref MachineDetailsBase md,
															ref MachineDescriptor mcd,
                                                            List<MeasurementProfileDetails> mpDetails,
															List<ResultParamsDetails> rpDetails,
                                                            AlarmColors alarmColors)
                {
					this.UILabelList = UILabelList;
					this.UIGridList = UIGridList;
					this.rp = rp;
					this.rpAvg = rpAvg;
					this.mp = mp;
					this.mpAvg = mpAvg;
					this.sp = sp;
					this.tsp = tsp;
					this.md = md;
					this.mpDetails = mpDetails;
					this.alarmColors = alarmColors;
					this.mcd = mcd;
					this.rpDetails = rpDetails;
					
					mp_uiDescMap = new Dictionary<string,DataGridViewCell>();
					mp_uiUnitsMap = new Dictionary<string,DataGridViewCell>();
					rp_uiDescMap = new Dictionary<string,DataGridViewCell>();
					rp_uiUnitsMap = new Dictionary<string,DataGridViewCell>();
					
					mp_uiRealMap = new Dictionary<string,DataGridViewCell>();
					mp_uiAvgMap = new Dictionary<string,DataGridViewCell>();
					rp_uiRealMap = new Dictionary<string,DataGridViewCell>();
					rp_uiAvgMap = new Dictionary<string,DataGridViewCell>();
					UIGridList[0].Rows.Clear();
					UIGridList[1].Rows.Clear();
					UIGridList[2].Rows.Clear();
					UIGridList[3].Rows.Clear();
					
					for(int i = 0;i<17;i++)
					{
						UIGridList[0].Rows.Add(new Object[]{"","","",""});
						UIGridList[2].Rows.Add(new Object[]{"","","",""});
					}
					for(int i = 0;i<5;i++)
					{
						UIGridList[1].Rows.Add(new Object[]{"","","",""});
						UIGridList[3].Rows.Add(new Object[]{"","","",""});
					}
					
					chAlarmListCurrent = new List<AlarmStatus>();
					chAlarmListPrevious = new List<AlarmStatus>();
                } 

            public override void UpdateStaticUIControls()
            {
			
				// Add all the mapping code here....
				
				
				
			
				// Do not modify following code...
				#region DO NOT MODIFY
				for (int i = 0; i < mpDetails.Count; i++)
				{
					MeasurementProfileDetails mpd = mpDetails[i];
					string pname = mpd.paramname;
					try
					{
						mp_uiDescMap[pname].Value = mpd.paramdescription;
						mp_uiUnitsMap[pname].Value = mpd.paramunits;
					}
					catch(Exception ex)
					{
						// Do Nothing...
					}

				}
				for (int i = 0; i < rpDetails.Count; i++)
				{
					ResultParamsDetails rpd = rpDetails[i];
					string pname = rpd.paramname;
					try
					{
						rp_uiDescMap[pname].Value = rpd.paramdescription;
						rp_uiUnitsMap[pname].Value = rpd.paramunits;
					}
					catch(Exception ex)
					{
						// Ignore the error...
					}
				}

                ResetAlarmColors(true);
				
				UILabelList[0].Text += sp.GetProjectName();
                UILabelList[1].Text += mcd.MachineUniqueCode;
				ToolTip tooltipmucode = new ToolTip();
				tooltipmucode.SetToolTip(UILabelList[1], mcd.MachineDesc);
                UILabelList[2].Text += md.GetTestReference();
                UILabelList[3].Text = "";
                UILabelList[4].Text = "";
                UILabelList[5].Text = "";
				
				#endregion
            }
			
			public override void UpdateDynamicUIControls(List<AlarmStatus> chAlarmListCurrent, bool isAverageUpdateRequired)
            {
				
				
				// Add the value update codes here...
				// Real Values
				// Add real values updates here....

				
				if(isAverageUpdateRequired)
				{

					// Add average values updates here..

				}
				
				
				#region DO NOT EDIT
				string pname;
				
				this.chAlarmListCurrent = chAlarmListCurrent;
				ResetAlarmColors(false);
				//this.chAlarmListPrevious = null;
				this.chAlarmListPrevious = this.chAlarmListCurrent;
				
				for(int i = 0;i<this.chAlarmListCurrent.Count;i++)
				{
					pname = this.chAlarmListCurrent[i].pname;
					if(this.chAlarmListCurrent[i].alarmType == AlarmType.HIGH_ALARM)
					mp_uiRealMap[pname].Style.ForeColor = alarmColors.highAlarmColor;
					else
					mp_uiRealMap[pname].Style.ForeColor = alarmColors.lowAlarmColor;
				}
				
				#endregion

            }
			
			
			#region INTERNAL FUNCTIONS
            private void ResetAlarmColors(bool isResetAll)
            {
				string pname;
				if(isResetAll)
				{
					for (int i = 0; i < UIGridList.Count; i++)
					{
						for(int j=0;j<UIGridList[i].Rows.Count;j++)
						{
							UIGridList[i].Rows[j].Cells[3].Style.ForeColor = alarmColors.defaultColor;
							UIGridList[i].Rows[j].Cells[3].Style.BackColor = alarmColors.defaultBackColor;
						}
					} 
					return;
				}
				else
				{
					for(int i = 0;i<chAlarmListPrevious.Count;i++)
					{
						pname = chAlarmListPrevious[i].pname;
						mp_uiRealMap[pname].Style.ForeColor = alarmColors.defaultColor;
					}
				}
				return;
            }
			
			#endregion

		}

    }
}
