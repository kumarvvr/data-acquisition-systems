using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;
using System.Windows.Forms;
using BHEL.PUMPSDAS.Datatypes;


namespace userscript
{
    namespace FA1B75
    {
        // Note This will be an external script file.

        public class FA1B75_LinePrinter : LinePrinterBase
		{
			// Constructor.
            public FA1B75_LinePrinter() : base()
            {             
                
            }
			public override List<LinePrintData> GenerateLinePrintData(List<MeasurementProfileDetails> mpDetails,
                    List<ResultParamsDetails> rpDetails,
                    MeasurementProfileBase mpAvg,
                    ResultParamsBase rpAvg)
			{
				
			
				List<LinePrintData> result = new List<LinePrintData>();
				
				LinePrintData data;
				data = new LinePrintData();
				string pname;
				//Fill up additional data for line print script here...
				//
				//
				//
				data.paramdesc = "Percentage Flow";
				data.paramunits = "%";
				data.value = rpAvg.GetValue("_percentageflow").ToString();
				result.Add(data);
				
				
				for(int i =0;i<mpDetails.Count;i++)
				{
					pname = mpDetails[i].paramname;
					data = new LinePrintData();
					data.paramdesc = mpDetails[i].paramdescription;
					data.paramunits = mpDetails[i].paramunits;
					data.value = mpAvg.GetValue(pname).ToString();
					result.Add(data);
				}
				
				return result;
				
				
				
			}
    }
}
}
