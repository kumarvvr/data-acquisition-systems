using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;
using System.Windows.Forms;
using BHEL.PUMPSDAS.Datatypes;


namespace userscript
{
    namespace FK4E36_DESIGN
    {
        // Note This will be an external script file.

        public class FK4E36_DESIGN_LinePrinter : LinePrinterBase
		{
			// Constructor.
            public FK4E36_DESIGN_LinePrinter() : base()
            {             
                
            }
			public override List<LinePrintData> GenerateLinePrintData(List<MeasurementProfileDetails> mpDetails,
                    List<ResultParamsDetails> rpDetails,
                    MeasurementProfileBase mpAvg,
                    ResultParamsBase rpAvg)
			{
				
			
				List<LinePrintData> result = new List<LinePrintData>();
				
				LinePrintData data;
				data = new LinePrintData();
				
				//Fill up additional data for line print script here...
				//
				//
				//
				
				
				
				for(int i =0;i<mpDetails.Count;i++)
				{
					pname = mpDetails[i].paramname;
					data = new LinePrintData();
					data.paramdesc = mpDetails[i].paramdescription;
					data.paramunits = mpDetails[i].paramunits;
					data.value = mpAvg.GetValue(pname).ToString();
					result.Add(data);
				}
				
				return result;
				
				
				
			}
    }
}
}
