﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;
using System.Windows.Forms;
using BHEL.PUMPSDAS.Datatypes;


namespace userscript
{
    namespace FK4E36_DESIGN
    {
        // Note This will be an external script file.

        public struct paramUILocation
        {
            public DataGridViewRow dgvRow;
        }


        public class FK4E36_DESIGN_UIConfig : UIConfigurator
		{
            List<Label> UILabelList;
            List<DataGridView> UIGridList;
            List<AlarmStatus> chAlarmListPrevious;
			List<AlarmStatus> chAlarmListCurrent;

            ResultParamsBase rp, rpAvg;
            MeasurementProfileBase mp, mpAvg;
            SpecifiedParamsBase sp;
            TestSetupParamsBase tsp;
            MachineDetailsBase md;
			MachineDescriptor mcd;

            List<MeasurementProfileDetails> mpDetails;
			List<ResultParamsDetails> rpDetails;
			
            AlarmColors alarmColors;

            Dictionary<string, DataGridViewCell> mp_uiDescMap,mp_uiUnitsMap,rp_uiDescMap,rp_uiUnitsMap;
			Dictionary<string, DataGridViewCell> mp_uiRealMap,mp_uiAvgMap,rp_uiRealMap,rp_uiAvgMap;

            public FK4E36_DESIGN_UIConfig() : base()
            {             
                
            }
			
			public override void InitializeUIConfig(ref List<Label> UILabelList,
                                                            ref List<DataGridView> UIGridList,
                                                            ref ResultParamsBase rp,
                                                            ref ResultParamsBase rpAvg,
                                                            ref MeasurementProfileBase mp,
                                                            ref MeasurementProfileBase mpAvg,
                                                            ref SpecifiedParamsBase sp,
                                                            ref TestSetupParamsBase tsp,
                                                            ref MachineDetailsBase md,
															ref MachineDescriptor mcd,
                                                            List<MeasurementProfileDetails> mpDetails,
															List<ResultParamsDetails> rpDetails,
                                                            AlarmColors alarmColors)
                {
					this.UILabelList = UILabelList;
					this.UIGridList = UIGridList;
					this.rp = rp;
					this.rpAvg = rpAvg;
					this.mp = mp;
					this.mpAvg = mpAvg;
					this.sp = sp;
					this.tsp = tsp;
					this.md = md;
					this.mpDetails = mpDetails;
					this.alarmColors = alarmColors;
					this.mcd = mcd;
					this.rpDetails = rpDetails;
					
					mp_uiDescMap = new Dictionary<string,DataGridViewCell>();
					mp_uiUnitsMap = new Dictionary<string,DataGridViewCell>();
					rp_uiDescMap = new Dictionary<string,DataGridViewCell>();
					rp_uiUnitsMap = new Dictionary<string,DataGridViewCell>();
					
					mp_uiRealMap = new Dictionary<string,DataGridViewCell>();
					mp_uiAvgMap = new Dictionary<string,DataGridViewCell>();
					rp_uiRealMap = new Dictionary<string,DataGridViewCell>();
					rp_uiAvgMap = new Dictionary<string,DataGridViewCell>();
					
					for(int i = 0;i<17;i++)
					{
						UIGridList[0].Rows.Add(new Object[]{"","","",""});
						UIGridList[2].Rows.Add(new Object[]{"","","",""});
					}
					for(int i = 0;i<5;i++)
					{
						UIGridList[1].Rows.Add(new Object[]{"","","",""});
						UIGridList[3].Rows.Add(new Object[]{"","","",""});
					}
					
					chAlarmListCurrent = new List<AlarmStatus>();
					chAlarmListPrevious = new List<AlarmStatus>();
                } 

            public override void UpdateStaticUIControls()
            {
			
				// Add all the mapping code here....
				rp_uiDescMap.Add("percentageflow",UIGridList[0].Rows[0].Cells[0]);
				mp_uiDescMap.Add("pumpspeed",UIGridList[0].Rows[1].Cells[0]);
				mp_uiDescMap.Add("suctiontemp",UIGridList[0].Rows[2].Cells[0]);
				mp_uiDescMap.Add("suctionflow",UIGridList[0].Rows[3].Cells[0]);
				mp_uiDescMap.Add("balancingleakflow",UIGridList[0].Rows[4].Cells[0]);
				mp_uiDescMap.Add("suctionpressure",UIGridList[0].Rows[5].Cells[0]);
				mp_uiDescMap.Add("dischargepressure",UIGridList[0].Rows[6].Cells[0]);

				mp_uiDescMap.Add("motorspeed",UIGridList[1].Rows[0].Cells[0]);
				mp_uiDescMap.Add("motorvoltage",UIGridList[1].Rows[1].Cells[0]);
				mp_uiDescMap.Add("motorcurrent",UIGridList[1].Rows[2].Cells[0]);
				mp_uiDescMap.Add("motorfieldcurrent",UIGridList[1].Rows[3].Cells[0]);
				mp_uiDescMap.Add("motorinputpower",UIGridList[1].Rows[4].Cells[0]);

				mp_uiDescMap.Add("inletoiltemp",UIGridList[2].Rows[0].Cells[0]);
				mp_uiDescMap.Add("journal_de",UIGridList[2].Rows[1].Cells[0]);
				mp_uiDescMap.Add("journal_nde",UIGridList[2].Rows[2].Cells[0]);
				mp_uiDescMap.Add("thrust_active1",UIGridList[2].Rows[3].Cells[0]);
				mp_uiDescMap.Add("thrust_active2",UIGridList[2].Rows[4].Cells[0]);
				mp_uiDescMap.Add("thrust_nonactive1",UIGridList[2].Rows[5].Cells[0]);
				mp_uiDescMap.Add("thrust_nonactive2",UIGridList[2].Rows[6].Cells[0]);
				mp_uiDescMap.Add("sealoutlettemp_de",UIGridList[2].Rows[7].Cells[0]);
				mp_uiDescMap.Add("sealoutlettemp_nde",UIGridList[2].Rows[8].Cells[0]);

				mp_uiDescMap.Add("vib_de_h",UIGridList[3].Rows[0].Cells[0]);
				mp_uiDescMap.Add("vib_de_v",UIGridList[3].Rows[1].Cells[0]);
				mp_uiDescMap.Add("vib_nde_h",UIGridList[3].Rows[2].Cells[0]);
				mp_uiDescMap.Add("vib_nde_v",UIGridList[3].Rows[3].Cells[0]);
				mp_uiDescMap.Add("vib_nde_a",UIGridList[3].Rows[4].Cells[0]);


				rp_uiUnitsMap.Add("percentageflow",UIGridList[0].Rows[0].Cells[1]);
				mp_uiUnitsMap.Add("pumpspeed",UIGridList[0].Rows[1].Cells[1]);
				mp_uiUnitsMap.Add("suctiontemp",UIGridList[0].Rows[2].Cells[1]);
				mp_uiUnitsMap.Add("suctionflow",UIGridList[0].Rows[3].Cells[1]);
				mp_uiUnitsMap.Add("balancingleakflow",UIGridList[0].Rows[4].Cells[1]);
				mp_uiUnitsMap.Add("suctionpressure",UIGridList[0].Rows[5].Cells[1]);
				mp_uiUnitsMap.Add("dischargepressure",UIGridList[0].Rows[6].Cells[1]);

				mp_uiUnitsMap.Add("motorspeed",UIGridList[1].Rows[0].Cells[1]);
				mp_uiUnitsMap.Add("motorvoltage",UIGridList[1].Rows[1].Cells[1]);
				mp_uiUnitsMap.Add("motorcurrent",UIGridList[1].Rows[2].Cells[1]);
				mp_uiUnitsMap.Add("motorfieldcurrent",UIGridList[1].Rows[3].Cells[1]);
				mp_uiUnitsMap.Add("motorinputpower",UIGridList[1].Rows[4].Cells[1]);

				mp_uiUnitsMap.Add("inletoiltemp",UIGridList[2].Rows[0].Cells[1]);
				mp_uiUnitsMap.Add("journal_de",UIGridList[2].Rows[1].Cells[1]);
				mp_uiUnitsMap.Add("journal_nde",UIGridList[2].Rows[2].Cells[1]);
				mp_uiUnitsMap.Add("thrust_active1",UIGridList[2].Rows[3].Cells[1]);
				mp_uiUnitsMap.Add("thrust_active2",UIGridList[2].Rows[4].Cells[1]);
				mp_uiUnitsMap.Add("thrust_nonactive1",UIGridList[2].Rows[5].Cells[1]);
				mp_uiUnitsMap.Add("thrust_nonactive2",UIGridList[2].Rows[6].Cells[1]);
				mp_uiUnitsMap.Add("sealoutlettemp_de",UIGridList[2].Rows[7].Cells[1]);
				mp_uiUnitsMap.Add("sealoutlettemp_nde",UIGridList[2].Rows[8].Cells[1]);

				mp_uiUnitsMap.Add("vib_de_h",UIGridList[3].Rows[0].Cells[1]);
				mp_uiUnitsMap.Add("vib_de_v",UIGridList[3].Rows[1].Cells[1]);
				mp_uiUnitsMap.Add("vib_nde_h",UIGridList[3].Rows[2].Cells[1]);
				mp_uiUnitsMap.Add("vib_nde_v",UIGridList[3].Rows[3].Cells[1]);
				mp_uiUnitsMap.Add("vib_nde_a",UIGridList[3].Rows[4].Cells[1]);

				rp_uiRealMap.Add("percentageflow",UIGridList[0].Rows[0].Cells[3]);
				mp_uiRealMap.Add("pumpspeed",UIGridList[0].Rows[1].Cells[3]);
				mp_uiRealMap.Add("suctiontemp",UIGridList[0].Rows[2].Cells[3]);
				mp_uiRealMap.Add("suctionflow",UIGridList[0].Rows[3].Cells[3]);
				mp_uiRealMap.Add("balancingleakflow",UIGridList[0].Rows[4].Cells[3]);
				mp_uiRealMap.Add("suctionpressure",UIGridList[0].Rows[5].Cells[3]);
				mp_uiRealMap.Add("dischargepressure",UIGridList[0].Rows[6].Cells[3]);

				mp_uiRealMap.Add("motorspeed",UIGridList[1].Rows[0].Cells[3]);
				mp_uiRealMap.Add("motorvoltage",UIGridList[1].Rows[1].Cells[3]);
				mp_uiRealMap.Add("motorcurrent",UIGridList[1].Rows[2].Cells[3]);
				mp_uiRealMap.Add("motorfieldcurrent",UIGridList[1].Rows[3].Cells[3]);
				mp_uiRealMap.Add("motorinputpower",UIGridList[1].Rows[4].Cells[3]);

				mp_uiRealMap.Add("inletoiltemp",UIGridList[2].Rows[0].Cells[3]);
				mp_uiRealMap.Add("journal_de",UIGridList[2].Rows[1].Cells[3]);
				mp_uiRealMap.Add("journal_nde",UIGridList[2].Rows[2].Cells[3]);
				mp_uiRealMap.Add("thrust_active1",UIGridList[2].Rows[3].Cells[3]);
				mp_uiRealMap.Add("thrust_active2",UIGridList[2].Rows[4].Cells[3]);
				mp_uiRealMap.Add("thrust_nonactive1",UIGridList[2].Rows[5].Cells[3]);
				mp_uiRealMap.Add("thrust_nonactive2",UIGridList[2].Rows[6].Cells[3]);
				mp_uiRealMap.Add("sealoutlettemp_de",UIGridList[2].Rows[7].Cells[3]);
				mp_uiRealMap.Add("sealoutlettemp_nde",UIGridList[2].Rows[8].Cells[3]);

				mp_uiRealMap.Add("vib_de_h",UIGridList[3].Rows[0].Cells[3]);
				mp_uiRealMap.Add("vib_de_v",UIGridList[3].Rows[1].Cells[3]);
				mp_uiRealMap.Add("vib_nde_h",UIGridList[3].Rows[2].Cells[3]);
				mp_uiRealMap.Add("vib_nde_v",UIGridList[3].Rows[3].Cells[3]);
				mp_uiRealMap.Add("vib_nde_a",UIGridList[3].Rows[4].Cells[3]);

				rp_uiAvgMap.Add("percentageflow",UIGridList[0].Rows[0].Cells[2]);
				mp_uiAvgMap.Add("pumpspeed",UIGridList[0].Rows[1].Cells[2]);
				mp_uiAvgMap.Add("suctiontemp",UIGridList[0].Rows[2].Cells[2]);
				mp_uiAvgMap.Add("suctionflow",UIGridList[0].Rows[3].Cells[2]);
				mp_uiAvgMap.Add("balancingleakflow",UIGridList[0].Rows[4].Cells[2]);
				mp_uiAvgMap.Add("suctionpressure",UIGridList[0].Rows[5].Cells[2]);
				mp_uiAvgMap.Add("dischargepressure",UIGridList[0].Rows[6].Cells[2]);

				mp_uiAvgMap.Add("motorspeed",UIGridList[1].Rows[0].Cells[2]);
				mp_uiAvgMap.Add("motorvoltage",UIGridList[1].Rows[1].Cells[2]);
				mp_uiAvgMap.Add("motorcurrent",UIGridList[1].Rows[2].Cells[2]);
				mp_uiAvgMap.Add("motorfieldcurrent",UIGridList[1].Rows[3].Cells[2]);
				mp_uiAvgMap.Add("motorinputpower",UIGridList[1].Rows[4].Cells[2]);

				mp_uiAvgMap.Add("inletoiltemp",UIGridList[2].Rows[0].Cells[2]);
				mp_uiAvgMap.Add("journal_de",UIGridList[2].Rows[1].Cells[2]);
				mp_uiAvgMap.Add("journal_nde",UIGridList[2].Rows[2].Cells[2]);
				mp_uiAvgMap.Add("thrust_active1",UIGridList[2].Rows[3].Cells[2]);
				mp_uiAvgMap.Add("thrust_active2",UIGridList[2].Rows[4].Cells[2]);
				mp_uiAvgMap.Add("thrust_nonactive1",UIGridList[2].Rows[5].Cells[2]);
				mp_uiAvgMap.Add("thrust_nonactive2",UIGridList[2].Rows[6].Cells[2]);
				mp_uiAvgMap.Add("sealoutlettemp_de",UIGridList[2].Rows[7].Cells[2]);
				mp_uiAvgMap.Add("sealoutlettemp_nde",UIGridList[2].Rows[8].Cells[2]);

				mp_uiAvgMap.Add("vib_de_h",UIGridList[3].Rows[0].Cells[2]);
				mp_uiAvgMap.Add("vib_de_v",UIGridList[3].Rows[1].Cells[2]);
				mp_uiAvgMap.Add("vib_nde_h",UIGridList[3].Rows[2].Cells[2]);
				mp_uiAvgMap.Add("vib_nde_v",UIGridList[3].Rows[3].Cells[2]);
				mp_uiAvgMap.Add("vib_nde_a",UIGridList[3].Rows[4].Cells[2]);
			
			
			for (int i = 0; i < mpDetails.Count; i++)
            {
                MeasurementProfileDetails mpd = mpDetails[i];
				string pname = mpd.paramname;
				try
				{
					mp_uiDescMap[pname].Value = mpd.paramdescription;
					mp_uiUnitsMap[pname].Value = mpd.paramunits;
				}
				catch(Exception ex)
				{
					// Do Nothing...
				}

            }
			for (int i = 0; i < rpDetails.Count; i++)
            {
                ResultParamsDetails rpd = rpDetails[i];
				string pname = rpd.paramname;
				try
				{
					rp_uiDescMap[pname].Value = rpd.paramdescription;
					rp_uiUnitsMap[pname].Value = rpd.paramunits;
				}
				catch(Exception ex)
				{
					// Ignore the error...
				}

            }

                ResetAlarmColors(true);
				
				UILabelList[0].Text += sp.GetProjectName();
                UILabelList[1].Text += mcd.MachineUniqueCode;
				ToolTip tooltipmucode = new ToolTip();
				tooltipmucode.SetToolTip(UILabelList[1], mcd.MachineDesc);
                UILabelList[2].Text += md.GetTestReference();
                UILabelList[3].Text = "";
                UILabelList[4].Text = "";
                UILabelList[5].Text = "";
            }

            private void ResetAlarmColors(bool isResetAll)
            {
				string pname;
				if(isResetAll)
				{
					for (int i = 0; i < UIGridList.Count; i++)
					{
						for(int j=0;j<UIGridList[i].Rows.Count;j++)
						{
							UIGridList[i].Rows[j].Cells[3].Style.ForeColor = alarmColors.defaultColor;
							UIGridList[i].Rows[j].Cells[3].Style.BackColor = alarmColors.defaultBackColor;
						}
					} 
					return;
				}
				else
				{
					if(chAlarmListPrevious != null)
					{
					for(int i = 0;i<chAlarmListPrevious.Count;i++)
					{
						pname = chAlarmListPrevious[i].pname;
						mp_uiRealMap[pname].Style.ForeColor = alarmColors.defaultColor;
					}
					}
				}
				return;
            }

            public override void UpdateDynamicUIControls(List<AlarmStatus> chAlarmListCurrent, bool isAverageUpdateRequired)
            {
				string pname;
				
				// Add the value update codes here...
				// Real Values
				UIGridList[0].Rows[0].Cells[3].Value=rp.GetValue("percentageflow");
				UIGridList[0].Rows[1].Cells[3].Value=mp.GetValue("pumpspeed");
				UIGridList[0].Rows[2].Cells[3].Value=mp.GetValue("suctiontemp");
				UIGridList[0].Rows[3].Cells[3].Value=mp.GetValue("suctionflow");
				UIGridList[0].Rows[4].Cells[3].Value=mp.GetValue("balancingleakflow");
				UIGridList[0].Rows[5].Cells[3].Value=mp.GetValue("suctionpressure");
				UIGridList[0].Rows[6].Cells[3].Value=mp.GetValue("dischargepressure");

				UIGridList[1].Rows[0].Cells[3].Value=mp.GetValue("motorspeed");
				UIGridList[1].Rows[1].Cells[3].Value=mp.GetValue("motorvoltage");
				UIGridList[1].Rows[2].Cells[3].Value=mp.GetValue("motorcurrent");
				UIGridList[1].Rows[3].Cells[3].Value=mp.GetValue("motorfieldcurrent");
				UIGridList[1].Rows[4].Cells[3].Value=mp.GetValue("motorinputpower");

				UIGridList[2].Rows[0].Cells[3].Value=mp.GetValue("inletoiltemp");
				UIGridList[2].Rows[1].Cells[3].Value=mp.GetValue("journal_de");
				UIGridList[2].Rows[2].Cells[3].Value=mp.GetValue("journal_nde");
				UIGridList[2].Rows[3].Cells[3].Value=mp.GetValue("thrust_active1");
				UIGridList[2].Rows[4].Cells[3].Value=mp.GetValue("thrust_active2");
				UIGridList[2].Rows[5].Cells[3].Value=mp.GetValue("thrust_nonactive1");
				UIGridList[2].Rows[6].Cells[3].Value=mp.GetValue("thrust_nonactive2");
				UIGridList[2].Rows[7].Cells[3].Value=mp.GetValue("sealoutlettemp_de");
				UIGridList[2].Rows[8].Cells[3].Value=mp.GetValue("sealoutlettemp_nde");

				UIGridList[3].Rows[0].Cells[3].Value=mp.GetValue("vib_de_h");
				UIGridList[3].Rows[1].Cells[3].Value=mp.GetValue("vib_de_v");
				UIGridList[3].Rows[2].Cells[3].Value=mp.GetValue("vib_nde_h");
				UIGridList[3].Rows[3].Cells[3].Value=mp.GetValue("vib_nde_v");
				UIGridList[3].Rows[4].Cells[3].Value=mp.GetValue("vib_nde_a");
				
				if(isAverageUpdateRequired)
				{

					UIGridList[0].Rows[0].Cells[2].Value=rpAvg.GetValue("percentageflow");
					UIGridList[0].Rows[1].Cells[2].Value=mpAvg.GetValue("pumpspeed");
					UIGridList[0].Rows[2].Cells[2].Value=mpAvg.GetValue("suctiontemp");
					UIGridList[0].Rows[3].Cells[2].Value=mpAvg.GetValue("suctionflow");
					UIGridList[0].Rows[4].Cells[2].Value=mpAvg.GetValue("balancingleakflow");
					UIGridList[0].Rows[5].Cells[2].Value=mpAvg.GetValue("suctionpressure");
					UIGridList[0].Rows[6].Cells[2].Value=mpAvg.GetValue("dischargepressure");

					UIGridList[1].Rows[0].Cells[2].Value=mpAvg.GetValue("motorspeed");
					UIGridList[1].Rows[1].Cells[2].Value=mpAvg.GetValue("motorvoltage");
					UIGridList[1].Rows[2].Cells[2].Value=mpAvg.GetValue("motorcurrent");
					UIGridList[1].Rows[3].Cells[2].Value=mpAvg.GetValue("motorfieldcurrent");
					UIGridList[1].Rows[4].Cells[2].Value=mpAvg.GetValue("motorinputpower");

					UIGridList[2].Rows[0].Cells[2].Value=mpAvg.GetValue("inletoiltemp");
					UIGridList[2].Rows[1].Cells[2].Value=mpAvg.GetValue("journal_de");
					UIGridList[2].Rows[2].Cells[2].Value=mpAvg.GetValue("journal_nde");
					UIGridList[2].Rows[3].Cells[2].Value=mpAvg.GetValue("thrust_active1");
					UIGridList[2].Rows[4].Cells[2].Value=mpAvg.GetValue("thrust_active2");
					UIGridList[2].Rows[5].Cells[2].Value=mpAvg.GetValue("thrust_nonactive1");
					UIGridList[2].Rows[6].Cells[2].Value=mpAvg.GetValue("thrust_nonactive2");
					UIGridList[2].Rows[7].Cells[2].Value=mpAvg.GetValue("sealoutlettemp_de");
					UIGridList[2].Rows[8].Cells[2].Value=mpAvg.GetValue("sealoutlettemp_nde");

					UIGridList[3].Rows[0].Cells[2].Value=mpAvg.GetValue("vib_de_h");
					UIGridList[3].Rows[1].Cells[2].Value=mpAvg.GetValue("vib_de_v");
					UIGridList[3].Rows[2].Cells[2].Value=mpAvg.GetValue("vib_nde_h");
					UIGridList[3].Rows[3].Cells[2].Value=mpAvg.GetValue("vib_nde_v");
					UIGridList[3].Rows[4].Cells[2].Value=mpAvg.GetValue("vib_nde_a");

				}
				this.chAlarmListCurrent = chAlarmListCurrent;
				ResetAlarmColors(false);
				//this.chAlarmListPrevious = null;
				this.chAlarmListPrevious = this.chAlarmListCurrent;
				
				for(int i = 0;i<this.chAlarmListCurrent.Count;i++)
				{
					pname = this.chAlarmListCurrent[i].pname;
					if(this.chAlarmListCurrent[i].alarmType == AlarmType.HIGH_ALARM)
					mp_uiRealMap[pname].Style.ForeColor = alarmColors.highAlarmColor;
					else
					mp_uiRealMap[pname].Style.ForeColor = alarmColors.lowAlarmColor;
				}

            }

		}

    }
}
