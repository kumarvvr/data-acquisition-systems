﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;
using BHEL.PUMPSDAS.Datatypes;

namespace userscript
{
    namespace <##MUCODE##>
    {
       public class <##MUCODE##>_ErrorCorrector : ErrorCorrector
	   {
			ResultParamsBase rp;
			MeasurementProfileBase mp;
			SpecifiedParamsBase sp;
			TestSetupParamsBase tsp;
			MachineDetailsBase md;
			MachineDescriptor mcd;
			
			double dischargepressure,flow,mf,pc,flowStretch,motorcurrent,motorpower,de_journal,nde_journal,inletoiltemp;
			
			public <##MUCODE##>_ErrorCorrector(){}
			
			public override void InitializeErrorCorrector(
                                    ref ResultParamsBase rp,
                                    ref MeasurementProfileBase mp,
                                    ref SpecifiedParamsBase sp,
                                    ref TestSetupParamsBase tsp,
                                    ref MachineDetailsBase md,
                                    ref MachineDescriptor mcd)
			{
				this.rp = rp;
				this.mp = mp;
				this.sp = sp;
				this.tsp = tsp;
				this.md = md;
				this.mcd = mcd;
				
				// Also initialize internal data stores....
				
				mf=1.0;
				pc = 0.0;

			}
			
			private void ExtractData()
			{				
				
			}
						
			private void UpdateData()
			{

			}
			public override void ErrorCorrect(int PointNumber, bool isAuto)
			{
				ExtractData();

				mf = 1;
				pc = 1;
				flowStretch = 1.0;
				
				
				
				if(PointNumber == 1)
				{
					// Modify Mf and Pc for various points here...
				}
				
				if(PointNumber == 1)
				{
					// Modify Mf and Pc for various points here...
				}
				
				UpdateData();
			}
				
	   }

        
    }
}
