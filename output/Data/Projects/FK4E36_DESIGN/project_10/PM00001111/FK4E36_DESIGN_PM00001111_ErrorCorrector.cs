using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;
using BHEL.PUMPSDAS.Datatypes;



namespace userscript
{
    namespace FK4E36_DESIGN
    {
       public class FK4E36_DESIGN_ErrorCorrector : ErrorCorrector
	   {
			ResultParamsBase rp;
			MeasurementProfileBase mp;
			SpecifiedParamsBase sp;
			TestSetupParamsBase tsp;
			MachineDetailsBase md;
			MachineDescriptor mcd;
			
			public FK4E36_DESIGN_ErrorCorrector(){}
			
			public override void InitializeErrorCorrector(
                                    ref ResultParamsBase rp,
                                    ref MeasurementProfileBase mp,
                                    ref SpecifiedParamsBase sp,
                                    ref TestSetupParamsBase tsp,
                                    ref MachineDetailsBase md,
                                    ref MachineDescriptor mcd)
			{
				this.rp = rp;
				this.mp = mp;
				this.sp = sp;
				this.tsp = tsp;
				this.md = md;
				this.mcd = mcd;

			}
			
			public override void ErrorCorrect(int PointNumber)
			{
				// Do everything in the derived class..
				if(PointNumber == 1)
				{
				this.mp.SetValue("pumpspeed",10.5);
				this.mp.SetValue("motorfieldcurrent",mp.GetValue("pumpspeed")+2.5);
				}
			}
				
	   }

        
    }
}
