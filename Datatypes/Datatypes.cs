﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;

namespace BHEL
{
    namespace PUMPSDAS
    {
        namespace Datatypes
        {
            // Currently, application only supports double, string and Lookup datatypes.
            // The following delegates are for accessing public properties of objects
            public delegate string GetStringDelegate();
            public delegate void SetStringDelegate(string str);
            public delegate double GetDoubleDelegate();
            public delegate void SetDoubleDelegate(double val);
            public delegate LookupData GetLookupDelegate();
            public delegate void SetLookupDelegate(LookupData data);

            public class DataContainer : MarshalByRefObject
            {
                /* README *
                                 * 
                                 * Programmed by Raghavendra Kumar V V. Date : 08.03.2013
                                 * Pumps Data Acquisition System prototype
                                 * 
                                 * This class is the base class for all dynamic data container
                                 * objects that are scripted by the user.
                                 * 
                                 * Data is loaded into these by using inputs from various xml files.
                                 * 
                                 * Once data is loaded, these objects can be used for computation
                                 * and user interaction.
                                 * 
                                 * More documentation will be coming through
                                 * 
                                 * TODO : Comprehensive documentation.
                                 * 
                                 */


                #region Members

                public List<MethodInfo> allMethods;
                public List<PropertyInfo> allProperties;
                public Dictionary<string, Type> propertyTypes;
                public List<DynamicDataProperty> dynProperties;

                public Dictionary<string, GetStringDelegate> GetStringDelegateDict;
                public Dictionary<string, SetStringDelegate> SetStringDelegateDict;

                public Dictionary<string, GetDoubleDelegate> GetDoubleDelegateDict;
                public Dictionary<string, SetDoubleDelegate> SetDoubleDelegateDict;

                public Dictionary<string, GetLookupDelegate> GetLookupDelegateDict;
                public Dictionary<string, SetLookupDelegate> SetLookupDelegateDict;

                #endregion

                // Initialize all internal member lists using reflection
                // Initialize delegate lists for fast access to internal unknown properties.
                #region Constructor

                public DataContainer()
                {
                    // Use Reflection to get all the property and other info.
                    PropertyInfo[] allprops;
                    MethodInfo[] allmeths;

                    System.Type thisType = this.GetType();
                    Type strType;
                    Type dblType;
                    Type ldType;

                    strType = (Type)typeof(System.String);
                    dblType = (Type)typeof(System.Double);
                    ldType = (Type)typeof(LookupData);

                    propertyTypes = new Dictionary<string, Type>();

                    GetStringDelegateDict = new Dictionary<string, GetStringDelegate>();
                    SetStringDelegateDict = new Dictionary<string, SetStringDelegate>();
                    GetDoubleDelegateDict = new Dictionary<string, GetDoubleDelegate>();
                    SetDoubleDelegateDict = new Dictionary<string, SetDoubleDelegate>();
                    GetLookupDelegateDict = new Dictionary<string, GetLookupDelegate>();
                    SetLookupDelegateDict = new Dictionary<string, SetLookupDelegate>();

                    allprops = thisType.GetProperties();
                    allmeths = thisType.GetMethods();
                    allProperties = new List<PropertyInfo>(0);
                    allMethods = new List<MethodInfo>(0);

                    this.dynProperties = new List<DynamicDataProperty>(0);

                    DynamicDataProperty tempProp;

                    for (int i = 0; i < allprops.Length; i++)
                    {
                        allProperties.Add(allprops[i]);
                        Type tpp = allprops[i].GetType();
                        if (allprops[i].PropertyType == strType)
                        {
                            propertyTypes.Add(allprops[i].Name, typeof(System.String));

                            GetStringDelegate gsd = (GetStringDelegate)Delegate.CreateDelegate(typeof(GetStringDelegate), this, allprops[i].GetGetMethod().Name);
                            GetStringDelegateDict.Add(allprops[i].Name, gsd);

                            SetStringDelegate ssd = (SetStringDelegate)Delegate.CreateDelegate(typeof(SetStringDelegate), this, allprops[i].GetSetMethod().Name);
                            SetStringDelegateDict.Add(allprops[i].Name, ssd);

                            tempProp = new DynamicDataProperty();

                            tempProp.pName = allprops[i].Name;
                            tempProp.type = "string";
                            tempProp.data = "";

                            this.dynProperties.Add(tempProp);
                        }
                        if (allprops[i].PropertyType == dblType)
                        {
                            propertyTypes.Add(allprops[i].Name, typeof(System.Double));

                            GetDoubleDelegate gsd = (GetDoubleDelegate)Delegate.CreateDelegate(typeof(GetDoubleDelegate), this, allprops[i].GetGetMethod().Name);
                            GetDoubleDelegateDict.Add(allprops[i].Name, gsd);

                            SetDoubleDelegate ssd = (SetDoubleDelegate)Delegate.CreateDelegate(typeof(SetDoubleDelegate), this, allprops[i].GetSetMethod().Name);
                            SetDoubleDelegateDict.Add(allprops[i].Name, ssd);

                            tempProp = new DynamicDataProperty();

                            tempProp.pName = allprops[i].Name;
                            tempProp.type = "double";
                            tempProp.data = "";

                            this.dynProperties.Add(tempProp);
                        }
                        if (allprops[i].PropertyType == ldType)
                        {
                            propertyTypes.Add(allprops[i].Name, typeof(LookupData));

                            GetLookupDelegate gsd = (GetLookupDelegate)Delegate.CreateDelegate(typeof(GetLookupDelegate), this, allprops[i].GetGetMethod().Name);
                            GetLookupDelegateDict.Add(allprops[i].Name, gsd);

                            SetLookupDelegate ssd = (SetLookupDelegate)Delegate.CreateDelegate(typeof(SetLookupDelegate), this, allprops[i].GetSetMethod().Name);
                            SetLookupDelegateDict.Add(allprops[i].Name, ssd);

                            tempProp = new DynamicDataProperty();

                            tempProp.pName = allprops[i].Name;
                            tempProp.type = "lookup";
                            tempProp.data = "";

                            this.dynProperties.Add(tempProp);
                        }

                    }



                    for (int i = 0; i < allmeths.Length; i++)
                        allMethods.Add(allmeths[i]);

                }

                #endregion

                // Provide a set of functions for getting and setting various properties.
                #region Property accessor methods.

                public void SetDoubleValue(string propertyName, double val)
                {
                    SetDoubleDelegate deg;
                    this.SetDoubleDelegateDict.TryGetValue(propertyName, out deg);
                    deg(val);
                }
                public void SetValue(string propertyName, double val) // Only sets a double value...
                {
                    SetDoubleValue(propertyName, val);
                }
                public void SetStringValue(string propertyName, string val)
                {
                    SetStringDelegate deg;
                    this.SetStringDelegateDict.TryGetValue(propertyName, out deg);
                    deg(val);
                }
                public void SetLookupValue(string propertyName, LookupData val)
                {
                    SetLookupDelegate deg;
                    this.SetLookupDelegateDict.TryGetValue(propertyName, out deg);
                    deg(val);
                }
                public double GetDoubleValue(string propertyName)
                {
                    GetDoubleDelegate deg;
                    this.GetDoubleDelegateDict.TryGetValue(propertyName, out deg);
                    return deg();
                }
                public double GetValue(string propertyName) // Also returns a default double value.
                {
                    return GetDoubleValue(propertyName);
                }
                public string GetStringValue(string propertyName)
                {
                    GetStringDelegate deg;
                    this.GetStringDelegateDict.TryGetValue(propertyName, out deg);
                    return deg();
                }
                public LookupData GetLookupValue(string propertyName)
                {
                    GetLookupDelegate deg;
                    this.GetLookupDelegateDict.TryGetValue(propertyName, out deg);
                    return deg();
                }
                public List<DynamicDataProperty> GetDynamicPropertyList()
                {
                    return this.dynProperties;
                }

                #endregion
            }

            public class MachineDescriptor : DataContainer
            {
                /* README *
                                 * 
                                 * Programmed By Raghavendra Kumar V V on 08.03.2013
                                 * 
                                 * Purpose of this class is to provide a framework for a 
                                 * Machine object that encapsulates all the requirements
                                 * of a machine.
                                 * 
                                 * TODO : Comprehensive Documentation.
                                 */

                // We assume certain data containers for doing out job.

                public MachineDetailsBase md { get; set; }
                public MeasurementProfileBase mp { get; set; }
                public ResultParamsBase rp { get; set; }
                public SpecifiedParamsBase sp { get; set; }
                public TestSetupParamsBase tsp { get; set; }
                public string MachineUniqueCode { get; set; }
                public string MachineDesc { get; set; }
                public MachineDescriptor(MachineDetailsBase md_base,
                MeasurementProfileBase mp_base,
                ResultParamsBase rp_base,
                SpecifiedParamsBase sp_base,
                TestSetupParamsBase tsp_base)
                {
                    this.md = md_base;
                    this.mp = mp_base;
                    this.rp = rp_base;
                    this.sp = sp_base;
                    this.tsp = tsp_base;
                }

                public virtual void InitializeInternalData()
                {
                }

                public virtual bool Compute()
                {
                    return true;
                }
            }

            public class MachineDetailsBase : DataContainer
            {

                protected string machinenumber;
                protected string testreference;
                protected string date;

                public MachineDetailsBase()
                    : base()
                {
                }

                public string GetMachineNumber()
                {
                    return this.machinenumber;
                }
                public void SetMachineNumber(string num)
                {
                    this.machinenumber = num;
                }
                public string GetTestReference()
                {
                    return this.testreference;
                }
                public void SetTestReference(string num)
                {
                    this.testreference = num;
                }

                public void SetDateOfTest(string date)
                {
                    this.date = date;
                }
                public string GetDateOfTest()
                {
                    return this.date;
                }
            }

            public class SpecifiedParamsBase : DataContainer
            {
                protected string ProjectName;

                public string GetProjectName()
                {
                    return this.ProjectName;
                }
                public void SetProjectName(string name)
                {
                    this.ProjectName = name;
                }
                public SpecifiedParamsBase()
                    : base()
                {

                }
            }

            public class TestSetupParamsBase : DataContainer
            {
                public TestSetupParamsBase()
                    : base()
                {

                }
            }

            public class MeasurementProfileBase : DataContainer
            {
                protected DateTime measurementDT;
                protected int pointNum;

                public void SetMeasurementDateTime(DateTime DT)
                {
                    this.measurementDT = DT;
                }
                public DateTime GetMeasurementDateTime()
                {
                    return this.measurementDT;
                }

                public void SetPointNum(int pt)
                {
                    this.pointNum = pt;
                }
                public int GetPointNum()
                {
                    return this.pointNum;
                }

                public MeasurementProfileBase()
                    : base()
                {

                }
            }

            public class ResultParamsBase : DataContainer
            {
                protected DateTime measurementDT;
                protected int pointNum;

                public void SetMeasurementDateTime(DateTime DT)
                {
                    this.measurementDT = DT;
                }
                public DateTime GetMeasurementDateTime()
                {
                    return this.measurementDT;
                }

                public void SetPointNum(int pt)
                {
                    this.pointNum = pt;
                }
                public int GetPointNum()
                {
                    return this.pointNum;
                }
                public ResultParamsBase()
                    : base()
                {

                }
            }

            public class LookupData
            {

                // TODO : Logic to be completely re-written

                // Maintains a lookup data list.
                // sorts it and searches it.

                SortedDictionary<double, double> _sortedData;

                List<double> _sortedKeys;

                public int Count = 0;

                public LookupData(Dictionary<double, double> val)
                {
                    Dictionary<double, double> _rawData;

                    Dictionary<double, double>.KeyCollection _keys;

                    Dictionary<double, double>.ValueCollection _values;

                    Count = val.Count;

                    _rawData = val;

                    this._sortedData = new SortedDictionary<double, double>();


                    // sort the values in ascending order.
                    int count = _rawData.Count;

                    _keys = _rawData.Keys;

                    _values = _rawData.Values;

                    _sortedKeys = new System.Collections.Generic.List<double>();

                    for (int i = 0; i < count; i++)
                    {
                        _sortedData.Add(_keys.ElementAt(i), _values.ElementAt(i));


                        _sortedKeys.Add(_keys.ElementAt(i));

                    }

                    // SortedKeys has to be manually sorted.

                    _sortedKeys.Sort(new LookupEntryComparer());

                }

                public double getValueAt(double key)
                {
                    // Searches and gets the value for the specified key.
                    double val = -1;

                    // Check whether the key given is within the range of the 
                    // the keys available in the data.

                    if (key <= _sortedData.Keys.Max() && key >= _sortedData.Keys.Min())
                    {

                        if (_sortedData.ContainsKey(key))
                        {
                            // Get the index of the key.
                            _sortedData.TryGetValue(key, out val);

                            return val;
                        }

                        // Search for keys that are above and below the specified
                        // key and get the averaged value

                        double minkey;
                        double maxkey;
                        double minval;
                        double maxval;

                        // Search the the keys that bound the input key
                        // in the sortedkeys.
                        int maxindex = 0;

                        for (int i = 0; i < _sortedKeys.Count; i++)
                        {
                            if (_sortedKeys.ElementAt(i) > key)
                            {
                                maxindex = i;
                                break;
                            }
                        }

                        maxkey = _sortedKeys.ElementAt(maxindex);

                        minkey = _sortedKeys.ElementAt(maxindex - 1);

                        maxval = _sortedData[maxkey];

                        minval = _sortedData[minkey];

                        val = maxval - ((maxval - minval) / (maxkey - minkey) * (maxkey - key));

                        return val;

                    }
                    return -1;
                }

                class LookupEntryComparer : IComparer<double>
                {
                    public int Compare(double x, double y)
                    {

                        if (x > y)
                            return 1;

                        if (x < y)
                            return -1;

                        return 0;

                    }
                }
            }

            public class DynamicDataProperty
            {
                public string type;
                public string pName;
                public object data;
            }

            public class UIConfigurator : DataContainer
            {
                public UIConfigurator()
                {
                    // Do Nothing here...                    
                }

                public virtual void InitializeUIConfig(
                    ref List<Label> UILabelList,
                    ref List<DataGridView> UIGridList,
                    ref ResultParamsBase rp,
                    ref ResultParamsBase rpAvg,
                    ref MeasurementProfileBase mp,
                    ref MeasurementProfileBase mpAvg,
                    ref SpecifiedParamsBase sp,
                    ref TestSetupParamsBase tsp,
                    ref MachineDetailsBase md,
                    ref MachineDescriptor mcd,
                    List<MeasurementProfileDetails> mpDetails,
                    List<ResultParamsDetails> rpDetails,
                    AlarmColors alColors)
                {

                }

                public virtual void UpdateStaticUIControls()
                {
                    return;
                }

                public virtual void UpdateDynamicUIControls(List<AlarmStatus> chAlarmListCurrent,bool isAverageUpdateRequired)
                {
                    return;
                }
            }

            public class ErrorCorrector : DataContainer
            {
                public ErrorCorrector()
                {
                    // Do nothing here...
                }

                public virtual void InitializeErrorCorrector(
                                    ref ResultParamsBase rp,
                                    ref MeasurementProfileBase mp,
                                    ref SpecifiedParamsBase sp,
                                    ref TestSetupParamsBase tsp,
                                    ref MachineDetailsBase md,
                                    ref MachineDescriptor mcd)
                {

                }

                public virtual void ErrorCorrect(int PointNumber, bool isAuto)
                {
                    // Do everything in the derived class..
                }
            }

            public class LinePrinterBase : DataContainer
            {
                public LinePrinterBase()
                {
                    // Do nothing for now.
                }

                public virtual List<LinePrintData> GenerateLinePrintData(
                    List<MeasurementProfileDetails> mpDetails,
                    List<ResultParamsDetails> rpDetails,
                    MeasurementProfileBase mpAvg,
                    ResultParamsBase rpAvg)
                {
                    return new List<LinePrintData>();
                }
            }

            public class HardwareBase : DataContainer
            {
            }
        }
    }
}
