﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
// YAHOO
namespace BHEL
{
    namespace PUMPSDAS
    {
        namespace Datatypes
        {
            public struct AppSettings
            {
                public AppPaths appPaths;
                public DatabaseDetails dbaseSettings;
            }

            public struct AppPaths
            {
                public string appPath { get; set; }
                public string dataPath { get; set; }
                public string scriptPath { get; set; }
                public string configPath { get; set; }
            }

            public struct DatabaseDetails
            {
                public string driver;
                public string server;
                public string port;
                public string databaseName;
                public string userName;
                public string password;
            }

            public struct LookupTableDetails
            {
                public int lookupid { get; set; }
                public string lookupdesc { get; set; }
                public string keytext { get; set; }
                public string valuetext { get; set; }
                public string tablename { get; set; }
                public bool iscommon { get; set; }
            }

            public struct LookupDataRow
            {
                public double key { get; set; }
                public double value { get; set; }
            }

            public struct InstalledMachine
            {
                public string machineuniquecode { get; set; }
                public string codefilepath { get; set; }
                public string dllrootpath { get; set; }
                public string dllfilename { get; set; }
                public string dllmajorversion { get; set; }
                public string dllminorversion { get; set; }
                public string dllversiondesc { get; set; }
                public int machinecode { get; set; }
            }

            public struct DefaultConfigurationData
            {
                public string fk_machineCode { get; set; }
                public string paramname { get; set; }
                public string paramtype { get; set; }
                public string paramdescription { get; set; }
                public string paramunits { get; set; }
                public string defaultdata { get; set; }
                public int displayindex { get; set; }

            }

            public struct MachineDetailsEntry
            {
                public int fk_machinecode { get; set; }
                public string machinenumber { get; set; }
                public string machinereference { get; set; }
                public string paramname { get; set; }
                public string paramtype { get; set; }
                public string paramdescription { get; set; }
                public string paramunits { get; set; }
                public string defaultdata { get; set; }
                public int displayindex { get; set; }

            }

            public struct SpecifiedParamsDetails
            {
                public int fk_machinecode { get; set; }
                public string paramname { get; set; }
                public string paramtype { get; set; }
                public string paramdescription { get; set; }
                public string paramunits { get; set; }
                public string defaultdata { get; set; }
                public int displayindex { get; set; }

            }

            public struct TestSetupParamsDetails
            {
                public int fk_machinecode { get; set; }
                public string paramname { get; set; }
                public string paramtype { get; set; }
                public string paramdescription { get; set; }
                public string paramunits { get; set; }
                public string defaultdata { get; set; }
                public int displayindex { get; set; }

            }

            public struct MeasurementProfileDetails
            {
                private string _meastype;
                public int fk_machineCode { get; set; }
                public string paramname { get; set; }
                public string paramtype { get; set; }
                public string paramdescription { get; set; }
                public string paramunits { get; set; }
                public string defaultdata { get; set; }
                public string meastype
                {
                    get
                    {
                        return _meastype;
                    }
                    set
                    {
                        // Linear computation
                        // "DCV", "DCA", "ACV", "ACA", "DCmV", "DCmA", "ACmV", "ACmA", 
                        // "RTD", "PULSE", "MANUAL", "MANUAL RANDOMISED", "TIME", "FREQUENCY", "SPEED_TIME", "SPEED_FREQUENCY"

                        if (value == "DCV" || value == "DCA" || value == "ACV" || value == "ACA")

                            this._MeasCode = 0;

                        else if (value == "DCmV" || value == "DCmA" || value == "ACmV" || value == "ACmA")

                            this._MeasCode = 1;

                        else if (value == "RTD")

                            this._MeasCode = 2;

                        else if (value == "PULSE")

                            this._MeasCode = 3;

                        else if (value == "MANUAL")

                            this._MeasCode = 4;

                        else if (value == "MANUAL RANDOMISED")

                            this._MeasCode = 5;

                        else if (value == "TIME")

                            this._MeasCode = 6;

                        else if (value == "FREQUENCY")

                            this._MeasCode = 7;

                        else if (value == "SPEED_TIME")

                            this._MeasCode = 8;

                        else if (value == "SPEED_FREQUENCY")

                            this._MeasCode = 9;

                        else if (value == "DIRECT")

                            this._MeasCode = 10;
                        else
                            this._MeasCode = -1;

                        this._meastype = value;
                    }
                }
                public int fk_deviceid { get; set; }
                public string channelid { get; set; }
                public double sensormin { get; set; }
                public double sensormax { get; set; }
                public double outputmin { get; set; }
                public double outputmax { get; set; }
                public double scale { get; set; }
                public double offsetvalue { get; set; }
                public double highalarm { get; set; }
                public double lowalarm { get; set; }
                public double slope { get; set; }
                public int displayindex { get; set; }
                public int pointnum { get; set; }
                int _MeasCode;
                public double ComputeValue(double rawdata)
                {
                    // Slope is calculated and inserted into this object during initialization
                    // of measurementprofile details in Installed machine manager...
                    /* value == "DCV" || value == "DCA" || value == "ACV" || value == "ACA" */
                    if (_MeasCode == 0) 
                    {
                        return ((outputmin + ((rawdata - sensormin) * (slope))+offsetvalue) * scale);
                    }

                    /* value == "DCmV" || value == "DCmA" || value == "ACmV" || value == "ACmA"  */
                    if (_MeasCode == 1)
                    {
                        return ((outputmin + (((rawdata/1000) - sensormin) * (slope)) + offsetvalue) * scale);
                    }

                    // RTD
                    if (_MeasCode == 2)
                    {
                        return (rawdata + offsetvalue) * scale;
                    }

                    //PULSE
                    if (_MeasCode == 3)
                    {
                        return (rawdata + offsetvalue) * scale;
                    }

                    //MANUAL
                    if (_MeasCode == 4)
                    {
                        return outputmax;
                    }

                    //MANUAL RANDOMIZED
                    if (_MeasCode == 5)
                    {
                        double result;
                        Random rr = new Random();
                        result = outputmin + (rr.NextDouble() * (outputmax - outputmin));
                        rr = null;
                        return result;
                    }

                    // Time
                    if (_MeasCode == 6)
                    {
                        return (rawdata + offsetvalue) * scale;
                    }

                    // Frequency
                    if (_MeasCode == 7)
                    {
                        return (rawdata + offsetvalue) * scale;
                    }

                    // Speed_Time
                    if (_MeasCode == 8)
                    {
                        if (rawdata != 0)
                            return ((1 / rawdata) + offsetvalue) * scale;
                        return 9999;
                    }

                    // Speed_Frequency
                    if (_MeasCode == 9)
                    {
                        return (rawdata + offsetvalue) * scale;
                    }

                    // Direct
                    if (_MeasCode == 10)
                    {
                        return rawdata;
                    }

                    return -1;
                }
                public HardwareBase device { get; set; }
                public string configString;
                public string measString;
                public string resetString;
            }

            public struct ResultParamsDetails
            {
                public string fk_machineCode { get; set; }
                public string paramname { get; set; }
                public string paramtype { get; set; }
                public string paramdescription { get; set; }
                public string paramunits { get; set; }
                public string defaultdata { get; set; }
                public int pointnum { get; set; }
                public int displayindex { get; set; }

            }

            public struct ProjectDetails
            {
                public int fk_machinecode { get; set; }
                public int projectcode { get; set; }
                public string projectname { get; set; }
                public string projectdescription { get; set; }
            }

            public struct DeviceDetails
            {
                public int deviceid { get; set; }
                public string devicename { get; set; }
                public string devicedesc { get; set; }
                public string connstring { get; set; }
            }

            public struct DeviceConfigDetails
            {
                public int fk_deviceid { get; set; }
                public string meastype { get; set; }
                public string resetstring { get; set; }
                public string configstring { get; set; }
                public string measstring { get; set; }
                public string addoncmd { get; set; }
            }

            public struct ChannelData
            {
                public double rawvalue { get; set; }
                public double computedValue { get; set; }
                public string pname { get; set; }
                public int pindex { get; set; }
            }

            public struct ObjectData
            {                
                public string pvalue{ get; set; }
                public string pname { get; set; }
                public int pindex { get; set; }
            }

            public struct LinePrintData
            {
                public string paramdesc { get; set; }
                public string paramunits { get; set; }
                public string value{ get; set; }
            }

            public struct ProjectMachineDetails
            {
                public int fk_machinecode { get; set; }
                public int fk_projectcode { get; set; }
                public string machinenumber { get; set; }
                public string machinedescription { get; set; }
            }

            public struct MachineTestDetails
            {
                public int fk_machinecode { get; set; }
                public int fk_projectcode { get; set; }
                public string fk_machinenumber { get; set; }
                public string testreference { get; set; }
                public string testremarks { get; set; }
                public string testdate { get; set; }
            }

            public struct PropertyUILink
            {
                public int fk_machinecode { get; set; }
                public int placeholder { get; set; }
                public ParamObject objType;
                public string paramname { get; set; }
            }


            // Enumerations

            public enum ParamObject
            {
                MEASUREMENTPROFILE = 0,
                RESULTPARAMETERS
            }

            public enum AlarmType
            {
                HIGH_ALARM,
                CRITICALLY_HIGH_ALARM,
                LOW_ALARM,
                CRITICALLY_LOW_ALARM,
                NORMAL_STATE
            }

            public struct AlarmColors
            {
                public Color highAlarmColor;
                public Color highAlarmBackColor;
                public Color lowAlarmColor;
                public Color lowAlarmBackColor;
                public Color defaultColor;
                public Color defaultBackColor;
            }

            public struct AlarmStatus
            {
                public AlarmType alarmType;
                public string pname;
            }

            public enum Styles
            {
                HIGH_ALARM_CELL_STYLE,
                CRITICALLY_HIGH_ALARM_CELL_STYLE,
                LOW_ALARM_CELL_STYLE,
                CRITICALLY_LOW_ALARM_CELL_STYLE,
                NORMAL_STATE_STYLE
            }

            public enum HardwareScanMode
            {
                BYPASS_TIME_MEASUREMENTS,
                NORMAL
            }

            public enum ScanMode
            {
                SAFE_START_MODE,
                NORMAL_MODE,
                MONITORING_MODE,
                FAST_MONITORING_MODE                
            }
        }
    }
}

