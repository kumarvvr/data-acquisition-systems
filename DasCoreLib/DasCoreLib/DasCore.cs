﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BHEL.PUMPSDAS.Datatypes;
using System.Collections;
using CodeMonkey;
using FileManager;
using System.Reflection;

namespace DasCoreLib 
{
    public class DasCore
    {
        //// List of functions which represent the public interface to this object.

        #region Global objects

        DatabaseManager _database;
        LookupTableManager ltmgr;
        ProjectManager pmgr;
        DeviceManager dmgr;
        HardwareManager hwmgr;
        UIConfigManager uicfgmgr;
        AppSettings settings;
        DatabaseDetails dbasedetails;
        DasCoreSettings dcSettings;
        UIConfigurator uiconfig;
        DataManager dataMgr;

        Compiler scriptCompiler;
        InstalledMachineManager imm;
        MachineFactory factory;

        InstalledMachine currentMachine;
        ProjectDetails currentProject;
        ProjectMachineDetails currentProjectMachine;
        MachineTestDetails currentMachineTest;

        MachineDetailsBase md;
        SpecifiedParamsBase sp;
        TestSetupParamsBase tsp;
        MeasurementProfileBase mp;
        ResultParamsBase rp;
        MachineDescriptor m;

        MachineDetailsBase testing_md;
        SpecifiedParamsBase testing_sp;
        TestSetupParamsBase testing_tsp;
        MeasurementProfileBase testing_mp_manip;
        MeasurementProfileBase testing_mp_manip_avg;
        MeasurementProfileBase testing_mp_actual;
        MeasurementProfileBase testing_mp_actual_avg;
        MeasurementProfileBase testing_mp_metadata;
        ResultParamsBase testing_rp_manip;
        ResultParamsBase testing_rp_manip_avg;
        ResultParamsBase testing_rp_actual;
        ResultParamsBase testing_rp_actual_avg;
        MachineDescriptor testing_mcd_manip;
        MachineDescriptor testing_mcd_manip_avg;
        MachineDescriptor testing_mcd_actual;
        MachineDescriptor testing_mcd_actual_avg;

        ErrorCorrector MachineErrorCorrector;

        List<MeasurementProfileDetails> testing_mpDetailsList;
        List<ResultParamsDetails> testing_rpDetailsList;

        List<ChannelData> chData, chDataAct;
        List<ChannelData> avgChData, avgChDataAct;
        List<ChannelData> sumChData, sumChDataAct;
        List<AlarmStatus> alarms;

        string dbasedriver, dbaseserver, dbaseport, dbaseName, dbaseuserName, dbasepassword;
        string runMode = "DEMO";

        bool IsAdmin = false;
        bool IsSimulation = false;

        int currentScan;
        int currentPoint;
        int maxPoint;
        int maxScan;        
        #endregion
             
        /******************************************************************************************************************************/
        /*****************************************           DASCORE Functions       **************************************************/
        /******************************************************************************************************************************/
        public DasCore()
        {
            // Initialise the database manager object.
            GetAppSettings();
            settings = GetAppSettings();
            dbasedetails = settings.dbaseSettings;

            _database = new DatabaseManager(dbasedetails);

            ltmgr = new LookupTableManager(_database);
            imm = new InstalledMachineManager(_database);
            pmgr = new ProjectManager(_database);
            dmgr = new DeviceManager(_database);
            dcSettings = new DasCoreSettings();
            uicfgmgr = new UIConfigManager(_database);
            uiconfig = new UIConfigurator();
            factory = new MachineFactory(_database, imm, ltmgr, pmgr);
            dataMgr = new DataManager(_database);

            // Upon database updation, objects have to be rebuilt with the new database...
        }
        
        //----------------------------------------------------------------------------------------------------------------------------
        // Functions related to app settings handling.
        //----------------------------------------------------------------------------------------------------------------------------

        #region App Settings Functions

        public AppSettings GetAppSettings()
        {
            //TODO: Read App Data Config File
            
            dbasedetails = GetDatabaseSettings();
            settings.dbaseSettings = dbasedetails;
            return settings;
        }
        public DatabaseDetails GetDatabaseSettings()
        {

            dbasedetails = new DatabaseDetails();

            dbasedetails.driver = "{PostgreSQL ANSI}";
            dbasedetails.server = "localhost";
            dbasedetails.port = "5432";
            dbasedetails.databaseName = "BHEL_PUMPS_DAS_2013";
            dbasedetails.userName = "postgres";
            dbasedetails.password = "bhel";

            return dbasedetails;
        }
        public void UpdateDatabaseSettings(DatabaseDetails dbasedetails)
        {
            this.dbasedetails = dbasedetails;
            this.settings.dbaseSettings = dbasedetails;
            //TODO : Write database info. to some file
        }
        public void UpdateAppSettings(AppSettings settings)
        {
            this.settings = settings;
        }
        public void SetProgramRunMode(string runMode)
        {
            this.runMode = runMode;
        }
        public bool CheckForAdminRights()
        {
            return IsAdmin;
        }
        public void SetAdminRights(bool IsAdmin)
        {
            this.IsAdmin = IsAdmin;
        }

        #endregion

        //----------------------------------------------------------------------------------------------------------------------------
        // Functions related to lookup tables handling.
        //----------------------------------------------------------------------------------------------------------------------------

        #region Lookup Table Functions
        public List<LookupTableDetails> GetAvailableCommonLookupTableDetails()
        {
            return ltmgr.GetAvailableCommonLookupTables();
        }
        public List<LookupTableDetails> GetProjectLookupDetails(InstalledMachine machine,ProjectDetails project)
        {
            return ltmgr.GetProjectLookupTables(machine, project);
        }
        public bool CreateNewCommonLookupTable(string tdesc, string keytext, string valuetext)
        {
            return ltmgr.CreateNewCommonLookupTable(tdesc, keytext, valuetext);
        }
        public bool DeleteLookupTable(LookupTableDetails table)
        {
            return ltmgr.DeleteLookupTable(table);
        }
        public List<LookupDataRow> GetLookupData(LookupTableDetails table)
        {
            return ltmgr.GetLookupData(table);
        }
        public bool UpdateLookupData(LookupTableDetails tabledetails, List<LookupDataRow> data)
        {
            return ltmgr.UpdateLookupData(tabledetails, data);
        }
        public LookupTableDetails GetLookupTableByID(int id)
        {
            return ltmgr.GetLookupTableDetailsByID(id);
        }
        public LookupTableDetails GetLookupTableByTableName(string tablename)
        {
            return ltmgr.GetLookupTableDetailsByTableName(tablename);
        }
        public bool CreateNewProjectLookupTable(InstalledMachine machine, ProjectDetails project,string tdesc,string keytext,string valuetext)
        {
            ltmgr.CreateNewProjectLookupTable(machine, project, tdesc, keytext, valuetext);
            return true;
        }

        #endregion

        //----------------------------------------------------------------------------------------------------------------------------
        // Function related to new machine handling.
        //----------------------------------------------------------------------------------------------------------------------------

        #region Machine Management Functions

        public InstalledMachine InstallNewMachine(string scriptpath, string outDllPath, string machineUCode, string majorVer, string minorVer,string codedesc)
        {
            bool isRecompilation = false;

            #region CHECKING FOR EXISTANCE OF THE GIVEN MACHINE UNIQUE CODE AND VERSIONS IN THE DATABASE.
            if (imm.IsMachineInstalled(machineUCode, majorVer, minorVer))
            {

                isRecompilation = true;
                UpdateCurrentMachine(imm.GetInstalledMachine(machineUCode));

                // Create backup of existing machine config file...
                FileManager.FileSystemHandler.CopyFile(
                    DasCoreSettings.GetMachineConfigScriptFileFullName(currentMachine),
                    DasCoreSettings.GetMachineConfigScriptFileFullName(currentMachine) + "_Backup_" + DateTime.Now.ToLongDateString() + ".bkp",
                    true);
            }
            #endregion
            
            FileManager.FileSystemHandler.CopyFile(
                    scriptpath,
                    DasCoreSettings.GetMachineConfigScriptFileFullName(machineUCode),
                    true);
            // Copy the user give script to the machine script folder...
            scriptpath = DasCoreSettings.GetMachineConfigScriptFileFullName(machineUCode);

            #region Script Compilation
            scriptCompiler = null;
            // Get the DLL file name from the Das Core Settings....

            string dllfilename = DasCoreSettings.GetCompiledDllFileName(machineUCode, majorVer, minorVer);// machineUCode + "_" + majorVer + "_" + minorVer + ".dll";
            scriptCompiler = new Compiler(dllfilename, outDllPath);
            String errors="";

            try
            {
                errors = scriptCompiler.Compile(scriptpath,false);
                if(errors !="")
                    throw (new Exception(errors));
            }
            catch (Exception ex)
            {
                throw (new Exception(errors));
            }
            #endregion

            #region Creating a New machine Data.

            if (isRecompilation == false)
            {
                // Handover control to InstalledMachinesManager for installing the new machine.
                // Default data is also created from this function itself.
                InstalledMachine machineToBeInstalled = new InstalledMachine();
                machineToBeInstalled.codefilepath = scriptpath;
                machineToBeInstalled.dllfilename = dllfilename;
                machineToBeInstalled.dllmajorversion = majorVer;
                machineToBeInstalled.dllminorversion = minorVer;
                machineToBeInstalled.dllrootpath = outDllPath;
                machineToBeInstalled.dllversiondesc = codedesc;
                machineToBeInstalled.machineuniquecode = machineUCode;

                factory.CreateTemplateObjects(machineToBeInstalled, out m, out mp, out rp, out sp, out tsp, out md);

                currentMachine = imm.CreateNewMachine(machineToBeInstalled, md, sp, tsp, mp, rp, m);
            }
            else
            {
                currentMachine.codefilepath = scriptpath;
                currentMachine.dllfilename = dllfilename;
                currentMachine.dllmajorversion = majorVer;
                currentMachine.dllminorversion = minorVer;
                currentMachine.dllrootpath = outDllPath;
                currentMachine.dllversiondesc = codedesc;

                imm.UpdateExistingMachine(currentMachine);
            }
            #endregion

            #region Creating Folders

            try
            {
                if(!isRecompilation)
                CreateNewMachineFolders(currentMachine);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Source + " : " + ex.Message);
            }
            #endregion

            #region Creating and writing machine specific files.

            // Create and modify the template for Error Correction script....
            if (!isRecompilation)
            {
                imm.CreateErrorCorrectorCodeFile(currentMachine);
                imm.CreateLinePrintCodeFile(currentMachine);
                imm.CreateUIConfigCodeFile(currentMachine);
                imm.CreateTestReportTemplateFile(currentMachine);
            }
            #endregion                      

            return currentMachine;
        }
        public List<InstalledMachine> GetListofInstalledMachines()
        {
            return imm.GetListofInstalledMachines();
        }
        public Dictionary<string, List<DefaultConfigurationData>> GetDefaultConfigurationData(InstalledMachine machine)
        {
            return imm.GetDefaultConfigurationData(machine);
        }
        public bool UpdateDefaultCongigurationData(InstalledMachine machine, Dictionary<string, List<DefaultConfigurationData>> updates)
        {
            return imm.UpdateDefaultConfigurationData(machine, updates);
        }
        public List<TestSetupParamsDetails> GetTestSetupParameters(InstalledMachine mc)
        {
            return imm.GetTestSetupParamsDetails(mc);
        }
        public List<MachineDetailsEntry> GetMachineDetailsEntries(InstalledMachine mc)
        {
            return imm.GetMachineDetailsEntries(mc);
        }
        public List<SpecifiedParamsDetails> GetSpecifiedParameters(InstalledMachine mc)
        {
            return imm.GetSpecifiedParametersDetails(mc);
        }
                
        #endregion

        //----------------------------------------------------------------------------------------------------------------------------
        // Function related to project details tables handling.
        //----------------------------------------------------------------------------------------------------------------------------

        #region Project Management Functions

        public ProjectDetails CreateNewProject(InstalledMachine mc,ProjectDetails project)
        {
            ProjectDetails pdetails = pmgr.CreateNewProject(mc, project);
            CreateProjectDataFolder(mc, project);
            return pdetails;
        }
        public bool UpdateExistingProject(InstalledMachine mc, ProjectDetails details)
        {
            UpdateProjectDataFolder(mc, details);
            return pmgr.UpdateExistingProject(details);
            
        }
        public bool DeleteProject(ProjectDetails details)
        {
            //TODO: to be completed
            return false;
        }
        public List<ProjectDetails> GetAllProjectDetails()
        {
            return pmgr.GetAllProjectDetails();
        }
        public List<ProjectDetails> GetProjectListByInstalledMachine(InstalledMachine machine)
        {
            return pmgr.GetProjectList(machine);
        }
        public List<SpecifiedParamsDetails> GetProjectSpecifiedParameters(InstalledMachine machine, ProjectDetails project)
        {
            return pmgr.GetProjectSpecifiedParameters(machine, project);
        }
        public bool UpdateProjectSpecifiedParameters(InstalledMachine machine, ProjectDetails project, List<SpecifiedParamsDetails> updates)
        {
            return pmgr.UpdateProjectSpecifiedParameters(machine, project, updates);
        }
        public List<ProjectMachineDetails> GetProjectMachines(ProjectDetails project)
        {
            return pmgr.GetProjectMachines(project);
        }
        public List<MachineTestDetails> GetMachineTests(ProjectMachineDetails pMachine)
        {
            return pmgr.GetMachineTests(pMachine);
        }
        public ProjectMachineDetails CreateProjectMachine(InstalledMachine mc, ProjectDetails project, string mNum, string mDesc)
        {
            //UpdateCurrentProject(project);
            ProjectMachineDetails pmd = pmgr.CreateProjectMachine(project, mNum, mDesc);
            //UpdateCurrentProjectMachine(pmd);
            CreateProjectMachineFolder(mc, project, pmd);

            // Copy the error corrector template file.....
            CreateProjectMachineErrorCorrectorScriptFile(mc, project, pmd);


            return pmd;
        }
        public bool UpdateProjectMachine(InstalledMachine mc, ProjectDetails pd, ProjectMachineDetails projmachine)
        {
            UpdateProjectMachineFolder(mc, pd, projmachine);
            // Copy the error corrector template file...
            CreateProjectMachineErrorCorrectorScriptFile(mc, pd, projmachine);
            return pmgr.UpdateProjectMachine(projmachine);
        }
        public bool DeleteProjectMachine(InstalledMachine mc, ProjectDetails pd, ProjectMachineDetails projmachine)
        {
            DeleteProjectMachineFolder(mc, pd, projmachine);
            return pmgr.DeleteProjectMachine(projmachine);
        }
        public MachineTestDetails CreateMachineTest(InstalledMachine mc, ProjectDetails pd, ProjectMachineDetails machine, string tRef, string tDate, string tRemarks)
        {
            //UpdateCurrentProjectMachine(machine);
            MachineTestDetails test = pmgr.CreateMachineTest(machine, tRef, tDate, tRemarks);
            //UpdateCurrentTest(test);
            CreateMachineTestReportsFolder(mc, pd, machine, test);
            return test;
        }
        public bool UpdateMachineTest(MachineTestDetails test)
        {
            return pmgr.UpdateMachineTest(test);
        }
        public bool DeleteMachineTest(MachineTestDetails test)
        {
            DeleteMachineTestFolder(currentMachine, currentProject, currentProjectMachine, test);
            return pmgr.DeleteMachineTest(test);
        }

        #endregion                
        
        //----------------------------------------------------------------------------------------------------------------------------
        // Functions related to Measurement configuration handling in measurementprofile table.
        //----------------------------------------------------------------------------------------------------------------------------

        #region Measurement Management Functions

        public bool UpdateMeasurementConfig(List<MeasurementProfileDetails> chdetails,InstalledMachine machine)
        {
            return imm.UpdateMeasurementConfig(chdetails, machine);
        }
        public List<string> GetSupportedMeasurementTypes()
        {
            return dmgr.GetMeasurementTypes();
        }
        public List<MeasurementProfileDetails> GetMeasurementProfileDetails(InstalledMachine mc)
        {
            return imm.GetMeasurementProfileDetails(mc);
        }
        public List<ResultParamsDetails> GetResultParamsDetails(InstalledMachine mc)
        {
            return imm.GetResultParamsDetails(mc);
        }

        #endregion

        //----------------------------------------------------------------------------------------------------------------------------
        // Function related to devicedetails tables handling.
        //----------------------------------------------------------------------------------------------------------------------------

        #region Device Management Functions

        public DeviceDetails CreateNewDevice(DeviceDetails device)
        {
            return dmgr.CreateNewDevice(device);
        }
        public bool UpdateExistingDeviceDetails(DeviceDetails details)
        {
            return dmgr.UpdateExistingDevice(details);
        }
        public bool UpdateExistingDeviceConfig(List<DeviceConfigDetails> details, DeviceDetails device)
        {
            return dmgr.UpdateExistingDeviceConfig(details, device);
        }
        public List<DeviceDetails> GetAllDevices()
        {
            return dmgr.GetAllDevices();
        }
        public List<DeviceConfigDetails> GetDeviceConfigDetails(DeviceDetails device)
        {
            return dmgr.GetDeviceConfigDetails(device);
        }

        #endregion

        //----------------------------------------------------------------------------------------------------------------------------
        // Functions related to User UI Management
        //----------------------------------------------------------------------------------------------------------------------------

        #region UI Management Functions

        /*
         
        public List<PropertyUILink> GetUIDisplayConfig(InstalledMachine mc)
        {
            return uicfgmgr.GetUIDisplayConfig(mc);
        }

        public List<PropertyUILink> GetUIDisplayConfig(string machineuniquecode)
        {
            InstalledMachine mc = imm.GetInstalledMachine(machineuniquecode);
            return uicfgmgr.GetUIDisplayConfig(mc);
        }

        public void CreateDefaultUIDisplayConfig(InstalledMachine mc)
        {
            uicfgmgr.CreateDefaultUIDisplayConfig(mc);
        }

        public void CreateDefaultUIDisplayConfig(string machineuniquecode)
        {
            InstalledMachine mc = imm.GetInstalledMachine(machineuniquecode);
            uicfgmgr.CreateDefaultUIDisplayConfig(mc);
        }

        public void UpdateUIDisplayConfig(InstalledMachine mc, List<PropertyUILink> config)
        {
            uicfgmgr.UpdateUIDisplayConfig(mc, config);
        }
        
         */

        public UIConfigurator GetUIConfigurator()
        {
            return this.uiconfig;
        }
        #endregion

        //----------------------------------------------------------------------------------------------------------------------------
        // Functions related to data acquisition
        //----------------------------------------------------------------------------------------------------------------------------

        #region Data Acquisition Functions

        // Scans hardware and returns data...
        public List<AlarmStatus> ScanHardware(out bool isAverageUpdated, ScanMode scMode)
        {
            // Perform scanning....
            // This is the root data.....          

            switch(scMode)
            {
                case ScanMode.NORMAL_MODE :
                    this.chData = hwmgr.ScanDataFromHardware(out alarms, HardwareScanMode.NORMAL);
                    isAverageUpdated = AssignAcquiredDataToObjects();
                    PerformActualComputation();
                    PerformErrorCorrection();
                    PerformManipComputation();
                    break;

                case ScanMode.SAFE_START_MODE :
                    this.chData = hwmgr.ScanDataFromHardware(out alarms, HardwareScanMode.BYPASS_TIME_MEASUREMENTS);
                    isAverageUpdated = AssignAcquiredDataToObjects();
                    //PerformComputation();
                    //PerformErrorCorrection();
                    //PerformManipComputation();
                    break;

                case ScanMode.MONITORING_MODE:
                    // Actual data...
                    this.chData = hwmgr.ScanDataFromHardware(out alarms, HardwareScanMode.NORMAL);
                    isAverageUpdated = AssignAcquiredDataToObjects();
                    PerformActualComputation();
                    //PerformErrorCorrection();
                    PerformManipComputation();
                    break;

                default: // Fast monitoring mode...
                    this.chData = hwmgr.ScanDataFromHardware(out alarms, HardwareScanMode.NORMAL);
                    isAverageUpdated = AssignAcquiredDataToObjects();
                    break;
            }                     
            
            

            // Note : The testing_mp_manip object presently contains the actual value.
            // This object, along with other objects are to be sent for manipulation to the manipulation
            // object here. 

            // --> Send testing_mp_manip to the manipulation object.

            // After returning from the manipulation object, the testing_mp_manip object contains
            // manipulated data.....

            // Do the computation after the manipulation.


            this.currentScan++;
            if (currentScan > maxScan)
                currentScan = 1;

            return alarms;
        }

        private bool AssignAcquiredDataToObjects()
        {
            bool isAverageUpdated = false;
            // Make a copy for the actual data....

            for (int i = 0; i < testing_mpDetailsList.Count; i++)
            {
                chDataAct.Add(new ChannelData());
            }

            for (int i = 0; i < testing_mpDetailsList.Count; i++)
            {
                //this.chDataAct.Add(this.chData[i]);

                this.chDataAct[i] = this.chData[i];
            }

            #region Before Manipulation

            for (int i = 0; i < testing_mpDetailsList.Count; i++)
            {
                // Update the sum data......
                ChannelData sumdata = this.sumChDataAct[i];
                sumdata.computedValue += chDataAct[i].computedValue;
                this.sumChDataAct[i] = sumdata;

                // Update the state of the internal objects with actual data here itself....
                // Update the actual measurement profile object.
                testing_mp_actual.SetDoubleValue(chDataAct[i].pname, chDataAct[i].computedValue);

                // Also update the testing measurement profile object here itself 
                // Note that at this point the values obtained from the hardware are not yet
                // manipulated.

                testing_mp_manip.SetDoubleValue(chDataAct[i].pname, chDataAct[i].computedValue);
            }

            // Check if the obtained scan is the final scan in the set
            if (currentScan == maxScan)
            {
                isAverageUpdated = true;
                /*
                 * Process Steps.
                 * 1. For each channel, compute the average data.
                 * 2. Save the average data into the appropriate list.
                 * 3. Update internal objects.
                 * 4. Reset the average and sum data lists... 
                 */
                for (int i = 0; i < testing_mpDetailsList.Count; i++)
                {
                    ChannelData adata = avgChDataAct[i];
                    ChannelData sdata = sumChDataAct[i];

                    adata.computedValue = sdata.computedValue / maxScan;

                    avgChDataAct[i] = adata;

                    sdata.computedValue = 0.0;

                    sumChDataAct[i] = sdata;

                    // Update the state of the internal variables....
                    // Update the actual average measurement profile object.
                    testing_mp_actual_avg.SetDoubleValue(adata.pname, adata.computedValue);

                }

                
            }

            #endregion

            return isAverageUpdated;
        }

        private void PerformErrorCorrection()
        {


            MachineErrorCorrector.ErrorCorrect(currentPoint, false);




            // At this point the following object are supposed to be full updated.

            /*
             * testing_mp_actual --
             * testing_mp_manip --
             * testing_mp_actual_avg --
             * testing_mp_manip_avg --
             * testing_mcd_actual --
             * testing_mcd_actual_avg --
             * testing_mcd_manip --
             * testing_mcd_manip_avg --
             * 
             */

        }

        private void PerformManipComputation()
        {
            #region After Manipulation

            this.testing_mcd_manip.Compute();

            // Manipulate mp data is to be inserted to the sum

            for (int i = 0; i < testing_mpDetailsList.Count; i++)
            {
                ChannelData sdata = sumChData[i];

                sdata.computedValue += testing_mp_manip.GetDoubleValue(sdata.pname);

                sumChData[i] = sdata;
            }

            // At this point the sum ch data is updates

            // Note, now testing_mp_manip object contains manipulated data.
            // testing_rp_manip contains manipulated results.

            if (currentScan == maxScan)
            {
                //currentScan = 1;
                for (int i = 0; i < testing_mpDetailsList.Count; i++)
                {

                    ChannelData adata = avgChData[i];
                    ChannelData sdata = sumChData[i];

                    adata.computedValue = sdata.computedValue / maxScan;

                    avgChData[i] = adata;

                    sdata.computedValue = 0.0;

                    sumChData[i] = sdata;

                    // Update the state of the internal variables....
                    // Update the actual average measurement profile object.
                    testing_mp_manip_avg.SetDoubleValue(adata.pname, adata.computedValue);
                }

                // At this point, avg channel data is filled up and ready to use....

                this.testing_mcd_manip_avg.Compute();

                // Only after averaging, the internal objects are to be updated.
            }

            #endregion
        }

        private void PerformActualComputation()
        {
            // Perform compute on the actual mcd object...
            testing_mcd_actual.Compute();

            // At this point, avg channel data is filled up and ready to use....
            this.testing_mcd_actual_avg.Compute();           
        }



        public void InitializeAndConfigureHardware()
        {
            InitializeScan(currentMachine, currentProject, currentProjectMachine, currentMachineTest);
        }

        private void InitializeScan(InstalledMachine mc, ProjectDetails pd, ProjectMachineDetails pmd, MachineTestDetails mtd)
        {
            // Prepare the objects.
            factory.CreateDataObjects(mc, pd, pmd, mtd,
                    out testing_md,
                    out testing_sp,
                    out testing_tsp,
                    out testing_mp_manip,
                    out testing_mp_manip_avg,
                    out testing_mp_actual,
                    out testing_mp_actual_avg,
                    out testing_mp_metadata,
                    out testing_rp_manip,
                    out testing_rp_manip_avg,
                    out testing_rp_actual,
                    out testing_rp_actual_avg,
                    out testing_mcd_manip,
                    out testing_mcd_manip_avg,
                    out testing_mcd_actual,
                    out testing_mcd_actual_avg);


            this.testing_mpDetailsList = imm.GetMeasurementProfileDetails(currentMachine);
            // Initialize the hardware...
            this.testing_rpDetailsList = imm.GetResultParamsDetails(currentMachine);

            // TODO : Update the current point for the relevant objects from database, if the test is already been conducted.



            // Initialise parameters for scanning and averaging...
            // TODO : Get the current point from the previous test if required.
            //this.currentPoint = 1; // Note ; Current point is to be set from the outside..
            this.currentScan = 1;
            this.maxScan = 10;

            this.chData = new List<ChannelData>(testing_mpDetailsList.Count);
            this.avgChData = new List<ChannelData>(testing_mpDetailsList.Count);
            this.sumChData = new List<ChannelData>(testing_mpDetailsList.Count);

            this.chDataAct = new List<ChannelData>(testing_mpDetailsList.Count);
            this.avgChDataAct = new List<ChannelData>(testing_mpDetailsList.Count);
            this.sumChDataAct = new List<ChannelData>(testing_mpDetailsList.Count);

            // Initialise all the channel data details.

            for (int i = 0; i < testing_mpDetailsList.Count; i++)
            {
                MeasurementProfileDetails mpd = testing_mpDetailsList[i];

                ChannelData avgchdata = new ChannelData();// avgChData[i];
                ChannelData sumchdata = new ChannelData();// sumChData[i];
                avgchdata.pindex = mpd.displayindex;
                avgchdata.pname = mpd.paramname;
                avgchdata.rawvalue = 0.0;
                avgchdata.computedValue = 0.0;

                avgChData.Add(avgchdata);

                sumchdata.pindex = mpd.displayindex;
                sumchdata.pname = mpd.paramname;
                sumchdata.rawvalue = 0.0;
                sumchdata.computedValue = 0.0;


                sumChData.Add(sumchdata);

                // Update the actual data too....

                ChannelData avgchdataact = new ChannelData();// avgChDataAct[i];
                ChannelData sumchdataact = new ChannelData();// sumChDataAct[i];

                avgchdataact.pindex = mpd.displayindex;
                avgchdataact.pname = mpd.paramname;
                avgchdataact.rawvalue = 0.0;
                avgchdataact.computedValue = 0.0;

                // Update the list after modification.

                avgChDataAct.Add(avgchdataact);

                sumchdataact.pindex = mpd.displayindex;
                sumchdataact.pname = mpd.paramname;
                sumchdataact.rawvalue = 0.0;
                sumchdataact.computedValue = 0.0;

                // update the list after modification.

                sumChDataAct.Add(sumchdataact);
            }


            // Create the error corrector scrpt object..
            try
            {
                ReLoadErrorCorrector();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            #region HARDWARE

            InitializeHardwareSettings();
            #endregion
        }

        // Resets hardware with new set of readings...
        private void InitializeHardwareSettings()
        {
            this.hwmgr = new HardwareManager(testing_mpDetailsList, dmgr, IsSimulation);

            if (!IsSimulation)
            {
                // Actually communicate with hardware...
                ReConfigureHardware(); // Sends commands to all hardware re reset them...
            }

        }

        // Resets hardware without changing any internal data...
        public void ReConfigureHardware()
        {
            // API change effects... Configure hardware requires resetting of all devices
            hwmgr.InitializeHardware();
        }



        public bool ReLoadErrorCorrector()
        {
            bool result;
            result = CompileAndLoadErrorScript(currentMachine, currentProject, currentProjectMachine);
            if (result == false)
            {
                MachineErrorCorrector = null;
                return false;
            }
            InitializeErrorCorrector(ref testing_rp_manip, ref testing_mp_manip, ref testing_sp, ref testing_tsp, ref testing_md, ref testing_mcd_manip);
            return true;
        }

        public MeasurementProfileBase GetCurrentMeasurementProfile()
        {
            return this.testing_mp_manip;
        }

        public MeasurementProfileBase GetCurrentMeasurementProfileAverage()
        {
            return this.testing_mp_manip_avg;
        }

        public ResultParamsBase GetCurrentResultParamsAverage()
        {
            return this.testing_rp_manip_avg;
        }

        public ResultParamsBase GetCurrentResultParams()
        {
            return this.testing_rp_manip;
        }

        public TestSetupParamsBase GetCurrentTestSetupParams()
        {
            return this.testing_tsp;
        }

        public MachineDetailsBase GetCurrentMachineDetails()
        {
            return this.testing_md;
        }

        public SpecifiedParamsBase GetCurrentSpecifiedParams()
        {
            return this.testing_sp;
        }




        #endregion

        //----------------------------------------------------------------------------------------------------------------------------
        // Functions related to User UI Management
        //----------------------------------------------------------------------------------------------------------------------------

        #region Data Management Functions
        public void RecordCurrentPoint(string noise1, string noise2)
        {
            int point = dataMgr.GetNumberOfRecordedPoints(currentMachine.machinecode, currentProject.projectcode, currentProjectMachine.machinenumber, currentMachineTest.testreference);
            if (point == -1)
            {
                point = 1;
            }
            else point++;

            #region Measurement Point Manip
            
            List<ObjectData> mpoint = new List<ObjectData>();
            List<ObjectData> rpoint = new List<ObjectData>();
            DateTime dt = DateTime.Now;

            for (int i = 0; i < testing_mpDetailsList.Count; i++)
            {
                ObjectData data = new ObjectData();
                data.pname = testing_mpDetailsList[i].paramname;
                data.pindex = testing_mpDetailsList[i].displayindex;
                data.pvalue = testing_mp_manip_avg.GetDoubleValue(data.pname).ToString();

                mpoint.Add(data);
            }

            for (int i = 0; i < testing_rpDetailsList.Count; i++)
            {
                ObjectData data = new ObjectData();
                data.pname = testing_rpDetailsList[i].paramname;
                data.pindex = testing_rpDetailsList[i].displayindex;
                data.pvalue = testing_rp_manip_avg.GetDoubleValue(data.pname).ToString();

                rpoint.Add(data);
            }

            dataMgr.RecordData(currentMachine.machinecode, currentProject.projectcode, currentProjectMachine.machinenumber,
                    currentMachineTest.testreference, mpoint, rpoint, dt, point, noise1, noise2, false);

            #endregion

            #region Measurement Point Actual

            mpoint.Clear();
            rpoint.Clear();

            mpoint = new List<ObjectData>();
            rpoint = new List<ObjectData>();

            for (int i = 0; i < testing_mpDetailsList.Count; i++)
            {
                ObjectData data = new ObjectData();
                data.pname = testing_mpDetailsList[i].paramname;
                data.pindex = testing_mpDetailsList[i].displayindex;
                data.pvalue = this.testing_mp_actual_avg.GetDoubleValue(data.pname).ToString();

                mpoint.Add(data);
            }

            for (int i = 0; i < testing_rpDetailsList.Count; i++)
            {
                ObjectData data = new ObjectData();
                data.pname = testing_rpDetailsList[i].paramname;
                data.pindex = testing_rpDetailsList[i].displayindex;
                data.pvalue = this.testing_rp_actual_avg.GetDoubleValue(data.pname).ToString();

                rpoint.Add(data);
            }

            dataMgr.RecordData(currentMachine.machinecode, currentProject.projectcode, currentProjectMachine.machinenumber,
                    currentMachineTest.testreference, mpoint, rpoint, dt, point, noise1, noise2, true);

            #endregion


            this.currentPoint = point + 1;
        }

        public void DeleteTestData(InstalledMachine mc, ProjectDetails pd, ProjectMachineDetails pmd, MachineTestDetails mtd)
        {
            dataMgr.DeleteTestData(mc.machinecode, pd.projectcode, pmd.machinenumber, mtd.testreference);
        }

        public int GetNumberOfRecordedPoints(InstalledMachine mc, ProjectDetails pd, ProjectMachineDetails pmd, MachineTestDetails mtd)
        {
            return dataMgr.GetNumberOfRecordedPoints(mc.machinecode,
                pd.projectcode, pmd.machinenumber, mtd.testreference);
        }

        public bool GetMeasurementDataPoint(InstalledMachine mc, ProjectDetails projc, ProjectMachineDetails pmd, MachineTestDetails mtd, int pointnum, bool isActual,
            out List<ObjectData> measPt, out List<ObjectData> resultPt,
            out DateTime dt, out string noise1, out string noise2)
        {
            return dataMgr.GetMeasurementDataPoint(mc.machinecode, projc.projectcode, pmd.machinenumber, mtd.testreference, pointnum, isActual, 
            out measPt, out resultPt,
            out dt, out noise1, out noise2);
        }
        #endregion

        //----------------------------------------------------------------------------------------------------------------------------
        // Functions related to internal state management
        //----------------------------------------------------------------------------------------------------------------------------

        #region Internal State Management

        public void InitializeCurrentTest(InstalledMachine mc, ProjectDetails pd, ProjectMachineDetails pmd, MachineTestDetails mtd, bool retainData)
        {
            this.currentMachine = mc;
            this.currentProject = pd;
            this.currentProjectMachine = pmd;
            this.currentMachineTest = mtd;                       

            //InitializeScan(this.currentMachine, this.currentProject, this.currentProjectMachine, this.currentMachineTest);
            
            int pcount = dataMgr.GetNumberOfRecordedPoints(currentMachine.machinecode, currentProject.projectcode, currentProjectMachine.machinenumber, currentMachineTest.testreference);

            if (retainData)
            {
                this.currentPoint = pcount + 1;
            }
            else
            {
                dataMgr.DeleteTestData(currentMachine.machinecode, currentProject.projectcode, currentProjectMachine.machinenumber, currentMachineTest.testreference);
                this.currentPoint = 1;
            }

        }
        
        private void UpdateCurrentMachine(InstalledMachine mc)
        {
            this.currentMachine = mc;
        }
        
        public void ReloadCurrentMeasurementProfile()
        {
            testing_mpDetailsList = imm.GetMeasurementProfileDetails(this.currentMachine);
            InitializeHardwareSettings();
            
        }

        public InstalledMachine GetCurrentMachine()
        {
            return this.currentMachine;
        }

        public ProjectDetails GetCurrentProject()
        {
            return this.currentProject;
        }

        public ProjectMachineDetails GetCurrentProjectMachine()
        {
            return this.currentProjectMachine;
        }

        public MachineTestDetails GetCurrentTest()
        {
            return this.currentMachineTest;
        }

        public MachineDescriptor GetCurrentMachineDescriptor()
        {
            return this.testing_mcd_manip;
        }

        public int GetCurrentPoint()
        {
            return currentPoint;
        }

        public int GetCurrentScan()
        {
            return this.currentScan;
        }

        public int GetMaxScan()
        {
            return maxScan;
        }

        #endregion

        //----------------------------------------------------------------------------------------------------------------------------
        // Functions related to file and folder handling
        //----------------------------------------------------------------------------------------------------------------------------
        
        #region File handling

        protected bool CreateNewMachineFolders(InstalledMachine mc)
        {
            FileSystemHandler.CreateMultipleFolders(DasCoreSettings.GetNewMachinePaths(mc));
            return true;
        }
        protected void CreateProjectDataFolder(InstalledMachine mc, ProjectDetails project)
        {
            string Path = DasCoreSettings.GetProjectPath(mc, project);
            FileSystemHandler.CreateFolder(Path);
        }
        protected void CreateProjectMachineFolder(InstalledMachine mc, ProjectDetails project, ProjectMachineDetails pmd)
        {
            string Path = DasCoreSettings.GetProjectMachinePath(mc, project, pmd);
            FileSystemHandler.CreateFolder(Path);
        }
        protected void CreateMachineTestReportsFolder(InstalledMachine mc, ProjectDetails project, ProjectMachineDetails pmd, MachineTestDetails mtd)
        {
            string Path = DasCoreSettings.GetMachineTestReportsPath(mc, project, pmd, mtd);
            FileSystemHandler.CreateFolder(Path);

            Path = DasCoreSettings.GetMachineTestLinePrintsPath(mc, project, pmd, mtd);
            FileSystemHandler.CreateFolder(Path);

            Path = DasCoreSettings.GetMachineTestScreenshotsPath(mc, project, pmd, mtd);
            FileSystemHandler.CreateFolder(Path);
        }
        protected void UpdateProjectDataFolder(InstalledMachine mc, ProjectDetails project)
        {
            string Path = DasCoreSettings.GetProjectPath(mc, project);
            FileSystemHandler.CreateFolder(Path);
        }
        protected void UpdateProjectMachineFolder(InstalledMachine mc, ProjectDetails project, ProjectMachineDetails pmd)
        {
            string Path = DasCoreSettings.GetProjectMachinePath(mc, project, pmd);
            FileSystemHandler.CreateFolder(Path);
        }
        protected void UpdateMachineTestReportsFolder(InstalledMachine mc, ProjectDetails project, ProjectMachineDetails pmd, MachineTestDetails mtd)
        {
            string Path = DasCoreSettings.GetMachineTestReportsPath(mc, project, pmd, mtd);
            FileSystemHandler.CreateFolder(Path);
        }
        protected void DeleteMachineTestFolder(InstalledMachine mc, ProjectDetails project, ProjectMachineDetails pmd, MachineTestDetails mtd)
        {
            string Path = DasCoreSettings.GetMachineTestReportsPath(mc, project, pmd, mtd);
            try
            {
                FileSystemHandler.DeleteFolder(Path, true);
            }
            catch (Exception ex)
            {
                //Do nothing...
            }
        }
        protected void DeleteProjectMachineFolder(InstalledMachine mc, ProjectDetails project, ProjectMachineDetails pmd)
        {
            string Path = DasCoreSettings.GetProjectMachinePath(mc, project, pmd);
            try
            {
                FileSystemHandler.DeleteFolder(Path, true);
            }
            catch (Exception ex)
            {
                //Do nothing...
            }
            
        }
        protected void DeleteProjectDataFolder(InstalledMachine mc, ProjectDetails project)
        {
            string Path = DasCoreSettings.GetProjectPath(mc, project);
            try
            {
                FileSystemHandler.DeleteFolder(Path, true);
            }
            catch (Exception ex)
            {
                //Do nothing...
            }
        }

        protected void CreateProjectMachineErrorCorrectorScriptFile(InstalledMachine machine, ProjectDetails project, ProjectMachineDetails pmd)
        {
            string fileToCopy = DasCoreSettings.GetMachineErrorCorrectorTemplateFileFullName(machine);
            string dest = DasCoreSettings.GetProjectMachineErrCorrScriptFileFullName(machine, project, pmd);

            FileSystemHandler.CopyFile(fileToCopy, dest, true);

        }

        #endregion

        //----------------------------------------------------------------------------------------------------------------------------
        // Functions related to file and folder handling
        //----------------------------------------------------------------------------------------------------------------------------

        #region Error Script Handling

        public bool CompileAndLoadErrorScript(InstalledMachine mc, ProjectDetails pd, ProjectMachineDetails pmd)
        {
            string eScript = DasCoreSettings.GetProjectMachineErrCorrScriptFileFullName(mc, pd, pmd);
            bool isCompiled;
            string errors="";
            scriptCompiler = new Compiler("", "");
            try
            {
                Assembly ecasm = scriptCompiler.CompileInMemory(eScript, out isCompiled, out errors);
                Type ecType;
                if (isCompiled)
                {
                    ecType = ecasm.GetType("userscript." + mc.machineuniquecode + "." + mc.machineuniquecode + "_ErrorCorrector");
                    MachineErrorCorrector = (ErrorCorrector)Activator.CreateInstance(ecType);
                    return true;
                }
            }
            catch (Exception ex)
            {
                MachineErrorCorrector = null;
                throw new Exception("Error Compiling : " + eScript + " : " + errors);
            }
            return false;
        }

        public void InitializeErrorCorrector(ref ResultParamsBase rp,
                                    ref MeasurementProfileBase mp,
                                    ref SpecifiedParamsBase sp,
                                    ref TestSetupParamsBase tsp,
                                    ref MachineDetailsBase md,
                                    ref MachineDescriptor mcd)
        {
            MachineErrorCorrector.InitializeErrorCorrector(ref rp,
                                                            ref mp,
                                                            ref sp,
                                                            ref tsp,
                                                            ref md,
                                                            ref mcd);
        }
        
        #endregion

        //----------------------------------------------------------------------------------------------------------------------------
        // Functions related to multi threading
        //----------------------------------------------------------------------------------------------------------------------------

        #region Multi Threading Experimental stuff

        public HardwareManager GetHardwareManager()
        {
            return this.hwmgr;
        }

        #endregion
        
    }
}
