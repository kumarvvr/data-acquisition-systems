﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Odbc;
using BHEL.PUMPSDAS.Datatypes;


namespace DasCoreLib
{
    /// <summary>
    /// Multiple instances of this object will create multiple connections to the database.
    /// Interface is straight forward. May not be thread-safe.
    /// Throws an exception during construction.
    /// 
    /// NOTES :
    /// 
    /// 1. All data in and out should be in standard data structures, as much as possible.
    /// 2. Structured table data can be returned in ODBC Data Reader object.
    /// </summary>
    public class DatabaseManager
    {

        #region PRIVATE MEMBERS

        OdbcConnection conn;

        DatabaseDetails dbasesettings;

        string connstring;

        public const string default_md_tablename = "machinedetails";

        public const string default_sp_tablename = "specifiedparameters";

        public const string default_mp_tablename = "measurementprofile";

        public const string default_rp_tablename = "resultparameters";

        public const string default_tsp_tablename = "testsetupparameters";

        public const string projectdetails_tablename = "projectdetails";

        public const string lookupinfo_tablename = "lookupinfo";

        public const string projectlookupinfo_tablename = "projectlookupinfo";

        #endregion

        /// <summary>
        /// Creates a New DatabaseManager Object and establishes a connection with it.
        /// An exception is thrown when a connection cannot be established.
        /// This is to connect to a PostgreSQL ANSI database over ODBC drivers only.
        /// </summary>
        /// <param name="server">IP address / Hostname of the server. Localhost in case of the same system.</param>
        /// <param name="dbname"> Case-Sensitive name of the database.</param>
        /// <param name="uname"> User name.</param>
        /// <param name="pass"> Plain text password</param>
        public DatabaseManager(DatabaseDetails dbasesettings)
        {
            this.dbasesettings.driver = dbasesettings.driver;
            this.dbasesettings.server = dbasesettings.server;
            this.dbasesettings.port = dbasesettings.port;
            this.dbasesettings.databaseName = dbasesettings.databaseName;
            this.dbasesettings.userName = dbasesettings.userName;
            this.dbasesettings.password = dbasesettings.password;

            // construct the connection string.
            // we are using a postgresql ansi connection.

            connstring = "Driver=" + this.dbasesettings.driver + ";" +
                "Server=" + this.dbasesettings.server + ";" +
                "Port=" + this.dbasesettings.port + ";" +
                "Database=" + this.dbasesettings.databaseName + ";" +
                "Uid=" + this.dbasesettings.userName + ";" +
                "Pwd=" + this.dbasesettings.password + ";";
            try
            {
                conn = new OdbcConnection(connstring);
                conn.Open();
            }
            catch(Exception e)
            {
                throw new Exception("DatabaseManager -> Error connecting to database.ERROR : " + e.Message);
            }
        }               

        /// <summary>
        /// Returns the internally generated connection string that is used to connect to the database.
        /// </summary>
        /// <returns>A string that contains the connection string.</returns>
        public string GetDatabaseDetails()
        {                       
            return conn.ConnectionString;
        }
        
        /// <summary>
        /// Executes a select query. Throws an exception if the query execution fails.
        /// </summary>
        /// <param name="qry">The query that is to be executed.</param>
        /// <returns>An OdbcDataReader object that contains the results of the select query.</returns>
        public OdbcDataReader ExecuteSelectQuery(string qry)
        {
            OdbcCommand cmd = new OdbcCommand(qry, this.conn);
            OdbcDataReader result;// = new OdbcDataReader();
            
            try
            {
                result = cmd.ExecuteReader();
            }
            catch (Exception ex)
            {
                throw new Exception("Error executing select query ;" + qry + " ERROR : " + ex.Message);
            }
            cmd.Dispose();
            return result;
        }

        /// <summary>
        /// Executes an insert query. Throws an exception if the query execution fails.
        /// </summary>
        /// <param name="qry">The insert query that is to be executed.</param>
        public void ExecuteInsertQuery(string qry)
        {
            OdbcCommand cmd = new OdbcCommand(qry, this.conn);

            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw new Exception("Error executing insert query ;" + qry + " ERROR : " + ex.Message);
            }
            cmd.Dispose();
        }

        /// <summary>
        /// Executes an update query. Throws an exception if the query execution fails.
        /// </summary>
        /// <param name="qry">The update query that is to be executed.</param>
        public void ExecuteUpdateQuery(string qry)
        {
            OdbcCommand cmd = new OdbcCommand(qry, this.conn);

            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw new Exception("Error executing update query ;"+qry+" ERROR : "+ex.Message);
            }
            cmd.Dispose();
        }

        /// <summary>
        /// Executes misc. queries (Ex: delete queries, etc).  Throws an exception if the query execution fails.
        /// </summary>
        /// <param name="qry">Query that is to be executed.</param>
        /// <returns>Returns true if the query execution was successful.</returns>
        public bool ExecuteSystemQuery(string qry)
        {
            OdbcCommand cmd = new OdbcCommand(qry, this.conn);

            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw new Exception("Error executing update query ;" + qry + " ERROR : " + ex.Message);
            }
            cmd.Dispose();
            return true;
        }


    }
}
