using System;
using System.Collections.Generic;
using System.Linq;
using Ivi.Visa.Interop;
using BHEL.PUMPSDAS.Datatypes;
using System.Threading;

namespace DasCoreLib
{
    public class HPBASICHardware : Hardware
    {

        FormattedIO488 io;

        ResourceManager rm;

        //List<string> measureStringsNormal, measureStringsChannelBypassed;

        List<MeasurementProfileDetails> manualMeasurements,hardwareMeasurements,hardwareMeasurementsBypassTime;

        string resetstring;

        HardwareScanMode previousMode;
        // Cannot be true for HPBASIC Hardware

        public HPBASICHardware(int fk_deviceid, DeviceManager devmgr)
            : base(fk_deviceid, devmgr)
        {
            previousMode = HardwareScanMode.BYPASS_TIME_MEASUREMENTS;
        }

        // Configures all internal settings only.
        public override void ConfigureHardware(List<MeasurementProfileDetails> mpDetailsList)
        {
            this.mpDetailsList = mpDetailsList;

            manualMeasurements = new List<MeasurementProfileDetails>();
            hardwareMeasurements = new List<MeasurementProfileDetails>();
            hardwareMeasurementsBypassTime = new List<MeasurementProfileDetails>();

            
            //measureStringsNormal = new List<string>();            
            //measureStringsChannelBypassed = new List<string>();

            // Seperate Measurement details into manual, hardware and hardwarebypass parts
            for (int i = 0; i < mpDetailsList.Count; i++)
            {
                MeasurementProfileDetails mpd = new MeasurementProfileDetails();
                string mtype = mpDetailsList[i].meastype;
                if (mtype == "MANUAL" || mtype == "MANUAL RANDOMISED")
                {
                    mpd = mpDetailsList[i];
                    mpd.configString = "";
                    mpd.resetString = "";
                    mpd.measString = "";
                    manualMeasurements.Add(mpd);
                }
                else
                {
                    mpd = mpDetailsList[i];                 
                    if (mpd.meastype == "DCV" || mpd.meastype == "DCA" || mpd.meastype == "ACV" || mpd.meastype == "ACA" || mpd.meastype == "DCmV"
                                    || mpd.meastype == "DCmA" || mpd.meastype == "ACmV" || mpd.meastype == "ACmA" || mpd.meastype == "RTD"
                                    || mpd.meastype == "DIRECT")
                    {
                        mpd.measString = GetMeasString(mpd.meastype, mpd.channelid);
                        //measureStringsNormal.Add(mpd.measString);
                        hardwareMeasurements.Add(mpd);
                    }
                    else
                    {
                        mpd.measString = GetMeasString(mpd.meastype, mpd.channelid);
                        this.hardwareMeasurementsBypassTime.Add(mpd);
                    }

                }
            }

            // Set all internal strings...
            InitializeHardwareSettings();

        }

        // To communicate with hardware and configure internal channel settings....
        public override void InitializeHardware()
        {
            EstablishCommunication();
        }

        public override List<ChannelData> ScanHardware(out List<AlarmStatus> alarms, HardwareScanMode mode)
        {
            List<ChannelData> result = new List<ChannelData>();
            List<AlarmStatus> alarms1, alarms2;

            if (previousMode != mode)
            {
                ResetHardware();

                previousMode = mode;
            }
            alarms = new List<AlarmStatus>();

            result.AddRange(GenerateManualChannelData());
            result.AddRange(ScanNormal(out alarms1));
            alarms.AddRange(alarms1);

            if (mode == HardwareScanMode.BYPASS_TIME_MEASUREMENTS)
            {
                // Only scan normal channels...
                result.AddRange(GenerateBypassedChannelData());
            }
            else
            {
                result.AddRange(ScanBypassedChannels(out alarms2));
                alarms.AddRange(alarms2);
            }

            return result;
        }
        private List<ChannelData> ScanNormal(out List<AlarmStatus> alarms)
        {

            List<ChannelData> result = new List<ChannelData>();
            alarms = new List<AlarmStatus>();
            AlarmStatus alStatus;

            string[] data = { };

            int l;
            data = new string[hardwareMeasurements.Count];

            for (int i = 0; i < this.hardwareMeasurements.Count; i++)
            {
                //Thread.Sleep(5);
                string r;
                r = SendCommand(hardwareMeasurements[i].measString, true);
                data[i] = r;
                l = data[i].Length;
                data[i] = data[i].Remove(l - 2, 2).TrimStart();
            }

            for (int i = 0; i < hardwareMeasurements.Count; i++)
            {
                ChannelData ch = new ChannelData();
                ch.pname = hardwareMeasurements[i].paramname;
                ch.pindex = hardwareMeasurements[i].displayindex;

                ch.rawvalue = double.Parse(data[i],
                    System.Globalization.NumberStyles.AllowDecimalPoint |
                    System.Globalization.NumberStyles.AllowExponent |
                    System.Globalization.NumberStyles.AllowLeadingSign);

                ch.computedValue = hardwareMeasurements[i].ComputeValue(ch.rawvalue);

                result.Add(ch);

                if (ch.computedValue > hardwareMeasurements[i].highalarm)
                {
                    alStatus = new AlarmStatus();
                    alStatus.alarmType = AlarmType.HIGH_ALARM;
                    alStatus.pname = ch.pname;
                    alarms.Add(alStatus);
                }
                else if (ch.computedValue < hardwareMeasurements[i].lowalarm)
                {
                    alStatus = new AlarmStatus();
                    alStatus.alarmType = AlarmType.LOW_ALARM;
                    alStatus.pname = ch.pname;
                    alarms.Add(alStatus);
                }

            }
            return result;
        }
        private List<ChannelData> ScanBypassedChannels(out List<AlarmStatus> alarms)
        {

            List<ChannelData> result = new List<ChannelData>();
            alarms = new List<AlarmStatus>();
            AlarmStatus alStatus;

            string[] data = { };

            int l;
            data = new string[this.hardwareMeasurementsBypassTime.Count];

            for (int i = 0; i < this.hardwareMeasurementsBypassTime.Count; i++)
            {
                //Thread.Sleep(5);
                string r;
                r = SendCommand(hardwareMeasurementsBypassTime[i].measString, true);
                data[i] = r;
                l = data[i].Length;
                data[i] = data[i].Remove(l - 2, 2).TrimStart();
            }

            for (int i = 0; i < hardwareMeasurementsBypassTime.Count; i++)
            {
                ChannelData ch = new ChannelData();
                ch.pname = hardwareMeasurementsBypassTime[i].paramname;
                ch.pindex = hardwareMeasurementsBypassTime[i].displayindex;

                ch.rawvalue = double.Parse(data[i],
                    System.Globalization.NumberStyles.AllowDecimalPoint |
                    System.Globalization.NumberStyles.AllowExponent |
                    System.Globalization.NumberStyles.AllowLeadingSign);

                ch.computedValue = hardwareMeasurementsBypassTime[i].ComputeValue(ch.rawvalue);

                result.Add(ch);

                if (ch.computedValue > hardwareMeasurementsBypassTime[i].highalarm)
                {
                    alStatus = new AlarmStatus();
                    alStatus.alarmType = AlarmType.HIGH_ALARM;
                    alStatus.pname = ch.pname;
                    alarms.Add(alStatus);
                }
                else if (ch.computedValue < hardwareMeasurementsBypassTime[i].lowalarm)
                {
                    alStatus = new AlarmStatus();
                    alStatus.alarmType = AlarmType.LOW_ALARM;
                    alStatus.pname = ch.pname;
                    alarms.Add(alStatus);
                }

            }
            return result;
        }
        private List<ChannelData> GenerateBypassedChannelData()
        {
            List<ChannelData> result = new List<ChannelData>();

            for (int i = 0; i < hardwareMeasurementsBypassTime.Count; i++)
            {
                ChannelData ch = new ChannelData();
                ch.pname = hardwareMeasurementsBypassTime[i].paramname;
                ch.pindex = hardwareMeasurementsBypassTime[i].displayindex;

                // equivalent to manual channels...
                ch.rawvalue = -1;
                ch.computedValue = hardwareMeasurementsBypassTime[i].outputmax;
                result.Add(ch);
            }
            return result;
        }
        private List<ChannelData> GenerateManualChannelData()
        {
            List<ChannelData> result = new List<ChannelData>();
            for (int i = 0; i < this.manualMeasurements.Count; i++)
            {
                ChannelData ch = new ChannelData();
                ch.pname = manualMeasurements[i].paramname;
                ch.pindex = manualMeasurements[i].displayindex;
                ch.rawvalue = -1;
                ch.computedValue = manualMeasurements[i].ComputeValue(ch.rawvalue);
                result.Add(ch);
            }

            return result;
        }

        public override void ResetHardware()
        {
            // Send command to reset the HP hardware.
            if (resetstring != "" && resetstring != null)
                SendCommand(resetstring, false);
        }

        // Dispose all internal objects...
        public override void DisposeHardware()
        {
            // Do stuff here....

            base.DisposeHardware();
            // Release com objects...
        }

        #region Private functions

        private void EstablishCommunication()
        {
            try
            {
                rm = new ResourceManagerClass();
                io = new FormattedIO488Class();
                io.IO = (IMessage)rm.Open(deviceDetails.connstring, AccessMode.NO_LOCK, 7000);
                io.IO.Timeout = 2000;

                // Resets and Configures Hardware
                ResetHardware();
            }
            catch (Exception e)
            {
                throw new Exception("ERROR : " + e.Message);
            }

            // Set the hardware too...            
        }

        private void InitializeHardwareSettings()
        {
            resetstring = "CLR;RST";
        }

        private string SendCommand(string cmd, bool fetchResult)
        {
            io.WriteString(cmd);
            //Thread.Sleep(10);
            if (fetchResult)
                return io.ReadString();

            return "";
        }

        private string ReadDataBuffer()
        {
            return io.ReadString();
        }

        #endregion

        protected override string GetConfigString(string meastype, string chId)
        {
            string confstr = "";
            for (int i = 0; i < devConfDetails.Count; i++)
            {
                if (devConfDetails[i].meastype == meastype)
                {
                    confstr = devConfDetails[i].configstring;
                }
            }
            return string.Format(confstr, chId);
        }
        protected override string GetResetString(string meastype, string chId)
        {
            string rststr = "";
            for (int i = 0; i < devConfDetails.Count; i++)
            {
                if (devConfDetails[i].meastype == meastype)
                {
                    rststr = devConfDetails[i].resetstring;
                }
            }
            return string.Format(rststr, chId);
        }
        protected override string GetMeasString(string meastype, string chId)
        {
            string measstr = "";
            for (int i = 0; i < devConfDetails.Count; i++)
            {
                if (devConfDetails[i].meastype == meastype)
                {
                    measstr = devConfDetails[i].measstring;
                }
            }
            return string.Format(measstr, chId);
        }
        protected override string GetAddonCommand(string meastype, string chId)
        {
            string addonstr = "";
            for (int i = 0; i < devConfDetails.Count; i++)
            {
                if (devConfDetails[i].meastype == meastype)
                {
                    addonstr = devConfDetails[i].addoncmd;
                }
            }
            return string.Format(addonstr, chId);
        }

    }
}
