﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Odbc;
using BHEL.PUMPSDAS.Datatypes;
using DasCoreUtilities;

namespace DasCoreLib
{
    public class LookupTableManager
    {
        #region PRIVATE MEMBERS

        DatabaseManager _database;

        #region Queries

        //Queries related to Lookup table list

        const string qry_commonLookupTableDetails =
            "select lookupid,lookupdesc,keytext,valuetext,tablename,iscommon from lookupinfo where iscommon=true order by lookupid";

        const string qry_projectLookupTableDetails =
            "select fk_lookupid from projectlookupinfo where fk_machinecode={0} and fk_projectcode={1}";

        const string qry_checkLookupTableExistenceByName =
            "select lookupid from lookupinfo where tablename='{0}'";

        const string qry_checkLookupTableExistenceByID =
            "select tablename from lookupinfo where lookupid={0}";

        const string qry_insertLookuptableInfo =
            "insert into lookupinfo (lookupdesc,keytext,valuetext,iscommon) values('{0}', '{1}', '{2}', {3}) returning lookupid,lookupdesc,keytext,valuetext,tablename,iscommon";

        const string qry_updateLookupInfoTableName =
            "update lookupinfo set tablename='{0}' where lookupid={1} returning lookupid,lookupdesc,keytext,valuetext,tablename,iscommon";

        const string qry_updateLookupInfoTable =
            "update lookupinfo set lookupdesc='{0}',keytext='{1}',valuetext='{2}' where lookupid={3} returning lookupid,lookupdesc,keytext,valuetext,tablename,iscommon";
        
        const string qry_selectLookupTableByID =
            "select lookupid,lookupdesc,keytext,valuetext,tablename,iscommon from lookupinfo where lookupid={0}";

        //const string qry_createLookupTable = "CREATE TABLE {0}(key double precision,value double precision)" +
          //  "WITH (OIDS=FALSE);ALTER TABLE {0}  OWNER TO postgres;";

        const string qry_InsertProjectLookupInfoEntry = "insert into projectlookupinfo (fk_lookupid,fk_projectcode,fk_machinecode)" +
            " values({0},{1},{2})";

        const string qry_lookupretrievedata = "SELECT key,value from lookupdata where fk_lookupid={0}";

        const string qry_lookupdeletedata = "DELETE from lookupdata where fk_lookupid={0}";
        
        const string qry_lookupinsertdata = "INSERT into lookupdata (fk_lookupid,key,value) values({0},{1},{2})";

        const string qry_deletelookuptabledata = "DELETE from lookupdata where fk_lookupid={0}";

        const string qry_deleteEntryFromLookupTableInfoByName = "DELETE from lookupinfo where tablename='{0}'";

        const string qry_deleteEntryFromLookupTableInfoById = "DELETE from lookupinfo where lookupid={0}";
        
        #endregion

        #endregion

        /// <summary>
        /// Constructs a LookupTableManager Object.
        /// </summary>
        /// <param name="db">A DatabaseManager Object that is fully initialised and ready for immediate use.</param>
        public LookupTableManager(DatabaseManager db)
        {
            this._database = db;
        }

        /// <summary>
        /// Fetches a list of all the available Common Lookup Table details. Note: Only the lookup table details are
        /// fetched, not the actual data.
        /// </summary>
        /// <returns>A list of LookupTableDetails objects that represent all the available common lookup tables in the system.</returns>
        public List<LookupTableDetails> GetAvailableCommonLookupTables()
        {
            List<LookupTableDetails> result = new List<LookupTableDetails>();
            LookupTableDetails lt;
            OdbcDataReader reader;
            string qry = string.Format(qry_commonLookupTableDetails);

            reader = _database.ExecuteSelectQuery(qry);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    lt = new LookupTableDetails();
                    lt.lookupid = reader.GetInt32(0);
                    lt.lookupdesc = reader["lookupdesc"].ToString();
                    lt.keytext = reader["keytext"].ToString();
                    lt.valuetext = reader["valuetext"].ToString();
                    lt.tablename = reader["tableName"].ToString();
                    lt.iscommon = reader.GetBoolean(5);

                    result.Add(lt);
                }
            }

            return result;
        }

        /// <summary>
        /// Fetches all the lookuptables linked to a particular project.
        /// </summary>
        /// <param name="machine">An InstalledMachine object that represents a machine present in the system.</param>
        /// <param name="project">A ProjectDetails object that represents an existing project in the system</param>
        /// <returns></returns>
        public List<LookupTableDetails> GetProjectLookupTables(InstalledMachine machine,ProjectDetails project)
        {
            List<LookupTableDetails> result = new List<LookupTableDetails>();
            LookupTableDetails lt;
            OdbcDataReader reader;
            int lkid;
            string qry = string.Format(qry_projectLookupTableDetails, machine.machinecode.ToString(), project.projectcode.ToString());
            reader = _database.ExecuteSelectQuery(qry);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    lkid = reader.GetInt32(0);
                    lt = this.GetLookupTableDetailsByID(lkid);
                    result.Add(lt);
                }
            }

            return result;
        }

        /// <summary>
        /// Checks for existance of a particular lookup table based on the lookup table name
        /// </summary>
        /// <param name="tname">Name of the lookup table.</param>
        /// <returns>True if the table exists. False if the table does not exist.</returns>
        public bool DoesLookupTableExist(string tname)
        {
            string qry = string.Format(qry_checkLookupTableExistenceByName, tname);
            // TODO : To check the actual availability of tables too.
            OdbcDataReader rd = _database.ExecuteSelectQuery(qry);

            if (rd.HasRows)
                return true;
            return false;
        }

        /// <summary>
        /// Checks for existance of a particular lookup table based on the lookup table ID
        /// </summary>
        /// <param name="tID">Id of the lookup table to be searched.</param>
        /// <returns>True if the table exists. False if the table does not exist.</returns>
        public bool DoesLookupTableExist(int tID)
        {
            string qry = string.Format(qry_checkLookupTableExistenceByID, tID);
            // TODO : To check the actual availability of tables too.
            OdbcDataReader rd = _database.ExecuteSelectQuery(qry);

            if (rd.HasRows)
                return true;
            return false;
        }

        /// <summary>
        /// Deletes a lookup table based on the name.Note : Both the data and the information regarding this table is deleted.
        /// If this table is being used somewhere in the system, say a particular project, an exception will be thrown. Unless
        /// all the references to the table are removed, it's data cannot be deleted.
        /// </summary>
        /// <param name="table">A LookupTableDetails object which has to be deleted</param>
        /// <returns>True if the table is deleted.</returns>
        public bool DeleteLookupTable(LookupTableDetails table)
        {
           
            string qry = string.Format(qry_deleteEntryFromLookupTableInfoById, table.lookupid);
            _database.ExecuteSystemQuery(qry);

            qry = string.Format(qry_deletelookuptabledata, table.lookupid);
            _database.ExecuteSystemQuery(qry);
            
            return true;
        }
        
        /// <summary>
        /// Fetches the details of a lookuptable based on the ID
        /// </summary>
        /// <param name="id">An integer representing the ID for the table.</param>
        /// <returns>A LookupTableDetails object that represents information about the lookup table.</returns>
        public LookupTableDetails GetLookupTableDetailsByID(int id)
        {
            string qry = string.Format(qry_selectLookupTableByID, id);
            OdbcDataReader re;
            LookupTableDetails result = new LookupTableDetails();

            re = _database.ExecuteSelectQuery(qry);
            if (re.HasRows)
            {
                re.Read();
                result.lookupid = re.GetInt32(0);
                result.lookupdesc = re["lookupdesc"].ToString();
                result.keytext = re["keytext"].ToString();
                result.valuetext = re["valuetext"].ToString();
                result.tablename = re["tableName"].ToString();
                result.iscommon = re.GetBoolean(5);
            }
            return result;
        }

        public LookupTableDetails GetLookupTableDetailsByTableName(string name)
        {
            LookupTableDetails result = new LookupTableDetails();

            string tID = "";

            tID = name.Split('_')[1];

            result = GetLookupTableDetailsByID(int.Parse(tID));

            return result;
        }

        /// <summary>
        /// Creates a new lookup table in the system. Default data contains no entries for the table. Note this function only creates the table, it is
        /// not concerned with project linking.
        /// </summary>
        /// <param name="tdesc">The table description for the new table</param>
        /// <param name="keytext">The heading for the Key values in the lookup table data.</param>
        /// <param name="valuetext">The heading for the values values in the lookup table data.</param>
        /// <param name="isCommon">A boolean specifying if the lookup table is common or not</param>
        /// <returns></returns>
        public bool CreateNewCommonLookupTable(string tdesc, string keytext, string valuetext)
        {
            // First create an entry into the lookupinfo table.
            bool isCommon = true;
            LookupTableDetails det = this.InsertLookupInfoEntry(tdesc, keytext, valuetext, isCommon);

            // With the newly created info entry, create the actual table in the database.
            /*
            string qry = string.Format(qry_createLookupTable, det.tablename);
            _database.ExecuteInsertQuery(qry);
            */
            return true;
        }

        /// <summary>
        /// Creates a new lookup table specific to a particular project.
        /// </summary>
        /// <param name="machine">An InstalledMachine object under which the project exists.</param>
        /// <param name="project">A ProjectDetails object under which the new lookup table is to be added.</param>
        /// <param name="tdesc">Table Description string</param>
        /// <param name="keytext">Label text for the Key values.</param>
        /// <param name="valuetext">Label text for the Value values.</param>
        /// <returns>True is the table was created successfully.</returns>
        public bool CreateNewProjectLookupTable(InstalledMachine machine, ProjectDetails project, string tdesc, string keytext, string valuetext)
        {
            // First create entries in the lookupinfo and projectlookupinfo..
            bool isCommon = false;
            string qry;
            // Info entry
            LookupTableDetails det = this.InsertLookupInfoEntry(tdesc, keytext, valuetext, isCommon);

            // With the newly created info entry, create the actual table in the database.
            /*
            qry = string.Format(qry_createLookupTable, det.tablename);
            _database.ExecuteInsertQuery(qry);
            */

            // Update the info in the project lookup info....
            qry = string.Format(qry_InsertProjectLookupInfoEntry, det.lookupid, project.projectcode, machine.machinecode);
            _database.ExecuteInsertQuery(qry);

            return true;
        }

        /// <summary>
        /// Fetches the lookup table data fro a given table.
        /// </summary>
        /// <param name="table">The LookupTableDetails object for which the data is to be procured.</param>
        /// <returns></returns>
        public List<LookupDataRow> GetLookupData(LookupTableDetails table)
        {
            // TODO : Preliminary checks...
            return GetLookupData(table.lookupid);
        }
        
        /// <summary>
        /// For a given LookupTableDetails object, updates its corresponding data with the data given. Also, the details of the lookup table are also updated.
        /// </summary>
        /// <param name="tabledetails"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public bool UpdateLookupData(LookupTableDetails tabledetails, List<LookupDataRow> data)
        {
            string qry;
            OdbcDataReader re;

            qry = string.Format(qry_lookupdeletedata, tabledetails.lookupid);
            re = _database.ExecuteSelectQuery(qry);

            qry = "";

            for (int i = 0; i < data.Count; i++)
            {
                qry += string.Format(qry_lookupinsertdata, tabledetails.lookupid, data[i].key, data[i].value) + ";";
            }

            if (qry != "")
            {
                _database.ExecuteInsertQuery(qry);
            }

            UpdateLookupInfoTable(tabledetails);

            return true;
        }

        /// <summary>
        /// Updates the table details of a lookup table in the internal table that tracks all the lookup tables in the system.
        /// </summary>
        /// <param name="tabledetails">The LookupTableDetails object that needs to be updated.</param>
        public void UpdateLookupInfoTable(LookupTableDetails tabledetails)
        {
            string qry = "";
            OdbcDataReader re;

            qry = string.Format(qry_updateLookupInfoTable, tabledetails.lookupdesc, tabledetails.keytext, tabledetails.valuetext, tabledetails.lookupid);
            re = _database.ExecuteSelectQuery(qry);
            _database.ExecuteInsertQuery(qry);

        }

        #region PRIVATE METHODS
        
        /// <summary>
        /// Inserts a lookup table info entry in the lookupinfo table. This is for internal tracking of the lookup tables.
        /// </summary>
        /// <param name="tdesc">Table Description.</param>
        /// <param name="keytext">Key values description text.</param>
        /// <param name="valuetext">Value values description text.</param>
        /// <param name="isCommon">Boolean saying whether the given table is a common table or a project table.</param>
        /// <returns></returns>
        private LookupTableDetails InsertLookupInfoEntry(string tdesc, string keytext, string valuetext, bool isCommon)
        {
            LookupTableDetails result = new LookupTableDetails();
            string iscom;

            if (isCommon)
                iscom = "true";
            else
                iscom = "false";

            string qry = string.Format(qry_insertLookuptableInfo, tdesc, keytext, valuetext, iscom);
            OdbcDataReader re = _database.ExecuteSelectQuery(qry);
            re.Read();
            string tcode = (re[0].ToString());

            string tname = "lookupdata_" + tcode;

            qry = string.Format(qry_updateLookupInfoTableName, tname, tcode);
            re = _database.ExecuteSelectQuery(qry);
            re.Read();

            result.lookupid = re.GetInt32(0);
            result.lookupdesc = re[1].ToString();
            result.keytext = re[2].ToString();
            result.valuetext = re[3].ToString();
            result.tablename = re[4].ToString();
            result.iscommon = re.GetBoolean(5);

            return result;


        }

        /// <summary>
        /// Fetches the lookup table data for a given tablename.
        /// </summary>
        /// <param name="tname">Name of the table for which the data is to be fetched.</param>
        /// <returns></returns>
        private List<LookupDataRow> GetLookupData(int lookupid)
        {
            List<LookupDataRow> result = new List<LookupDataRow>();
            LookupDataRow row;

            OdbcDataReader reader;
            string qry = string.Format(qry_lookupretrievedata, lookupid);

            reader = _database.ExecuteSelectQuery(qry);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    row = new LookupDataRow();
                    row.key = DefaultConversion.GetDoubleValue(reader["key"].ToString());
                    row.value = DefaultConversion.GetDoubleValue(reader["value"].ToString());
                    result.Add(row);
                }
            }

            return result;
        }

        #endregion
    }
}
