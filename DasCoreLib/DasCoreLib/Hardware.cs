﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ivi.Visa.Interop;
using System.Threading;
using BHEL.PUMPSDAS.Datatypes;

namespace DasCoreLib
{

    public class Hardware : HardwareBase
    { 
        protected DeviceManager devmgr;
        public int fk_deviceid;
        protected List<DeviceConfigDetails> devConfDetails;
        protected List<string> supportedMeasTypes;
        protected bool IsHardwareActive = false;
        protected DeviceDetails deviceDetails;
        protected List<MeasurementProfileDetails> mpDetailsList;
    
        public Hardware(int fk_deviceid, DeviceManager devmgr)
        {
            this.fk_deviceid = fk_deviceid;
            this.deviceDetails = devmgr.GetDeviceDetailsById(fk_deviceid);
            this.devmgr = devmgr;
            supportedMeasTypes = devmgr.GetMeasurementTypes();
            devConfDetails = devmgr.GetDeviceConfigDetails(this.deviceDetails);
        }

        // Version 2.0

        // To set the measurement profile only...
        public virtual void ConfigureHardware(List<MeasurementProfileDetails> mpDetailsList)
        {
            this.mpDetailsList = mpDetailsList;
        }

        // To communicate with hardware and configure internal channel settings....
        public virtual void InitializeHardware()
        {
        }

        // Perform scan and return the data back....
        public virtual List<ChannelData> ScanHardware(out List<AlarmStatus> alarms, HardwareScanMode mode)
        {
            alarms = new List<AlarmStatus>();
            return null;
        }

        // Dispose all internal objects...
        public virtual void DisposeHardware()
        {
        }

        // Reset the whole hardware...
        public virtual void ResetHardware()
        {

        }
                
        // Virtual functions to retrieve all the settings from the database....
        protected virtual string GetConfigString(string meastype, string chId)
        {
            return "";
        }

        protected virtual string GetResetString(string meastype, string chId)
        {
            return "";
        }

        protected virtual string GetMeasString(string meastype, string chId)
        {
            return "";
        }

        protected virtual string GetAddonCommand(string meastype, string chId)
        {
            return "";
        }
    }

    public class SIMULATEDHardware : Hardware
    {

        List<MeasurementProfileDetails> manualMeasurements;

        List<MeasurementProfileDetails> hardwareMeasurements;


        public SIMULATEDHardware(int fk_deviceid, DeviceManager devmgr)
            : base(fk_deviceid, devmgr)
        {

        }

        // Configures all internal settings only.
        public override void ConfigureHardware(List<MeasurementProfileDetails> mpDetailsList)
        {

            this.mpDetailsList = mpDetailsList;

            manualMeasurements = new List<MeasurementProfileDetails>();
            hardwareMeasurements = new List<MeasurementProfileDetails>();

            // Seperate manual and hardware channels.
            for (int i = 0; i < mpDetailsList.Count; i++)
            {
                MeasurementProfileDetails mpd = new MeasurementProfileDetails();
                string mtype = mpDetailsList[i].meastype;
                if (mtype == "MANUAL" || mtype == "MANUAL RANDOMISED")
                {
                    mpd = mpDetailsList[i];
                    manualMeasurements.Add(mpd);
                }
                else
                {
                    mpd = mpDetailsList[i];
                    hardwareMeasurements.Add(mpd);
                }
            }

            // Set all internal strings...
            InitializeHardwareSettings();

        }

        // To communicate with hardware and configure internal channel settings....
        public override void InitializeHardware()
        {
            //EstablishCommunication();
        }

        public override List<ChannelData> ScanHardware(out List<AlarmStatus> alarms, HardwareScanMode mode)
        {
            List<ChannelData> result = new List<ChannelData>();

            alarms = new List<AlarmStatus>();

            AlarmStatus alStatus;

            #region MANUAL MEASUREMENTS

            for (int i = 0; i < this.manualMeasurements.Count; i++)
            {
                ChannelData ch = new ChannelData();
                ch.pname = manualMeasurements[i].paramname;
                ch.pindex = manualMeasurements[i].displayindex;
                ch.rawvalue = -1;
                ch.computedValue = manualMeasurements[i].ComputeValue(ch.rawvalue);
                result.Add(ch);
            }

            #endregion
            Thread.Sleep(1000);
            #region HARDWARE MEASUREMENTS

            string[] data = new string[hardwareMeasurements.Count];

            //data = this.MeasureAndReadChannelList(measuresequence);
            for (int i = 0; i < hardwareMeasurements.Count; i++)
            {
                data[i] = (new Random()).NextDouble().ToString();
            }

            for (int i = 0; i < hardwareMeasurements.Count; i++)
            {
                ChannelData ch = new ChannelData();
                ch.pname = hardwareMeasurements[i].paramname;
                ch.pindex = hardwareMeasurements[i].displayindex;

                ch.rawvalue = double.Parse(data[i],
                    System.Globalization.NumberStyles.AllowDecimalPoint |
                    System.Globalization.NumberStyles.AllowExponent |
                    System.Globalization.NumberStyles.AllowLeadingSign);

                ch.computedValue = hardwareMeasurements[i].ComputeValue(ch.rawvalue);

                result.Add(ch);

                if (ch.computedValue > hardwareMeasurements[i].highalarm)
                {
                    alStatus = new AlarmStatus();
                    alStatus.alarmType = AlarmType.HIGH_ALARM;
                    alStatus.pname = ch.pname;
                    alarms.Add(alStatus);
                }
                else if (ch.computedValue < hardwareMeasurements[i].lowalarm)
                {
                    alStatus = new AlarmStatus();
                    alStatus.alarmType = AlarmType.LOW_ALARM;
                    alStatus.pname = ch.pname;
                    alarms.Add(alStatus);
                }

            }

            #endregion

            return result;
        }

        public override void ResetHardware()
        {
        }

        // Dispose all internal objects...
        public override void DisposeHardware()
        {
            // Do stuff here....


            base.DisposeHardware();
            // Release com objects...
        }


        private void InitializeHardwareSettings()
        {
        }

        protected override string GetConfigString(string meastype, string chId)
        {
            string confstr = "";
            for (int i = 0; i < devConfDetails.Count; i++)
            {
                if (devConfDetails[i].meastype == meastype)
                {
                    confstr = devConfDetails[i].configstring;
                }
            }
            return string.Format(confstr, chId);
        }
        protected override string GetResetString(string meastype, string chId)
        {
            string rststr = "";
            for (int i = 0; i < devConfDetails.Count; i++)
            {
                if (devConfDetails[i].meastype == meastype)
                {
                    rststr = devConfDetails[i].resetstring;
                }
            }
            return string.Format(rststr, chId);
        }
        protected override string GetMeasString(string meastype, string chId)
        {
            string measstr = "";
            for (int i = 0; i < devConfDetails.Count; i++)
            {
                if (devConfDetails[i].meastype == meastype)
                {
                    measstr = devConfDetails[i].measstring;
                }
            }
            return string.Format(measstr, chId);
        }
        protected override string GetAddonCommand(string meastype, string chId)
        {
            string addonstr = "";
            for (int i = 0; i < devConfDetails.Count; i++)
            {
                if (devConfDetails[i].meastype == meastype)
                {
                    addonstr = devConfDetails[i].addoncmd;
                }
            }
            return string.Format(addonstr, chId);
        }

    }


}

