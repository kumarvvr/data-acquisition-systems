﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ivi.Visa.Interop;
using BHEL.PUMPSDAS.Datatypes;

namespace DasCoreLib
{
    public class VISAHardware : Hardware
    {


        // Version 1.0

        /*
         *         FormattedIO488 io;
        ResourceManager rm;
        List<string> configStrings, routeStrings, measureStrings, routeStringsBypassed, measureStringsBypassed;
        string configurationCommandString, routeCommandString, routeCommandStringBypassed;
        List<MeasurementProfileDetails> manualMeasurements;
        List<MeasurementProfileDetails> hardwareMeasurements;
        string measuresequence, resetstring;
        private bool isScanRouted = false;
        HardwareScanMode previousScanMode = HardwareScanMode.BYPASS_TIME_MEASUREMENTS;

        public VISAHardware(int fk_deviceid, DeviceManager devmgr)
            : base(fk_deviceid, devmgr)
        {

        }

        // Configures all internal settings only.
        public override void ConfigureHardware(List<MeasurementProfileDetails> mpDetailsList)
        {
            this.mpDetailsList = mpDetailsList;

            manualMeasurements = new List<MeasurementProfileDetails>();
            hardwareMeasurements = new List<MeasurementProfileDetails>();

            configStrings = new List<string>();
            routeStrings = new List<string>();
            measureStrings = new List<string>();
            routeStringsBypassed = new List<string>();
            measureStringsBypassed = new List<string>();

            // Seperate manual and hardware channels.
            for (int i = 0; i < mpDetailsList.Count; i++)
            {
                MeasurementProfileDetails mpd = new MeasurementProfileDetails();
                string mtype = mpDetailsList[i].meastype;
                if (mtype == "MANUAL" || mtype == "MANUAL RANDOMISED")
                {
                    mpd = mpDetailsList[i];
                    mpd.configString = "";
                    mpd.resetString = "";
                    mpd.measString = "";
                    manualMeasurements.Add(mpd);
                }
                else
                {
                    mpd = mpDetailsList[i];
                    mpd.configString = GetConfigString(mpd.meastype, mpd.channelid);
                    mpd.resetString = GetResetString(mpd.meastype, mpd.channelid);
                    mpd.measString = GetMeasString(mpd.meastype, mpd.channelid);

                    ConfigureCommandList(mpd);    
                    hardwareMeasurements.Add(mpd);
                }
            }

            // Set all internal strings...
            InitializeHardwareSettings();

        }

        private void ConfigureCommandList(MeasurementProfileDetails mpd)
        {  
            configStrings.Add(mpd.configString);
            routeStrings.Add(mpd.channelid);
            measureStrings.Add(mpd.measString);

            if (mpd.meastype == "DCV" || mpd.meastype == "DCA" || mpd.meastype == "ACV" || mpd.meastype == "ACA" || mpd.meastype == "DCmV"
                            || mpd.meastype == "DCmA" || mpd.meastype == "ACmV" || mpd.meastype == "ACmA" || mpd.meastype == "RTD"
                            || mpd.meastype == "DIRECT")
            {
                routeStringsBypassed.Add(mpd.channelid);
                measureStringsBypassed.Add(mpd.measString);
            }
        }
        
        // To communicate with hardware and configure internal channel settings....
        public override void InitializeHardware()
        {
            EstablishCommunication();
        }

        public override List<ChannelData> ScanHardware(out List<AlarmStatus> alarms, HardwareScanMode mode)
        {
            List<ChannelData> result = new List<ChannelData>();
            alarms = new List<AlarmStatus>();
            AlarmStatus alStatus;

            if (mode != previousScanMode)
            {
                previousScanMode = mode;
                ResetHardware();
            }

            #region MANUAL MEASUREMENTS

            for (int i = 0; i < this.manualMeasurements.Count; i++)
            {
                ChannelData ch = new ChannelData();
                ch.pname = manualMeasurements[i].paramname;
                ch.pindex = manualMeasurements[i].displayindex;
                ch.rawvalue = -1;
                ch.computedValue = manualMeasurements[i].ComputeValue(ch.rawvalue);
                result.Add(ch);
            }

            #endregion

            #region HARDWARE MEASUREMENTS

            string[] data = { };
            if (isScanRouted)
                data = this.MeasureAndReadChannelList(measuresequence);
            else
            {
                data = new string[measureStrings.Count];
                int l;
                for (int i = 0; i < measureStrings.Count; i++)
                {
                    //Thread.Sleep(5);
                    data[i] = SendCommand(measureStrings[i], true);
                    l = data[i].Length;
                    data[i] = data[i].Remove(l - 1, 1);

                }
            }

            for (int i = 0; i < hardwareMeasurements.Count; i++)
            {
                ChannelData ch = new ChannelData();
                ch.pname = hardwareMeasurements[i].paramname;
                ch.pindex = hardwareMeasurements[i].displayindex;

                ch.rawvalue = double.Parse(data[i],
                    System.Globalization.NumberStyles.AllowDecimalPoint |
                    System.Globalization.NumberStyles.AllowExponent |
                    System.Globalization.NumberStyles.AllowLeadingSign);

                ch.computedValue = hardwareMeasurements[i].ComputeValue(ch.rawvalue);

                result.Add(ch);

                if (ch.computedValue > hardwareMeasurements[i].highalarm)
                {
                    alStatus = new AlarmStatus();
                    alStatus.alarmType = AlarmType.HIGH_ALARM;
                    alStatus.pname = ch.pname;
                    alarms.Add(alStatus);
                }
                else if (ch.computedValue < hardwareMeasurements[i].lowalarm)
                {
                    alStatus = new AlarmStatus();
                    alStatus.alarmType = AlarmType.LOW_ALARM;
                    alStatus.pname = ch.pname;
                    alarms.Add(alStatus);
                }

            }

            #endregion

            // One by one channel scanning...
            return result;
        }

        public override void ResetHardware()
        {
            // Send command to reset the VISA hardware.
            if (resetstring != "" && resetstring != null)
                SendCommand(resetstring, false);
            if (isScanRouted)
            {                
                if (configurationCommandString != "" && configurationCommandString != null)
                    SendCommand(configurationCommandString, false);
                PrepareRouting();
            }
        }

        // Dispose all internal objects...
        public override void DisposeHardware()
        {
            // Do stuff here....


            base.DisposeHardware();
            // Release com objects...
        }
        
        #region Private Functions

        private string[] MeasureAndReadChannelList(string cmd)
        {
            string[] data = { };
            string result;
            result = SendCommand(cmd, true);
            data = result.Split(',');
            int l = data.Length;
            data[l - 1] = data[l - 1].Remove(data[l - 1].Length - 1, 1);
            return data;
        }
        private void EstablishCommunication()
        {
            try
            {
                rm = new ResourceManagerClass();
                io = new FormattedIO488Class();
                io.IO = (IMessage)rm.Open(deviceDetails.connstring, AccessMode.NO_LOCK, 7000);
            }
            catch (Exception e)
            {
                throw new Exception("ERROR : " + e.Message);
            }

            // Set the hardware too...

            // Resets and Configures Hardware
            ResetHardware();
        }
        private void InitializeHardwareSettings()
        {

            int id = this.fk_deviceid;

            string cs = "";
            string rsAllChannels = "ROUT:SCAN:ORDERED OFF;:ROUT:SCAN (@";
            string rsbypassedChannels = "ROUT:SCAN:ORDERED OFF;:ROUT:SCAN (@";

            // Generate a single command string for configuring channels..
            for (int i = 0; i < this.configStrings.Count; i++)
                cs += ":" + configStrings[i] + ";";

            // Generate a single command for the routing...

            //For all channels...
            for (int i = 0; i < routeStrings.Count; i++)
                rsAllChannels += routeStrings[i] + ",";

            rsAllChannels = rsAllChannels.Remove(rsAllChannels.Length - 1, 1);
            rsAllChannels += ");";

            //For channels which are not bypassed..
            for (int i = 0; i < routeStringsBypassed.Count; i++)
                rsbypassedChannels += routeStrings[i] + ",";

            rsbypassedChannels = rsbypassedChannels.Remove(rsbypassedChannels.Length - 1, 1);
            rsbypassedChannels += ");";

            configurationCommandString = cs;
            routeCommandString = rsAllChannels;
            routeCommandStringBypassed = rsbypassedChannels;

            measuresequence = "INIT;:FETC?;";
            resetstring = "*RST;*CLS;";
        }
        private string SendCommand(string cmd, bool fetchResult)
        {
            io.WriteString(cmd);
            if (fetchResult)
                return io.ReadString();

            return "";
        }
        private string ReadDataBuffer()
        {
            return io.ReadString();
        }
        private void PrepareRouting()
        {
            if (previousScanMode == HardwareScanMode.NORMAL)
            {
                if (routeCommandString != "" && routeCommandString != null)
                    SendCommand(routeCommandString, false);
            }
            else
            {
                if (routeCommandStringBypassed != "" && routeCommandStringBypassed != null)
                    SendCommand(routeCommandStringBypassed, false);
            }
        }

        #endregion

        protected override string GetConfigString(string meastype, string chId)
        {
            string confstr = "";
            for (int i = 0; i < devConfDetails.Count; i++)
            {
                if (devConfDetails[i].meastype == meastype)
                {
                    confstr = devConfDetails[i].configstring;
                }
            }
            return string.Format(confstr, chId);
        }
        protected override string GetResetString(string meastype, string chId)
        {
            string rststr = "";
            for (int i = 0; i < devConfDetails.Count; i++)
            {
                if (devConfDetails[i].meastype == meastype)
                {
                    rststr = devConfDetails[i].resetstring;
                }
            }
            return string.Format(rststr, chId);
        }
        protected override string GetMeasString(string meastype, string chId)
        {
            string measstr = "";
            for (int i = 0; i < devConfDetails.Count; i++)
            {
                if (devConfDetails[i].meastype == meastype)
                {
                    measstr = devConfDetails[i].measstring;
                }
            }
            return string.Format(measstr, chId);
        }
        protected override string GetAddonCommand(string meastype, string chId)
        {
            string addonstr = "";
            for (int i = 0; i < devConfDetails.Count; i++)
            {
                if (devConfDetails[i].meastype == meastype)
                {
                    addonstr = devConfDetails[i].addoncmd;
                }
            }
            return string.Format(addonstr, chId);
        }
         * 
         *          
         */

        // Version 2.0 - Without Routing...
        FormattedIO488 io;
        ResourceManager rm;
        List<MeasurementProfileDetails> manualMeasurements,hardwareMeasurementsNormal, hardwareMeasurementsBypassed;
        string resetstring;
        HardwareScanMode previousMode = HardwareScanMode.BYPASS_TIME_MEASUREMENTS;

        public VISAHardware(int fk_deviceid, DeviceManager devmgr)
            : base(fk_deviceid, devmgr)
        {

        }

        // Configures all internal settings only.
        public override void ConfigureHardware(List<MeasurementProfileDetails> mpDetailsList)
        {
            this.mpDetailsList = mpDetailsList;

            manualMeasurements = new List<MeasurementProfileDetails>();
            hardwareMeasurementsNormal = new List<MeasurementProfileDetails>();
            hardwareMeasurementsBypassed = new List<MeasurementProfileDetails>();

            // Seperate manual and hardware channels.
            for (int i = 0; i < mpDetailsList.Count; i++)
            {
                MeasurementProfileDetails mpd = new MeasurementProfileDetails();
                string mtype = mpDetailsList[i].meastype;
                if (mtype == "MANUAL" || mtype == "MANUAL RANDOMISED")
                {
                    mpd = mpDetailsList[i];
                    mpd.configString = "";
                    mpd.resetString = "";
                    mpd.measString = "";
                    manualMeasurements.Add(mpd);
                }
                else
                {
                    mpd = mpDetailsList[i];
                    if (mpd.meastype == "DCV" || mpd.meastype == "DCA" || mpd.meastype == "ACV" || mpd.meastype == "ACA" || mpd.meastype == "DCmV"
                                    || mpd.meastype == "DCmA" || mpd.meastype == "ACmV" || mpd.meastype == "ACmA" || mpd.meastype == "RTD"
                                    || mpd.meastype == "DIRECT")
                    {
                        mpd.measString = GetMeasString(mpd.meastype, mpd.channelid);
                        hardwareMeasurementsNormal.Add(mpd);
                    }
                    else
                    {
                        mpd.measString = GetMeasString(mpd.meastype, mpd.channelid);
                        this.hardwareMeasurementsBypassed.Add(mpd);
                    }

                }
            }

            // Set all internal strings...
            InitializeHardwareSettings();

        }
        
        // To communicate with hardware and configure internal channel settings....
        public override void InitializeHardware()
        {
            EstablishCommunication();
        }

        public override List<ChannelData> ScanHardware(out List<AlarmStatus> alarms, HardwareScanMode mode)
        {
            List<ChannelData> result = new List<ChannelData>();
            List<AlarmStatus> alarms1, alarms2;

            if (previousMode != mode)
            {
                ResetHardware();

                previousMode = mode;
            }
            alarms = new List<AlarmStatus>();

            result.AddRange(GenerateManualChannelData());
            result.AddRange(ScanNormal(out alarms1));
            alarms.AddRange(alarms1);

            if (mode == HardwareScanMode.BYPASS_TIME_MEASUREMENTS)
            {
                // Only scan normal channels...
                result.AddRange(GenerateBypassedChannelData());
            }
            else
            {
                result.AddRange(ScanBypassedChannels(out alarms2));
                alarms.AddRange(alarms2);
            }

            return result;
        }
        private List<ChannelData> ScanNormal(out List<AlarmStatus> alarms)
        {

            List<ChannelData> result = new List<ChannelData>();
            alarms = new List<AlarmStatus>();
            AlarmStatus alStatus;

            string[] data = { };

            int l;
            data = new string[hardwareMeasurementsNormal.Count];

            for (int i = 0; i < this.hardwareMeasurementsNormal.Count; i++)
            {
                //Thread.Sleep(5);
                string r;
                r = SendCommand(hardwareMeasurementsNormal[i].measString, true);
                data[i] = r;
                l = data[i].Length;
                data[i] = data[i].Remove(l - 2, 2).TrimStart();
            }

            for (int i = 0; i < hardwareMeasurementsNormal.Count; i++)
            {
                ChannelData ch = new ChannelData();
                ch.pname = hardwareMeasurementsNormal[i].paramname;
                ch.pindex = hardwareMeasurementsNormal[i].displayindex;

                ch.rawvalue = double.Parse(data[i],
                    System.Globalization.NumberStyles.AllowDecimalPoint |
                    System.Globalization.NumberStyles.AllowExponent |
                    System.Globalization.NumberStyles.AllowLeadingSign);

                ch.computedValue = hardwareMeasurementsNormal[i].ComputeValue(ch.rawvalue);

                result.Add(ch);

                if (ch.computedValue > hardwareMeasurementsNormal[i].highalarm)
                {
                    alStatus = new AlarmStatus();
                    alStatus.alarmType = AlarmType.HIGH_ALARM;
                    alStatus.pname = ch.pname;
                    alarms.Add(alStatus);
                }
                else if (ch.computedValue < hardwareMeasurementsNormal[i].lowalarm)
                {
                    alStatus = new AlarmStatus();
                    alStatus.alarmType = AlarmType.LOW_ALARM;
                    alStatus.pname = ch.pname;
                    alarms.Add(alStatus);
                }

            }
            return result;
        }
        private List<ChannelData> ScanBypassedChannels(out List<AlarmStatus> alarms)
        {

            List<ChannelData> result = new List<ChannelData>();
            alarms = new List<AlarmStatus>();
            AlarmStatus alStatus;

            string[] data = { };

            int l;
            data = new string[this.hardwareMeasurementsBypassed.Count];

            for (int i = 0; i < this.hardwareMeasurementsBypassed.Count; i++)
            {
                //Thread.Sleep(5);
                string r;
                r = SendCommand(hardwareMeasurementsBypassed[i].measString, true);
                data[i] = r;
                l = data[i].Length;
                data[i] = data[i].Remove(l - 2, 2).TrimStart();
            }

            for (int i = 0; i < hardwareMeasurementsBypassed.Count; i++)
            {
                ChannelData ch = new ChannelData();
                ch.pname = hardwareMeasurementsBypassed[i].paramname;
                ch.pindex = hardwareMeasurementsBypassed[i].displayindex;

                ch.rawvalue = double.Parse(data[i],
                    System.Globalization.NumberStyles.AllowDecimalPoint |
                    System.Globalization.NumberStyles.AllowExponent |
                    System.Globalization.NumberStyles.AllowLeadingSign);

                ch.computedValue = hardwareMeasurementsBypassed[i].ComputeValue(ch.rawvalue);

                result.Add(ch);

                if (ch.computedValue > hardwareMeasurementsBypassed[i].highalarm)
                {
                    alStatus = new AlarmStatus();
                    alStatus.alarmType = AlarmType.HIGH_ALARM;
                    alStatus.pname = ch.pname;
                    alarms.Add(alStatus);
                }
                else if (ch.computedValue < hardwareMeasurementsBypassed[i].lowalarm)
                {
                    alStatus = new AlarmStatus();
                    alStatus.alarmType = AlarmType.LOW_ALARM;
                    alStatus.pname = ch.pname;
                    alarms.Add(alStatus);
                }

            }
            return result;
        }
        private List<ChannelData> GenerateBypassedChannelData()
        {
            List<ChannelData> result = new List<ChannelData>();

            for (int i = 0; i < this.hardwareMeasurementsBypassed.Count; i++)
            {
                ChannelData ch = new ChannelData();
                ch.pname = hardwareMeasurementsBypassed[i].paramname;
                ch.pindex = hardwareMeasurementsBypassed[i].displayindex;

                // equivalent to manual channels...
                ch.rawvalue = -1;
                ch.computedValue = hardwareMeasurementsBypassed[i].outputmax;
                result.Add(ch);
            }
            return result;
        }
        private List<ChannelData> GenerateManualChannelData()
        {
            List<ChannelData> result = new List<ChannelData>();
            for (int i = 0; i < this.manualMeasurements.Count; i++)
            {
                ChannelData ch = new ChannelData();
                ch.pname = manualMeasurements[i].paramname;
                ch.pindex = manualMeasurements[i].displayindex;
                ch.rawvalue = -1;
                ch.computedValue = manualMeasurements[i].ComputeValue(ch.rawvalue);
                result.Add(ch);
            }

            return result;
        }
        public override void ResetHardware()
        {
            // Send command to reset the VISA hardware.
            if (resetstring != "" && resetstring != null)
                SendCommand(resetstring, false);
        }

        // Dispose all internal objects...
        public override void DisposeHardware()
        {
            // Do stuff here....


            base.DisposeHardware();
            // Release com objects...
        }
        
        #region Private Functions

        private void EstablishCommunication()
        {
            try
            {
                rm = new ResourceManagerClass();
                io = new FormattedIO488Class();
                io.IO = (IMessage)rm.Open(deviceDetails.connstring, AccessMode.NO_LOCK, 7000);
            }
            catch (Exception e)
            {
                throw new Exception("ERROR : " + e.Message);
            }

            // Set the hardware too...

            // Resets and Configures Hardware
            ResetHardware();
        }
        private void InitializeHardwareSettings()
        {
            resetstring = "*RST;*CLS;";
        }
        private string SendCommand(string cmd, bool fetchResult)
        {
            io.WriteString(cmd);
            if (fetchResult)
                return io.ReadString();

            return "";
        }
        private string ReadDataBuffer()
        {
            return io.ReadString();
        }

        #endregion

        protected override string GetConfigString(string meastype, string chId)
        {
            string confstr = "";
            for (int i = 0; i < devConfDetails.Count; i++)
            {
                if (devConfDetails[i].meastype == meastype)
                {
                    confstr = devConfDetails[i].configstring;
                }
            }
            return string.Format(confstr, chId);
        }
        protected override string GetResetString(string meastype, string chId)
        {
            string rststr = "";
            for (int i = 0; i < devConfDetails.Count; i++)
            {
                if (devConfDetails[i].meastype == meastype)
                {
                    rststr = devConfDetails[i].resetstring;
                }
            }
            return string.Format(rststr, chId);
        }
        protected override string GetMeasString(string meastype, string chId)
        {
            string measstr = "";
            for (int i = 0; i < devConfDetails.Count; i++)
            {
                if (devConfDetails[i].meastype == meastype)
                {
                    measstr = devConfDetails[i].measstring;
                }
            }
            return string.Format(measstr, chId);
        }
        protected override string GetAddonCommand(string meastype, string chId)
        {
            string addonstr = "";
            for (int i = 0; i < devConfDetails.Count; i++)
            {
                if (devConfDetails[i].meastype == meastype)
                {
                    addonstr = devConfDetails[i].addoncmd;
                }
            }
            return string.Format(addonstr, chId);
        }

    }
}
