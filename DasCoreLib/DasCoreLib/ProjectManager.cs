﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Odbc;
using DasCoreUtilities;
using FileManager;
using BHEL.PUMPSDAS.Datatypes;

namespace DasCoreLib
{
    public class ProjectManager
    {
        DatabaseManager _database;
        InstalledMachineManager pmImm;
        
        #region Queries

        //Queries related to project list

        const string qry_insertNewProjectDetails =
            "insert into projectdetails (fk_machinecode,projectname,projectdescription) values({0},'{1}','{2}') returning (projectcode)";

        const string qry_updateProjectDetails =
            "update projectdetails set projectname='{0}',projectdescription='{1}' where fk_machinecode={2} and projectcode={3}";

        const string qry_retrieveAllProjectDetails =
            "select fk_machinecode,projectcode,projectname,projectdescription from projectdetails";

        const string qry_retrieveProjectDetailsByMachinecode =
            "select projectcode,projectname,projectdescription from projectdetails where fk_machinecode = {0} order by projectcode";

        const string qry_retrieveProjectDetailsByProjectcode =
            "select projectname,projectdescription from projectdetails where fk_machinecode = {0} and projectcode={1}";

        const string qry_deleteExistingProjectSpecifiedParameters =
            "delete from projectspecifiedparameters where fk_machinecode={0} and fk_projectcode={1}";

        const string qry_insertParamDetailsToProjectSpecifiedParameters =
            "insert into projectspecifiedparameters (" +
            "fk_machinecode" + "," +
            "fk_projectcode" + "," +
            "paramname" + "," +
            "paramtype" + "," +
            "paramdescription" + "," +
            "paramunits" + "," +
            "projectspecificdata" + "," +
            "displayindex" + ")" +
            " values(" +
            "{0}" + "," +
            "{1}" + "," +
            "'{2}'" + "," +
            "'{3}'" + "," +
            "'{4}'" + "," +
            "'{5}'" + "," +
            "'{6}'" + "," +
            "{7}" + ")";

        const string qry_GetProjectSpecifiedParameters =
            "select fk_machinecode,fk_projectcode,paramname,paramtype,paramdescription,paramunits,projectspecificdata,displayindex " +
            " from projectspecifiedparameters where fk_machinecode={0} and fk_projectcode={1}";

        const string qry_GetProjectMachine =
            "select machinenumber,machinedescription from projectmachinedetails where fk_machinecode={0} and fk_projectcode={1}";

        const string qry_CreateProjectMachine =
            "insert into projectmachinedetails (fk_machinecode,fk_projectcode,machinenumber,machinedescription) values({0},{1},'{2}','{3}') returning fk_machinecode,fk_projectcode,machinenumber,machinedescription";

        const string qry_UpdateProjectMachine =            
            "update projectmachinedetails set machinenumber='{0}',machinedescription='{1}' where fk_machinecode={2} and fk_projectcode={3}";

        const string qry_DeleteProjectMachine =
            "delete from projectmachinedetails where fk_machinecode={0} and fk_projectcode={1} and machinenumber='{2}'";
        
        const string qry_GetMachineTestDetails =
            "select testreference,testdate,testremarks from machinetestdetails where fk_machinecode={0} and fk_projectcode={1} and fk_machinenumber='{2}'";

        const string qry_CreateMachineTest =
            "insert into machinetestdetails (fk_machinecode,fk_projectcode,fk_machinenumber,testreference,testdate,testremarks) values({0},{1},'{2}','{3}','{4}','{5}') returning fk_machinecode,fk_projectcode,fk_machinenumber,testreference,testdate,testremarks";

        const string qry_UpdateMachineTest =
            "update machinetestdetails set testremarks='{0}',testdate='{1}' where fk_machinecode={2} and fk_projectcode={3} and fk_machinenumber='{4}' and testreference='{5}'";

        const string qry_DeleteMachineTest =
            "delete from machinetestdetails where fk_machinecode={0} and fk_projectcode={1} and fk_machinenumber='{2}' and testreference='{3}'";

        #endregion

        // Database connection is assumed to be opened.
        public ProjectManager(DatabaseManager db)
        {
            if (db == null)
                throw new ArgumentNullException("db", "db is null.");
            this._database = db;
            pmImm = new InstalledMachineManager(this._database);            
        }

        #region Functions related to project management

        #region Project

        public ProjectDetails CreateNewProject(InstalledMachine mc,ProjectDetails details)
        {
            
            OdbcDataReader reader;
            string qry = string.Format(qry_insertNewProjectDetails,details.fk_machinecode.ToString(),details.projectname,details.projectdescription);

            reader = _database.ExecuteSelectQuery(qry);

            if (reader.HasRows)
            {
                reader.Read();

                details.projectcode = DefaultConversion.GetIntValue(reader["projectcode"].ToString());
            }
            // Delete any existing project specific data.
            string deleteqry = string.Format(qry_deleteExistingProjectSpecifiedParameters, details.fk_machinecode, details.projectcode);

            _database.ExecuteSystemQuery(deleteqry);

            // Note apart from creating a new project, project specified parameters too should be added.
            // Retrieve the specified parameters list from default tables and load them into the new project specified params.

            List<SpecifiedParamsDetails> sps = pmImm.GetSpecifiedParametersDetails(mc);

            string querylist = "";

            for (int i = 0; i < sps.Count; i++)
            {
                string ql = string.Format(qry_insertParamDetailsToProjectSpecifiedParameters, mc.machinecode, details.projectcode,
                    sps[i].paramname,
                    sps[i].paramtype,
                    sps[i].paramdescription,
                    sps[i].paramunits,
                    sps[i].defaultdata,
                    sps[i].displayindex);

                querylist += ql + ";";
            }

            _database.ExecuteInsertQuery(querylist);

            // Once the project is created, also create the folder in the disc.  

            return details;
        }

        public bool UpdateExistingProject(ProjectDetails details)
        {
            OdbcDataReader reader;
            string qry = string.Format(qry_updateProjectDetails, details.projectname, details.projectdescription,details.fk_machinecode, details.projectcode);

            reader = _database.ExecuteSelectQuery(qry);

            return true;
        }

        public List<ProjectDetails> GetAllProjectDetails()
        {
            OdbcDataReader reader;
            List<ProjectDetails> projectlist = new List<ProjectDetails>();
            ProjectDetails details;
            string qry = string.Format(qry_retrieveAllProjectDetails);
            reader = _database.ExecuteSelectQuery(qry);
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    details = new ProjectDetails();
                    details.fk_machinecode = int.Parse(reader["fk_machinecode"].ToString());
                    details.projectcode = int.Parse(reader["projectcode"].ToString());
                    details.projectname = reader["projectname"].ToString();
                    details.projectdescription = reader["projectdescription"].ToString();

                    projectlist.Add(details);
                }

            }

            return projectlist;
        }

        public List<ProjectDetails> GetProjectList(InstalledMachine machine)
        {
            OdbcDataReader reader;
            List<ProjectDetails> projectlist = new List<ProjectDetails>();
            ProjectDetails details;
            string qry = string.Format(qry_retrieveProjectDetailsByMachinecode, machine.machinecode);
            reader = _database.ExecuteSelectQuery(qry);
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    details = new ProjectDetails();
                    details.fk_machinecode = machine.machinecode;
                    details.projectcode = int.Parse(reader["projectcode"].ToString());
                    details.projectname = reader["projectname"].ToString();
                    details.projectdescription = reader["projectdescription"].ToString();

                    projectlist.Add(details);
                }

            }

            return projectlist;
        }

        public ProjectDetails GetProjectDetails(InstalledMachine machine, int projectCode)
        {
            OdbcDataReader reader;
            ProjectDetails details = new ProjectDetails();
            details.projectcode = -1;
            string qry = string.Format(qry_retrieveProjectDetailsByProjectcode, machine.machinecode, projectCode);
            reader = _database.ExecuteSelectQuery(qry);
            if (reader.HasRows)
            {
                reader.Read();
                details.fk_machinecode = machine.machinecode;
                details.projectcode = projectCode;
                details.projectname = reader["projectname"].ToString();
                details.projectdescription = reader["projectdescription"].ToString();
            }

            return details;
        }        


        #endregion

        #region Project Specified parameters

        public List<SpecifiedParamsDetails> GetProjectSpecifiedParameters(InstalledMachine machine, ProjectDetails project)
        {
            List<SpecifiedParamsDetails> result = new List<SpecifiedParamsDetails>(0);
            SpecifiedParamsDetails pdetail = new SpecifiedParamsDetails();
            OdbcDataReader reader;

            string qry = string.Format(qry_GetProjectSpecifiedParameters, machine.machinecode, project.projectcode);

            reader = _database.ExecuteSelectQuery(qry);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    pdetail = new SpecifiedParamsDetails();
                    pdetail.fk_machinecode = DefaultConversion.GetIntValue(reader["fk_machinecode"].ToString());
                    pdetail.paramname = DefaultConversion.GetStringValue(reader["paramname"].ToString());
                    pdetail.paramtype = DefaultConversion.GetStringValue(reader["paramtype"].ToString());
                    pdetail.paramdescription = DefaultConversion.GetStringValue(reader["paramdescription"].ToString());
                    pdetail.paramunits = DefaultConversion.GetStringValue(reader["paramunits"].ToString());
                    pdetail.defaultdata = DefaultConversion.GetStringValue(reader["projectspecificdata"].ToString());
                    pdetail.displayindex = DefaultConversion.GetIntValue(reader["displayindex"].ToString());

                    result.Add(pdetail);
                }
            }


            return result;
        }

        public bool UpdateProjectSpecifiedParameters(InstalledMachine machine, ProjectDetails project, List<SpecifiedParamsDetails> updates)
        {
            // Delete project specific parameters.....
            string deleteqry = string.Format(qry_deleteExistingProjectSpecifiedParameters, machine.machinecode, project.projectcode);

            _database.ExecuteSystemQuery(deleteqry);

            string querylist = "";

            for (int i = 0; i < updates.Count; i++)
            {
                string ql = string.Format(qry_insertParamDetailsToProjectSpecifiedParameters, machine.machinecode, project.projectcode,
                    updates[i].paramname,
                    updates[i].paramtype,
                    updates[i].paramdescription,
                    updates[i].paramunits,
                    updates[i].defaultdata,
                    updates[i].displayindex);

                querylist += ql + ";";
            }

            _database.ExecuteInsertQuery(querylist);

            return true;
        }

        #endregion

        #region Project machine

        public List<ProjectMachineDetails> GetProjectMachines(ProjectDetails project)
        {
            List<ProjectMachineDetails> result = new List<ProjectMachineDetails>();
            ProjectMachineDetails pmd;

            OdbcDataReader reader;

            string qry = string.Format(qry_GetProjectMachine, project.fk_machinecode, project.projectcode);

            reader = _database.ExecuteSelectQuery(qry);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    pmd = new ProjectMachineDetails();
                    pmd.fk_machinecode = project.fk_machinecode;
                    pmd.fk_projectcode = project.projectcode;
                    pmd.machinedescription = reader["machinedescription"].ToString();;
                    pmd.machinenumber = reader["machinenumber"].ToString();
                    result.Add(pmd);
                }
            }

            return result;
        }

        public List<MachineTestDetails> GetMachineTests(ProjectMachineDetails pmd)
        {
            List<MachineTestDetails> result = new List<MachineTestDetails>();
            MachineTestDetails mtd;

            OdbcDataReader reader;

            string qry = string.Format(qry_GetMachineTestDetails, pmd.fk_machinecode, pmd.fk_projectcode, pmd.machinenumber);

            reader = _database.ExecuteSelectQuery(qry);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    mtd = new MachineTestDetails();
                    mtd.fk_machinecode = pmd.fk_machinecode;
                    mtd.fk_projectcode = pmd.fk_projectcode;
                    mtd.fk_machinenumber = pmd.machinenumber;
                    mtd.testreference = reader["testreference"].ToString();
                    mtd.testdate = reader["testdate"].ToString();
                    mtd.testremarks = reader["testremarks"].ToString();
                    result.Add(mtd);
                }
            }

            return result;
        }

        public ProjectMachineDetails CreateProjectMachine(ProjectDetails project, string mNum, string mDesc)
        {            
            ProjectMachineDetails result = new ProjectMachineDetails();

            OdbcDataReader reader;

            string qry = string.Format(qry_CreateProjectMachine, project.fk_machinecode, project.projectcode, mNum, mDesc);

            reader = _database.ExecuteSelectQuery(qry);

            if (reader.HasRows)
            {
                reader.Read();
                result.fk_machinecode = int.Parse(reader["fk_machinecode"].ToString());
                result.fk_projectcode = int.Parse(reader["fk_projectcode"].ToString());
                result.machinenumber = reader["machinenumber"].ToString();
                result.machinedescription = reader["machinedescription"].ToString();
            }

            return result;
        }

        public bool UpdateProjectMachine(ProjectMachineDetails pmd)
        {
            string qry = string.Format(qry_UpdateProjectMachine, pmd.machinenumber, pmd.machinedescription, pmd.fk_machinecode, pmd.fk_projectcode);

            _database.ExecuteUpdateQuery(qry);

            return true;
        }

        public bool DeleteProjectMachine(ProjectMachineDetails pmd)
        {
            string qry = string.Format(qry_DeleteProjectMachine, pmd.fk_machinecode, pmd.fk_projectcode, pmd.machinenumber);

            _database.ExecuteSystemQuery(qry);

            return true;
        }
              

        #endregion

        #region Machine test

        public MachineTestDetails CreateMachineTest(ProjectMachineDetails pmd, string tRef, string tDate, string tRemarks)
        {
            MachineTestDetails result = new MachineTestDetails();

            OdbcDataReader reader;

            string qry = string.Format(qry_CreateMachineTest, pmd.fk_machinecode, pmd.fk_projectcode, pmd.machinenumber, tRef, tDate, tRemarks);

            reader = _database.ExecuteSelectQuery(qry);

            if (reader.HasRows)
            {
                reader.Read();
                result.fk_machinecode = int.Parse(reader["fk_machinecode"].ToString());
                result.fk_projectcode = int.Parse(reader["fk_projectcode"].ToString());
                result.fk_machinenumber = reader["fk_machinenumber"].ToString();
                result.testreference = reader["testreference"].ToString();
                result.testdate = reader["testdate"].ToString();
                result.testremarks = reader["testremarks"].ToString();
            }

            return result;
        }

        public bool UpdateMachineTest(MachineTestDetails test)
        {
            string qry = string.Format(qry_UpdateMachineTest, test.testremarks, test.testdate, test.fk_machinecode, test.fk_projectcode,test.fk_machinenumber,test.testreference);

            _database.ExecuteUpdateQuery(qry);

            return true;
        }

        public bool DeleteMachineTest(MachineTestDetails test)
        {
            string qry = string.Format(qry_DeleteMachineTest, test.fk_machinecode, test.fk_projectcode, test.fk_machinenumber, test.testreference);

            _database.ExecuteSystemQuery(qry);

            return true;
        }

        #endregion

        #endregion

        #region DEPRECATED

        /*public ProjectDetails GetProjectDetails(InstalledMachine machine, ProjectDetails project)
        {
            OdbcDataReader reader;
            ProjectDetails details = new ProjectDetails();
            string qry = string.Format(qry_retrieveProjectDetailsByProjectcode, machine.machinecode, project.projectcode);
            reader = _database.ExecuteSelectQuery(qry);
            if (reader.HasRows)
            {
                reader.Read();  
                details = new ProjectDetails();
                details.fk_machinecode = machine.machinecode;
                details.projectcode = project.projectcode;
                details.projectname = reader["projectname"].ToString();
                details.projectdescription = reader["projectdescription"].ToString();
                
            }

            return details;
        }*/

        #endregion

    }
}
