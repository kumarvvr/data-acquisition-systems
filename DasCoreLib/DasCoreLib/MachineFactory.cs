﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DasCoreLib;
using BHEL.PUMPSDAS.Datatypes;
using System.Collections;

using System.Reflection;

namespace DasCoreLib
{
    // Creates template objects or actual "for test" objects.
    public class MachineFactory
    {
        //Requires access to database too.
        DatabaseManager _database;

        Type mdtype, mptype, rptype, sptype, tsptype, mtype;

        Assembly asb;

        InstalledMachine handledMachine;
        InstalledMachineManager imm;
        LookupTableManager ltm;
        ProjectManager pm;

        public MachineFactory(DatabaseManager _db, InstalledMachineManager imm, LookupTableManager ltm, ProjectManager pm)
        {
            this._database = _db;
            this.imm = imm;
            this.ltm = ltm;
            this.pm = pm;
        }

        public void CreateTemplateObjects(
            InstalledMachine machine, // Unique frame number in case of pumps
            out MachineDescriptor mcd, // Machine descriptor that houses the remaining necessary objects.
            out MeasurementProfileBase mp, // Measurement Profile object.
            out ResultParamsBase rp,
            out SpecifiedParamsBase sp,
            out TestSetupParamsBase tsp,
            out MachineDetailsBase md)
        {

            this.handledMachine = machine;

            string dllpath =  handledMachine.dllrootpath+"\\"+handledMachine.dllfilename;

            asb = Assembly.LoadFile(dllpath);

            mdtype = asb.GetType("userscript." + machine.machineuniquecode + ".MachineDetails");
            md = (MachineDetailsBase)Activator.CreateInstance(mdtype);

            mptype = asb.GetType("userscript." + machine.machineuniquecode + ".MeasurementProfile");
            mp = (MeasurementProfileBase)Activator.CreateInstance(mptype);

            sptype = asb.GetType("userscript." + machine.machineuniquecode + ".SpecifiedParameters");
            sp = (SpecifiedParamsBase)Activator.CreateInstance(sptype);

            rptype = asb.GetType("userscript." + machine.machineuniquecode + ".ResultParameters");
            rp = (ResultParamsBase)Activator.CreateInstance(rptype);

            tsptype = asb.GetType("userscript." + machine.machineuniquecode + ".TestSetupParameters");
            tsp = (TestSetupParamsBase)Activator.CreateInstance(tsptype);

            mtype = asb.GetType("userscript." + machine.machineuniquecode + ".Machine");
            mcd = (MachineDescriptor)Activator.CreateInstance(mtype, new object[] { md, mp, rp, sp, tsp });
        }

        public void CreateDataObjects(
            InstalledMachine machine,
            ProjectDetails project,
            ProjectMachineDetails projectmachine,
            MachineTestDetails test,
            out MachineDetailsBase testing_md,
            out SpecifiedParamsBase testing_sp,
            out TestSetupParamsBase testing_tsp,
            out MeasurementProfileBase testing_mp_manip,
            out MeasurementProfileBase testing_mp_manip_avg,
            out MeasurementProfileBase testing_mp_actual,
            out MeasurementProfileBase testing_mp_actual_avg,
            out MeasurementProfileBase testing_mp_metadata,
            out ResultParamsBase testing_rp_manip,
            out ResultParamsBase testing_rp_manip_avg,
            out ResultParamsBase testing_rp_actual,
            out ResultParamsBase testing_rp_actual_avg,
            out MachineDescriptor testing_mcd_manip,
            out MachineDescriptor testing_mcd_manip_avg,
            out MachineDescriptor testing_mcd_actual,
            out MachineDescriptor testing_mcd_actual_avg)
        {

            #region Assembly Loading
            this.handledMachine = machine;

            string dllpath = handledMachine.dllrootpath + "\\" + handledMachine.dllfilename;

            asb = Assembly.LoadFile(dllpath);
            #endregion

            #region Creating Object Templates
            // TODO move them to DasCorSettings.....
            mdtype = asb.GetType("userscript." + machine.machineuniquecode + ".MachineDetails");
            testing_md = (MachineDetailsBase)Activator.CreateInstance(mdtype);

            mptype = asb.GetType("userscript." + machine.machineuniquecode + ".MeasurementProfile");
            testing_mp_manip = (MeasurementProfileBase)Activator.CreateInstance(mptype);
            testing_mp_manip_avg = (MeasurementProfileBase)Activator.CreateInstance(mptype);
            testing_mp_actual = (MeasurementProfileBase)Activator.CreateInstance(mptype);
            testing_mp_actual_avg = (MeasurementProfileBase)Activator.CreateInstance(mptype);
            testing_mp_metadata = (MeasurementProfileBase)Activator.CreateInstance(mptype);

            sptype = asb.GetType("userscript." + machine.machineuniquecode + ".SpecifiedParameters");
            testing_sp = (SpecifiedParamsBase)Activator.CreateInstance(sptype);

            rptype = asb.GetType("userscript." + machine.machineuniquecode + ".ResultParameters");
            testing_rp_manip = (ResultParamsBase)Activator.CreateInstance(rptype);
            testing_rp_manip_avg = (ResultParamsBase)Activator.CreateInstance(rptype);
            testing_rp_actual = (ResultParamsBase)Activator.CreateInstance(rptype);
            testing_rp_actual_avg = (ResultParamsBase)Activator.CreateInstance(rptype);
            
            tsptype = asb.GetType("userscript." + machine.machineuniquecode + ".TestSetupParameters");
            testing_tsp = (TestSetupParamsBase)Activator.CreateInstance(tsptype);

            mtype = asb.GetType("userscript." + machine.machineuniquecode + ".Machine");
            testing_mcd_manip = (MachineDescriptor)Activator.CreateInstance(mtype, new object[] { testing_md, testing_mp_manip, testing_rp_manip, testing_sp, testing_tsp });
            testing_mcd_manip_avg = (MachineDescriptor)Activator.CreateInstance(mtype, new object[] { testing_md, testing_mp_manip_avg, testing_rp_manip_avg, testing_sp, testing_tsp });
            testing_mcd_actual = (MachineDescriptor)Activator.CreateInstance(mtype, new object[] { testing_md, testing_mp_actual, testing_rp_actual, testing_sp, testing_tsp });
            testing_mcd_actual_avg = (MachineDescriptor)Activator.CreateInstance(mtype, new object[] { testing_md, testing_mp_actual_avg, testing_rp_actual_avg, testing_sp, testing_tsp });

            #endregion

            testing_md.SetMachineNumber(projectmachine.machinenumber);
            testing_md.SetTestReference(test.testreference);
            testing_md.SetDateOfTest(test.testdate);

            testing_sp.SetProjectName(project.projectname);

            testing_mcd_manip.MachineUniqueCode=machine.machineuniquecode;
            testing_mcd_manip.MachineDesc=machine.dllversiondesc;

            testing_mcd_manip_avg.MachineUniqueCode=machine.machineuniquecode;
            testing_mcd_manip_avg.MachineDesc=machine.dllversiondesc;

            testing_mcd_actual.MachineUniqueCode=machine.machineuniquecode;
            testing_mcd_actual.MachineDesc=machine.dllversiondesc;

            testing_mcd_actual_avg.MachineUniqueCode=machine.machineuniquecode;
            testing_mcd_actual_avg.MachineDesc=machine.dllversiondesc;
           
            UpdateMachineDetailsObject(machine, ref testing_md);
            UpdateSpecifiedParamsObject(machine,project, ref testing_sp);
            UpdateTestSetupParamsObject(machine, project, ref testing_tsp);

            // Initialise machine descriptors...
            testing_mcd_manip.InitializeInternalData();
            testing_mcd_manip_avg.InitializeInternalData();
            testing_mcd_actual.InitializeInternalData();
            testing_mcd_actual_avg.InitializeInternalData();

        }

        private void UpdateMachineDetailsObject(InstalledMachine machine, ref MachineDetailsBase mdb)
        {
            List<MachineDetailsEntry> details = imm.GetMachineDetailsEntries(machine);

            List<DynamicDataProperty> pinfo = mdb.GetDynamicPropertyList();

            LookupData ldata;
            LookupTableDetails ltdetails = new LookupTableDetails();

            for (int i = 0; i < pinfo.Count; i++)
            {
                if (pinfo[i].type == "string")
                {
                    int count = details.Count;
                    for (int j = 0; j < count; j++)
                    {
                        if (details[j].paramname == pinfo[i].pName)
                        {
                            mdb.SetStringValue(pinfo[i].pName, details[j].defaultdata);
                            //details.RemoveAt(j);
                            //count = details.Count;
                            break;
                        }
                    }
                }

                if (pinfo[i].type == "double")
                {
                    int count = details.Count;
                    for (int j = 0; j < count; j++)
                    {
                        if (details[j].paramname == pinfo[i].pName)
                        {
                            mdb.SetDoubleValue(pinfo[i].pName, double.Parse(details[j].defaultdata));
                            //details.RemoveAt(j);
                            //count = details.Count;
                            break;
                        }
                    }
                }

                if (pinfo[i].type == "lookup")
                {
                    int count = details.Count;
                    List<LookupDataRow> lookupdata;
                    Dictionary<double,double> rawdata;
                    for (int j = 0; j < count; j++)
                    {
                        if (details[j].paramname == pinfo[i].pName)
                        {
                            ltdetails = ltm.GetLookupTableDetailsByTableName(details[j].defaultdata);

                            lookupdata = new List<LookupDataRow>();

                            lookupdata = ltm.GetLookupData(ltdetails);

                            rawdata = new Dictionary<double,double>();

                            for(int k =0;k<lookupdata.Count;k++)
                            {
                                rawdata.Add(lookupdata[k].key,lookupdata[k].value);
                            }

                            ldata = new LookupData(rawdata);

                            mdb.SetLookupValue(pinfo[i].pName, ldata);
                            //details.RemoveAt(j);
                            //count = details.Count;
                            break;
                        }
                    }
                }
            }
        }

        private void UpdateSpecifiedParamsObject(InstalledMachine machine,ProjectDetails project, ref SpecifiedParamsBase spb)
        {
            //List<MachineDetailsEntry> details = imm.GetMachineDetailsEntries(machine);

            //List<DynamicDataProperty> pinfo = mdb.GetDynamicPropertyList();

            List<SpecifiedParamsDetails> details = pm.GetProjectSpecifiedParameters(machine, project);

            List<DynamicDataProperty> pinfo = spb.GetDynamicPropertyList();

            LookupData ldata;
            LookupTableDetails ltdetails = new LookupTableDetails();

            for (int i = 0; i < pinfo.Count; i++)
            {
                if (pinfo[i].type == "string")
                {
                    //int count = details.Count;
                    for (int j = 0; j < details.Count/*count*/; j++)
                    {
                        if (details[j].paramname == pinfo[i].pName)
                        {
                            spb.SetStringValue(pinfo[i].pName, details[j].defaultdata);
                            //details.RemoveAt(j);
                            //count = details.Count;
                            break;
                        }
                    }
                }

                if (pinfo[i].type == "double")
                {
                    //int count = details.Count;
                    for (int j = 0; j < details.Count/*count*/; j++)
                    {
                        if (details[j].paramname == pinfo[i].pName)
                        {
                            spb.SetDoubleValue(pinfo[i].pName, double.Parse(details[j].defaultdata));
                            //details.RemoveAt(j);
                            //count = details.Count;
                            break;
                        }
                    }
                }

                if (pinfo[i].type == "lookup")
                {
                    //int count = details.Count;
                    List<LookupDataRow> lookupdata;
                    Dictionary<double, double> rawdata;
                    for (int j = 0; j < details.Count/*count*/; j++)
                    {
                        if (details[j].paramname == pinfo[i].pName)
                        {
                            ltdetails = ltm.GetLookupTableDetailsByTableName(details[j].defaultdata);

                            lookupdata = new List<LookupDataRow>();

                            lookupdata = ltm.GetLookupData(ltdetails);

                            rawdata = new Dictionary<double, double>();

                            for (int k = 0; k < lookupdata.Count; k++)
                            {
                                rawdata.Add(lookupdata[k].key, lookupdata[k].value);
                            }

                            ldata = new LookupData(rawdata);

                            spb.SetLookupValue(pinfo[i].pName, ldata);
                            //details.RemoveAt(j);
                            //count = details.Count;
                            break;
                        }
                    }
                }
            }
        }

        private void UpdateTestSetupParamsObject(InstalledMachine machine, ProjectDetails project, ref TestSetupParamsBase tspb)
        {
            List<TestSetupParamsDetails> details = imm.GetTestSetupParamsDetails(machine);
            List<DynamicDataProperty> pinfo = tspb.GetDynamicPropertyList();
            LookupData ldata;
            LookupTableDetails ltdetails = new LookupTableDetails();

            for (int i = 0; i < pinfo.Count; i++)
            {
                if (pinfo[i].type == "string")
                {
                    int count = details.Count;
                    for (int j = 0; j < count; j++)
                    {
                        if (details[j].paramname == pinfo[i].pName)
                        {
                            tspb.SetStringValue(pinfo[i].pName, details[j].defaultdata);
                            //details.RemoveAt(j);
                            //count = details.Count;
                            break;
                        }
                    }
                }

                if (pinfo[i].type == "double")
                {
                    int count = details.Count;
                    for (int j = 0; j < count; j++)
                    {
                        if (details[j].paramname == pinfo[i].pName)
                        {
                            tspb.SetDoubleValue(pinfo[i].pName, double.Parse(details[j].defaultdata));
                            //details.RemoveAt(j);
                            //count = details.Count;
                            break;
                        }
                    }
                }

                if (pinfo[i].type == "lookup")
                {
                    int count = details.Count;
                    List<LookupDataRow> lookupdata;
                    Dictionary<double, double> rawdata;
                    for (int j = 0; j < count; j++)
                    {
                        if (details[j].paramname == pinfo[i].pName)
                        {
                            ltdetails = ltm.GetLookupTableDetailsByTableName(details[j].defaultdata);
                            lookupdata = new List<LookupDataRow>();
                            lookupdata = ltm.GetLookupData(ltdetails);
                            rawdata = new Dictionary<double, double>();

                            for (int k = 0; k < lookupdata.Count; k++)
                            {
                                rawdata.Add(lookupdata[k].key, lookupdata[k].value);
                            }

                            ldata = new LookupData(rawdata);
                            tspb.SetLookupValue(pinfo[i].pName, ldata);
                            //details.RemoveAt(j);
                            //count = details.Count;
                            break;
                        }
                    }
                }
            }
        }

    }
}
