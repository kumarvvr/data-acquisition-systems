﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DasCoreLib;
using DasCoreUtilities;
using BHEL.PUMPSDAS.Datatypes;
using System.Threading;
using System.Diagnostics;
using Ivi.Visa.Interop;

namespace DasCoreLib
{
    public class HardwareManager
    {
        static string[] _SUPPORTEDHARDWARE = { "AGILENT-34980A","HP3852S","SIMULATED" };

        List<MeasurementProfileDetails> mpDetails;
        int chCount;
        DeviceManager devmgr;

        Dictionary<int, Hardware> hardwareDevices;
        Dictionary<int, DeviceDetails> deviceDetailsDict;
        List<int> deviceIds;
        Dictionary<int, List<MeasurementProfileDetails>> deviceSpecificMPDetailsDict;

        public HardwareManager(List<MeasurementProfileDetails> mpDetails, DeviceManager devmgr, bool isSimulation)
        {
            // Configure all the hardware devices...
            ConfigureHardware(mpDetails,devmgr,isSimulation);
        }

        public void ConfigureHardware(List<MeasurementProfileDetails> mpDetails, DeviceManager devmgr, bool isSimulation)
        {
            this.mpDetails = mpDetails;
            this.chCount = mpDetails.Count;
            this.devmgr = devmgr;

            hardwareDevices = new Dictionary<int, Hardware>();
            deviceDetailsDict = new Dictionary<int, DeviceDetails>();

            // Get list of unique device id's
            List<int> dids = GetUniqueDevices();

            deviceIds = dids;
            if (!isSimulation)
            {
                // iterate through the device ids to fill up list of device details...
                for (int i = 0; i < deviceIds.Count; i++)
                {
                    DeviceDetails dd = devmgr.GetDeviceDetailsById(deviceIds[i]);
                    deviceDetailsDict.Add(deviceIds[i], dd);

                    // Also load and safe-keep the hardware devices...
                    hardwareDevices.Add(dd.deviceid, GetHardwareObjectByDeviceDesc(dd));
                    dd = new DeviceDetails();
                }
            }
            else
            {
                for (int i = 0; i < deviceIds.Count; i++)
                {
                    DeviceDetails dd = devmgr.GetDeviceDetailsById(deviceIds[i]);
                    deviceDetailsDict.Add(deviceIds[i], dd);

                    // Also load and safe-keep the hardware devices...
                    hardwareDevices.Add(dd.deviceid, new SIMULATEDHardware(-1, this.devmgr));
                    dd = new DeviceDetails();
                }
            }

            // generate device specific MPDetails..
            deviceSpecificMPDetailsDict = new Dictionary<int, List<MeasurementProfileDetails>>();

            for (int i = 0; i < deviceIds.Count; i++)
            {
                // Iterate through each device...
                List<MeasurementProfileDetails> dSpecList = new List<MeasurementProfileDetails>();

                for (int j = 0; j < mpDetails.Count; j++)
                {
                    if (mpDetails[j].fk_deviceid == deviceIds[i])
                    {
                        dSpecList.Add(mpDetails[j]);
                    }
                }

                deviceSpecificMPDetailsDict.Add(deviceIds[i], dSpecList);
            }

            ConfigureDevice();
        }
        
        public void InitializeHardware(int deviceID = -1)
        {
            if (deviceID == -1)
            {
                // Reset all hardware devices..
                for (int i = 0; i < hardwareDevices.Count; i++)
                {
                    hardwareDevices.ElementAt(i).Value.InitializeHardware();
                }
            }
            else
            {
                // Reset only that hardware device with given device ID
                Hardware hw;                
                if (hardwareDevices.TryGetValue(deviceID, out hw))
                    hw.InitializeHardware();
            }
        }

        public void ResetHardware(int deviceID = -1)
        {
            if (deviceID == -1)
            {
                // Reset all hardware devices..
                for (int i = 0; i < hardwareDevices.Count; i++)
                {
                    hardwareDevices.ElementAt(i).Value.ResetHardware();
                }
            }
            else
            {
                // Reset only that hardware device with given device ID
                Hardware hw;

                if(hardwareDevices.TryGetValue(deviceID, out hw))
                    hw.ResetHardware();
            }
        }

        public List<ChannelData> ScanDataFromHardware(out List<AlarmStatus> alarms, HardwareScanMode hwscanmode)
        {
            List<ChannelData> result = new List<ChannelData>();

            alarms = new List<AlarmStatus>();

            // Iterate through all hardware devices.
            for (int i = 0; i < hardwareDevices.Count; i++)
            {
                List<AlarmStatus> deviceAlarms = new List<AlarmStatus>();

                Hardware hw = hardwareDevices.ElementAt(i).Value;

                result.AddRange(hw.ScanHardware(out deviceAlarms, hwscanmode));

                alarms.AddRange(deviceAlarms);
            }

            return result;
        }

        #region Private Functions

        private void InitializeHardwareChannels()
        {
            // Instruct each hardware object to initialize its hardware channels...
            for (int i = 0; i < hardwareDevices.Count; i++)
            {
                Hardware hw = ((Hardware)(hardwareDevices.ElementAt(i).Value));
                int deviceID = ((int)(hardwareDevices.ElementAt(i).Key));
                hw.InitializeHardware();
            }
        }
        private Hardware GetHardwareObjectByDeviceDesc(DeviceDetails dd)
        {
            if (dd.devicedesc == "AGILENT-34980A")
                return new VISAHardware(dd.deviceid, this.devmgr);
            if (dd.devicedesc == "HP3852S")
                return new HPBASICHardware(dd.deviceid, this.devmgr);
            if (dd.devicedesc == "SIMULATED")
                return new SIMULATEDHardware(dd.deviceid, this.devmgr);

            return new SIMULATEDHardware(dd.deviceid, this.devmgr);
        }
        private List<int> GetUniqueDevices()
        {
            List<int> result = new List<int>(0);

            for (int i = 0; i < mpDetails.Count; i++)
            {
                if (!result.Contains(mpDetails[i].fk_deviceid))
                    result.Add(mpDetails[i].fk_deviceid);
            }

            return result;
        }
        private void ConfigureDevice()
        {
            for (int i = 0; i < hardwareDevices.Count; i++)
            {
                Hardware hw = ((Hardware)(hardwareDevices.ElementAt(i).Value));
                int deviceID = ((int)(hardwareDevices.ElementAt(i).Key));
                hw.ConfigureHardware(deviceSpecificMPDetailsDict[deviceID]);
            }
        }

#endregion
        
        public static List<string> GetSupportedHardwareTypes()
        {
            List<string> result = _SUPPORTEDHARDWARE.ToList<string>();
            return result;
        }
        public static string GetDefaultHardwareType()
        {
            return _SUPPORTEDHARDWARE[_SUPPORTEDHARDWARE.Length - 1];
        }

    }
}
