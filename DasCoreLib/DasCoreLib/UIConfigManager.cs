﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data.Odbc;
using DasCoreUtilities;
using BHEL.PUMPSDAS.Datatypes;

namespace DasCoreLib
{
    public class UIConfigManager
    {

        DatabaseManager _database;
        InstalledMachine mc;

        const int NumberOfRows = 40;

        #region QUERIES

        const string qry_insertNewSingleUILinkConfig =
            "insert into displayconfig (fk_machinecode,placeholder,objectidentifier,paramname) values({0},{1},{2},'{3}')";

        const string qry_updateConfig =
            "update displayconfig set objectidentifier={0},paramname='{1}' where fk_machinecode={2} and placeholder={3}";

        const string qry_deleteConfig =
            "delete from displayconfig where fk_machinecode={0} and placeholder={1}";

        const string qry_retrieveAllConfigForMachine =
            "select fk_machinecode,placeholder,objectidentifier,paramname from displayconfig where fk_machinecode={0}";

        const string qry_retrieveSinglePlaceholderConfigForMachine =
            "select fk_machinecode,placeholder,objectidentifier,paramname from displayconfig where fk_machinecode={0} and placeholder={1}";

        #endregion

        public UIConfigManager(DatabaseManager _database)
        {
            if (_database == null)
                throw new ArgumentNullException("_database", "_database is null.");

            this._database = _database;
        }

        public List<PropertyUILink> GetUIDisplayConfig(InstalledMachine mc)
        {
            List<PropertyUILink> result = new List<PropertyUILink>();
            PropertyUILink data;
            OdbcDataReader rd;
            string qry = string.Format(qry_retrieveAllConfigForMachine, mc.machinecode);

            rd = _database.ExecuteSelectQuery(qry);

            if (rd.HasRows)
            {
                while (rd.Read())
                {
                    data = new PropertyUILink();
                    data.fk_machinecode = DefaultConversion.GetIntValue(rd["fk_machinecode"].ToString());
                    data.placeholder = DefaultConversion.GetIntValue(rd["placeholder"].ToString());
                    if (rd["objectidentifier"].ToString() == ParamObject.MEASUREMENTPROFILE.ToString())
                    {
                        data.objType = ParamObject.MEASUREMENTPROFILE;
                    }
                    else
                    {
                        data.objType = ParamObject.RESULTPARAMETERS;
                    }

                    data.paramname = DefaultConversion.GetStringValue(rd["paramname"].ToString());

                    result.Add(data);
                }
            }

            return result;

        }

        public void CreateDefaultUIDisplayConfig(InstalledMachine mc)
        {
            string qry = "";
            for (int i = 0; i < NumberOfRows; i++)
                qry += string.Format(qry_insertNewSingleUILinkConfig, mc.machinecode, i, 0, "*")+";";
            _database.ExecuteInsertQuery(qry);
            return;
        }

        public void UpdateUIDisplayConfig(InstalledMachine mc, List<PropertyUILink> config)
        {
            string qry = "";

            for (int i = 0; i < config.Count; i++)
            {
                qry += string.Format(qry_updateConfig, config[i].objType.ToString(), config[i].paramname, mc.machinecode, config[i].placeholder)+";";
            }

            _database.ExecuteUpdateQuery(qry);
        }
    }
}
