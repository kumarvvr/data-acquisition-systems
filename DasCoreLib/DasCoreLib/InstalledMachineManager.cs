﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Odbc;
using BHEL.PUMPSDAS.Datatypes;
using DasCoreUtilities;
using FileManager;


namespace DasCoreLib
{
    public class InstalledMachineManager
    {
        #region PRIVATE MEMBERS

        DatabaseManager _db;

        
        
        #region Queries

        const string qry_insertnewmachine = "insert into installed_machines " +
            "(machineuniquecode,codefilepath,dllrootpath,dllfilename,dllmajorversion,dllminorversion,dllversiondescription)" +
            " values ('{0}','{1}','{2}','{3}','{4}','{5}','{6}') returning (machinecode)";

        const string qry_UpdateExistingMachine = "update installed_machines set codefilepath='{1}',dllrootpath='{2}',dllfilename='{3}',dllmajorversion='{4}',dllminorversion='{5}',dllversiondescription='{6}' where machinecode='{0}'";

        const string qry_selectmachine_mucode = "select " +
        "machinecode,machineuniquecode,codefilepath,dllrootpath,dllfilename,dllmajorversion,dllminorversion,dllversiondescription" +
        " from installed_machines where machineuniquecode='{0}'";
        
        const string qry_ismachineinstalled = "select machinecode from installed_machines where machineuniquecode='{0}'" +
            " and dllmajorversion='{1}' and dllminorversion='{2}'";

        const string qry_getinstalledmachinelist = "select * from installed_machines";

        const string qry_deleteDefaultData_Table =
            "delete from {0} where fk_machinecode={1}";

        const string qry_insertDefaultDataParam_Table =
            "insert into {0} (fk_machinecode,paramname,paramtype,paramdescription,paramunits,defaultdata,displayindex) " +
            "values({1},'{2}','{3}','{4}','{5}','{6}',{7})";

        const string qry_SelectDefaultMachineConfigData_Table =
            "select paramname,paramtype,paramdescription,paramunits,defaultdata,displayindex from {0} where fk_machinecode={1} order by displayindex";

        const string qry_UpdateDefaultConfigDataRow_Table =
            "update {0} set paramdescription='{1}',paramunits='{2}',defaultdata='{3}',displayindex={4} where fk_machinecode={5} and paramname='{6}'";

        const string qry_GetMeasurementProfileParamList =
            "select paramname from measurementprofile where fk_machinecode={0}";

        const string qry_GetResultParamsDetails =
            "select fk_machinecode,paramname,paramtype,paramdescription,paramunits,defaultdata,displayindex "+
            "from resultparameters where fk_machinecode={0} order by displayindex";

        const string qry_GetSpecifiedParamsDetails =
            "select fk_machinecode,paramname,paramtype,paramdescription,paramunits,defaultdata,displayindex from specifiedparameters where fk_machinecode={0}";

        const string qry_retrieveChannelConfigDetailsByMcode =
            "select meastype,paramunits,paramdescription,paramtype,paramname,channelid," +
            "fk_deviceid,sensormin,sensormax,outputmin,outputmax," +
            "scale,offsetvalue,highalarm,lowalarm,displayindex,defaultdata from measurementprofile where fk_machinecode={0} order by displayindex";

        const string qry_updateChannelConfigDetails =
            "update measurementprofile set meastype='{0}',channelid='{1}'," +
            "fk_deviceid={2},sensormin={3},sensormax={4},outputmin={5},outputmax={6}," +
            "scale={7},offsetvalue={8},highalarm={9},lowalarm={10},displayindex={11} where fk_machinecode={12} and paramname='{13}'";
        
        const string qry_deleteChannelConfigDetails =
            "delete meastype,fk_deviceid,channelid," +
            "sensormin,sensormax,outputmin,outputmax,scale,offsetvalue,highalarm,lowalarm from measurementprofile where fk_machinecode={0} and paramname='{1}'";

        const string qry_GetMachineDetailsEntries =
            "select fk_machinecode,paramname,paramtype,paramdescription,paramunits,defaultdata,displayindex from machinedetails where fk_machinecode={0}";

        const string qry_GetTestSetupParamsDetails =
            "select fk_machinecode,paramname,paramtype,paramdescription,paramunits,defaultdata,displayindex from testsetupparameters where fk_machinecode={0}";

        #endregion

        #endregion

        /// <summary>
        /// Constructs an Installed Machine Manager object.
        /// </summary>
        /// <param name="_db">A fully Initialised DatabaseManager object for immediate use by the object.</param>
        public InstalledMachineManager(DatabaseManager _db)
        {
            this._db = _db;
        }

        /// <summary>
        /// Creates a new Machine in the system. Default data for the machine is also created in appropriate tables.
        /// </summary>
        /// <param name="machine">A MachineDetails object that contains all the details of the machine
        /// to be added. Note : The machinecode is generated internally. Any value given by the user is ignored and
        /// after the new machine is added, over-written.</param>
        /// <param name="template_md"> A MachineDetailsBase object that contains the properties
        /// that are to be present for this new machine.</param>
        /// <param name="template_sp">A SpecifiedParamsBase object that contains the properties
        /// that are to be present for this new machine.</param>
        /// <param name="template_tsp">A TestSetupParamsBase object that contains the properties
        /// that are to be present for this new machine.</param>
        /// <param name="template_mp">A MeasurementProfileBase object that contains the properties
        /// that are to be present for this new machine.</param>
        /// <param name="template_rp">A ResultParamsBase object that contains the properties
        /// that are to be present for this new machine.</param>
        /// <param name="template_m">A MachineDescriptor object that contains the properties
        /// that are to be present for this new machine.</param>
        /// <returns></returns>
        public InstalledMachine CreateNewMachine(
            InstalledMachine machine,
            MachineDetailsBase template_md,
            SpecifiedParamsBase template_sp,
            TestSetupParamsBase template_tsp,
            MeasurementProfileBase template_mp,
            ResultParamsBase template_rp,
            MachineDescriptor template_m)
        {
            OdbcDataReader reader;

            InstalledMachine FinalMachine = new InstalledMachine();

            //FinalMachine = machine;

            // Generate the query to insert the machine details into the installed_machines table.
            string iqry = string.Format(qry_insertnewmachine, 
                machine.machineuniquecode,
                machine.codefilepath,
                machine.dllrootpath,
                machine.dllfilename,
                machine.dllmajorversion,
                machine.dllminorversion,
                machine.dllversiondesc);

            reader = _db.ExecuteSelectQuery(iqry);

            if (reader.HasRows)
                reader.Read();

            int mcode = int.Parse(reader.GetString(0));
            FinalMachine.machinecode = mcode;
            FinalMachine.machineuniquecode = machine.machineuniquecode;
            FinalMachine.codefilepath = machine.codefilepath;
            FinalMachine.dllrootpath = machine.dllrootpath;
            FinalMachine.dllfilename = machine.dllfilename;
            FinalMachine.dllmajorversion = machine.dllmajorversion;
            FinalMachine.dllminorversion = machine.dllminorversion;
            FinalMachine.dllversiondesc = machine.dllversiondesc;

            // Once the installed_machine process is complete, generate queries for the default data.

            CreateDefaultTableData(FinalMachine,
                template_md,
                template_sp,
                template_tsp,
                template_mp,
                template_rp,
                template_m);

            return FinalMachine;
        }

        /// <summary>
        /// Returns an InstalledMachine object.
        /// </summary>
        /// <param name="machineuniquecode">The unique code with which the available machines are searched.</param>
        /// <returns></returns>
        public InstalledMachine GetInstalledMachine(string machineuniquecode)
        {
            InstalledMachine imresult = new InstalledMachine();

            string selquery = string.Format(qry_selectmachine_mucode, machineuniquecode);

            OdbcDataReader result = _db.ExecuteSelectQuery(selquery);

            if (result.HasRows)
            {
                result.Read();

                imresult.machinecode = int.Parse(result.GetString(0));
                imresult.machineuniquecode = result.GetString(1);
                imresult.codefilepath = result.GetString(2);
                imresult.dllrootpath = result.GetString(3);
                imresult.dllfilename = result.GetString(4);
                imresult.dllmajorversion = result.GetString(5);
                imresult.dllminorversion = result.GetString(6);
                imresult.dllversiondesc = result.GetString(7);

            }
            return imresult;
        }

        /// <summary>
        /// Checks for existance of a machine based on the given details.
        /// </summary>
        /// <param name="mucode">The machineuniquecode of the required machine.</param>
        /// <param name="majver">Major version number of the required machine.</param>
        /// <param name="minver">Minor Version number of the required machine.</param>
        /// <returns></returns>
        public bool IsMachineInstalled(string mucode, string majver, string minver)
        {
            string qry = string.Format(qry_ismachineinstalled, mucode, majver, minver);
            OdbcDataReader res;
            try
            {
                res = _db.ExecuteSelectQuery(qry);
            }
            catch (Exception ex)
            {
                throw new Exception("Error accessing database to retrieve existing machine details in IsMachineInstalled.ERROR :" + ex.Message);
            }

            if (res.HasRows)
                return true;
            else
                return false;
        }
        
        /// </summary>
        /// <returns>The list of installed machines.</returns>
        public List<InstalledMachine> GetListofInstalledMachines()
        {
            List<InstalledMachine> machineList = new List<InstalledMachine>();
            InstalledMachine im = new InstalledMachine();

            string qry = string.Format(qry_getinstalledmachinelist);
            OdbcDataReader rec;
            try
            {
                rec = _db.ExecuteSelectQuery(qry);

                while (rec.Read())
                {                   
                    im.machineuniquecode = rec["machineuniquecode"].ToString();
                    im.codefilepath = rec["codefilepath"].ToString();
                    im.dllrootpath = rec["dllrootpath"].ToString();
                    im.dllfilename = rec["dllfilename"].ToString();
                    im.dllmajorversion = rec["dllmajorversion"].ToString();
                    im.dllminorversion = rec["dllminorversion"].ToString();
                    im.dllversiondesc = rec["dllversiondescription"].ToString();
                    im.machinecode = int.Parse(rec["machinecode"].ToString());

                    machineList.Add(im);
                }

            }
            catch (Exception ex)
            {
                throw new Exception("Error accessing database to retrieve existing machine details in GetListofInstalledMachines. ERROR :" + ex.Message);
            }
                      

            return machineList;
        }

        /// <summary>
        /// Gets the default configuration data for a given installed machine.
        /// </summary>
        /// <param name="machine">The InstalledMachine object for which the details are required.</param>
        /// <returns>A dictionary that contains the configuration data. The keys to the dictionary are the 
        /// database table names, defined as constants in DatabaseManager object. The value is a list of 
        /// configuration data for that particular table.</returns>
        public Dictionary<string, List<DefaultConfigurationData>> GetDefaultConfigurationData(InstalledMachine machine)
        {
            //TODO : This function has a clumsy interface. Individual configuration data (mp, sp, etc..) 
            //       should have different functions. This is to be updated.
            Dictionary<string, List<DefaultConfigurationData>> result = new Dictionary<string, List<DefaultConfigurationData>>();
            List<DefaultConfigurationData> data;
            DefaultConfigurationData record;
            OdbcDataReader reader;
            string selqry;
            // Get configuration data for MachineDetails.
            #region Default Configuration data loading for MachineDetails

            selqry = string.Format(qry_SelectDefaultMachineConfigData_Table, DatabaseManager.default_md_tablename, machine.machinecode);

            try
            {
                reader = _db.ExecuteSelectQuery(selqry);
                data = new List<DefaultConfigurationData>(0);

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        record = new DefaultConfigurationData();
                        record.defaultdata = reader["defaultdata"].ToString();
                        record.fk_machineCode = machine.machinecode.ToString();
                        record.paramdescription = reader["paramdescription"].ToString();
                        record.paramname = reader["paramname"].ToString();
                        record.paramtype = reader["paramtype"].ToString();
                        record.paramunits = reader["paramunits"].ToString();
                        record.displayindex = GetDefaultIntValueIfEmpty(reader["displayindex"].ToString());
                        data.Add(record);
                    }

                    result.Add(DatabaseManager.default_md_tablename, data);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error Fetching Data from Database. Query - :" + selqry);
            }

            #endregion
            #region Default Configuration data loading for Measurement Profile

            selqry = string.Format(qry_SelectDefaultMachineConfigData_Table, DatabaseManager.default_mp_tablename, machine.machinecode);

            try
            {
                reader = _db.ExecuteSelectQuery(selqry);
                data = new List<DefaultConfigurationData>(0);

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        record = new DefaultConfigurationData();
                        record.defaultdata = reader["defaultdata"].ToString();
                        record.fk_machineCode = machine.machinecode.ToString();
                        record.paramdescription = reader["paramdescription"].ToString();
                        record.paramname = reader["paramname"].ToString();
                        record.paramtype = reader["paramtype"].ToString();
                        record.paramunits = reader["paramunits"].ToString();
                        record.displayindex = GetDefaultIntValueIfEmpty(reader["displayindex"].ToString());
                        data.Add(record);
                    }

                    result.Add(DatabaseManager.default_mp_tablename, data);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error Fetching Data from Database. Query - :" + selqry);
            }

            #endregion
            #region Default Configuration data loading for ResultParameters

            selqry = string.Format(qry_SelectDefaultMachineConfigData_Table, DatabaseManager.default_rp_tablename, machine.machinecode);

            try
            {
                reader = _db.ExecuteSelectQuery(selqry);
                data = new List<DefaultConfigurationData>(0);

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        record = new DefaultConfigurationData();
                        record.defaultdata = reader["defaultdata"].ToString();
                        record.fk_machineCode = machine.machinecode.ToString();
                        record.paramdescription = reader["paramdescription"].ToString();
                        record.paramname = reader["paramname"].ToString();
                        record.paramtype = reader["paramtype"].ToString();
                        record.paramunits = reader["paramunits"].ToString();
                        record.displayindex = GetDefaultIntValueIfEmpty(reader["displayindex"].ToString());
                        data.Add(record);
                    }

                    result.Add(DatabaseManager.default_rp_tablename, data);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error Fetching Data from Database. Query - :" + selqry);
            }

            #endregion
            #region Default Configuration data loading for Specified Parameters

            selqry = string.Format(qry_SelectDefaultMachineConfigData_Table, DatabaseManager.default_sp_tablename, machine.machinecode);

            try
            {
                reader = _db.ExecuteSelectQuery(selqry);
                data = new List<DefaultConfigurationData>(0);

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        record = new DefaultConfigurationData();
                        record.defaultdata = reader["defaultdata"].ToString();
                        record.fk_machineCode = machine.machinecode.ToString();
                        record.paramdescription = reader["paramdescription"].ToString();
                        record.paramname = reader["paramname"].ToString();
                        record.paramtype = reader["paramtype"].ToString();
                        record.paramunits = reader["paramunits"].ToString();
                        record.displayindex = GetDefaultIntValueIfEmpty(reader["displayindex"].ToString());
                        data.Add(record);
                    }

                    result.Add(DatabaseManager.default_sp_tablename, data);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error Fetching Data from Database. Query - :" + selqry);
            }

            #endregion
            #region Default Configuration data loading for Test Setup Parameters

            selqry = string.Format(qry_SelectDefaultMachineConfigData_Table, DatabaseManager.default_tsp_tablename, machine.machinecode);

            try
            {
                reader = _db.ExecuteSelectQuery(selqry);
                data = new List<DefaultConfigurationData>(0);

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        record = new DefaultConfigurationData();
                        record.defaultdata = reader["defaultdata"].ToString();
                        record.fk_machineCode = machine.machinecode.ToString();
                        record.paramdescription = reader["paramdescription"].ToString();
                        record.paramname = reader["paramname"].ToString();
                        record.paramtype = reader["paramtype"].ToString();
                        record.paramunits = reader["paramunits"].ToString();
                        record.displayindex = GetDefaultIntValueIfEmpty(reader["displayindex"].ToString());
                        data.Add(record);
                    }

                    result.Add(DatabaseManager.default_tsp_tablename, data);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error Fetching Data from Database. Query - :" + selqry);
            }

            #endregion

            return result;
        }

        /// <summary>
        /// For a given machine, it updates the configuration data of the machine with the one that is provided. Once
        /// again, the keys are the tablenames in DatabaseManager class.
        /// Note : This method updates the data based on the machinecode and paramname. If the given data
        /// contains newer paramnames, then an exception will be thrown. The best way to use this function is
        /// to obtain the data for a machine using the "GetDefaultConfigurationData" method and then simply update
        /// the values of the data provided.
        /// </summary>
        /// <param name="machine">An InstalledMachine object that represents a machine existing in the system.</param>
        /// <param name="updates">A dictionary that contains the updates that are to be applied for this machine.</param>
        /// <returns></returns>
        public bool UpdateDefaultConfigurationData(InstalledMachine machine, Dictionary<string, List<DefaultConfigurationData>> updates)
        {
            List<string> updateQueries = new List<string>(0);
            string tablename;

            // Loop through the given dictionary.

            for (int i = 0; i < updates.Count; i++)
            {
                tablename = updates.ElementAt(i).Key;
                List<DefaultConfigurationData> updateData = updates[tablename];
                for (int j = 0; j < updateData.Count; j++)
                {

                    string qry = string.Format(qry_UpdateDefaultConfigDataRow_Table,
                        tablename, updateData[j].paramdescription, updateData[j].paramunits,
                        updateData[j].defaultdata,updateData[j].displayindex, machine.machinecode, updateData[j].paramname);

                    updateQueries.Add(qry);
                }
            }

            
            string totalquery = "";
            for (int i = 0; i < updateQueries.Count; i++)
            {
                totalquery += updateQueries[i] + ";";
                //_db.ExecuteUpdateQuery(updateQueries[i]);
            }
            _db.ExecuteUpdateQuery(totalquery);
            return true;
        }

        /// <summary>
        /// Fetches the MeasurementProfileDetails for an installed machine.
        /// </summary>
        /// <param name="mc">An InstalledMachine object that represents a machine present in the system.</param>
        /// <returns>A list of MeasurementProfileDetails objects for this machine.</returns>
        public List<MeasurementProfileDetails> GetMeasurementProfileDetails(InstalledMachine mc)
        {

            OdbcDataReader reader;
            List<MeasurementProfileDetails> channellist = new List<MeasurementProfileDetails>();
            MeasurementProfileDetails details;
            string qry = string.Format(qry_retrieveChannelConfigDetailsByMcode, mc.machinecode);
            reader = _db.ExecuteSelectQuery(qry);
            
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    details = new MeasurementProfileDetails();
                    
                    details.fk_machineCode = mc.machinecode;                    
                    details.paramname = reader["paramname"].ToString();
                    details.paramtype = reader["paramtype"].ToString();
                    details.paramdescription = reader["paramdescription"].ToString();
                    details.paramunits = reader["paramunits"].ToString();
                    details.defaultdata = reader["defaultdata"].ToString();
                    details.meastype = reader["meastype"].ToString();
                    details.fk_deviceid = DefaultConversion.GetIntValue(reader["fk_deviceid"].ToString());
                    details.channelid = reader["channelid"].ToString();
                    details.sensormin = DefaultConversion.GetDoubleValue(reader["sensormin"].ToString());
                    details.sensormax = DefaultConversion.GetDoubleValue(reader["sensormax"].ToString());
                    details.outputmin = DefaultConversion.GetDoubleValue(reader["outputmin"].ToString());
                    details.outputmax = DefaultConversion.GetDoubleValue(reader["outputmax"].ToString());
                    details.scale = DefaultConversion.GetDoubleValue(reader["scale"].ToString());
                    details.offsetvalue = DefaultConversion.GetDoubleValue(reader["offsetvalue"].ToString());
                    details.highalarm = DefaultConversion.GetDoubleValue(reader["highalarm"].ToString());
                    details.lowalarm = DefaultConversion.GetDoubleValue(reader["lowalarm"].ToString());
                    details.displayindex = DefaultConversion.GetIntValue(reader["displayindex"].ToString());
                    details.pointnum = -1;

                    if ((details.sensormax - details.sensormin) == 0)
                        details.slope = -1;
                    else details.slope = ((details.outputmax - details.outputmin) / (details.sensormax - details.sensormin));

                    channellist.Add(details);
                }
            }

            return channellist;
        }

        public List<ResultParamsDetails> GetResultParamsDetails(InstalledMachine mc)
        {

            OdbcDataReader reader;
            List<ResultParamsDetails> channellist = new List<ResultParamsDetails>();
            ResultParamsDetails details;
            string qry = string.Format(qry_GetResultParamsDetails, mc.machinecode);
            reader = _db.ExecuteSelectQuery(qry);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    details = new ResultParamsDetails();

                    details.fk_machineCode = mc.machinecode.ToString();
                    details.paramname = reader["paramname"].ToString();
                    details.paramtype = reader["paramtype"].ToString();
                    details.paramdescription = reader["paramdescription"].ToString();
                    details.paramunits = reader["paramunits"].ToString();
                    details.defaultdata = reader["defaultdata"].ToString();
                    details.displayindex = DefaultConversion.GetIntValue(reader["displayindex"].ToString());
                    details.pointnum = -1;
                    channellist.Add(details);
                }
            }

            return channellist;
        }

        /// <summary>
        /// Fetches the SpecifiedParameters for a specified machine.
        /// </summary>
        /// <param name="mc">An InstalledMachine object that represents a machine present in the system.</param>
        /// <returns>A list of SpecifiedParametersDetails objects which represent all the specified parameters for
        /// this particular machine.</returns>
        public List<SpecifiedParamsDetails> GetSpecifiedParametersDetails(InstalledMachine mc)
        {
            string qry = string.Format(qry_GetSpecifiedParamsDetails, mc.machinecode);

            OdbcDataReader reader;

            List<SpecifiedParamsDetails> result = new List<SpecifiedParamsDetails>();

            SpecifiedParamsDetails sp = new SpecifiedParamsDetails();

            reader = _db.ExecuteSelectQuery(qry);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    sp = new SpecifiedParamsDetails();
                    sp.defaultdata = reader["defaultdata"].ToString();
                    sp.fk_machinecode = mc.machinecode;
                    sp.paramdescription = reader["paramdescription"].ToString();
                    sp.paramname = reader["paramname"].ToString();
                    sp.paramtype = reader["paramtype"].ToString();
                    sp.paramunits = reader["paramunits"].ToString();
                    sp.displayindex = int.Parse(reader["displayindex"].ToString());

                    result.Add(sp);
                }
            }

            return result;
        }

        /// <summary>
        /// Channel Configuration details are specified for a particular Machine.
        /// These details are present in the measurementprofile table, along with,
        /// the measurementprofile details.
        /// </summary>
        /// <param name="chdetails">A list of the channel details that are to be updated.</param>
        /// <param name="machine">The unique machine for which the details are to be updated.</param>
        /// <returns></returns>
        public bool UpdateMeasurementConfig(List<MeasurementProfileDetails> chdetails, InstalledMachine machine)
        {
            if (chdetails.Count > 0)
            {
                string qry = "";
                MeasurementProfileDetails details;

                for (int i = 0; i < chdetails.Count; i++)
                {
                    details = chdetails[i];
                    qry += string.Format(qry_updateChannelConfigDetails,
                                                        details.meastype,
                                                        details.channelid,
                                                        details.fk_deviceid,
                                                        details.sensormin,
                                                        details.sensormax,
                                                        details.outputmin,
                                                        details.outputmax,
                                                        details.scale,
                                                        details.offsetvalue,
                                                        details.highalarm,
                                                        details.lowalarm,
                                                        details.displayindex,
                                                        machine.machinecode,  // The same should be filled up in the chdetails object.
                        // But it has been included here for clarity.
                                                        details.paramname) + ";";
                }

                _db.ExecuteUpdateQuery(qry);
                return true;
            }
            else return false;
        }
        
        public List<MachineDetailsEntry> GetMachineDetailsEntries(InstalledMachine mc)
        {
            string qry = string.Format(qry_GetMachineDetailsEntries, mc.machinecode);

            OdbcDataReader reader;
            List<MachineDetailsEntry> result = new List<MachineDetailsEntry>();

            MachineDetailsEntry md = new MachineDetailsEntry();

            reader = _db.ExecuteSelectQuery(qry);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    md = new MachineDetailsEntry();
                    md.defaultdata = reader["defaultdata"].ToString();
                    md.fk_machinecode = mc.machinecode;
                    md.paramdescription = reader["paramdescription"].ToString();
                    md.paramname = reader["paramname"].ToString();
                    md.paramtype = reader["paramtype"].ToString();
                    md.paramunits = reader["paramunits"].ToString();
                    md.displayindex = int.Parse(reader["displayindex"].ToString());
                    result.Add(md);
                }
            }

            return result;
        }

        public List<TestSetupParamsDetails> GetTestSetupParamsDetails(InstalledMachine mc)
        {
            string qry = string.Format(qry_GetTestSetupParamsDetails, mc.machinecode);

            OdbcDataReader reader;
            List<TestSetupParamsDetails> result = new List<TestSetupParamsDetails>();

            TestSetupParamsDetails tsp = new TestSetupParamsDetails();

            reader = _db.ExecuteSelectQuery(qry);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    tsp = new TestSetupParamsDetails();
                    tsp.defaultdata = reader["defaultdata"].ToString();
                    tsp.fk_machinecode = mc.machinecode;
                    tsp.paramdescription = reader["paramdescription"].ToString();
                    tsp.paramname = reader["paramname"].ToString();
                    tsp.paramtype = reader["paramtype"].ToString();
                    tsp.paramunits = reader["paramunits"].ToString();
                    tsp.displayindex = int.Parse(reader["displayindex"].ToString());
                    result.Add(tsp);
                }
            }

            return result;
        }

        public void UpdateExistingMachine(InstalledMachine mc)
        {
            // codefilepath='{1}',dllrootpath='{2}',dllfilename='{3}',dllmajorversion='{4}'
            //,dllminorversion='{5}',dllversiondescription='{6}' where machineuniquecode='{0}'
            string qry = string.Format(qry_UpdateExistingMachine,
                                        mc.machinecode,
                                        mc.codefilepath,
                                        mc.dllrootpath,
                                        mc.dllfilename,
                                        mc.dllmajorversion,
                                        mc.dllminorversion,
                                        mc.dllversiondesc);

            _db.ExecuteUpdateQuery(qry);
        }

        public void CreateErrorCorrectorCodeFile(InstalledMachine mc)
        {
            string templateCode = FileSystemHandler.ReadFileToText(DasCoreSettings.GetRootErrorCorrectorTemplateFileFullName());

            // Replace the markers....

            templateCode = templateCode.Replace(DasCoreSettings.GetErrorCorrectorReplacementMarker(),mc.machineuniquecode);

            FileSystemHandler.WriteToTextFile(DasCoreSettings.GetMachineErrorCorrectorTemplateFileFullName(mc), false, templateCode);

        }

        public void CreateLinePrintCodeFile(InstalledMachine mc)
        {
            string templateCode = FileSystemHandler.ReadFileToText(DasCoreSettings.GetRootLinePrintScriptTemplateFileFullName());

            // Replace the markers....

            templateCode = templateCode.Replace(DasCoreSettings.GetErrorCorrectorReplacementMarker(), mc.machineuniquecode);

            FileSystemHandler.WriteToTextFile(DasCoreSettings.GetMachineLinePrinterScriptFileFullName(mc), false, templateCode);

        }

        public void CreateUIConfigCodeFile(InstalledMachine mc)
        {
            string templateCode = FileSystemHandler.ReadFileToText(DasCoreSettings.GetRootUIScriptTemplateFileFullName());

            // Replace the markers....

            templateCode = templateCode.Replace(DasCoreSettings.GetErrorCorrectorReplacementMarker(), mc.machineuniquecode);

            FileSystemHandler.WriteToTextFile(DasCoreSettings.GetMachineUIConfigScriptFileFullName(mc), false, templateCode);

        }

        internal void CreateTestReportTemplateFile(InstalledMachine currentMachine)
        {
            string sf, df;

            sf = DasCoreSettings.GetRootTestReportTemplateFileFullName();
            df = DasCoreSettings.GetMachineTestReportTemplateFileFullName(currentMachine);

            FileManager.FileSystemHandler.CopyFile(sf, df, true);

            // After copying the file, template data has to be filled up in the new file...

            
        }

        #region PRIVATE METHODS

        /// <summary>
        /// Creates default table data for a newly installed machine. Since the property names are to be obtained using 
        /// reflection, this function has to be called externally.
        /// </summary>
        /// <param name="machine"></param>
        /// <param name="template_md"></param>
        /// <param name="template_sp"></param>
        /// <param name="template_tsp"></param>
        /// <param name="template_mp"></param>
        /// <param name="template_rp"></param>
        /// <param name="template_m"></param>
        /// <returns></returns>
        private bool CreateDefaultTableData(InstalledMachine machine,
            MachineDetailsBase template_md,
            SpecifiedParamsBase template_sp,
            TestSetupParamsBase template_tsp,
            MeasurementProfileBase template_mp,
            ResultParamsBase template_rp,
            MachineDescriptor template_m)
        {
            List<DynamicDataProperty> propertyList;

            string tablename, qry, errors = "";
            string allQueries = "";

            //TODO : Transactional System for the whole set of queries.
            // TODO : Check if the machine default data already exists.

            #region Generation of Queries for updating tables

            // Update from MachineDetails
            tablename = DatabaseManager.default_md_tablename;
            propertyList = template_md.GetDynamicPropertyList();
            qry = string.Format(qry_deleteDefaultData_Table, tablename, machine.machinecode);
            qry = qry + ";";
            allQueries += qry;
            for (int i = 0; i < propertyList.Count; i++)
            {
                qry = string.Format(qry_insertDefaultDataParam_Table, tablename, machine.machinecode, propertyList[i].pName, propertyList[i].type, "", "", "", 1);
                qry = qry + ";";
                allQueries += qry;
            }

            // Update from testsetupparameters
            tablename = DatabaseManager.default_tsp_tablename;
            propertyList = template_tsp.GetDynamicPropertyList();
            qry = string.Format(qry_deleteDefaultData_Table, tablename, machine.machinecode);
            qry = qry + ";";
            allQueries += qry;
            for (int i = 0; i < propertyList.Count; i++)
            {
                qry = string.Format(qry_insertDefaultDataParam_Table, tablename, machine.machinecode, propertyList[i].pName, propertyList[i].type, "", "", "", 1);
                qry = qry + ";";
                allQueries += qry;
            }

            // Update from SpecifiedParameters
            tablename = DatabaseManager.default_sp_tablename;
            propertyList = template_sp.GetDynamicPropertyList();
            qry = string.Format(qry_deleteDefaultData_Table, tablename, machine.machinecode);
            qry = qry + ";";
            allQueries += qry;
            for (int i = 0; i < propertyList.Count; i++)
            {
                qry = string.Format(qry_insertDefaultDataParam_Table, tablename, machine.machinecode, propertyList[i].pName, propertyList[i].type, "", "", "", 1);
                qry = qry + ";";
                allQueries += qry;
            }

            // Update from measurementprofile
            tablename = DatabaseManager.default_mp_tablename;
            propertyList = template_mp.GetDynamicPropertyList();
            qry = string.Format(qry_deleteDefaultData_Table, tablename, machine.machinecode);
            qry = qry + ";";
            allQueries += qry;
            for (int i = 0; i < propertyList.Count; i++)
            {
                qry = string.Format(qry_insertDefaultDataParam_Table, tablename, machine.machinecode, propertyList[i].pName, propertyList[i].type, "", "", "", 1);
                qry = qry + ";";
                allQueries += qry;
            }

            // Update from resultparameters
            tablename = DatabaseManager.default_rp_tablename;
            propertyList = template_rp.GetDynamicPropertyList();
            qry = string.Format(qry_deleteDefaultData_Table, tablename, machine.machinecode);
            qry = qry + ";";
            allQueries += qry;
            for (int i = 0; i < propertyList.Count; i++)
            {
                qry = string.Format(qry_insertDefaultDataParam_Table, tablename, machine.machinecode, propertyList[i].pName, propertyList[i].type, "", "", "", 1);
                qry = qry + ";";
                allQueries += qry;
            }

            #endregion
            try
            {
                _db.ExecuteInsertQuery(allQueries);
            }
            catch (Exception ex)
            {
                errors += "Error " + " : " + ex.Message + "\n";
            }

            if (errors != "")
                throw new Exception(errors);

            return true;
        }

        /// <summary>
        /// Converts a string into an Interger value. If the cannot be converted to an Integer value, default 
        /// value of 1 is returned.
        /// </summary>
        /// <param name="rawdata">The string to be converted to Integer.</param>
        /// <returns>The converted Integer value of the default value.</returns>
        private int GetDefaultIntValueIfEmpty(string rawdata)
        {
            int temp;
            if (int.TryParse(rawdata, out temp))
            {
                return temp;
            }
            else
            {
                return 1;
            }
        }

        #endregion

        #region DEPRECATED

        /// <summary>
        /// Gets a list of parameter names in the MeasurementProfile for a particular installed machine.
        /// </summary>
        /// <param name="mc">An InstalledMachine object that represents a machine installed in the system.</param>
        /// <returns>A list of strings where each string is the parametername</returns>
        /*public List<string> GetMeasurementProfileParametersList(InstalledMachine mc)
        {
            string qry = string.Format(qry_GetMeasurementProfileParamList, mc.machinecode);

            OdbcDataReader reader;

            List<string> result = new List<string>();

            reader = _db.ExecuteSelectQuery(qry);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    result.Add(reader.GetString(0));
                }
            }

            return result;
        }*/

        #endregion


    }
}
