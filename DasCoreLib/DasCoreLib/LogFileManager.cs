﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FileManager;
using System.ComponentModel;

namespace DasCoreLib
{    
    public class LogFileManager
    {
        BackgroundWorker bgw;
        string errorLogFilePath, testLogFilePath, dbaseLogFilePath, appChangesLogFilePath;
        string fullpath="", text="";
        bool append=true;

        public LogFileManager(string errorLogFilePath, string dbaseLogFilePath, string appChangesLogFilePath)
        {
            this.errorLogFilePath = errorLogFilePath;
            this.dbaseLogFilePath = dbaseLogFilePath;
            this.appChangesLogFilePath = appChangesLogFilePath;

            bgw = new BackgroundWorker();
            bgw.DoWork += new DoWorkEventHandler(bgw_DoWork);
            bgw.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgw_RunWorkerCompleted);
            bgw.ProgressChanged += new ProgressChangedEventHandler(bgw_ProgressChanged);
        }

        void bgw_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            
        }

        void bgw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            
        }

        void bgw_DoWork(object sender, DoWorkEventArgs e)
        {
            e.Result = (bool)WriteFile(sender as BackgroundWorker);
        }

        private bool WriteFile(BackgroundWorker sender)
        {            
            FileSystemHandler.WriteToTextFile(fullpath, append, text);
            return true;
        }

        public void WriteErrorLog(string err)
        {
           /* bgw.
            bgw.RunWorkerAsync();*/
        }

        public void WriteTestLog(string fullpath, string logtext)
        {

        }

        public void WriteDatabaseLog(string logtext)
        {

        }

        public void WriteAppChangesLog(string logtext)
        {

        }



    }
}
