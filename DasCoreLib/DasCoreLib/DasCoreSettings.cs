﻿using System;
using System.Collections.Generic;
using System.Linq;
using BHEL.PUMPSDAS.Datatypes;
using System.Drawing;


namespace DasCoreLib
{
    public class DasCoreSettings
    {

        #region STRINGS FOR APPLICATION PATHS, DIRECTORIES & SUB-DIRECTORIES
        
        // Filled up in the constructor.
        static string AppRootPath = "";

        static string ProjectsRootFolder = "Projects\\";

        static string DataRootPath = "Data\\";

        static string TemplatesRootPath = "Templates\\";

        static string ScriptsRootPath = "Scripts\\";

        static string CompiledScriptsRootPath = "CompiledScripts\\";

        static string HardwareScriptsRootPath = "HardwareScripts\\";
     
        // Internal folders

        static string LinePrintFolder = "lineprints\\";

        static string ScreenShotsFolder = "screenshots\\";

        // File Names Templates        

        static string TestScreenshotFileName = "{0}_{1}_Screenshot_{2}.png";

        static string ErrorCorrectorTemplateMUCodeTemplate = "<##MUCODE##>";

        static string LinePrintExcelSheetName = "lineprint";

        static string LinePrintOutputSheetName = "lineprintoutput";

        static string LinePrintTemplateFileName = "LinePrintTemplate.xlsx";

        static string MachineCfgScriptFileFullName = "{0}_McCfg_script.cs";

        static string ProjectFolderTemplateName = "project_{0}";

        static string TestReportFileNameTemplate = "{0}{1}_{2}_Logsheet_Report.xlsx";

        // error corrector script file template names.
        static string RootErrorCorrectorTemplateFileName = "RootErrorCorrectorTemplate.cs";
        static string MachineErrorCorrectorTemplateFileName = "{0}_ErrorCorrector.cs";
        static string ProjectMachineErrorCorrectorFileName = "{0}_{1}_ErrorCorrector.cs";

        static string RootLinePrintScriptTemplateFileName = "RootLinePrinterScriptTemplate.cs";
        static string MachineLinePrintScriptFileFullName = "{0}{1}_LinePrinter_script.cs";

        static string RootUIConfigScriptTemplateFileName = "RootUIConfigScriptTemplate.cs";
        static string MachineUICfgScriptFileFullName = "{0}{1}_UICfg_script.cs";

        static string RootTestReportTemplateFileName = "TestDataRootTemplate.xlsx";
        static string MachineTestReportTemplateFileFullName = "{0}{1}_TestDataTemplate.xlsx";

        #endregion

        #region PROCESS COMMANDS STRINGS & METHODS

        static string ScriptCodeEditorCommand = "{0}\\npp\\unicode\\notepad++.exe";

        static string ScriptCodeEditorCommandArguments = " -nosession -multiInst -lcs \"{0}\"";

        public static string GetScriptEditorFileFullName()
        {
            return ScriptCodeEditorCommand;
        }

        public static string GetMachineEditUIConfigScriptProcessArguments(InstalledMachine mc)
        {
            return string.Format(ScriptCodeEditorCommandArguments, GetMachineUIConfigScriptFileFullName(mc));
        }

        public static string GetMachineEditLinePrintScriptProcessArguments(InstalledMachine mc)
        {
            return string.Format(ScriptCodeEditorCommandArguments, GetMachineLinePrinterScriptFileFullName(mc));
        }

        public static string GetProjectMachineEditErrorCorrectorScriptProcessArguments(InstalledMachine mc, ProjectDetails pd,ProjectMachineDetails pmd)
        {
            return string.Format(ScriptCodeEditorCommandArguments, GetProjectMachineErrCorrScriptFileFullName(mc, pd, pmd));
        }


        #endregion

        #region COLOR & THEME SETTINGS

        static Color highColor,highBackColor, lowColor,lowBackColor, defaultColor,defaultBackColor;
        static AlarmColors alarmColors;

        // Windows Colors...



        #endregion


        public DasCoreSettings()
        {
            AppRootPath = @"F:\PUMPS DAS V2\source\output\";

            ScriptCodeEditorCommand = string.Format(ScriptCodeEditorCommand, AppRootPath);

            highColor = Color.FromArgb(0xFF, 0x00, 0x00);
            highBackColor = Color.Black;

            lowColor = Color.FromArgb(0xFF, 0xCC, 0x00);
            lowBackColor = Color.Black;

            defaultColor = Color.FromArgb(192, 255, 192);
            defaultBackColor = Color.Black;


            alarmColors = new AlarmColors();

            alarmColors.highAlarmColor = highColor;
            alarmColors.highAlarmBackColor = highBackColor;
            alarmColors.lowAlarmColor = lowColor;
            alarmColors.lowAlarmBackColor = lowBackColor;
            alarmColors.defaultColor = defaultColor;
            alarmColors.defaultBackColor = defaultBackColor;
        }

        #region FOLDER PATHS
        
        #region PATHS SPECIFIC TO APPLICATION

        public static string GetAppDataPath()
        {
            return AppRootPath + DataRootPath;


        }
        public static string GetAppTemplateRootPath()
        {
            return GetAppDataPath() + TemplatesRootPath;
        }
        public static string GetAppProjectsRootPath()
        {
            return GetAppDataPath() + ProjectsRootFolder;


        }
        public static string GetAppInstalledMachineScriptsRootPath()
        {
            return GetAppDataPath() + ScriptsRootPath;
        }
        public static string GetCompiledScriptsRootPath()
        {
            return GetAppDataPath() + CompiledScriptsRootPath;
        }
        public static string GetHardwareScriptsRootPath()
        {
            return GetAppDataPath() + HardwareScriptsRootPath;
        }

        #endregion

        #region PATHS SPECIFIC TO MACHINE

        public static string GetInstalledMachineTemplatePath(InstalledMachine machine)
        {
            return GetAppTemplateRootPath() + machine.machineuniquecode + "\\";
        }        
        public static List<string> GetNewMachinePaths(InstalledMachine mc)
        {
            List<string> pathList = new List<string>();
            pathList.Add(DasCoreSettings.GetAppTemplateRootPath() + mc.machineuniquecode+ "\\");
            pathList.Add(DasCoreSettings.GetAppProjectsRootPath() + mc.machineuniquecode + "\\");
            return pathList;
        }
        #endregion

        #region PROJECT, MACHINE & TEST SPECIFIC PATHS

        public static string GetProjectPath(InstalledMachine machine, ProjectDetails project)
        {
            string result = GetAppProjectsRootPath() +
                                            machine.machineuniquecode + "\\" +
                                            GetProjectFolder(project) + "\\";

            return result;
        }        
        public static string GetProjectMachinePath(InstalledMachine machine, ProjectDetails project, ProjectMachineDetails pmd)
        {
            string result = GetProjectPath(machine, project) +
                                    pmd.machinenumber + "\\";

            return result;
        }
        public static string GetMachineTestReportsPath(InstalledMachine machine, ProjectDetails project, ProjectMachineDetails pmd, MachineTestDetails mtd)
        {
            string result = GetProjectMachinePath(machine, project, pmd) +
                                    mtd.testreference + "\\";

            return result;
        }
        public static string GetMachineTestScreenshotsPath(InstalledMachine mc, ProjectDetails pd, ProjectMachineDetails pmd, MachineTestDetails mtd)
        {
            return GetMachineTestReportsPath(mc, pd, pmd, mtd) + ScreenShotsFolder;
        }
        public static string GetMachineTestLinePrintsPath(InstalledMachine mc, ProjectDetails pd, ProjectMachineDetails pmd, MachineTestDetails mtd)
        {
            return GetMachineTestReportsPath(mc, pd, pmd, mtd) + LinePrintFolder;
        }

        private static string GetProjectFolder(ProjectDetails project)
        {
            return string.Format(ProjectFolderTemplateName,project.projectcode);
        }
        #endregion

        #endregion

        #region FILE FULL NAMES

        #region APPLICATION GLOBAL FILE FULL NAMES

        // Used while compiling a machine dll.
        public static string GetCompiledDllFileName(string machineUCode, string majorVer, string minorVer)
        {
            return machineUCode + "_" + majorVer + "_" + minorVer + ".dll";
        }

        // Used when creating a machine specific error corrector script file..
        public static string GetRootErrorCorrectorTemplateFileFullName()
        {
            return GetAppTemplateRootPath() +
                RootErrorCorrectorTemplateFileName;

        }

        // Used when creating a machine specific line print file..
        public static string GetRootLinePrintScriptTemplateFileFullName()
        {
            return GetAppTemplateRootPath() + RootLinePrintScriptTemplateFileName;
        }

        //Used when creating a machine specific ui script file..
        public static string GetRootUIScriptTemplateFileFullName()
        {
            return GetAppTemplateRootPath() + RootUIConfigScriptTemplateFileName;
        }

        // Used when generating line print EXCEL files....
        // NOTE : This is not a script code file.
        public static string GetLinePrintTemplateFileFullName()
        {
            return GetAppTemplateRootPath() +
                GetLinePrintTemplateFileName();
        }

        // Used for creating a machine test report data template file during addition of a new machine.
        public static string GetRootTestReportTemplateFileFullName()
        {
            return GetAppTemplateRootPath() + 
                RootTestReportTemplateFileName;
        }
        #endregion
        
        #region MACHINE SPECIFIC

        
        public static string GetMachineErrorCorrectorTemplateFileFullName(InstalledMachine mc)
        {
            return string.Format(MachineErrorCorrectorTemplateFileName, GetAppTemplateRootPath() + mc.machineuniquecode + "\\" + mc.machineuniquecode);
        }

        public static string GetMachineLinePrinterScriptFileFullName(InstalledMachine mc)
        {
            string result = String.Format(MachineLinePrintScriptFileFullName, GetAppInstalledMachineScriptsRootPath(), mc.machineuniquecode);
            return result;
        }

        public static string GetMachineUIConfigScriptFileFullName(InstalledMachine mc)
        {
            string result = String.Format(MachineUICfgScriptFileFullName, GetAppInstalledMachineScriptsRootPath(), mc.machineuniquecode);
            return result;
        }

        public static string GetMachineConfigScriptFileFullName(InstalledMachine mc)
        {
            return string.Format(MachineCfgScriptFileFullName, GetAppInstalledMachineScriptsRootPath() + mc.machineuniquecode);
        }

        public static string GetMachineConfigScriptFileFullName(string mucode)
        {
            // "{0}_McCfg_script.cs"
            return string.Format(MachineCfgScriptFileFullName, GetAppInstalledMachineScriptsRootPath() + mucode);
        }

        public static string GetMachineTestReportTemplateFileFullName(InstalledMachine mc)
        {
            //  "{0}{1}_TestDataTemplate.xlsx";
            return string.Format(MachineTestReportTemplateFileFullName, GetAppTemplateRootPath() + mc.machineuniquecode + "\\", mc.machineuniquecode);
        }

        #endregion

        #region PROJECT SPECIFIC
        
        #endregion

        #region MACHINE NUMBER SPECIFIC
        
        #endregion

        #region TEST SPECIFIC

        public static string GetMachineTestReportFileFullName(InstalledMachine mc, ProjectDetails pd, ProjectMachineDetails pmd, MachineTestDetails mtd)
        {
            // <pump no>_<testref>_testreport

            return string.Format(TestReportFileNameTemplate, GetMachineTestReportsPath(mc, pd, pmd, mtd), pmd.machinenumber, mtd.testreference);
        }

        public static string GetTestScreenshotFullName(InstalledMachine machine, ProjectDetails project, ProjectMachineDetails pmd, MachineTestDetails mtd, string append)
        {
            return string.Format(TestScreenshotFileName, GetMachineTestScreenshotsPath(machine, project, pmd, mtd) + pmd.machinenumber, mtd.testreference, append);
        }
                
        public static string GetProjectMachineErrCorrScriptFileFullName(InstalledMachine machine, ProjectDetails project, ProjectMachineDetails pmd)
        {
            return string.Format(ProjectMachineErrorCorrectorFileName, GetProjectMachinePath(machine, project, pmd) + machine.machineuniquecode, pmd.machinenumber);
        }
        #endregion

        #region EXCEL LINE PRINTING


        public static string GetMachineLinePrintFileName(InstalledMachine machine,ProjectDetails project,ProjectMachineDetails pmd,MachineTestDetails mtd, int point)
        {
            return GetMachineTestReportsPath(machine, project, pmd, mtd) + LinePrintFolder +
                "\\LinePrint_" + point.ToString() + ".xlsx";
        }
        public static string GetLinePrintSheetName()
        {
            return LinePrintOutputSheetName;
            //return LinePrintExcelSheetName;
        }


        private static string GetLinePrintTemplateFileName()
        {
            return LinePrintTemplateFileName;
        }

        #endregion

        #endregion         

        public static AlarmColors GetAlarmColors()
        {
            return alarmColors;
        }
        public static string GetErrorCorrectorReplacementMarker()
        {
            return ErrorCorrectorTemplateMUCodeTemplate;
        }  
        public static string GetMachineUIScriptObjectName(InstalledMachine mc)
        {
            return String.Format("userscript.{0}.{0}_UIConfig", mc.machineuniquecode);
        }
        public static string GetMachineLinePrinterObjectName(InstalledMachine mc)
        {
            return String.Format("userscript.{0}.{0}_LinePrinter", mc.machineuniquecode);
        }

        
    }
}
