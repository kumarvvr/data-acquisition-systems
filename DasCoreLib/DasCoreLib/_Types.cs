﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _DasCoreLib
{
    public struct AppSettings
    {
        public AppPaths appPaths;
        public DatabaseDetails dbaseSettings;
    }

    public struct AppPaths
    {
        public string appPath { get; set; }
        public string dataPath { get; set; }
        public string scriptPath { get; set; }
        public string configPath { get; set; }
    }

    public struct DatabaseDetails
    {
        public string driver, server, port, databaseName, userName, password;
    }
    
    public struct LookupTableDetails
    {
        public int lookupid { get; set; }
        public string lookupdesc { get; set; }
        public string keytext { get; set; }
        public string valuetext { get; set; }
        public string tablename { get; set; }
        public bool iscommon { get; set; }
    }

    public struct LookupDataRow
    {
        public double key { get; set; }
        public double value { get; set; }
    }

    public struct InstalledMachine
    {
        public string machineuniquecode { get; set; }
        public string codefilepath { get; set; }
        public string dllrootpath { get; set; }
        public string dllfilename { get; set; }
        public string dllmajorversion { get; set; }
        public string dllminorversion { get; set; }
        public string dllversiondesc { get; set; }
        public int machinecode { get; set; }
    }

    public struct DefaultConfigurationData
    {
        public string fk_machineCode { get; set; }
        public string paramname { get; set; }
        public string paramtype { get; set; }
        public string paramdescription { get; set; }
        public string paramunits { get; set; }
        public string defaultdata { get; set; }
        public int displayindex { get; set; }

    }

    public struct MachineDetailsEntry
    {
        public int fk_machinecode { get; set; }
        public string machinenumber { get; set; }
        public string machinereference { get; set; }
        public string paramname { get; set; }
        public string paramtype { get; set; }
        public string paramdescription { get; set; }
        public string paramunits { get; set; }
        public string defaultdata { get; set; }
        public int displayindex { get; set; }

    }

    public struct SpecifiedParamsDetails
    {
        public int fk_machinecode { get; set; }
        public string paramname { get; set; }
        public string paramtype { get; set; }
        public string paramdescription { get; set; }
        public string paramunits { get; set; }
        public string defaultdata { get; set; }
        public int displayindex { get; set; }

    }

    public struct TestSetupParamsDetails
    {
        public int fk_machinecode { get; set; }
        public string paramname { get; set; }
        public string paramtype { get; set; }
        public string paramdescription { get; set; }
        public string paramunits { get; set; }
        public string defaultdata { get; set; }
        public int displayindex { get; set; }

    }

    public struct MeasurementProfileDetails
    {
        private string _meastype;
        public int fk_machineCode { get; set; }
        public string paramname { get; set; }
        public string paramtype { get; set; }
        public string paramdescription { get; set; }
        public string paramunits { get; set; }
        public string defaultdata { get; set; }
        public string meastype 
        {
            get
            {
                return _meastype;
            }
            set
            {
                if (value == "DCV" || value == "DCA" || value == "ACV" || value == "ACA")
                {
                    this._MeasCode = 0;
                }
                if (value == "PULSE")
                    this._MeasCode = 1;
                if (value == "MANUAL")
                    this._MeasCode = 2;
                if (value == "MANUAL RANDOMISED")
                    this._MeasCode = 3;

                this._meastype = value;
            } 
        }
        public int fk_deviceid { get; set; }
        public string channelid { get; set; }
        public double sensormin { get; set; }
        public double sensormax { get; set; }
        public double outputmin { get; set; }
        public double outputmax { get; set; }
        public double scale { get; set; }
        public double offsetvalue { get; set; }
        public double highalarm { get; set; }
        public double lowalarm { get; set; }
        public int displayindex { get; set; }
        public int pointnum { get; set; }
        int _MeasCode;
        public double ComputeValue(double rawdata)
        {
            if (_MeasCode == 0) /* value == "DCV" || value == "DCA" || value == "ACV" || value == "ACA" */
            {
                // One form of computation.
                return rawdata * 1;
            }

            if (_MeasCode == 1)
                return rawdata * 1;

            if (_MeasCode == 2)
                return rawdata * 1;

            return rawdata;
        }
        public Hardware device { get; set; }
        public string configString;
        public string measString;
        public string resetString;
    }

    public struct ResultParamsStruc
    {
        public string fk_machineCode { get; set; }
        public string paramname { get; set; }
        public string paramtype { get; set; }
        public string paramdescription { get; set; }
        public string paramunits { get; set; }
        public string defaultdata { get; set; }
        public string pointnum { get; set; }
        public int displayindex { get; set; }

    }

    public struct ProjectDetails
    {
        public int fk_machinecode { get; set; }
        public int projectcode { get; set; }
        public string projectname { get; set; }
        public string projectdescription { get; set; }        
    }

    public struct DeviceDetails
    {
        public int deviceid { get; set; }
        public string devicename { get; set; }
        public string devicedesc { get; set; }
        public string connstring { get; set; }
    }

    public struct DeviceConfigDetails
    {
        public int fk_deviceid { get; set; }
        public string meastype { get; set; }
        public string resetstring { get; set; }
        public string configstring { get; set; }
        public string measstring { get; set; }
        public string addoncmd { get; set; }
    }

    public struct ChannelData
    {
        public double rawvalue;
        public double computedValue;
        public string pname;
        public int pindex;
    }

    public struct ProjectMachineDetails
    {
        public int fk_machinecode;
        public int fk_projectcode;
        public string machinenumber;
        public string machinedescription;
    }

    public struct MachineTestDetails
    {
        public int fk_machinecode;
        public int fk_projectcode;
        public string fk_machinenumber;
        public string testreference;
        public string testremarks;
        public string testdate;
    }

    public struct PropertyUILink
    {
        public int fk_machinecode { get; set; }
        public int placeholder { get; set; }
        public ParamObject objType;
        public string paramname;
    }


    // Enumerations

    public enum ParamObject
    {
        MEASUREMENTPROFILE=0,
        RESULTPARAMETERS
    }

    public enum AlarmType
    {
        HIGH_ALARM,
        CRITICALLY_HIGH_ALARM,
        LOW_ALARM,
        CRITICALLY_LOW_ALARM,
        NORMAL_STATE
    }

    public enum AlarmColors
    {
        HIGH_ALARM_COLOR,
        CRITICALLY_HIGH_ALARM_COLOR,
        LOW_ALARM_COLOR,
        CRITICALLY_LOW_ALARM_COLOR,
        NORMAL_STATE_COLOR
    }

    public struct AlarmStatus
    {
        public AlarmType alarmType;
        public AlarmColors alarmColor;
    }
}
