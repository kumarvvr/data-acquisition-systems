﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DasCoreLib;
using System.Data.Odbc;
using BHEL.PUMPSDAS.Datatypes;
using DasCoreUtilities;

namespace DasCoreLib
{
    public class DeviceManager
    {

        #region PRIVATE MEMBERS

        DatabaseManager _database;

        // Measurement types supported by this system.
        string[] _MEASUREMENTTYPES = { "DCV", "DCA", "ACV", "ACA", "DCmV", "DCmA", "ACmV", "ACmA", "RTD", "PULSE", "MANUAL", "MANUAL RANDOMISED", "TIME", "FREQUENCY", "SPEED_TIME", "SPEED_FREQUENCY","DIRECT"};

        List<string> MEASUREMENTTYPES = new List<string>();

        #region Queries

        //Queries related to Device Management

        const string qry_insertNewDeviceDetails =
            "insert into devicedetails (devicename,devicedesc,connstring) values('{0}','{1}','{2}') returning deviceid";

        const string qry_updateDeviceDetails =
            "update devicedetails set devicename='{0}',devicedesc='{1}',connstring='{2}' where deviceid={3}";

        const string qry_deleteDeviceDetails =
            "delete from devicedetails where devicename='{0}'";

        const string qry_retrieveAllDeviceDetails =
            "select deviceid,devicename,devicedesc,connstring from devicedetails";

        const string qry_retrieveAllDeviceIds =
            "select deviceid from devicedetails";

        const string qry_retrieveDeviceDetailsByDeviceId =
            "select deviceid,devicename,devicedesc,connstring from devicedetails where deviceid = {0}";



        // Queries related to Device Configuration Management

        const string qry_insertNewDeviceConfigDetails =
            "insert into deviceconfigdetails (fk_deviceid,meastype,resetstring,configstring,measstring,addoncmd) values({0},'{1}','{2}','{3}','{4}','{5}')";


        // Modified : 15.04.2013, By Raghavendra Kumar.
        // 
        // Bug : The foreign key constraint in deviceconfigdetails is a combination of the device ID and the 
        //          measurement type, so in the following query "qry_updateDeviceConfigDetails", we cannot overwrite
        //          the measurement type also.
        // Version 1.0 --> const string qry_updateDeviceConfigDetails ="update deviceconfigdetails set meastype='{0}',
        //                 resetstring='{1}',configstring='{2}',measstring='{3}',addoncmd='{4}' where fk_deviceid={5}";
        // Version 2.0 -->
        const string qry_updateDeviceConfigDetails =
            "update deviceconfigdetails set resetstring='{1}',configstring='{2}',measstring='{3}',addoncmd='{4}' where fk_deviceid={5} and meastype='{0}'";

        const string qry_deleteDeviceConfigDetails =
            "delete from deviceconfigdetails where fk_deviceid={0}";

        const string qry_retrieveDeviceConfigDetailsByDeviceId =
            "select meastype,resetstring,configstring,measstring,addoncmd from deviceconfigdetails where fk_deviceid={0}";

        #endregion

        #endregion

        /// <summary>
        /// Constructs a Device Manager object.
        /// </summary>
        /// <param name="db">A fully initialised and ready to use DatabaseManager object.</param>
        public DeviceManager(DatabaseManager db)
        {
            this._database = db;  
        }

        /// <summary>
        /// Creates a new Device in the system. Once a device has been successfully created in the system, it
        /// also adds default entries for various config details for the device in the appropriate tables.
        /// </summary>
        /// <param name="newDevice"> A DeviceDetails object that contains the details of the new device that is 
        /// to be created in the system. Note : The device ID field can be left empty. It is ignored by the 
        /// function.</param>
        /// <returns>Returns a DeviceDetails object that contains the newly created device.</returns>
        public DeviceDetails CreateNewDevice(DeviceDetails newDevice)
        {
            DeviceDetails device = new DeviceDetails();

            string qry = string.Format(qry_insertNewDeviceDetails, newDevice.devicename,newDevice.devicedesc,newDevice.connstring);

            OdbcDataReader reader = _database.ExecuteSelectQuery(qry);

            reader.Read();

            int devid =  DefaultConversion.GetIntValue(reader["deviceid"].ToString());

            device = newDevice;
            device.deviceid = devid;

            // Create default data for new device and insert it into the deviceconfigdetails table.
            List<DeviceConfigDetails> configDetails = new List<DeviceConfigDetails>(0);
            for (int i = 0; i < _MEASUREMENTTYPES.Length; i++)
            {
                DeviceConfigDetails detail = new DeviceConfigDetails();
                detail.addoncmd = "";
                detail.configstring = "";
                detail.fk_deviceid = device.deviceid;
                detail.measstring = "";
                detail.meastype = _MEASUREMENTTYPES[i];
                detail.resetstring = "";
                configDetails.Add(detail);
            }

            CreateNewDeviceConfigData(configDetails, device);

            return device;
        }

        /// <summary>
        /// Modifies the all details of a device based on the "deviceid" field in the DeviceDetails object sent.
        /// All the parameters are updated.
        /// </summary>
        /// <param name="details"> A DeviceDetails object for which the data is updated.</param>
        /// <returns>True if the modification is successful. Throws an exception if the modification was not successful.</returns>
        public bool UpdateExistingDevice(DeviceDetails details)
        {
            string qry = string.Format(qry_updateDeviceDetails, details.devicename, 
                                                                details.devicedesc, 
                                                                details.connstring,
                                                                details.deviceid        );
            _database.ExecuteUpdateQuery(qry);
            return true;
        }

        /// <summary>
        /// Returns a list of DeviceDetails objects which represent the list of all devices present in the system.
        /// </summary>
        /// <returns></returns>
        public List<DeviceDetails> GetAllDevices()
        {
            OdbcDataReader reader;
            List<DeviceDetails> devicelist = new List<DeviceDetails>();
            DeviceDetails details;
            string qry = string.Format(qry_retrieveAllDeviceDetails);
            reader = _database.ExecuteSelectQuery(qry);
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    details = new DeviceDetails();
                    details.deviceid = DefaultConversion.GetIntValue(reader["deviceid"].ToString());
                    details.devicename = reader["devicename"].ToString();
                    details.devicedesc = reader["devicedesc"].ToString();
                    details.connstring = reader["connstring"].ToString();

                    devicelist.Add(details);
                }

            }

            return devicelist;
        }

        /// <summary>
        /// Gets the details for the device with the given id. Throws an exception if the device was not found.
        /// </summary>
        /// <param name="deviceid"></param>
        /// <returns></returns>
        public DeviceDetails GetDeviceDetailsById(int deviceid)
        {
            OdbcDataReader reader;            
            DeviceDetails device = new DeviceDetails();
            string qry = string.Format(qry_retrieveDeviceDetailsByDeviceId, deviceid);
            reader = _database.ExecuteSelectQuery(qry);
            if (reader.HasRows)
            {
                reader.Read();
                    device = new DeviceDetails();
                    device.deviceid = DefaultConversion.GetIntValue(reader["deviceid"].ToString());
                    device.devicename = reader["devicename"].ToString();
                    device.devicedesc = reader["devicedesc"].ToString();
                    device.connstring = reader["connstring"].ToString();             
            }

            return device;
        }

        /// <summary>
        /// Returns a string list of "measurementtypes" supported by the system.
        /// </summary>
        /// <returns> List if strings that represent the measurement types supported by the system.</returns>
        public List<string> GetMeasurementTypes()
        {
            this.MEASUREMENTTYPES = this._MEASUREMENTTYPES.ToList();
            return this.MEASUREMENTTYPES;
        }

        /// <summary>
        /// For a given device, it updates the configuration details.
        /// </summary>
        /// <param name="details">List of DeviceConfigDetails that are to replace the existing DeviceConfigDetails.
        /// Note : Only those details given in the list are updated. Other details present in the database
        /// are not modified. This fucntion does not reset the details that are present in the database but are not
        /// given here.</param>
        /// <param name="device">The Device for which the config details are updated.</param>
        /// <returns></returns>
        public bool UpdateExistingDeviceConfig(List<DeviceConfigDetails> details, DeviceDetails device)
        {
            string qry = "";
            if (details.Count > 0)
            {
                for (int i = 0; i < details.Count; i++)
                {
                    qry += string.Format(qry_updateDeviceConfigDetails,
                                                                details[i].meastype,
                                                                details[i].resetstring,
                                                                details[i].configstring,
                                                                details[i].measstring,
                                                                details[i].addoncmd,
                                                                device.deviceid) + ";";
                }

                _database.ExecuteUpdateQuery(qry);
            }
            return true;
        }

        /// <summary>
        /// Retrieves the list of device configuration details for the give device.
        /// </summary>
        /// <param name="device">A DeviceDetails object for which the details are required.</param>
        /// <returns></returns>
        public List<DeviceConfigDetails> GetDeviceConfigDetails(DeviceDetails device)
        {
            OdbcDataReader reader;
            List<DeviceConfigDetails> deviceConfigList = new List<DeviceConfigDetails>();
            DeviceConfigDetails deviceConfig;
            string qry = string.Format(qry_retrieveDeviceConfigDetailsByDeviceId, device.deviceid);
            reader = _database.ExecuteSelectQuery(qry);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    deviceConfig = new DeviceConfigDetails();
                    deviceConfig.meastype = reader["meastype"].ToString();
                    deviceConfig.resetstring = reader["resetstring"].ToString();
                    deviceConfig.configstring = reader["configstring"].ToString();
                    deviceConfig.measstring = reader["measstring"].ToString();
                    deviceConfig.addoncmd = reader["addoncmd"].ToString();

                    deviceConfigList.Add(deviceConfig);
                }
            }

            return deviceConfigList;
        }

        public List<DeviceConfigDetails> GetDeviceConfigDetails(int fk_deviceid)
        {
            OdbcDataReader reader;
            List<DeviceConfigDetails> deviceConfigList = new List<DeviceConfigDetails>();
            DeviceConfigDetails deviceConfig;
            string qry = string.Format(qry_retrieveDeviceConfigDetailsByDeviceId, fk_deviceid);
            reader = _database.ExecuteSelectQuery(qry);

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    deviceConfig = new DeviceConfigDetails();
                    deviceConfig.meastype = reader["meastype"].ToString();
                    deviceConfig.resetstring = reader["resetstring"].ToString();
                    deviceConfig.configstring = reader["configstring"].ToString();
                    deviceConfig.measstring = reader["measstring"].ToString();
                    deviceConfig.addoncmd = reader["addoncmd"].ToString();

                    deviceConfigList.Add(deviceConfig);
                }
            }

            return deviceConfigList;
        }
        

        #region PRIVATE METHODS

        /// <summary>
        /// Inserts the device configuration data for a new device. Note : This function does not check for the 
        /// existance of the given device. An exception is thrown when this function is called for a device 
        /// which is already existing in the databse.
        /// </summary>
        /// <param name="details">A List of Device Config Details that are to be inserted for the present device.</param>
        /// <param name="device"> A DeviceDetails object that contains details of the device for which the config details
        /// are being added.</param>
        /// <returns>True : If addition was successful.</returns>
        private bool CreateNewDeviceConfigData(List<DeviceConfigDetails> details, DeviceDetails device)
        {
            string qry = "";
            if (details.Count > 0)
            {
                for (int i = 0; i < details.Count; i++)
                {
                    qry += string.Format(qry_insertNewDeviceConfigDetails, device.deviceid,
                                                                     details[i].meastype,
                                                                     details[i].resetstring,
                                                                     details[i].configstring,
                                                                     details[i].measstring,
                                                                     details[i].addoncmd) + ";";
                }

                _database.ExecuteInsertQuery(qry);
            }
            return true;
        }

        #endregion

        #region DEPRECATED

        // A list of device ID's can be constructed using the 
        // RetrieveAllDevices method.
        /* public List<string> RetrieveAllDeviceIds()
        {
            OdbcDataReader reader;
            List<string> deviceids = new List<string>();
            string qry = string.Format(qry_retrieveAllDeviceIds);
            reader = _database.ExecuteSelectQuery(qry);
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    deviceids.Add(reader["deviceid"].ToString());
                }

            }

            return deviceids;
        }*/

        #endregion
    }
}
