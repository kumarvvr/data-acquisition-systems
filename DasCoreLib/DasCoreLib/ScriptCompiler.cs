﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.CodeDom.Compiler;
using Microsoft.CSharp;
using System.IO;
using System.Reflection;
using System.Reflection.Emit;
using System.Reflection.Cache;

namespace DasCoreLib
{
    public class Compiler
    {
        // List of code source files.
        String outFileDir;
        String oName;

        public Compiler(String outfilename, String outfiledir)
        {
            this.outFileDir = outfiledir;
            this.oName = outfilename;
        }

        public String Compile(String inputfilepath)
        {

            CompilerResults res = null;
            CSharpCodeProvider provider = new CSharpCodeProvider();
            String errors = "";

            if (provider != null)
            {
                try
                {
                    Assembly asb = Assembly.Load("BHEL.PUMPSDAS.Datatypes, Version=1.0.0.0, Culture=neutral, PublicKeyToken=81d3de1e03a5907d"); 
                    CompilerParameters options = new CompilerParameters();
                    options.GenerateExecutable = false;
                    options.OutputAssembly = String.Format(outFileDir + oName);
                    options.GenerateInMemory = false;
                    options.TreatWarningsAsErrors = false;
                    options.ReferencedAssemblies.Add("System.dll");
                    options.ReferencedAssemblies.Add("System.Core.dll");
                    options.ReferencedAssemblies.Add("System.Xml.dll");
                    options.ReferencedAssemblies.Add("System.Xml.dll");
                    options.ReferencedAssemblies.Add(asb.Location);
                    res = provider.CompileAssemblyFromFile(options, inputfilepath);
                    errors = "";
                    if (res.Errors.HasErrors)
                    {
                        for (int i = 0; i < res.Errors.Count; i++)
                        {
                            errors += "\n " + i + ". " + res.Errors[i].ErrorText;
                        }
                    }
                }

                catch (Exception e)
                {
                    throw (new Exception("Compilation Failed with Exception!\n" + e.Message +
                        "\n Compilation errors : \n" + errors + "\n"));
                }

            }
            return errors;
        }
    }
}
