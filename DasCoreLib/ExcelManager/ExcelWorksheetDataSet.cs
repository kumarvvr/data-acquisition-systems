﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExcelManager
{
    public class ExcelWorksheetDataSet
    {
        string sheetName;
        Dictionary<string, string> markerReplacements;
        Dictionary<string, List<string>> columnReplacements;

        public ExcelWorksheetDataSet(string sheet, Dictionary<string, string> mr, Dictionary<string, List<string>> cr)
        {
            sheetName = sheet;
            this.markerReplacements = mr;
            this.columnReplacements = cr;
        }

        public Dictionary<string, string> GetMarkerReplacements()
        {
            return this.markerReplacements;
        }

        public Dictionary<string, List<string>> GetColumnReplacements()
        {
            return this.columnReplacements;
        }
    }
}
