﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExcelManager
{
    public class ExcelWorkBookDataSet
    {
        string fileName;

        Dictionary<string,ExcelWorksheetDataSet> worksheetsData;

        ExcelFile xlFile;

        public ExcelWorkBookDataSet(string fileFullName)
        {
            fileName = fileFullName;
            worksheetsData = new Dictionary<string,ExcelWorksheetDataSet>();
        }

        public void AddWorkSheetDataSet(string sheetName, ExcelWorksheetDataSet dataSet)
        {
            this.worksheetsData.Add(sheetName,dataSet);
        }

        public ExcelWorksheetDataSet GetWorkSheetDataSet(string sheetName)
        {
            ExcelWorksheetDataSet data = null;
            try
            {
                this.worksheetsData.TryGetValue(sheetName, out data);
            }
            catch (Exception ex)
            {
                return null;
            }

            return data;
        }

        public bool WriteData()
        {
            try
            {
                xlFile = new ExcelFile(this.fileName);
            

                for (int i = 0; i < worksheetsData.Count; i++)
                {
                    KeyValuePair<string, ExcelWorksheetDataSet> kvpair;
                    kvpair = worksheetsData.ElementAt(i);

                    ExcelWorksheetDataSet sheetDataSet = kvpair.Value;

                    xlFile.Open(false);

                    xlFile.SelectWorksheet(kvpair.Key);

                    if (sheetDataSet.GetMarkerReplacements() != null)
                        xlFile.FindAndReplaceMarkers(sheetDataSet.GetMarkerReplacements());

                    Dictionary<string, List<string>> cr = sheetDataSet.GetColumnReplacements();

                    if (cr != null)
                    {
                        for (int j = 0; j < cr.Count; j++)
                        {
                            int r, c;
                            if (xlFile.FindMarkedCell(out r, out c, cr.ElementAt(j).Key))
                            {
                                r = r + 1;
                                xlFile.FillDataInColumn(r, c, cr.ElementAt(j).Value);
                            }
                        }
                    }
                }

                xlFile.SaveFile();
                xlFile.CloseFile();
            }
            catch (Exception ex)
            {
                throw new Exception("Error accessing excel file... ERROR : " + ex.Message);
            }
            return true;
        }

        public string GetFileName()
        {
            return fileName;
        }
    }
}
