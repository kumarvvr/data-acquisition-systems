﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DasCoreLib;
using BHEL.PUMPSDAS.Datatypes;
using DasCoreUtilities;


namespace ExcelManager
{
    class DataExportManager
    {
        public DataExportManager()
        {
        }

        public ExcelWorkBookDataSet GenerateLinePrint(
                                    string date,
                                    string time,
                                    int pointNum,
                                    List<LinePrintData> data,
                                    string noise1,
                                    string noise2,
                                    string targetFileName,
                                    InstalledMachine mc,
                                    ProjectDetails pd,
                                    ProjectMachineDetails pmd,
                                    MachineTestDetails mtd)
        {
            ExcelWorkBookDataSet result = null;

            result = new ExcelWorkBookDataSet(targetFileName);

            Dictionary<string, string> markerReps = new Dictionary<string, string>();

            // Generate Marker replacements...
            markerReps.Add("<##projectname##>", pd.projectname);
            markerReps.Add("<##dllversiondesc##>", mc.dllversiondesc);
            markerReps.Add("<##machinenumber##>", pmd.machinenumber);
            markerReps.Add("<##date##>", date);
            markerReps.Add("<##testreference##>", mtd.testreference);
            markerReps.Add("<##pointnum##>", pointNum.ToString());
            markerReps.Add("<##time##>", time);

            //Generate column replacements

            Dictionary<string, List<string>> columnReps = new Dictionary<string, List<string>>();

            // Data for param description.
            List<string> paramdescdata = new List<string>();
            List<string> paramunitsdata = new List<string>();
            List<string> paramvaluesdata = new List<string>();

            LinePrintData dt;

            for (int i = 0; i < data.Count; i++)
            {
                dt = data[i];
                paramdescdata.Add(dt.paramdesc);
                paramunitsdata.Add(dt.paramunits);
                paramvaluesdata.Add(dt.value);
            }

            paramdescdata.Add("Noise Level 1");
            paramunitsdata.Add("dB");
            paramvaluesdata.Add(noise1);


            paramdescdata.Add("Noise Level 2");
            paramunitsdata.Add("dB");
            paramvaluesdata.Add(noise2);

            columnReps.Add("<##paramdesc##>", paramdescdata);
            columnReps.Add("<##units##>", paramunitsdata);
            columnReps.Add("<##value##>", paramvaluesdata);

            ExcelWorksheetDataSet worksheet = new ExcelWorksheetDataSet("lineprint", markerReps, columnReps);

            result.AddWorkSheetDataSet("lineprint", worksheet);

            return result;
        }


        public ExcelWorkBookDataSet GenerateReportData(
            string targetfilefullname,
            List<MachineDetailsEntry> md,
            List<TestSetupParamsDetails> tsp,
            List<SpecifiedParamsDetails> sp,
            List<MeasurementProfileDetails> mpDetails,
            List<ResultParamsDetails> rpDetails,
            Dictionary<int, List<ObjectData>> logsheetdatadict,
            Dictionary<int, List<ObjectData>> resultdatadict)
        {
            ExcelWorkBookDataSet reportdata = new ExcelWorkBookDataSet(targetfilefullname);
            ExcelWorksheetDataSet worksheet = null;

            #region Machine Details
            Dictionary<string, List<string>> columnReps = new Dictionary<string, List<string>>();

            List<string> paramnamedata = new List<string>();
            List<string> paramdescdata = new List<string>();
            List<string> paramtypedata = new List<string>();
            List<string> paramunitsdata = new List<string>();
            List<string> paramvaluesdata = new List<string>();



            if (md != null)
            {
                MachineDetailsEntry mpdt;
                for (int i = 0; i < md.Count; i++)
                {
                    mpdt = md[i];
                    paramnamedata.Add(mpdt.paramname);
                    paramdescdata.Add(mpdt.paramdescription);
                    paramtypedata.Add(mpdt.paramtype);
                    paramunitsdata.Add(mpdt.paramunits);
                    paramvaluesdata.Add(mpdt.defaultdata);
                }

                columnReps.Add("<##paramdesc##>", paramdescdata);
                columnReps.Add("<##paramname##>", paramnamedata);
                columnReps.Add("<##paramtype##>", paramtypedata);
                columnReps.Add("<##paramunits##>", paramunitsdata);
                columnReps.Add("<##paramvalue##>", paramvaluesdata);
            }
            worksheet = new ExcelWorksheetDataSet("machinedetails", null, columnReps);
            reportdata.AddWorkSheetDataSet("machinedetails", worksheet);
            #endregion

            #region Test Setup Parameters

            columnReps = new Dictionary<string, List<string>>();

            paramnamedata = new List<string>();
            paramdescdata = new List<string>();
            paramtypedata = new List<string>();
            paramunitsdata = new List<string>();
            paramvaluesdata = new List<string>();

            if (tsp != null)
            {
                TestSetupParamsDetails tspdt;

                for (int i = 0; i < tsp.Count; i++)
                {
                    tspdt = tsp[i];
                    paramnamedata.Add(tspdt.paramname);
                    paramdescdata.Add(tspdt.paramdescription);
                    paramtypedata.Add(tspdt.paramtype);
                    paramunitsdata.Add(tspdt.paramunits);
                    paramvaluesdata.Add(tspdt.defaultdata);
                }

                columnReps.Add("<##paramdesc##>", paramdescdata);
                columnReps.Add("<##paramname##>", paramnamedata);
                columnReps.Add("<##paramtype##>", paramtypedata);
                columnReps.Add("<##paramunits##>", paramunitsdata);
                columnReps.Add("<##paramvalue##>", paramvaluesdata);
            }
            worksheet = new ExcelWorksheetDataSet("testsetupparameters", null, columnReps);
            reportdata.AddWorkSheetDataSet("testsetupparameters", worksheet);

            #endregion

            #region Specified parameters

            columnReps = new Dictionary<string, List<string>>();

            paramnamedata = new List<string>();
            paramdescdata = new List<string>();
            paramtypedata = new List<string>();
            paramunitsdata = new List<string>();
            paramvaluesdata = new List<string>();

            if (sp != null)
            {
                SpecifiedParamsDetails spdt;

                for (int i = 0; i < sp.Count; i++)
                {
                    spdt = sp[i];
                    paramnamedata.Add(spdt.paramname);
                    paramdescdata.Add(spdt.paramdescription);
                    paramtypedata.Add(spdt.paramtype);
                    paramunitsdata.Add(spdt.paramunits);
                    paramvaluesdata.Add(spdt.defaultdata);
                }

                columnReps.Add("<##paramdesc##>", paramdescdata);
                columnReps.Add("<##paramname##>", paramnamedata);
                columnReps.Add("<##paramtype##>", paramtypedata);
                columnReps.Add("<##paramunits##>", paramunitsdata);
                columnReps.Add("<##paramvalue##>", paramvaluesdata);
            }
            worksheet = new ExcelWorksheetDataSet("specifiedparameters", null, columnReps);
            reportdata.AddWorkSheetDataSet("specifiedparameters", worksheet);

            #endregion

            #region Measurement profile

            columnReps = new Dictionary<string, List<string>>();

            paramnamedata = new List<string>();
            paramdescdata = new List<string>();
            paramvaluesdata = new List<string>();
            paramunitsdata = new List<string>();

            List<string> channeliddata = new List<string>();
            List<string> deviceiddata = new List<string>();
            List<string> sensormindata = new List<string>();
            List<string> sensormaxdata = new List<string>();
            List<string> outputmindata = new List<string>();
            List<string> outputmaxdata = new List<string>();
            List<string> scaledata = new List<string>();
            List<string> offsetdata = new List<string>();
            List<string> displayindexdata = new List<string>();

            if (mpDetails != null)
            {
                MeasurementProfileDetails mpDetailsdt;

                for (int i = 0; i < mpDetails.Count; i++)
                {
                    mpDetailsdt = mpDetails[i];
                    paramnamedata.Add(mpDetailsdt.paramname);
                    paramdescdata.Add(mpDetailsdt.paramdescription);
                    paramunitsdata.Add(mpDetailsdt.paramunits);
                    paramvaluesdata.Add(mpDetailsdt.defaultdata);

                    channeliddata.Add(mpDetailsdt.channelid);
                    deviceiddata.Add(mpDetailsdt.fk_deviceid.ToString());
                    sensormindata.Add(mpDetailsdt.sensormin.ToString());
                    sensormaxdata.Add(mpDetailsdt.sensormax.ToString());
                    outputmindata.Add(mpDetailsdt.sensormin.ToString());
                    outputmaxdata.Add(mpDetailsdt.sensormax.ToString());
                    scaledata.Add(mpDetailsdt.scale.ToString());
                    offsetdata.Add(mpDetailsdt.offsetvalue.ToString());
                    displayindexdata.Add(mpDetailsdt.displayindex.ToString());
                }

                columnReps.Add("<##paramdesc##>", paramdescdata);
                columnReps.Add("<##paramname##>", paramnamedata);
                columnReps.Add("<##paramvalue##>", paramvaluesdata);
                columnReps.Add("<##paramunits##>", paramunitsdata);
                columnReps.Add("<##channelid##>", channeliddata);
                columnReps.Add("<##deviceid##>", deviceiddata);
                columnReps.Add("<##sensormin##>", sensormindata);
                columnReps.Add("<##sensormax##>", sensormaxdata);
                columnReps.Add("<##outputmin##>", outputmindata);
                columnReps.Add("<##outputmax##>", outputmaxdata);
                columnReps.Add("<##scale##>", scaledata);
                columnReps.Add("<##offset##>", offsetdata);
                columnReps.Add("<##displayindex##>", displayindexdata);

            }
            worksheet = new ExcelWorksheetDataSet("measurementprofile", null, columnReps);
            reportdata.AddWorkSheetDataSet("measurementprofile", worksheet);

            #endregion

            #region Result Parameters

            columnReps = new Dictionary<string, List<string>>();

            paramnamedata = new List<string>();
            paramdescdata = new List<string>();
            paramtypedata = new List<string>();
            paramunitsdata = new List<string>();
            displayindexdata = new List<string>();
            if (rpDetails != null)
            {
                ResultParamsDetails rpDetailsdt;

                for (int i = 0; i < rpDetails.Count; i++)
                {
                    rpDetailsdt = rpDetails[i];
                    paramnamedata.Add(rpDetailsdt.paramname);
                    paramdescdata.Add(rpDetailsdt.paramdescription);
                    paramtypedata.Add(rpDetailsdt.paramtype);
                    paramunitsdata.Add(rpDetailsdt.paramunits);
                    displayindexdata.Add(rpDetailsdt.displayindex.ToString());
                }

                columnReps.Add("<##paramdesc##>", paramdescdata);
                columnReps.Add("<##paramname##>", paramnamedata);
                columnReps.Add("<##paramtype##>", paramtypedata);
                columnReps.Add("<##paramunits##>", paramunitsdata);
                columnReps.Add("<##displayindex##>", displayindexdata);
            }
            worksheet = new ExcelWorksheetDataSet("resultparameters", null, columnReps);
            reportdata.AddWorkSheetDataSet("resultparameters", worksheet);

            #endregion

            #region Logsheet Data

            paramnamedata = new List<string>();
            displayindexdata = new List<string>();
            columnReps = new Dictionary<string, List<string>>();
            if (logsheetdatadict != null && logsheetdatadict.Count > 0)
            {
                for (int j = 1; j <= logsheetdatadict.Count; j++)
                {
                    List<ObjectData> logsheetdata = logsheetdatadict[j];

                    #region Logsheet Data Addition

                    paramvaluesdata = new List<string>();
                    ObjectData objdt;

                    for (int i = 0; i < logsheetdata.Count; i++)
                    {
                        objdt = logsheetdata[i];
                        paramvaluesdata.Add(objdt.pvalue);
                        if (j == 1)
                        {
                            paramnamedata.Add(objdt.pname);
                            displayindexdata.Add(objdt.pindex.ToString());
                        }
                    }

                    if (j == 1)
                    {
                        columnReps.Add("<##paramname##>", paramnamedata);
                        columnReps.Add("<##displayindex##>", displayindexdata);
                    }
                    columnReps.Add("<##point" + j + "##>", paramvaluesdata);

                    #endregion
                }
            }

            worksheet = new ExcelWorksheetDataSet("logsheetdata", null, columnReps);
            reportdata.AddWorkSheetDataSet("logsheetdata", worksheet);
            #endregion

            #region Report Data

            columnReps = new Dictionary<string, List<string>>();
            displayindexdata = new List<string>();
            paramnamedata = new List<string>();
            if (resultdatadict != null && resultdatadict.Count > 0)
            {
                for (int j = 1; j <= resultdatadict.Count; j++)
                {
                    List<ObjectData> resultdata = resultdatadict[j];

                    #region Result Data Addition

                    paramvaluesdata = new List<string>();
                    ObjectData objdt;

                    for (int i = 0; i < resultdata.Count; i++)
                    {
                        objdt = resultdata[i];
                        paramvaluesdata.Add(objdt.pvalue);
                        if (j == 1)
                        {
                            paramnamedata.Add(objdt.pname);
                            displayindexdata.Add(objdt.pindex.ToString());
                        }
                    }

                    if (j == 1)
                    {
                        columnReps.Add("<##paramname##>", paramnamedata);
                        columnReps.Add("<##displayindex##>", displayindexdata);
                    }

                    columnReps.Add("<##point" + j + "##>", paramvaluesdata);

                    #endregion

                }
            }
            worksheet = new ExcelWorksheetDataSet("reportdata", null, columnReps);
            reportdata.AddWorkSheetDataSet("reportdata", worksheet);

            #endregion

            return reportdata;

        }
    }

    public class TestReportGenerator
    {
        DasCore dc;
        DataExportManager dexport;

        public TestReportGenerator(DasCore dc)
        {
            this.dc = dc;
            this.dexport = new DataExportManager();
        }

        public ExcelWorkBookDataSet GenerateReportWorkBook(
            string targetFileName,
            InstalledMachine mc,
            ProjectDetails pd,
            ProjectMachineDetails pmd,
            MachineTestDetails mtd,
            bool isActualData
            )
        {
            ExcelWorkBookDataSet result = null;

            List<MachineDetailsEntry> md = dc.GetMachineDetailsEntries(mc);

            List<TestSetupParamsDetails> tsp = dc.GetTestSetupParameters(mc);

            List<SpecifiedParamsDetails> sp = dc.GetProjectSpecifiedParameters(mc, pd); //dc.GetSpecifiedParameters(mc);

            List<MeasurementProfileDetails> mpDetails = dc.GetMeasurementProfileDetails(mc);

            List<ResultParamsDetails> rpDetails = dc.GetResultParamsDetails(mc);

            Dictionary<int, List<ObjectData>> logsheetdatadict = new Dictionary<int, List<ObjectData>>();

            Dictionary<int, List<ObjectData>> resultdatadict = new Dictionary<int, List<ObjectData>>();

            List<ObjectData> testmeasurementpoint, testresultpoint;

            DateTime dt;

            string n1, n2;

            int npoints;

            // Fetching and generating log sheet data..                

            testmeasurementpoint = null;

            testresultpoint = null;

            npoints = dc.GetNumberOfRecordedPoints(mc, pd, pmd, mtd);

            if (npoints > 0)
            {
                for (int i = 1; i <= npoints; i++)
                {
                    testmeasurementpoint = new List<ObjectData>();
                    testresultpoint = new List<ObjectData>();
                    ObjectData time, noise1, noise2;
                    dc.GetMeasurementDataPoint(mc, pd, pmd, mtd, i, isActualData, out testmeasurementpoint, out testresultpoint, out dt, out n1, out n2);


                    time = new ObjectData();
                    time.pindex = -1;
                    time.pname = "TIME";
                    time.pvalue = DateTimeFormatting.GetTimeStamp(dt, false);

                    noise1 = new ObjectData();
                    noise1.pindex = -1;
                    noise1.pname = "Noise 1";
                    noise1.pvalue = n1;

                    noise2 = new ObjectData();
                    noise2.pindex = -1;
                    noise2.pname = "Noise 2";
                    noise2.pvalue = n2;

                    testmeasurementpoint.Add(time);
                    testmeasurementpoint.Add(noise1);
                    testmeasurementpoint.Add(noise2);

                    // For each of the point add the values to the dictionary..

                    logsheetdatadict.Add(i, testmeasurementpoint);
                    resultdatadict.Add(i, testresultpoint);
                }
            }

            result = dexport.GenerateReportData(targetFileName, md, tsp, sp, mpDetails, rpDetails, logsheetdatadict, resultdatadict);

            return result;
        }

        public ExcelWorkBookDataSet GenerateLinePrintWorkBook(
            string date,
            string time,
            int pointNum,
            List<LinePrintData> data,
            string noise1,
            string noise2,
            string targetFileName,
            InstalledMachine mc,
            ProjectDetails pd,
            ProjectMachineDetails pmd,
            MachineTestDetails mtd
            )
        {
            return dexport.GenerateLinePrint(date, time, pointNum, data, noise1, noise2, targetFileName, mc, pd, pmd, mtd);
        }


        public ExcelWorkBookDataSet GenerateReportTemplateWorkBook(string targetFileName, InstalledMachine mc)
        {
            ExcelWorkBookDataSet result = null;

            List<MachineDetailsEntry> md = dc.GetMachineDetailsEntries(mc);

            List<TestSetupParamsDetails> tsp = dc.GetTestSetupParameters(mc);

            List<SpecifiedParamsDetails> sp = dc.GetSpecifiedParameters(mc);

            List<MeasurementProfileDetails> mpDetails = dc.GetMeasurementProfileDetails(mc);

            List<ResultParamsDetails> rpDetails = dc.GetResultParamsDetails(mc);

            Dictionary<int, List<ObjectData>> logsheetdatadict = new Dictionary<int, List<ObjectData>>();

            Dictionary<int, List<ObjectData>> resultdatadict = new Dictionary<int, List<ObjectData>>();
            
            List<ObjectData> testmeasurementpoint, testresultpoint;

            // Fetching and generating log sheet data..                
            
            for (int i = 1; i <= 1; i++)
            {
                testmeasurementpoint = new List<ObjectData>();
                testresultpoint = new List<ObjectData>();
                ObjectData time, noise1, noise2;
                ObjectData data;
                for (int j = 0; j < mpDetails.Count; j++)
                {
                    data = new ObjectData();
                    data.pindex = mpDetails[j].displayindex;
                    data.pname = mpDetails[j].paramname;
                    data.pvalue = "*";
                    testmeasurementpoint.Add(data);
                }

                for (int j = 0; j < rpDetails.Count; j++)
                {
                    data = new ObjectData();
                    data.pindex = rpDetails[j].displayindex;
                    data.pname = rpDetails[j].paramname;
                    data.pvalue = "*";
                    testresultpoint.Add(data);
                }


                time = new ObjectData();
                time.pindex = -1;
                time.pname = "TIME";
                time.pvalue = "*";

                noise1 = new ObjectData();
                noise1.pindex = -1;
                noise1.pname = "Noise 1";
                noise1.pvalue = "*";

                noise2 = new ObjectData();
                noise2.pindex = -1;
                noise2.pname = "Noise 2";
                noise2.pvalue = "*";

                testmeasurementpoint.Add(time);
                testmeasurementpoint.Add(noise1);
                testmeasurementpoint.Add(noise2);

                // For each of the point add the values to the dictionary..

                logsheetdatadict.Add(i, testmeasurementpoint);
                resultdatadict.Add(i, testresultpoint);
            }
            result = dexport.GenerateReportData(targetFileName, md, tsp, sp, mpDetails, rpDetails, logsheetdatadict, resultdatadict);

            return result;
        }

    }
}