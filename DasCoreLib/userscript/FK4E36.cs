﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;
using Datatypes;
using DasCoreLib;

/*
namespace userscript
{
    namespace FK4E36
    {
        // Note This will be an external script file.
        public class MachineDetails : MachineDetailsBase
        {
            public string FrameName
            {
                get;
                set;
            }

            public string FrameType
            {
                get;
                set;
            }

            public MachineDetails()
                : base()
            {
            }
        }
        public class MeasurementProfile : MeasurementProfileBase
        {
            public double temp { get; set; }
            public double pressure { get; set; }
            public MeasurementProfile() : base() { }
        }
        public class ResultParameters : ResultParamsBase
        {
            public double power { get; set; }
            public double efficiency { get; set; }
            public ResultParameters() : base() { }
        }
        public class SpecifiedParameters : SpecifiedParamsBase
        {
            public double flow { get; set; }
            public double speed { get; set; }
            public SpecifiedParameters() : base() { }
        }
        public class TestSetupParameters : TestSetupParamsBase
        {
            public double suctionPipeID { get; set; }
            public double dischargePipeID { get; set; }
            public TestSetupParameters() : base() { }
        }
        public class Machine : MachineDescriptor
        {

            //md - Machine details
            //mp - Measurement Parameters
            //rp - Result Parameters
            //sp - SpecifiedParamaters
            //tsp - TestSetupParameters

            // The framework creates the above objects and call this class
            // with the objects in the constructor.
            // We can override the compute function to do the necessary stuff.

            // Constructor
            public Machine(MachineDetailsBase d1, MeasurementProfileBase d2, ResultParamsBase d3, SpecifiedParamsBase d4, TestSetupParamsBase d5) : base(d1, d2, d3, d4, d5) { }

            public bool Compute()
            {
                // Modify the objects here
                rp.SetDoubleValue("power", sp.GetDoubleValue("flow") * 200.5);
                rp.SetDoubleValue("efficiency", 250.2);

                return base.Compute(false);
            }

        }
    }
}
*/