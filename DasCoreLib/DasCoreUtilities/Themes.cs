﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DasCoreUtilities
{
    public struct AlarmColor
    {
        public int r;
        public int g;
        public int b;
        public int a;
    }

}
