﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DasCoreUtilities
{
    public class DateTimeFormatting
    {
        public static  string GetDateTimeStamp(bool isForFileName)
        {
            DateTime now = DateTime.Now;
            return GetDateStamp(now, isForFileName) + " " + GetTimeStamp(now, isForFileName);
        }
        public static string GetDateStamp(DateTime now, bool isForFileName)
        {
            int date, month, year;
            string d, m, y;

            date = now.Day;
            month = now.Month;
            year = now.Year;

            d = date.ToString();
            m = month.ToString();
            y = year.ToString();

            if (date < 10)
                d = "0" + d;

            if (month < 10)
                m = "0" + m;
            if (isForFileName)
                return d + "_" + m + "_" + y;
            return d + "/" + m + "/" + y;
        }
        public static string GetTimeStamp(DateTime now, bool isForFileName)
        {
            int hour, minute, second;
            string h, mn, sec;



            hour = now.Hour;
            minute = now.Minute;
            second = now.Second;



            h = hour.ToString();
            mn = minute.ToString();
            sec = second.ToString();


            if (hour < 10)
                h = "0" + h;

            if (minute < 10)
                mn = "0" + mn;

            if (second < 10)
                sec = "0" + sec;
            if (isForFileName)
                return h + "-" + mn + "-" + sec;
            return h + ":" + mn + ":" + sec;
        }
    }
}
