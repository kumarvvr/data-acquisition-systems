﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace DasCoreUtilities
{
    public class ProcessHelper
    {
        public static void OpenProcess(string filename, string args, bool isWaitForExit, ProcessWindowStyle style, bool isShellProcess = true)
        {
            Process process = new Process();
            ProcessStartInfo pStartInfo = new ProcessStartInfo();
            pStartInfo.ErrorDialog = true;
            pStartInfo.CreateNoWindow = false;
            pStartInfo.LoadUserProfile = true;
            pStartInfo.UseShellExecute = isShellProcess;
            pStartInfo.WindowStyle = style;
            pStartInfo.FileName = filename;
            pStartInfo.Arguments = args;

            process.StartInfo = pStartInfo;

            process.Start();
            if (isWaitForExit)
                process.WaitForExit();
        }
    }
}
