﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DasCoreUtilities
{
    public class DefaultConversion
    {
        public static double GetDoubleValue(string rawdata)
        {
            if (rawdata == "" || rawdata == null)
            {
                return -1.0;
            }
            else return double.Parse(rawdata);
        }

        public static int GetIntValue(string rawdata)
        {
            if (rawdata == "" || rawdata == null)
            {
                return -1;
            }
            else return int.Parse(rawdata);
        }

        public static bool GetBoolValue(string rawdata)
        {
            if (rawdata == bool.FalseString)
                return false;
            if (rawdata == bool.TrueString)
                return true;
            return false;
        }

        public static string GetStringValue(string rawdata)
        {
            if (rawdata == null)
                return "";
            return rawdata;
        }
    }
}
