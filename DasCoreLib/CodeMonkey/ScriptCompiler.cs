﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.CodeDom.Compiler;
using Microsoft.CSharp;
using System.Reflection;

namespace CodeMonkey
{
    public class Compiler
    {
        // List of code source files.
        String outFileDir;
        String oName;

        public Compiler(String outfilename, String outfiledir)
        {
            this.outFileDir = outfiledir;
            this.oName = outfilename;
        }

        public String Compile(String inputfilepath, bool isGeneratedInMemory)
        {

            CompilerResults res = null;
            CSharpCodeProvider provider = new CSharpCodeProvider();
            String errors = "";

            if (provider != null)
            {
                try
                {
                    Assembly asb = Assembly.Load("BHEL.PUMPSDAS.Datatypes, Version=1.0.0.0, Culture=neutral, PublicKeyToken=81d3de1e03a5907d"); 
                    CompilerParameters options = new CompilerParameters();
                    options.GenerateExecutable = false;
                    if(!isGeneratedInMemory)
                    options.OutputAssembly = String.Format(outFileDir + oName);
                    options.GenerateInMemory = isGeneratedInMemory;
                    options.TreatWarningsAsErrors = false;
                    options.ReferencedAssemblies.Add("System.dll");
                    options.ReferencedAssemblies.Add("System.Core.dll");
                    options.ReferencedAssemblies.Add("System.Xml.dll");
                    options.ReferencedAssemblies.Add("System.Xml.dll");
                    options.ReferencedAssemblies.Add("System.Windows.Forms.dll");
                    options.ReferencedAssemblies.Add(asb.Location);
                    res = provider.CompileAssemblyFromFile(options, inputfilepath);
                    
                    errors = "";
                    if (res.Errors.HasErrors)
                    {
                        for (int i = 0; i < res.Errors.Count; i++)
                        {
                            errors += "Line : " + res.Errors[i].Line + "--------------------------------" + Environment.NewLine;
                            errors += res.Errors[i].ToString() + Environment.NewLine;
                            errors += i + ". " + res.Errors[i].ErrorText+ Environment.NewLine;;
                            errors += "------------------------------------------------------------------------------------------"+ Environment.NewLine;
                        }
                    }
                }

                catch (Exception e)
                {
                    throw (new Exception("Compilation Failed with Exception!\n" + e.Message +
                        "\n Compilation errors : \n" + errors + "\n"));
                }

            }
            return errors;
        }

        public Assembly CompileInMemory(String codeFilePath,out bool isSuccessful,out string errors)
        {
            CompilerResults res = null;
            CSharpCodeProvider provider = new CSharpCodeProvider();
            //String errors = "";
            errors = "";
            isSuccessful = false;
            if (provider != null)
            {
                try
                {
                    Assembly asb = Assembly.Load("BHEL.PUMPSDAS.Datatypes, Version=1.0.0.0, Culture=neutral, PublicKeyToken=81d3de1e03a5907d");
                    CompilerParameters options = new CompilerParameters();
                    options.GenerateExecutable = false;
                    options.OutputAssembly = String.Format(outFileDir + oName);
                    options.GenerateInMemory = true;
                    options.TreatWarningsAsErrors = false;
                    options.ReferencedAssemblies.Add("System.dll");
                    options.ReferencedAssemblies.Add("System.Core.dll");
                    options.ReferencedAssemblies.Add("System.Xml.dll");
                    options.ReferencedAssemblies.Add("System.Xml.dll");
                    options.ReferencedAssemblies.Add("System.Drawing.dll");
                    options.ReferencedAssemblies.Add("System.Windows.Forms.dll");
                    options.ReferencedAssemblies.Add(asb.Location);
                    res = provider.CompileAssemblyFromFile(options, codeFilePath);

                    errors = "";
                    if (res.Errors.HasErrors)
                    {
                        for (int i = 0; i < res.Errors.Count; i++)
                        {
                            errors += "\n " + i + ". " + res.Errors[i].ErrorText;
                        }
                        isSuccessful = false;
                    }
                }

                catch (Exception e)
                {
                    throw (new Exception("Compilation Failed with Exception!\n" + e.Message +
                        "\n Compilation errors : \n" + errors + "\n"));                    
                }
            }

            isSuccessful = true;
            Assembly compiledAssembly = res.CompiledAssembly;

            return compiledAssembly;
        }
    }
}
