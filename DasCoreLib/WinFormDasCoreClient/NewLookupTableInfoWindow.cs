﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WinFormDasCoreClient
{
    public partial class NewLookupTableInfoWindow : Form
    {      

        public string _tabledesc;
        public string _keyText;
        public string _valueText;

        public NewLookupTableInfoWindow()
        {
            InitializeComponent();
            DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        private void btnCancelTable_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        private void btnSaveTable_Click(object sender, EventArgs e)
        {
            if (this.tbltdesc.Text == "" || this.tbltkname.Text == "" || this.tbltvname.Text == "")
            {
                MessageBox.Show("Invalid Data Entered !!");
                return;
            }

            this._keyText = this.tbltkname.Text;
            this._valueText = this.tbltvname.Text;
            this._tabledesc = this.tbltdesc.Text;
            DialogResult = System.Windows.Forms.DialogResult.OK;

            this.Close();
        }


    }
}
