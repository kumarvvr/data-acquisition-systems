﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DasCoreLib;
using BHEL.PUMPSDAS.Datatypes;

namespace WinFormDasCoreClient
{
    public partial class ModifyProjWindow : Form
    {
        InstalledMachine mc;
        ProjectDetails modifiedProj;

        public ModifyProjWindow(InstalledMachine mc)
        {
            InitializeComponent();
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Text = "Modify Project - " + mc.machineuniquecode;
            this.mc = mc;

        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            modifiedProj = new ProjectDetails();
            if (tbProjDesc.Text == "" || tbProjName.Text == "")
            {
                MessageBox.Show("Invalid Data Entered!");
                return;
            }

            else
            {
                modifiedProj.fk_machinecode = mc.machinecode;
                modifiedProj.projectname = tbProjName.Text;
                modifiedProj.projectdescription = tbProjDesc.Text;
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                this.Close();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        public ProjectDetails GetNewProjectDetails()
        {
            return this.modifiedProj;
        }

    }
}
