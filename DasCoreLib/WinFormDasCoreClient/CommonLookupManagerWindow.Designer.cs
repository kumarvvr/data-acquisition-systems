﻿namespace WinFormDasCoreClient
{
    partial class CommonLookupManagerWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CommonLookupManagerWindow));
            this.label2 = new System.Windows.Forms.Label();
            this.dgvAvailableLookups = new System.Windows.Forms.DataGridView();
            this.dgvEditLookup = new System.Windows.Forms.DataGridView();
            this.btnModify = new System.Windows.Forms.Button();
            this.btnAddNew = new System.Windows.Forms.Button();
            this.btnSelect = new System.Windows.Forms.Button();
            this.btnDiscard = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.gbEditLookupData = new System.Windows.Forms.GroupBox();
            this.tbLookupTableValueLabel = new System.Windows.Forms.TextBox();
            this.tbLookupTableKeylabel = new System.Windows.Forms.TextBox();
            this.tbLookupTblDesc = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.gbAvlLookupTables = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAvailableLookups)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEditLookup)).BeginInit();
            this.gbEditLookupData.SuspendLayout();
            this.gbAvlLookupTables.SuspendLayout();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Wheat;
            this.label2.Location = new System.Drawing.Point(520, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 29);
            this.label2.TabIndex = 0;
            // 
            // dgvAvailableLookups
            // 
            this.dgvAvailableLookups.AllowUserToAddRows = false;
            this.dgvAvailableLookups.AllowUserToDeleteRows = false;
            this.dgvAvailableLookups.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.ColumnHeader;
            this.dgvAvailableLookups.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.DarkGray;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvAvailableLookups.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvAvailableLookups.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAvailableLookups.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.dgvAvailableLookups.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvAvailableLookups.Location = new System.Drawing.Point(7, 32);
            this.dgvAvailableLookups.MultiSelect = false;
            this.dgvAvailableLookups.Name = "dgvAvailableLookups";
            this.dgvAvailableLookups.RowTemplate.Height = 30;
            this.dgvAvailableLookups.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvAvailableLookups.Size = new System.Drawing.Size(655, 610);
            this.dgvAvailableLookups.TabIndex = 1;
            this.dgvAvailableLookups.SelectionChanged += new System.EventHandler(this.dgvAvailableLookups_SelectionChanged);
            // 
            // dgvEditLookup
            // 
            this.dgvEditLookup.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.DarkGray;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvEditLookup.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvEditLookup.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvEditLookup.Location = new System.Drawing.Point(6, 160);
            this.dgvEditLookup.Name = "dgvEditLookup";
            this.dgvEditLookup.RowTemplate.Height = 30;
            this.dgvEditLookup.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgvEditLookup.Size = new System.Drawing.Size(305, 482);
            this.dgvEditLookup.TabIndex = 1;
            // 
            // btnModify
            // 
            this.btnModify.BackColor = System.Drawing.Color.Transparent;
            this.btnModify.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnModify.BackgroundImage")));
            this.btnModify.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnModify.FlatAppearance.BorderSize = 0;
            this.btnModify.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.btnModify.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btnModify.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnModify.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnModify.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnModify.Location = new System.Drawing.Point(522, 648);
            this.btnModify.Name = "btnModify";
            this.btnModify.Size = new System.Drawing.Size(140, 30);
            this.btnModify.TabIndex = 2;
            this.btnModify.Text = "Modify";
            this.btnModify.UseVisualStyleBackColor = false;
            this.btnModify.Click += new System.EventHandler(this.btnModify_Click);
            // 
            // btnAddNew
            // 
            this.btnAddNew.BackColor = System.Drawing.Color.Transparent;
            this.btnAddNew.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnAddNew.BackgroundImage")));
            this.btnAddNew.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAddNew.FlatAppearance.BorderSize = 0;
            this.btnAddNew.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.btnAddNew.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btnAddNew.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnAddNew.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddNew.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnAddNew.Location = new System.Drawing.Point(376, 648);
            this.btnAddNew.Name = "btnAddNew";
            this.btnAddNew.Size = new System.Drawing.Size(140, 30);
            this.btnAddNew.TabIndex = 2;
            this.btnAddNew.Text = "Add New Table";
            this.btnAddNew.UseVisualStyleBackColor = false;
            this.btnAddNew.Click += new System.EventHandler(this.btnAddNew_Click);
            // 
            // btnSelect
            // 
            this.btnSelect.BackColor = System.Drawing.Color.Transparent;
            this.btnSelect.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnSelect.BackgroundImage")));
            this.btnSelect.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSelect.FlatAppearance.BorderSize = 0;
            this.btnSelect.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btnSelect.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btnSelect.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnSelect.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSelect.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnSelect.Location = new System.Drawing.Point(7, 648);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(140, 30);
            this.btnSelect.TabIndex = 2;
            this.btnSelect.Text = "Select";
            this.btnSelect.UseVisualStyleBackColor = false;
            this.btnSelect.Click += new System.EventHandler(this.btnSelect_Click);
            // 
            // btnDiscard
            // 
            this.btnDiscard.BackColor = System.Drawing.Color.Transparent;
            this.btnDiscard.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnDiscard.BackgroundImage")));
            this.btnDiscard.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnDiscard.FlatAppearance.BorderSize = 0;
            this.btnDiscard.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.btnDiscard.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btnDiscard.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnDiscard.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDiscard.ForeColor = System.Drawing.SystemColors.InfoText;
            this.btnDiscard.Location = new System.Drawing.Point(6, 648);
            this.btnDiscard.Name = "btnDiscard";
            this.btnDiscard.Size = new System.Drawing.Size(140, 30);
            this.btnDiscard.TabIndex = 2;
            this.btnDiscard.Text = "Discard Changes";
            this.btnDiscard.UseVisualStyleBackColor = false;
            this.btnDiscard.Click += new System.EventHandler(this.btnDiscard_Click);
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.Transparent;
            this.btnSave.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnSave.BackgroundImage")));
            this.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSave.FlatAppearance.BorderSize = 0;
            this.btnSave.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnSave.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnSave.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.ForeColor = System.Drawing.SystemColors.InfoText;
            this.btnSave.Location = new System.Drawing.Point(171, 648);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(140, 30);
            this.btnSave.TabIndex = 2;
            this.btnSave.Text = "Save Data";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // gbEditLookupData
            // 
            this.gbEditLookupData.Controls.Add(this.tbLookupTableValueLabel);
            this.gbEditLookupData.Controls.Add(this.tbLookupTableKeylabel);
            this.gbEditLookupData.Controls.Add(this.tbLookupTblDesc);
            this.gbEditLookupData.Controls.Add(this.label4);
            this.gbEditLookupData.Controls.Add(this.label3);
            this.gbEditLookupData.Controls.Add(this.label1);
            this.gbEditLookupData.Controls.Add(this.btnSave);
            this.gbEditLookupData.Controls.Add(this.btnDiscard);
            this.gbEditLookupData.Controls.Add(this.dgvEditLookup);
            this.gbEditLookupData.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbEditLookupData.ForeColor = System.Drawing.SystemColors.ControlText;
            this.gbEditLookupData.Location = new System.Drawing.Point(683, 2);
            this.gbEditLookupData.Name = "gbEditLookupData";
            this.gbEditLookupData.Size = new System.Drawing.Size(318, 688);
            this.gbEditLookupData.TabIndex = 3;
            this.gbEditLookupData.TabStop = false;
            this.gbEditLookupData.Text = "Edit Table Data";
            // 
            // tbLookupTableValueLabel
            // 
            this.tbLookupTableValueLabel.Location = new System.Drawing.Point(143, 117);
            this.tbLookupTableValueLabel.Name = "tbLookupTableValueLabel";
            this.tbLookupTableValueLabel.Size = new System.Drawing.Size(167, 29);
            this.tbLookupTableValueLabel.TabIndex = 4;
            // 
            // tbLookupTableKeylabel
            // 
            this.tbLookupTableKeylabel.Location = new System.Drawing.Point(143, 73);
            this.tbLookupTableKeylabel.Name = "tbLookupTableKeylabel";
            this.tbLookupTableKeylabel.Size = new System.Drawing.Size(167, 29);
            this.tbLookupTableKeylabel.TabIndex = 4;
            // 
            // tbLookupTblDesc
            // 
            this.tbLookupTblDesc.Location = new System.Drawing.Point(143, 32);
            this.tbLookupTblDesc.Name = "tbLookupTblDesc";
            this.tbLookupTblDesc.Size = new System.Drawing.Size(167, 29);
            this.tbLookupTblDesc.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label4.Location = new System.Drawing.Point(41, 117);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(82, 17);
            this.label4.TabIndex = 3;
            this.label4.Text = "Value Label :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label3.Location = new System.Drawing.Point(52, 73);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 17);
            this.label3.TabIndex = 3;
            this.label3.Text = "Key Label :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label1.Location = new System.Drawing.Point(6, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(117, 17);
            this.label1.TabIndex = 3;
            this.label1.Text = "Table Description :";
            // 
            // gbAvlLookupTables
            // 
            this.gbAvlLookupTables.Controls.Add(this.btnSelect);
            this.gbAvlLookupTables.Controls.Add(this.btnAddNew);
            this.gbAvlLookupTables.Controls.Add(this.btnModify);
            this.gbAvlLookupTables.Controls.Add(this.dgvAvailableLookups);
            this.gbAvlLookupTables.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbAvlLookupTables.ForeColor = System.Drawing.SystemColors.ControlText;
            this.gbAvlLookupTables.Location = new System.Drawing.Point(1, 2);
            this.gbAvlLookupTables.Name = "gbAvlLookupTables";
            this.gbAvlLookupTables.Size = new System.Drawing.Size(668, 687);
            this.gbAvlLookupTables.TabIndex = 4;
            this.gbAvlLookupTables.TabStop = false;
            this.gbAvlLookupTables.Text = "Available Tables";
            // 
            // CommonLookupManagerWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1003, 693);
            this.Controls.Add(this.gbAvlLookupTables);
            this.Controls.Add(this.gbEditLookupData);
            this.Controls.Add(this.label2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CommonLookupManagerWindow";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "System Data Tables";
            ((System.ComponentModel.ISupportInitialize)(this.dgvAvailableLookups)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEditLookup)).EndInit();
            this.gbEditLookupData.ResumeLayout(false);
            this.gbEditLookupData.PerformLayout();
            this.gbAvlLookupTables.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView dgvAvailableLookups;
        private System.Windows.Forms.DataGridView dgvEditLookup;
        private System.Windows.Forms.Button btnModify;
        private System.Windows.Forms.Button btnAddNew;
        private System.Windows.Forms.Button btnSelect;
        private System.Windows.Forms.Button btnDiscard;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.GroupBox gbEditLookupData;
        private System.Windows.Forms.GroupBox gbAvlLookupTables;
        private System.Windows.Forms.TextBox tbLookupTableValueLabel;
        private System.Windows.Forms.TextBox tbLookupTableKeylabel;
        private System.Windows.Forms.TextBox tbLookupTblDesc;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
    }
}