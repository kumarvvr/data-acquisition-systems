﻿namespace WinFormDasCoreClient
{
    partial class DefaultConfigDataEditWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DefaultConfigDataEditWindow));
            this.tabCtrlParams = new System.Windows.Forms.TabControl();
            this.tabMD = new System.Windows.Forms.TabPage();
            this.dgvTSP = new System.Windows.Forms.DataGridView();
            this.tabSP = new System.Windows.Forms.TabPage();
            this.dgvMD = new System.Windows.Forms.DataGridView();
            this.tabTSP = new System.Windows.Forms.TabPage();
            this.dgvSP = new System.Windows.Forms.DataGridView();
            this.tabMP = new System.Windows.Forms.TabPage();
            this.dgvMP = new System.Windows.Forms.DataGridView();
            this.tabRP = new System.Windows.Forms.TabPage();
            this.dgvRP = new System.Windows.Forms.DataGridView();
            this.btnBack = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.lblTitle = new System.Windows.Forms.Label();
            this.tabCtrlParams.SuspendLayout();
            this.tabMD.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTSP)).BeginInit();
            this.tabSP.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMD)).BeginInit();
            this.tabTSP.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSP)).BeginInit();
            this.tabMP.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMP)).BeginInit();
            this.tabRP.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRP)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabCtrlParams
            // 
            this.tabCtrlParams.Controls.Add(this.tabMD);
            this.tabCtrlParams.Controls.Add(this.tabSP);
            this.tabCtrlParams.Controls.Add(this.tabTSP);
            this.tabCtrlParams.Controls.Add(this.tabMP);
            this.tabCtrlParams.Controls.Add(this.tabRP);
            this.tabCtrlParams.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabCtrlParams.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.tabCtrlParams.Location = new System.Drawing.Point(0, 52);
            this.tabCtrlParams.Margin = new System.Windows.Forms.Padding(0, 2, 0, 0);
            this.tabCtrlParams.Name = "tabCtrlParams";
            this.tabCtrlParams.SelectedIndex = 0;
            this.tabCtrlParams.Size = new System.Drawing.Size(1201, 673);
            this.tabCtrlParams.TabIndex = 9;
            // 
            // tabMD
            // 
            this.tabMD.BackColor = System.Drawing.Color.DimGray;
            this.tabMD.Controls.Add(this.dgvTSP);
            this.tabMD.Location = new System.Drawing.Point(4, 30);
            this.tabMD.Margin = new System.Windows.Forms.Padding(0, 0, 0, 0);
            this.tabMD.Name = "tabMD";
            this.tabMD.Padding = new System.Windows.Forms.Padding(1);
            this.tabMD.Size = new System.Drawing.Size(1193, 639);
            this.tabMD.TabIndex = 0;
            this.tabMD.Text = "Test Setup Parameters";
            // 
            // dgvTSP
            // 
            this.dgvTSP.AllowUserToAddRows = false;
            this.dgvTSP.AllowUserToDeleteRows = false;
            this.dgvTSP.AllowUserToResizeRows = false;
            this.dgvTSP.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvTSP.BackgroundColor = System.Drawing.Color.White;
            this.dgvTSP.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvTSP.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTSP.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvTSP.GridColor = System.Drawing.Color.DimGray;
            this.dgvTSP.Location = new System.Drawing.Point(1, 1);
            this.dgvTSP.MultiSelect = false;
            this.dgvTSP.Name = "dgvTSP";
            this.dgvTSP.RowTemplate.Height = 30;
            this.dgvTSP.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgvTSP.Size = new System.Drawing.Size(1191, 637);
            this.dgvTSP.TabIndex = 0;
            this.dgvTSP.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dgvAll_CellBeginEdit);
            this.dgvTSP.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvALL_CellClick);
            this.dgvTSP.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvAll_CellDoubleClick);
            // 
            // tabSP
            // 
            this.tabSP.BackColor = System.Drawing.Color.DimGray;
            this.tabSP.Controls.Add(this.dgvMD);
            this.tabSP.Location = new System.Drawing.Point(4, 30);
            this.tabSP.Margin = new System.Windows.Forms.Padding(0, 0, 0, 0);
            this.tabSP.Name = "tabSP";
            this.tabSP.Padding = new System.Windows.Forms.Padding(1);
            this.tabSP.Size = new System.Drawing.Size(1193, 641);
            this.tabSP.TabIndex = 1;
            this.tabSP.Text = "Machine Details";
            // 
            // dgvMD
            // 
            this.dgvMD.AllowUserToAddRows = false;
            this.dgvMD.AllowUserToDeleteRows = false;
            this.dgvMD.AllowUserToResizeRows = false;
            this.dgvMD.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvMD.BackgroundColor = System.Drawing.Color.White;
            this.dgvMD.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvMD.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMD.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvMD.GridColor = System.Drawing.Color.DimGray;
            this.dgvMD.Location = new System.Drawing.Point(1, 1);
            this.dgvMD.MultiSelect = false;
            this.dgvMD.Name = "dgvMD";
            this.dgvMD.RowHeadersVisible = false;
            this.dgvMD.RowTemplate.Height = 30;
            this.dgvMD.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgvMD.Size = new System.Drawing.Size(1191, 639);
            this.dgvMD.TabIndex = 9;
            this.dgvMD.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvALL_CellClick);
            // 
            // tabTSP
            // 
            this.tabTSP.BackColor = System.Drawing.Color.DimGray;
            this.tabTSP.Controls.Add(this.dgvSP);
            this.tabTSP.Location = new System.Drawing.Point(4, 30);
            this.tabTSP.Margin = new System.Windows.Forms.Padding(0, 0, 0, 0);
            this.tabTSP.Name = "tabTSP";
            this.tabTSP.Padding = new System.Windows.Forms.Padding(1);
            this.tabTSP.Size = new System.Drawing.Size(1193, 641);
            this.tabTSP.TabIndex = 2;
            this.tabTSP.Text = "Machine Specified Parameters";
            // 
            // dgvSP
            // 
            this.dgvSP.AllowUserToAddRows = false;
            this.dgvSP.AllowUserToDeleteRows = false;
            this.dgvSP.AllowUserToResizeRows = false;
            this.dgvSP.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvSP.BackgroundColor = System.Drawing.Color.White;
            this.dgvSP.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvSP.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSP.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvSP.GridColor = System.Drawing.Color.DimGray;
            this.dgvSP.Location = new System.Drawing.Point(1, 1);
            this.dgvSP.MultiSelect = false;
            this.dgvSP.Name = "dgvSP";
            this.dgvSP.RowTemplate.Height = 30;
            this.dgvSP.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgvSP.Size = new System.Drawing.Size(1191, 639);
            this.dgvSP.TabIndex = 13;
            this.dgvSP.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvALL_CellClick);
            // 
            // tabMP
            // 
            this.tabMP.BackColor = System.Drawing.Color.DimGray;
            this.tabMP.Controls.Add(this.dgvMP);
            this.tabMP.Location = new System.Drawing.Point(4, 30);
            this.tabMP.Margin = new System.Windows.Forms.Padding(0, 0, 0, 0);
            this.tabMP.Name = "tabMP";
            this.tabMP.Padding = new System.Windows.Forms.Padding(1);
            this.tabMP.Size = new System.Drawing.Size(1193, 641);
            this.tabMP.TabIndex = 3;
            this.tabMP.Text = "Measurement profile";
            // 
            // dgvMP
            // 
            this.dgvMP.AllowUserToAddRows = false;
            this.dgvMP.AllowUserToDeleteRows = false;
            this.dgvMP.AllowUserToResizeRows = false;
            this.dgvMP.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvMP.BackgroundColor = System.Drawing.Color.White;
            this.dgvMP.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvMP.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMP.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvMP.Location = new System.Drawing.Point(1, 1);
            this.dgvMP.MultiSelect = false;
            this.dgvMP.Name = "dgvMP";
            this.dgvMP.RowTemplate.Height = 30;
            this.dgvMP.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgvMP.Size = new System.Drawing.Size(1191, 639);
            this.dgvMP.TabIndex = 13;
            this.dgvMP.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvALL_CellClick);
            // 
            // tabRP
            // 
            this.tabRP.BackColor = System.Drawing.Color.DimGray;
            this.tabRP.Controls.Add(this.dgvRP);
            this.tabRP.Location = new System.Drawing.Point(4, 30);
            this.tabRP.Margin = new System.Windows.Forms.Padding(0, 0, 0, 0);
            this.tabRP.Name = "tabRP";
            this.tabRP.Padding = new System.Windows.Forms.Padding(1);
            this.tabRP.Size = new System.Drawing.Size(1193, 641);
            this.tabRP.TabIndex = 4;
            this.tabRP.Text = "Result Parameters";
            // 
            // dgvRP
            // 
            this.dgvRP.AllowUserToAddRows = false;
            this.dgvRP.AllowUserToDeleteRows = false;
            this.dgvRP.AllowUserToResizeRows = false;
            this.dgvRP.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvRP.BackgroundColor = System.Drawing.Color.White;
            this.dgvRP.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvRP.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvRP.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvRP.Location = new System.Drawing.Point(1, 1);
            this.dgvRP.MultiSelect = false;
            this.dgvRP.Name = "dgvRP";
            this.dgvRP.RowTemplate.Height = 30;
            this.dgvRP.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgvRP.Size = new System.Drawing.Size(1191, 639);
            this.dgvRP.TabIndex = 13;
            this.dgvRP.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvALL_CellClick);
            // 
            // btnBack
            // 
            this.btnBack.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnBack.BackColor = System.Drawing.Color.Transparent;
            this.btnBack.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnBack.BackgroundImage")));
            this.btnBack.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnBack.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnBack.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBack.Location = new System.Drawing.Point(907, 10);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(140, 30);
            this.btnBack.TabIndex = 10;
            this.btnBack.Text = "Back";
            this.btnBack.UseVisualStyleBackColor = false;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnSave.BackColor = System.Drawing.Color.Transparent;
            this.btnSave.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnSave.BackgroundImage")));
            this.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnSave.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Location = new System.Drawing.Point(1058, 10);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(140, 30);
            this.btnSave.TabIndex = 10;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.SystemColors.Control;
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.tabCtrlParams, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel4, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(2, 2);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(2);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1201, 775);
            this.tableLayoutPanel1.TabIndex = 11;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 4;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel2.Controls.Add(this.btnSave, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.btnBack, 2, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 725);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(0, 0, 0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1201, 50);
            this.tableLayoutPanel2.TabIndex = 10;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel1, 0, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(0, 0, 0, 0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(1205, 779);
            this.tableLayoutPanel3.TabIndex = 12;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.BackgroundImage = global::WinFormDasCoreClient.Properties.Resources.Diffused_White_soft1;
            this.tableLayoutPanel4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tableLayoutPanel4.ColumnCount = 1;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.Controls.Add(this.lblTitle, 0, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel4.Margin = new System.Windows.Forms.Padding(0, 0, 0, 0);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(1201, 50);
            this.tableLayoutPanel4.TabIndex = 11;
            // 
            // lblTitle
            // 
            this.lblTitle.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblTitle.AutoEllipsis = true;
            this.lblTitle.AutoSize = true;
            this.lblTitle.BackColor = System.Drawing.Color.Transparent;
            this.lblTitle.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.ForeColor = System.Drawing.Color.Black;
            this.lblTitle.Location = new System.Drawing.Point(0, 13);
            this.lblTitle.Margin = new System.Windows.Forms.Padding(0);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(377, 23);
            this.lblTitle.TabIndex = 4;
            this.lblTitle.Text = "Default Configuration Data for Pump Type : {0}";
            // 
            // DefaultConfigDataEditWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1205, 779);
            this.Controls.Add(this.tableLayoutPanel3);
            this.DoubleBuffered = true;
            this.MinimizeBox = false;
            this.Name = "DefaultConfigDataEditWindow";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Default Configuration Editor";
            this.Load += new System.EventHandler(this.EditTableDataWaindow_Load);
            this.tabCtrlParams.ResumeLayout(false);
            this.tabMD.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvTSP)).EndInit();
            this.tabSP.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvMD)).EndInit();
            this.tabTSP.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvSP)).EndInit();
            this.tabMP.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvMP)).EndInit();
            this.tabRP.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvRP)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabCtrlParams;
        private System.Windows.Forms.TabPage tabMD;
        private System.Windows.Forms.DataGridView dgvTSP;
        private System.Windows.Forms.TabPage tabSP;
        private System.Windows.Forms.TabPage tabTSP;
        private System.Windows.Forms.TabPage tabMP;
        private System.Windows.Forms.TabPage tabRP;
        private System.Windows.Forms.DataGridView dgvMD;
        private System.Windows.Forms.DataGridView dgvSP;
        private System.Windows.Forms.DataGridView dgvMP;
        private System.Windows.Forms.DataGridView dgvRP;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Label lblTitle;
    }
}