﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DasCoreLib;
using DasCoreUtilities;
using BHEL.PUMPSDAS.Datatypes;

namespace WinFormDasCoreClient
{
    public partial class ChannelConfigWindow : Form
    {
        DasCore dc;
        InstalledMachine mc;

        DataGridStyles styleSettings;

        DataGridViewColumn  mdescCol, 
            chIdCol, sminCol, smaxCol,
            ominCol, omaxCol, pnameCol, sclCol, offsetCol, highalmCol, lowalmCol, dispIndexCol;

        DataGridViewComboBoxColumn mtypeCol,  devIdCol;
        DataGridViewCellStyle disabledColStyle;
                
        List<MeasurementProfileDetails> mpdetails;
        List<string> measTypes, deviceIds;
        List<DeviceDetails> deviceList;
                
        public ChannelConfigWindow(DasCore dc, InstalledMachine mc)
        {
            InitializeComponent();
            this.dc = dc;
            this.mc = mc;

            styleSettings = new DataGridStyles();

            measTypes = dc.GetSupportedMeasurementTypes();
            deviceList = dc.GetAllDevices();

            deviceIds = new List<string>(0);
            for(int i=0;i<deviceList.Count;i++)
                deviceIds.Add(deviceList[i].deviceid.ToString());

            InitializeGrid();
            RefreshGrid();

            disabledColStyle = new DataGridViewCellStyle();
            disabledColStyle.BackColor = SystemColors.Control;
        }
        private void InitializeGrid()
        {
            mtypeCol = new DataGridViewComboBoxColumn();
            mtypeCol.Name = "meastype";
            mtypeCol.HeaderText = "Measurement Type";
            mtypeCol.Width = 100;
            mtypeCol.ReadOnly = false;
            for (int i = 0; i < measTypes.Count; i++)
            {
                mtypeCol.Items.Add(measTypes[i]);
            }

            mdescCol = new DataGridViewTextBoxColumn();
            mdescCol.Name = "paramdescription";
            mdescCol.HeaderText = "Measurement Desc.";
            mdescCol.Width = 100;
            mdescCol.ReadOnly = true;
            mdescCol.DefaultCellStyle = styleSettings.GetDisabledTextStyle();

            pnameCol = new DataGridViewTextBoxColumn();
            pnameCol.Name = "paramname";
            pnameCol.HeaderText = "Parameter Name";
            pnameCol.Width = 100;
            pnameCol.ReadOnly = true;
            pnameCol.DefaultCellStyle = styleSettings.GetDisabledTextStyle();

            chIdCol = new DataGridViewTextBoxColumn();
            chIdCol.Name = "channelid";
            chIdCol.HeaderText = "Channel Id";
            chIdCol.Width = 100;
            chIdCol.ReadOnly = false;

            devIdCol = new DataGridViewComboBoxColumn();
            devIdCol.Name = "fk_deviceid";
            devIdCol.HeaderText = "Device Id";
            devIdCol.Width = 100;
            devIdCol.ReadOnly = false;
            for (int i = 0; i < deviceList.Count; i++)
            {
                devIdCol.Items.Add(deviceList[i].deviceid.ToString());
            }


            sminCol = new DataGridViewTextBoxColumn();
            sminCol.Name = "sensormin";
            sminCol.HeaderText = "Sensor Min. Value";
            sminCol.Width = 100;
            sminCol.ReadOnly = false;            

            smaxCol = new DataGridViewTextBoxColumn();
            smaxCol.Name = "sensormax";
            smaxCol.HeaderText = "Sensor Max. Value";
            smaxCol.Width = 100;
            smaxCol.ReadOnly = false;

            ominCol = new DataGridViewTextBoxColumn();
            ominCol.Name = "outputmin";
            ominCol.HeaderText = "Output Min. Value";
            ominCol.Width = 100;
            ominCol.ReadOnly = false;

            omaxCol = new DataGridViewTextBoxColumn();
            omaxCol.Name = "outputmax";
            omaxCol.HeaderText = "Output Max. Value";
            omaxCol.Width = 100;
            omaxCol.ReadOnly = false;

            sclCol = new DataGridViewTextBoxColumn();
            sclCol.Name = "scale";
            sclCol.HeaderText = "Scale";
            sclCol.Width = 100;
            sclCol.ReadOnly = false;

            offsetCol = new DataGridViewTextBoxColumn();
            offsetCol.Name = "offsetvalue";
            offsetCol.HeaderText = "Offset";
            offsetCol.Width = 100;
            offsetCol.ReadOnly = false;

            highalmCol = new DataGridViewTextBoxColumn();
            highalmCol.Name = "highalarm";
            highalmCol.HeaderText = "Alarm (High)";
            highalmCol.Width = 100;
            highalmCol.ReadOnly = false;

            lowalmCol = new DataGridViewTextBoxColumn();
            lowalmCol.Name = "lowalarm";
            lowalmCol.HeaderText = "Alarm (Low)";
            lowalmCol.Width = 100;
            lowalmCol.ReadOnly = false;

            dispIndexCol = new DataGridViewTextBoxColumn();
            dispIndexCol.Name = "displayindex";
            dispIndexCol.HeaderText = "Display Order";
            dispIndexCol.Width = 100;
            dispIndexCol.ReadOnly = false;

            dgvChannelConfig.Columns.Add(pnameCol);
            dgvChannelConfig.Columns.Add(mdescCol);
            dgvChannelConfig.Columns.Add(mtypeCol);
            dgvChannelConfig.Columns.Add(chIdCol); 
            dgvChannelConfig.Columns.Add(devIdCol); 
            dgvChannelConfig.Columns.Add(sminCol); 
            dgvChannelConfig.Columns.Add(smaxCol); 
            dgvChannelConfig.Columns.Add(ominCol); 
            dgvChannelConfig.Columns.Add(omaxCol);
            dgvChannelConfig.Columns.Add(sclCol); 
            dgvChannelConfig.Columns.Add(offsetCol); 
            dgvChannelConfig.Columns.Add(highalmCol); 
            dgvChannelConfig.Columns.Add(lowalmCol);
            dgvChannelConfig.Columns.Add(dispIndexCol);

            this.dgvChannelConfig.RowHeadersVisible = false;

            this.dgvChannelConfig.ColumnHeadersDefaultCellStyle = styleSettings.GetColumnHeaderStyle();

        }        
        private void RefreshGrid()
        {
            dgvChannelConfig.Rows.Clear();
            try
            {
                mpdetails = dc.GetMeasurementProfileDetails(mc);
                for (int i = 0; i < mpdetails.Count; i++)
                {                    
                    dgvChannelConfig.Rows.Add(new object[] {
                                                            mpdetails[i].paramname,
                                                            mpdetails[i].paramdescription,
                                                            ShowDefaultvalueIfValueNotAllowed(mpdetails[i].meastype, measTypes),
                                                            mpdetails[i].channelid,
                                                            ShowDefaultvalueIfValueNotAllowed(mpdetails[i].fk_deviceid.ToString(), deviceIds),
                                                            mpdetails[i].sensormin,
                                                            mpdetails[i].sensormax,
                                                            mpdetails[i].outputmin,
                                                            mpdetails[i].outputmax,
                                                            mpdetails[i].scale,
                                                            mpdetails[i].offsetvalue,
                                                            mpdetails[i].highalarm,
                                                            mpdetails[i].lowalarm,
                                                            mpdetails[i].displayindex});
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                this.Close();
            }            
        }
        private string ShowDefaultvalueIfValueNotAllowed(string cellvalue, List<string> allowedValues)
        {
            for (int j = 0; j < allowedValues.Count; j++)
            {
                if (cellvalue == allowedValues[j])
                {
                    return cellvalue;
                }
            }
            return allowedValues[0];
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            List<MeasurementProfileDetails>  chdetails = new List<MeasurementProfileDetails>();
            MeasurementProfileDetails channel = new MeasurementProfileDetails();

            for (int i = 0; i<dgvChannelConfig.Rows.Count; i++)
            {
                channel.fk_machineCode = mc.machinecode;
                channel.meastype = DefaultConversion.GetStringValue(dgvChannelConfig.Rows[i].Cells["meastype"].Value.ToString());
                channel.paramdescription = DefaultConversion.GetStringValue(dgvChannelConfig.Rows[i].Cells["paramdescription"].Value.ToString());
                channel.paramname = DefaultConversion.GetStringValue(dgvChannelConfig.Rows[i].Cells["paramname"].Value.ToString());
                channel.channelid = DefaultConversion.GetStringValue(dgvChannelConfig.Rows[i].Cells["channelid"].Value.ToString());
                channel.fk_deviceid = DefaultConversion.GetIntValue(dgvChannelConfig.Rows[i].Cells["fk_deviceid"].Value.ToString());
                channel.sensormin = DefaultConversion.GetDoubleValue(dgvChannelConfig.Rows[i].Cells["sensormin"].Value.ToString());
                channel.sensormax = DefaultConversion.GetDoubleValue(dgvChannelConfig.Rows[i].Cells["sensormax"].Value.ToString());
                channel.outputmin = DefaultConversion.GetDoubleValue(dgvChannelConfig.Rows[i].Cells["outputmin"].Value.ToString());
                channel.outputmax = DefaultConversion.GetDoubleValue(dgvChannelConfig.Rows[i].Cells["outputmax"].Value.ToString());
                channel.scale = DefaultConversion.GetDoubleValue(dgvChannelConfig.Rows[i].Cells["scale"].Value.ToString());
                channel.offsetvalue = DefaultConversion.GetDoubleValue(dgvChannelConfig.Rows[i].Cells["offsetvalue"].Value.ToString());
                channel.highalarm = DefaultConversion.GetDoubleValue(dgvChannelConfig.Rows[i].Cells["highalarm"].Value.ToString());
                channel.lowalarm = DefaultConversion.GetDoubleValue(dgvChannelConfig.Rows[i].Cells["lowalarm"].Value.ToString());
                channel.displayindex = DefaultConversion.GetIntValue(dgvChannelConfig.Rows[i].Cells["displayindex"].Value.ToString());

                chdetails.Add(channel);
            }

            try
            {
                dc.UpdateMeasurementConfig(chdetails, this.mc);
                RefreshGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }


    }
}
