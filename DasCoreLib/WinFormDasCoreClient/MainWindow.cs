﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using BHEL.PUMPSDAS.Datatypes;
using DasCoreLib;
using System.Diagnostics;
using System.Speech.Synthesis;
using CodeMonkey;
using System.Reflection;
using FileManager;
using DasCoreUtilities;


namespace WinFormDasCoreClient
{
    public partial class MainWindow : Form
    {
        #region Global Objects

        private DasCore _dasCore;

        InstalledMachinesWindow inWin;
        CommonLookupManagerWindow lkpMgrWindow;
        AddMachineWindow AddMcWin;
        DefaultConfigDataEditWindow configEditWin;
        ProjectManagerWindow projManWin;
        ChannelConfigWindow channelConfigWin;
        DeviceManagerWindow devMgrWin;
        TestManagementWindow testWin;
        DXAcqWindow dxacqwin;
        DatabaseSettingsWindow dbaseSettingsWin;
        TestSummaryWindow tstSumWin;
                
        InstalledMachine defaultConfigSelectedMachine;
        Compiler scriptCompiler;
        Assembly UIScriptAssembly;
        bool inWin_CancelStatus;
        bool addMcWin_CancelStatus = true;

        #endregion
                

        public MainWindow(DasCore dc)
        {
            this._dasCore = dc;
            InitializeComponent();
            Prompt say = new Prompt("Welcome to Pumps Testing Department");
            SpeechSynthesizer synth = new SpeechSynthesizer();
            synth.SetOutputToDefaultAudioDevice();
            synth.Volume = 100;
            //synth.SpeakAsync(say);
        } 
        private void EditDefaultConfiguration()
        {
            inWin = new InstalledMachinesWindow(_dasCore);
            inWin.FormClosing += new FormClosingEventHandler(inWin_FormClosing);
            inWin.ShowDialog();
            if (!inWin_CancelStatus)
            {
                configEditWin = new DefaultConfigDataEditWindow(_dasCore, defaultConfigSelectedMachine);
                configEditWin.ShowDialog();
            }
            inWin = null;
            configEditWin = null;
        }
        void inWin_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (inWin.DialogResult == System.Windows.Forms.DialogResult.OK)
            {
                defaultConfigSelectedMachine = inWin.GetSelectedMachine();
                inWin_CancelStatus = false;
            }
            else inWin_CancelStatus = true;
        }
        private void EditCommonLookupData()
        {
            this.lkpMgrWindow = new CommonLookupManagerWindow(this._dasCore);
            lkpMgrWindow.ShowDialog();
        }
        private void AddNewMachine()
        {
            AddMcWin = new AddMachineWindow(_dasCore);
            AddMcWin.FormClosing += new FormClosingEventHandler(AddMcWin_FormClosing);
            AddMcWin.ShowDialog();

            // Additional windows for modifying specified parameters...

            if (!addMcWin_CancelStatus)
            {                
                configEditWin = new DefaultConfigDataEditWindow(_dasCore, defaultConfigSelectedMachine);
                configEditWin.ShowDialog();
                defaultConfigSelectedMachine = new InstalledMachine();
            }
            
        }

        
        private void ManageProjects()
        {
            inWin = new InstalledMachinesWindow(_dasCore);
            inWin.FormClosing += new FormClosingEventHandler(inWin_FormClosing);
            inWin.ShowDialog();
            if (!inWin_CancelStatus)
            {
                projManWin = new ProjectManagerWindow(_dasCore, defaultConfigSelectedMachine);
                projManWin.ShowDialog();
            }
            inWin = null;
            projManWin = null;
        }
        private void EditChannelConfiguration()
        {
            inWin = new InstalledMachinesWindow(_dasCore);
            inWin.FormClosing += new FormClosingEventHandler(inWin_FormClosing);
            inWin.ShowDialog();
            if (!inWin_CancelStatus)
            {
                channelConfigWin = new ChannelConfigWindow(_dasCore, defaultConfigSelectedMachine);
                channelConfigWin.ShowDialog();
            }
            inWin = null;
            configEditWin = null;
        }
        private void ManageDevices()
        {
            devMgrWin = new DeviceManagerWindow(_dasCore);
            devMgrWin.ShowDialog();
        }        
        private bool CompileScript()
        {
            bool isSuccessful;
            string compileErrors = "";

            scriptCompiler = null;
            scriptCompiler = new Compiler("", "");

            string scriptpath = DasCoreSettings.GetMachineUIConfigScriptFileFullName(defaultConfigSelectedMachine);
            if (UIScriptAssembly != null)
                UIScriptAssembly = null;

            try
            {
                //errors = scriptCompiler.Compile(scriptpath, false);
                UIScriptAssembly = scriptCompiler.CompileInMemory(scriptpath, out isSuccessful, out compileErrors);
                if (!isSuccessful)
                {
                    MessageBox.Show("UI Script Compilation failed! : " + compileErrors);
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("UI Script Compilation failed! : " + ex.Source + " : " + ex.Message + " : " + compileErrors);
                return false;
            }
        }
        private void OpenUIScriptEditor(InstalledMachine selectedMachine)
        {
            // Open an NPP window to show the script...
            string editScriptCommand = DasCoreSettings.GetScriptEditorFileFullName();
            string args = DasCoreSettings.GetMachineEditUIConfigScriptProcessArguments(selectedMachine);
            ProcessHelper.OpenProcess(editScriptCommand, args, true, ProcessWindowStyle.Maximized);
        }
        private void CreateBackupScriptIfExists(InstalledMachine selectedMachine)
        {
            try
            {
                FileManager.FileSystemHandler.CopyFile(DasCoreSettings.GetMachineUIConfigScriptFileFullName(selectedMachine), DasCoreSettings.GetMachineUIConfigScriptFileFullName(selectedMachine) +".bkp", true);
                FileManager.FileSystemHandler.CopyFile(DasCoreSettings.GetMachineUIConfigScriptFileFullName(selectedMachine), DasCoreSettings.GetMachineUIConfigScriptFileFullName(selectedMachine) + DateTimeFormatting.GetDateTimeStamp(true) + ".bkp", true);
            
            }
            catch (Exception ex)
            {
                //Do nothing
            }
        }
        private void RollbackChanges(InstalledMachine selectedMachine)
        {
            try
            {
                FileManager.FileSystemHandler.CopyFile(DasCoreSettings.GetMachineUIConfigScriptFileFullName(selectedMachine) + ".bkp", DasCoreSettings.GetMachineUIConfigScriptFileFullName(selectedMachine), true);
                FileManager.FileSystemHandler.DeleteFile(DasCoreSettings.GetMachineUIConfigScriptFileFullName(selectedMachine) + ".bkp");
            }
            catch (Exception ex)
            {
                //Do nothing
            }
        }        
        private void CloseThisWindow()
        {
            this.Close();
        }
        private void ShowManageTestsWindow()
        {
            inWin = new InstalledMachinesWindow(_dasCore);
            inWin.FormClosing += new FormClosingEventHandler(inWin_FormClosing);
            inWin.ShowDialog();
            if (!inWin_CancelStatus)
            {
                testWin = new TestManagementWindow(_dasCore, defaultConfigSelectedMachine);
                testWin.FormClosing += new FormClosingEventHandler(testWin_FormClosing);
                testWin.ShowDialog();
            }
            inWin = null;
            testWin = null;
        }

        private void EditAndCompileMachineErrorScriptTemplate()
        {
            inWin = new InstalledMachinesWindow(_dasCore);
            inWin.FormClosing += new FormClosingEventHandler(inWin_FormClosing);
            inWin.ShowDialog();
            if (!inWin_CancelStatus)
            {
                // Take backup of template script..

                string sf, df;

                sf = DasCoreSettings.GetMachineErrorCorrectorTemplateFileFullName(defaultConfigSelectedMachine);
                df = sf + ".bkp";

                FileSystemHandler.CopyFile(sf, df, true);

                ProcessHelper.OpenProcess(
                DasCoreSettings.GetScriptEditorFileFullName(),
                sf,
                true,
                ProcessWindowStyle.Normal);
                scriptCompiler = new Compiler("", "");
                string error = "";
                
                // Compile the newly updated script..
                error = scriptCompiler.Compile(sf, true);

                if (error != "")
                {
                    MessageBox.Show("Error Compiling Error Script. Error :"+Environment.NewLine+error);
                    FileSystemHandler.CopyFile(df, sf, true);
                }
                else
                {
                    MessageBox.Show("Successfully compiled script !");
                }
                
            }
            inWin = null;
            testWin = null;
        }

        #region Event Handlers

        void AddMcWin_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (AddMcWin.DialogResult == System.Windows.Forms.DialogResult.OK)
            {
                addMcWin_CancelStatus = false;
                defaultConfigSelectedMachine = AddMcWin.GetNewInstalledMachine();
            }
        }

        private void exitToolStripMenuItem1_Click_1(object sender, EventArgs e)
        {
            CloseThisWindow();
        }

        private void manageProjectsToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            ManageProjects();
        }

        private void commonLookupTablesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EditCommonLookupData();
        }

        private void addNewMachineToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddNewMachine();
        }

        private void editDefaultConfigurationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EditDefaultConfiguration();
        }

        private void channelConfigurationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EditChannelConfiguration();
        }

        private void manageHardwareDevicesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ManageDevices();
        }

        private void toolStripButtonConductTest_Click(object sender, EventArgs e)
        {
            ShowManageTestsWindow();                        
        }

        private void maxWindowToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //TestManagementWindow twin = new TestManagementWindow();
            //twin.ShowDialog();
        }

        private void manageTestsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ShowManageTestsWindow();            
        }        

        void testWin_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (testWin.isTestRequested)
            {
                testWin.Hide();

                dxacqwin = new DXAcqWindow(_dasCore);
                //threadDasWindow tdWin = new threadDasWindow(_dasCore);
                try
                {
                    dxacqwin.ShowDialog();
                    //tdWin.ShowDialog();
                }
                catch (Exception ex)
                {
                    testWin.Show();
                }
            }            
        }

        private void toolStripDropDownButton4_Click(object sender, EventArgs e)
        {

        }

        private void dXAcqWinToolStripMenuItem_Click(object sender, EventArgs e)
        {
            /*dxacqwin = new DXAcqWindow(_dasCore,);
            DialogResult showDialog = dxacqwin.ShowDialog();*/
        }

        private void databaseSettingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            dbaseSettingsWin = new DatabaseSettingsWindow(_dasCore);
            dbaseSettingsWin.ShowDialog();
        }
                
        private void editDefaultUIConfigurationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            inWin = new InstalledMachinesWindow(_dasCore);
            inWin.FormClosing += new FormClosingEventHandler(inWin_FormClosing);
            inWin.ShowDialog();
            if (!inWin_CancelStatus)
            {
                CreateBackupScriptIfExists(defaultConfigSelectedMachine);
                OpenUIScriptEditor(defaultConfigSelectedMachine);
                
                // Compile the newly updated script..
                if (this.CompileScript())
                {
                    // Script Compilation successful...
                    MessageBox.Show("Successfully created UI Config!");
                }
                else
                {
                    RollbackChanges(defaultConfigSelectedMachine);
                }

            }
            inWin = null;           
            
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Maximized)
                WindowState = FormWindowState.Normal;
            else this.WindowState = FormWindowState.Maximized;
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        #endregion

        private void toolStripButton1_Click_1(object sender, EventArgs e)
        {
            deleteable dt = new deleteable();
            dt.Show();
        }

        private void editDefaultMachineErrorScriptToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EditAndCompileMachineErrorScriptTemplate();
        }

        
    }
}
