﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


namespace WinFormDasCoreClient
{
    public partial class ActivityCommentsWindow : Form
    {
        List<string> activities, comments;

        public ActivityCommentsWindow()
        {
            InitializeComponent();
        }

        public void UpdateActivityComments(List<string> activities, List<string> comments)
        {
            this.activities = activities;
            this.comments = comments;

            InitializeUI();
        }

        public List<string> GetComments()
        {
            this.comments.Clear();
            foreach (string s in this.tbComments.Lines)
            {
                this.comments.Add(s);
            }
            return this.comments;
        }

        private void InitializeUI()
        {
            lstActivities.Items.Clear();

            for (int i = 0; i < activities.Count; i++)
            {
                this.lstActivities.Items.Add(activities[i]);
            }

            this.tbComments.Text = "";
            for (int i = 0; i < comments.Count; i++)
            {
                this.tbComments.Text += comments[i] + Environment.NewLine;
            }
        }

        private void CloseWindow()
        {
            //this.Close();
            this.Hide();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            CloseWindow();
        }
    }
}
