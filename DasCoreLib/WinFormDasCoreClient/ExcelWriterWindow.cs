﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ExcelManager;
using System.Threading;

namespace WinFormDasCoreClient
{
    public partial class ExcelWriterWindow : Form
    {
        bool isWorkDone = false;

        ExcelWorkBookDataSet dataset;

        public ExcelWriterWindow(ExcelWorkBookDataSet data)
        {
            InitializeComponent();
            this.dataset = data;
            progressBar1.Style = ProgressBarStyle.Marquee;
            progressBar1.MarqueeAnimationSpeed = 20;
            
            tbConsole.Text = "";
            tbConsole.Text += "Writing to excel file : " + data.GetFileName();

            excelWriterWorker.RunWorkerAsync();
        }

        private void ExcelWriterWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!isWorkDone)
                e.Cancel = true; ;
        }

        private void excelWriterWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                Thread.Sleep(50);
                dataset.WriteData();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            e.Result = "Done";
        }


        private void excelWriterWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.isWorkDone = true;

            // Print the reports...
            //open the excel file...
            
            
            this.Close();
        }
    }
}
