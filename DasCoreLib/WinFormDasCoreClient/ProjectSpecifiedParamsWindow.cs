﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DasCoreLib;
using System.Diagnostics;
using DasCoreUtilities;
using BHEL.PUMPSDAS.Datatypes;

namespace WinFormDasCoreClient
{
    public partial class ProjectSpecifiedParamsWindow : Form
    {
        DasCore dc;
        InstalledMachine machine;
        List<SpecifiedParamsDetails> pSP;
        bool isDirty = true;
        DataGridViewCellStyle disabledTextStyle;
        DataGridViewCellStyle disabledButtonStyle;
        DataGridViewColumn pnamecol, ptype, pdesc, punits, pdefaultdata, pdispindex, pbtn;
        ProjectLookupManagerWindow plmw;
        ProjectDetails project;

        string lookupparamname, lookuptablename,lookuptabledesc;

        public ProjectSpecifiedParamsWindow(DasCore dc, InstalledMachine machine, ProjectDetails pd)
        {
            InitializeComponent();
            this.dc = dc;
            this.machine = machine;
            this.project = pd;

            disabledTextStyle = new DataGridViewCellStyle();
            disabledButtonStyle = new DataGridViewCellStyle();

            disabledTextStyle.BackColor = SystemColors.Control;
            disabledButtonStyle.ForeColor = SystemColors.ControlDark;
            disabledButtonStyle.BackColor = SystemColors.InactiveCaptionText;
            
        }

        private void EditTableDataWaindow_Load(object sender, EventArgs e)
        {
            try
            {
                pSP = dc.GetProjectSpecifiedParameters(this.machine, this.project);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            // Set the Datagrid Column.

            GenerateGridColumnObjects(out pnamecol, out ptype, out pdesc,out pbtn, out punits, out pdefaultdata, out pdispindex);
            InitializeDatagrid(dgvSP,pSP);
        }
        
        private void GenerateGridColumnObjects(out DataGridViewColumn pname,
        out DataGridViewColumn ptype,
        out DataGridViewColumn pdesc,
        out DataGridViewColumn pbtn,
        out DataGridViewColumn punits,
        out DataGridViewColumn pdefaultdata,
        out DataGridViewColumn pdispindex)
        {
            pname = new DataGridViewTextBoxColumn();
            pname.HeaderText = "Parameter Name";
            pname.Name = "paramname";
            pname.Width = 200;
            pname.ReadOnly = true;
            //pname.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;            

            ptype = new DataGridViewTextBoxColumn();
            ptype.HeaderText = "Parameter Type";
            ptype.Name = "paramtype";
            ptype.Width = 80;
            ptype.ReadOnly = true;

            //ptype.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;

            pdesc = new DataGridViewTextBoxColumn();
            pdesc.HeaderText = "Parameter Description";
            pdesc.Name = "paramdescription";
            pdesc.Width = 200;
            //pdesc.ReadOnly = true;

            pbtn = new DataGridViewButtonColumn();
            pbtn.HeaderText = "Lookup Select";
            pbtn.Name = "lookupbutton";
            pbtn.Width = 80;

            punits = new DataGridViewTextBoxColumn();
            punits.HeaderText = "Parameter Units";
            punits.Name = "paramunits";
            punits.Width = 80;
            //punits.ReadOnly = true;

            pdefaultdata = new DataGridViewTextBoxColumn();
            pdefaultdata.HeaderText = "Project Data";
            pdefaultdata.Name = "defaultdata"; // Actually contains project specific data.
            pdefaultdata.Width = 200;
            //pdefaultdata.ReadOnly = true;

            pdispindex = new DataGridViewTextBoxColumn();
            pdispindex.HeaderText = "Display Index";
            pdispindex.Name = "displayindex";
            pdispindex.Width = 60;
        }

        private void InitializeDatagrid(DataGridView target, List<SpecifiedParamsDetails> data)
        {
            target.Columns.Add(pnamecol);
            target.Columns.Add(ptype);
            target.Columns.Add(pdesc);
            target.Columns.Add(pbtn);
            target.Columns.Add(punits);
            target.Columns.Add(pdefaultdata);
            target.Columns.Add(pdispindex);
            
            for (int i = 0; i < data.Count; i++)
            {
                target.Rows.Add(new object[]{
                    data[i].paramname,
                    data[i].paramtype,
                    data[i].paramdescription,
                    "Select",
                    data[i].paramunits,
                    data[i].defaultdata,
                    data[i].displayindex
                });
            }

            for (int i = 0; i < data.Count; i++)
            {
                target.Rows[i].Cells[0].Style = disabledTextStyle;
                target.Rows[i].Cells[1].Style = disabledTextStyle;

                if (target.Rows[i].Cells["paramtype"].Value.ToString() != "lookup")
                {
                    target.Rows[i].Cells["lookupbutton"].Style = disabledButtonStyle;
                }
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {            
            bool isDone = false;
            try
            {
                if (dc.UpdateProjectSpecifiedParameters(this.machine,this.project, ExtractProjectSpecifiedParametersFromGrid()))
                {
                    isDone = true;
                    isDirty = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
            if (isDone)
                MessageBox.Show("Updated Successfully");            
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            if (isDirty)
            {
                if (MessageBox.Show("Do you want to close this windows??", "Cancel?", MessageBoxButtons.OKCancel) == System.Windows.Forms.DialogResult.OK)
                    this.Close();
                else
                    return;
            }
            this.Close();
        }

        private List<SpecifiedParamsDetails> ExtractProjectSpecifiedParametersFromGrid()
        {
            SpecifiedParamsDetails row;
            List<SpecifiedParamsDetails> data = new List<SpecifiedParamsDetails>();
            DataGridView target = this.dgvSP;
            DataGridViewCellCollection cells;
            for (int i = 0; i < target.Rows.Count; i++)
            {
                row = new SpecifiedParamsDetails();
                cells = target.Rows[i].Cells;
                for (int j = 0; j < cells.Count; j++)
                {
                    if (cells[j].Value == null)
                        cells[j].Value = "";
                }
                row.fk_machinecode = this.machine.machinecode;
                row.paramname = DefaultConversion.GetStringValue(cells["paramname"].Value.ToString());
                row.paramtype = DefaultConversion.GetStringValue(cells["paramtype"].Value.ToString());
                row.paramdescription = DefaultConversion.GetStringValue(cells["paramdescription"].Value.ToString());
                row.paramunits = DefaultConversion.GetStringValue(cells["paramunits"].Value.ToString());
                row.defaultdata = DefaultConversion.GetStringValue(cells["defaultdata"].Value.ToString());
                row.displayindex = DefaultConversion.GetIntValue(cells["displayindex"].Value.ToString());
                data.Add(row);
            }

            return data;
        }

        private void dgvALL_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView target = (DataGridView)sender;
            if (e.RowIndex == -1)
                return;
            if (target.Rows[e.RowIndex].Cells["paramtype"].Value.ToString() == "lookup" && e.ColumnIndex == 3/*target.Rows[e.RowIndex].Cells["lookupbutton"].ColumnIndex*/)
            {
                //MessageBox.Show(dgvTSP.Rows[e.RowIndex].Cells[0].Value.ToString());
                lookupparamname = target.Rows[e.RowIndex].Cells["paramname"].Value.ToString();
                // TODO
                plmw = new ProjectLookupManagerWindow(this.dc, this.machine, this.project);
                plmw.FormClosing += new FormClosingEventHandler(lmw_FormClosing);
                plmw.ShowDialog();

                target.Rows[e.RowIndex].Cells["defaultdata"].Value = this.lookuptablename;
                target.Rows[e.RowIndex].Cells["paramdescription"].Value = this.lookuptabledesc;
                this.lookuptablename = "";
                this.lookuptabledesc = "";
            }
        }

        void lmw_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.lookuptablename = plmw.SelectedLookup.tablename;
            this.lookuptabledesc = plmw.SelectedLookup.lookupdesc;
            //throw new NotImplementedException();
        }

        private void dgvAll_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void dgvAll_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            //MessageBox.Show("Begin Edit");
            DataGridView target = (DataGridView)sender;
            if (target.Rows[e.RowIndex].Cells["paramtype"].Value.ToString() == "lookup" && (e.ColumnIndex == 2 || e.ColumnIndex == 5))
            {
                e.Cancel = true;
                return;
            }
        }

        
    }
}
