﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DasCoreLib;

namespace WinFormDasCoreClient
{
    public partial class LookupDataWindow : Form
    {
        //DASCORE API
        DasCoreLib.DasCore dc;
        List<LookupDataRow> lookupData;
        string tname;

        public LookupDataWindow(DasCoreLib.DasCore dc, string tname)
        {
            InitializeComponent();
            this.dc = dc;
            this.tname = tname;

            //DASCORE API
            lookupData = dc.RetrieveLookupData(tname);

            // SETTING FOR LOOKUP TABLE LIST DATA GRID VIEW.
            this.dgvLookupDataList.AutoGenerateColumns = false;
            this.dgvLookupDataList.AutoSize = true;

            DataGridViewColumn keyCol = new DataGridViewTextBoxColumn();
            keyCol.Name = "Key";
            keyCol.Width = 180;
            keyCol.ValueType = typeof(string);

            DataGridViewColumn valueCol = new DataGridViewTextBoxColumn();
            valueCol.Name = "Value";
            valueCol.Width = 180;
            valueCol.ValueType = typeof(string);

            this.dgvLookupDataList.Columns.Add(keyCol);
            this.dgvLookupDataList.Columns.Add(valueCol);

            for (int i = 0; i < lookupData.Count; i++)
            {
                this.dgvLookupDataList.Rows.Add(new object[] { lookupData[i].key.ToString(), lookupData[i].value.ToString() });
            }

            this.dgvLookupDataList.Invalidate();
            btnSave.Enabled = true;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            DataGridViewRowCollection rowcoll = this.dgvLookupDataList.Rows;
            this.lookupData.Clear();

            for (int i = 0; i < rowcoll.Count-1; i++)
            {
                LookupDataRow row = new LookupDataRow();
                row.key = double.Parse((string)rowcoll[i].Cells[0].Value);
                row.value = double.Parse((string)rowcoll[i].Cells[1].Value);
                //r = (LookupDataRow)rowcoll[i].DataBoundItem;
                this.lookupData.Add(row);
            }

            // DASCORE API
            dc.ModifyLookupData(this.tname, lookupData);

            this.Close();
            btnSave.Enabled=false;

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Exit without saving changes?", "Data modified", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == System.Windows.Forms.DialogResult.Yes)
                this.Close();
            else
                return;
        }

        private void dgvLookupDataList_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this.dgvLookupDataList.SelectedCells[0].RowIndex == this.lookupData.Count - 1)
            {
            }

        }
        
    }
}
