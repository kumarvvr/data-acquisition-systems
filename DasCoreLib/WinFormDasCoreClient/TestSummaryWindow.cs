﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BHEL.PUMPSDAS.Datatypes;
using DasCoreLib;
using DasCoreUtilities;
using System.Diagnostics;
using FileManager;
using ExcelManager;

namespace WinFormDasCoreClient
{
    public partial class TestSummaryWindow : Form
    {
        DasCore dc;

        InstalledMachine mc;
        ProjectDetails pd;
        ProjectMachineDetails pmd;
        MachineTestDetails mtd;

        ObjectViewWindow computeWindow;

        List<MeasurementProfileDetails> mpDetails;
        List<ResultParamsDetails> rpDetails;
        List<SpecifiedParamsDetails> spDetails;
        List<TestSetupParamsDetails> tspDetails;

        List<Point> SPlookupLocations;
        List<Point> TSPLookupLocations;

        Font linkFont, defaultFont;
        Color linkcolor, defaultcolor;
        int nPoints;

        TestReportGenerator reportGenerator;
        
        public TestSummaryWindow(DasCore dc, InstalledMachine mc, ProjectDetails pd, ProjectMachineDetails pmd, MachineTestDetails mtd)
        {
            InitializeComponent();

            this.dc = dc;
            this.mc = mc;
            this.pd = pd;
            this.pmd = pmd;
            this.mtd = mtd;

            nPoints = dc.GetNumberOfRecordedPoints(mc, pd, pmd, mtd);

            UpdateLabels();
            UpdateRecordedPointsState();
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;

            mpDetails = dc.GetMeasurementProfileDetails(mc);
            rpDetails = dc.GetResultParamsDetails(mc);
            spDetails = dc.GetProjectSpecifiedParameters(mc, pd);
            tspDetails = dc.GetTestSetupParameters(mc);

            computeWindow = new ObjectViewWindow(pd.projectname, mc.dllversiondesc, pmd.machinenumber, mtd.testreference);
            SPlookupLocations = new List<Point>();
            TSPLookupLocations = new List<Point>();

            defaultFont = dgvSP.DefaultCellStyle.Font;
            defaultcolor = dgvSP.DefaultCellStyle.ForeColor;

            linkFont = new Font(defaultFont.FontFamily, defaultFont.Size, FontStyle.Underline);
            linkcolor = new Color();
            linkcolor = Color.Blue;


            InitializeAndFillSPGrid();

            InitializeAndFillTSPGrid();

            reportGenerator = new TestReportGenerator(this.dc);
            
        }

        private void InitializeAndFillSPGrid()
        {
            for (int i = 0; i < spDetails.Count; i++)
            {
                dgvSP.Rows.Add(new object[] { spDetails[i].paramdescription, spDetails[i].paramtype, spDetails[i].paramunits, spDetails[i].defaultdata });
                if (spDetails[i].paramtype == "lookup")
                {
                    Point p = new Point(i, 3);
                    SPlookupLocations.Add(p);

                    dgvSP.Rows[p.X].Cells[p.Y].Style.ForeColor = linkcolor;
                    dgvSP.Rows[p.X].Cells[p.Y].Style.Font = linkFont;
                }
            }

            // Add event handlers for the particular cells...
        }

        private void InitializeAndFillTSPGrid()
        {
            for (int i = 0; i < tspDetails.Count; i++)
            {
                dgvTSP.Rows.Add(new object[] { tspDetails[i].paramdescription, tspDetails[i].paramtype, tspDetails[i].paramunits, tspDetails[i].defaultdata });
                if (tspDetails[i].paramtype == "lookup")
                {
                    Point p = new Point(i, 3);
                    this.TSPLookupLocations.Add(p);

                    dgvTSP.Rows[p.X].Cells[p.Y].Style.ForeColor = linkcolor;
                    dgvTSP.Rows[p.X].Cells[p.Y].Style.Font = linkFont;
                }
            }

            // Add event handlers for the particular cells...
        }

        private void UpdateRecordedPointsState()
        {
            if (nPoints == -1)
            {
                btnRecPts.Enabled = false;
                btnGenReport.Enabled = false;
                lblRecPoint.Text = "Recorded Points : 0";
            }
            else
            {
                lblRecPoint.Text = "Recorded Points : " + nPoints;
            }
        }

        private void UpdateLabels()
        {
            lblMUcode.Text = "Frame : " + mc.dllversiondesc;
            lblMcNum.Text = "Pump No. : " + pmd.machinenumber;
            lblProject.Text = "Project : " + pd.projectname;
            lblCurveNum.Text = "Curve no. : " + mtd.testreference;
        }

        private void btnStartTest_Click(object sender, EventArgs e)
        {
            //Update dascore           

            
            if (nPoints != -1)
            {
                DialogResult dr = MessageBox.Show(nPoints + " points are already recorded. Do you want to retain the data?", "Existing Data", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
                if (dr == System.Windows.Forms.DialogResult.Yes)
                {
                    dc.InitializeCurrentTest(mc, pd, pmd, mtd, true);
                    this.DialogResult = System.Windows.Forms.DialogResult.OK;
                }
                else if (dr == System.Windows.Forms.DialogResult.No)
                {
                    dc.InitializeCurrentTest(mc, pd, pmd, mtd, false);
                    this.DialogResult = System.Windows.Forms.DialogResult.OK;
                }
                else return;
            }
            else
            {
                dc.InitializeCurrentTest(mc, pd, pmd, mtd, false);
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
            }      

            this.Close();
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        private void btnRecPts_Click(object sender, EventArgs e)
        {
            ShowRecordedPoints();
        }

        private void ShowRecordedPoints()
        {
            if (nPoints == -1)
                return;

            List<List<LinePrintData>> dataList = new List<List<LinePrintData>>();
            List<LinePrintData> data;
            List<ObjectData> mp, rp;
            DateTime dt;
            string noise1, noise2;
            List<string> dates, times, points;

            dates = new List<string>();
            times = new List<string>();
            points = new List<string>();

            for (int i = 0; i < nPoints; i++)
            {
                dc.GetMeasurementDataPoint(mc, pd, pmd, mtd,
                    i + 1, false, out mp, out rp, out dt, out noise1, out noise2);
                dates.Add(dt.Day + "/" + dt.Month + "/" + dt.Year);
                times.Add(dt.Hour + ":" + dt.Minute + ":" + dt.Second);
                points.Add((i+1).ToString());
                data = new List<LinePrintData>();
                for (int j = 0; j < mpDetails.Count; j++)
                {
                    LinePrintData lp = new LinePrintData();
                    lp.paramdesc = mpDetails[j].paramdescription;
                    lp.paramunits = mpDetails[j].paramunits;
                    for (int k = 0; i < mp.Count; k++)
                    {
                        if (mp[k].pindex == mpDetails[j].displayindex)
                        {
                            lp.value = mp[k].pvalue;
                            break;
                        }
                    }

                    data.Add(lp);
                }

                dataList.Add(data);
            }

            computeWindow.UpdateMultiValueData(dates, times, points, dataList,true);
            computeWindow.ShowDialog();
        }



        private void LookupCellChangeOnEnter(DataGridView dgv,Point p)
        {
            //DataGridViewCell cell = dgv.Rows[p.X].Cells[p.Y];
            dgv.Cursor = Cursors.Hand;

        }
        private void LookupCellChangeOnLeave(DataGridView dgv,Point p)
        {
            //DataGridViewCell cell = dgv.Rows[p.X].Cells[p.Y];
            dgv.Cursor = Cursors.Default;

        }
        private void OpenLookupDataWindow(DataGridView sender, Point p)
        {
            string tablename = (sender.Rows[p.X].Cells[p.Y].Value.ToString());
            LookupTableDetails details = dc.GetLookupTableByTableName(tablename);            
            LookupViewWindow lvWin = new LookupViewWindow(details, dc.GetLookupData(details));
            lvWin.Show();
        }

        private void dgvSP_CellMouseEnter(object sender, DataGridViewCellEventArgs e)
        {
            // Change the color of the cells when the mouse enters...

            Point p = new Point(e.RowIndex, e.ColumnIndex);

            for (int i = 0; i < SPlookupLocations.Count; i++)
            {
                if (p == SPlookupLocations[i])
                {
                    LookupCellChangeOnEnter(((DataGridView)sender), p);
                    break;
                }
            }
        }
        private void dgvSP_CellMouseLeave(object sender, DataGridViewCellEventArgs e)
        {
            Point p = new Point(e.RowIndex, e.ColumnIndex);

            for (int i = 0; i < SPlookupLocations.Count; i++)
            {
                if (p == SPlookupLocations[i])
                {
                    LookupCellChangeOnLeave((DataGridView)sender, p);
                    break;
                }
            }
        }
        private void dgvSP_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            Point p = new Point(e.RowIndex, e.ColumnIndex);

            
            for (int i = 0; i < SPlookupLocations.Count; i++)
            {
                if (p == SPlookupLocations[i])
                {
                    dgvSP.ClearSelection();
                    OpenLookupDataWindow((DataGridView)sender,p);
                    break;
                }
            }

        }
        

        private void dgvTSP_CellMouseEnter(object sender, DataGridViewCellEventArgs e)
        {
            Point p = new Point(e.RowIndex, e.ColumnIndex);

            for (int i = 0; i < TSPLookupLocations.Count; i++)
            {
                if (p == TSPLookupLocations[i])
                {
                    LookupCellChangeOnEnter(((DataGridView)sender), p);
                    break;
                }
            }
        }
        private void dgvTSP_CellMouseLeave(object sender, DataGridViewCellEventArgs e)
        {
            Point p = new Point(e.RowIndex, e.ColumnIndex);

            for (int i = 0; i < TSPLookupLocations.Count; i++)
            {
                if (p == TSPLookupLocations[i])
                {
                    LookupCellChangeOnLeave((DataGridView)sender, p);
                    break;
                }
            }
        }
        private void dgvTSP_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            Point p = new Point(e.RowIndex, e.ColumnIndex);


            for (int i = 0; i < TSPLookupLocations.Count; i++)
            {
                if (p == TSPLookupLocations[i])
                {
                    dgvTSP.ClearSelection();
                    OpenLookupDataWindow((DataGridView)sender, p);
                    break;
                }
            }
        }

        private void btnMp_Click(object sender, EventArgs e)
        {
            ChannelConfigWindow chCfgWin = new ChannelConfigWindow(dc, mc);
            chCfgWin.ShowDialog();
        }

        private void btnGenReport_Click(object sender, EventArgs e)
        {
            string sf, df;

            sf = DasCoreSettings.GetMachineTestReportTemplateFileFullName(mc);

            df = DasCoreSettings.GetMachineTestReportFileFullName(mc, pd, pmd, mtd);

            FileSystemHandler.CopyFile(sf, df, true);

            ExcelWorkBookDataSet excelworkbook = reportGenerator.GenerateReportWorkBook(df, mc, pd, pmd, mtd, false);

            //Create a modal window to write and open the new file...

            ExcelWriterWindow ewWin = new ExcelWriterWindow(excelworkbook);

            ewWin.ShowDialog();

            ewWin.Dispose();

            ProcessHelper.OpenProcess("\"" + df + "\"", "", false, ProcessWindowStyle.Normal);
        }

    }
}
