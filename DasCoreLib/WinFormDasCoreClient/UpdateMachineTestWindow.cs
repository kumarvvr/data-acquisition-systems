﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DasCoreLib;
using DasCoreUtilities;
using BHEL.PUMPSDAS.Datatypes;

namespace WinFormDasCoreClient
{
    public partial class UpdateMachineTestWindow : Form
    {
        ProjectMachineDetails projmachine;
        MachineTestDetails mctest;

        public UpdateMachineTestWindow(ProjectMachineDetails projmachine)
        {
            InitializeComponent();
            DialogResult = System.Windows.Forms.DialogResult.Cancel;
            Text = "Add New Test for - " + projmachine.machinenumber;
            this.projmachine = projmachine;
            dateTimePicker.Format = DateTimePickerFormat.Custom;
            dateTimePicker.CustomFormat = "dd/MM/yyyy";
        }

        public UpdateMachineTestWindow(ProjectMachineDetails projmachine, MachineTestDetails mctest)
        {
            InitializeComponent();
            DialogResult = System.Windows.Forms.DialogResult.Cancel;
            Text = "Modify Test Details for - " + projmachine.machinenumber;
            this.projmachine = projmachine;
            this.mctest = mctest;
            dateTimePicker.Format = DateTimePickerFormat.Custom;
            dateTimePicker.CustomFormat = "dd/MM/yyyy";
            tbTestRefNum.Text = mctest.testreference;
            tbTestDesc.Text = mctest.testremarks;
            dateTimePicker.Text = mctest.testdate;
            tbTestRefNum.Enabled = false;
            tbTestRefNum.BackColor = Color.Black;
        }              

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (tbTestRefNum.Text == "" || tbTestRefNum.Text == null || dateTimePicker.Text == "" || dateTimePicker.Text == null)
            {
                MessageBox.Show("Invalid Data Entered!");
                return;
            }
            else
            {
                mctest.fk_machinecode = projmachine.fk_machinecode;
                mctest.fk_projectcode = projmachine.fk_projectcode;
                mctest.fk_machinenumber = projmachine.machinenumber;
                mctest.testreference = tbTestRefNum.Text;
                mctest.testdate = dateTimePicker.Text;
                mctest.testremarks = tbTestDesc.Text;
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                this.Close();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        public MachineTestDetails GetNewMachineTestDetails()
        {
            return mctest;
        }

    }
}
