﻿namespace WinFormDasCoreClient
{
    partial class DASWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tbConsole = new System.Windows.Forms.TextBox();
            this.btnStop = new System.Windows.Forms.Button();
            this.btnCompute = new System.Windows.Forms.Button();
            this.btnPause = new System.Windows.Forms.Button();
            this.btnStartAcq = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.lblTime = new System.Windows.Forms.Label();
            this.lblProject = new System.Windows.Forms.Label();
            this.lblMachineUCode = new System.Windows.Forms.Label();
            this.lblTestReference = new System.Windows.Forms.Label();
            this.lblDate = new System.Windows.Forms.Label();
            this.tableMain = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new WinFormDasCoreClient.MyTableLayoutPanel();
            this.button1 = new System.Windows.Forms.Button();
            this.tableLayoutPanel5 = new WinFormDasCoreClient.MyTableLayoutPanel();
            this.tableLayoutPanel6 = new WinFormDasCoreClient.MyTableLayoutPanel();
            this.tableLayoutPanel3 = new WinFormDasCoreClient.MyTableLayoutPanel();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this.label66 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.label69 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this.label74 = new System.Windows.Forms.Label();
            this.label75 = new System.Windows.Forms.Label();
            this.label76 = new System.Windows.Forms.Label();
            this.label77 = new System.Windows.Forms.Label();
            this.label78 = new System.Windows.Forms.Label();
            this.label79 = new System.Windows.Forms.Label();
            this.label80 = new System.Windows.Forms.Label();
            this.label81 = new System.Windows.Forms.Label();
            this.label82 = new System.Windows.Forms.Label();
            this.label83 = new System.Windows.Forms.Label();
            this.label84 = new System.Windows.Forms.Label();
            this.label85 = new System.Windows.Forms.Label();
            this.label86 = new System.Windows.Forms.Label();
            this.label87 = new System.Windows.Forms.Label();
            this.label88 = new System.Windows.Forms.Label();
            this.label89 = new System.Windows.Forms.Label();
            this.label90 = new System.Windows.Forms.Label();
            this.label91 = new System.Windows.Forms.Label();
            this.label92 = new System.Windows.Forms.Label();
            this.label93 = new System.Windows.Forms.Label();
            this.label94 = new System.Windows.Forms.Label();
            this.label95 = new System.Windows.Forms.Label();
            this.label96 = new System.Windows.Forms.Label();
            this.label97 = new System.Windows.Forms.Label();
            this.label98 = new System.Windows.Forms.Label();
            this.label99 = new System.Windows.Forms.Label();
            this.label100 = new System.Windows.Forms.Label();
            this.label101 = new System.Windows.Forms.Label();
            this.label102 = new System.Windows.Forms.Label();
            this.label103 = new System.Windows.Forms.Label();
            this.label104 = new System.Windows.Forms.Label();
            this.label105 = new System.Windows.Forms.Label();
            this.label106 = new System.Windows.Forms.Label();
            this.label107 = new System.Windows.Forms.Label();
            this.label108 = new System.Windows.Forms.Label();
            this.label109 = new System.Windows.Forms.Label();
            this.label110 = new System.Windows.Forms.Label();
            this.label111 = new System.Windows.Forms.Label();
            this.label112 = new System.Windows.Forms.Label();
            this.label113 = new System.Windows.Forms.Label();
            this.label114 = new System.Windows.Forms.Label();
            this.label115 = new System.Windows.Forms.Label();
            this.label116 = new System.Windows.Forms.Label();
            this.label117 = new System.Windows.Forms.Label();
            this.label118 = new System.Windows.Forms.Label();
            this.label119 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new WinFormDasCoreClient.MyTableLayoutPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.tableLayoutPanel4 = new WinFormDasCoreClient.MyTableLayoutPanel();
            this.label17 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.tableLayoutPanel7 = new WinFormDasCoreClient.MyTableLayoutPanel();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label120 = new System.Windows.Forms.Label();
            this.label121 = new System.Windows.Forms.Label();
            this.label122 = new System.Windows.Forms.Label();
            this.label123 = new System.Windows.Forms.Label();
            this.label124 = new System.Windows.Forms.Label();
            this.label125 = new System.Windows.Forms.Label();
            this.label126 = new System.Windows.Forms.Label();
            this.label127 = new System.Windows.Forms.Label();
            this.label128 = new System.Windows.Forms.Label();
            this.label129 = new System.Windows.Forms.Label();
            this.label130 = new System.Windows.Forms.Label();
            this.label131 = new System.Windows.Forms.Label();
            this.label132 = new System.Windows.Forms.Label();
            this.label133 = new System.Windows.Forms.Label();
            this.label134 = new System.Windows.Forms.Label();
            this.label135 = new System.Windows.Forms.Label();
            this.label136 = new System.Windows.Forms.Label();
            this.label137 = new System.Windows.Forms.Label();
            this.label150 = new System.Windows.Forms.Label();
            this.label138 = new System.Windows.Forms.Label();
            this.label139 = new System.Windows.Forms.Label();
            this.label140 = new System.Windows.Forms.Label();
            this.label141 = new System.Windows.Forms.Label();
            this.label142 = new System.Windows.Forms.Label();
            this.label143 = new System.Windows.Forms.Label();
            this.label144 = new System.Windows.Forms.Label();
            this.label145 = new System.Windows.Forms.Label();
            this.label146 = new System.Windows.Forms.Label();
            this.label147 = new System.Windows.Forms.Label();
            this.label148 = new System.Windows.Forms.Label();
            this.label149 = new System.Windows.Forms.Label();
            this.label151 = new System.Windows.Forms.Label();
            this.label152 = new System.Windows.Forms.Label();
            this.label153 = new System.Windows.Forms.Label();
            this.label154 = new System.Windows.Forms.Label();
            this.tableMain.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.SuspendLayout();
            // 
            // tbConsole
            // 
            this.tbConsole.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbConsole.Location = new System.Drawing.Point(240, 3);
            this.tbConsole.Multiline = true;
            this.tbConsole.Name = "tbConsole";
            this.tbConsole.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tbConsole.Size = new System.Drawing.Size(142, 33);
            this.tbConsole.TabIndex = 0;
            // 
            // btnStop
            // 
            this.btnStop.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnStop.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStop.Location = new System.Drawing.Point(1451, 6);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(186, 33);
            this.btnStop.TabIndex = 1;
            this.btnStop.Text = "Stop Acquisition";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // btnCompute
            // 
            this.btnCompute.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnCompute.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCompute.Location = new System.Drawing.Point(736, 6);
            this.btnCompute.Name = "btnCompute";
            this.btnCompute.Size = new System.Drawing.Size(186, 33);
            this.btnCompute.TabIndex = 1;
            this.btnCompute.Text = "Compute";
            this.btnCompute.UseVisualStyleBackColor = true;
            // 
            // btnPause
            // 
            this.btnPause.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnPause.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPause.Location = new System.Drawing.Point(499, 6);
            this.btnPause.Name = "btnPause";
            this.btnPause.Size = new System.Drawing.Size(186, 33);
            this.btnPause.TabIndex = 1;
            this.btnPause.Text = "Refresh";
            this.btnPause.UseVisualStyleBackColor = true;
            this.btnPause.Click += new System.EventHandler(this.btnPause_Click);
            // 
            // btnStartAcq
            // 
            this.btnStartAcq.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnStartAcq.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStartAcq.Location = new System.Drawing.Point(25, 6);
            this.btnStartAcq.Name = "btnStartAcq";
            this.btnStartAcq.Size = new System.Drawing.Size(186, 33);
            this.btnStartAcq.TabIndex = 1;
            this.btnStartAcq.Text = "Start Acquisition";
            this.btnStartAcq.UseVisualStyleBackColor = true;
            this.btnStartAcq.Click += new System.EventHandler(this.btnStartAcq_Click);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Gold;
            this.label1.Location = new System.Drawing.Point(638, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(388, 39);
            this.label1.TabIndex = 3;
            this.label1.Text = "PUMPS TESTING DEPARTMENT";
            // 
            // lblTime
            // 
            this.lblTime.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblTime.AutoSize = true;
            this.lblTime.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTime.ForeColor = System.Drawing.Color.Lime;
            this.lblTime.Location = new System.Drawing.Point(1589, 5);
            this.lblTime.Name = "lblTime";
            this.lblTime.Size = new System.Drawing.Size(74, 29);
            this.lblTime.TabIndex = 4;
            this.lblTime.Text = "label1";
            // 
            // lblProject
            // 
            this.lblProject.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblProject.AutoSize = true;
            this.tableLayoutPanel5.SetColumnSpan(this.lblProject, 3);
            this.lblProject.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProject.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.lblProject.Location = new System.Drawing.Point(3, 2);
            this.lblProject.Name = "lblProject";
            this.lblProject.Size = new System.Drawing.Size(82, 29);
            this.lblProject.TabIndex = 4;
            this.lblProject.Text = "Project";
            // 
            // lblMachineUCode
            // 
            this.lblMachineUCode.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblMachineUCode.AutoSize = true;
            this.tableLayoutPanel5.SetColumnSpan(this.lblMachineUCode, 2);
            this.lblMachineUCode.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMachineUCode.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.lblMachineUCode.Location = new System.Drawing.Point(793, 2);
            this.lblMachineUCode.Name = "lblMachineUCode";
            this.lblMachineUCode.Size = new System.Drawing.Size(74, 29);
            this.lblMachineUCode.TabIndex = 4;
            this.lblMachineUCode.Text = "label1";
            // 
            // lblTestReference
            // 
            this.lblTestReference.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblTestReference.AutoSize = true;
            this.tableLayoutPanel5.SetColumnSpan(this.lblTestReference, 2);
            this.lblTestReference.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTestReference.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.lblTestReference.Location = new System.Drawing.Point(1331, 2);
            this.lblTestReference.Name = "lblTestReference";
            this.lblTestReference.Size = new System.Drawing.Size(116, 29);
            this.lblTestReference.TabIndex = 4;
            this.lblTestReference.Text = "Test Curve";
            // 
            // lblDate
            // 
            this.lblDate.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblDate.AutoSize = true;
            this.lblDate.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDate.ForeColor = System.Drawing.Color.Lime;
            this.lblDate.Location = new System.Drawing.Point(3, 5);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(74, 29);
            this.lblDate.TabIndex = 4;
            this.lblDate.Text = "label1";
            // 
            // tableMain
            // 
            this.tableMain.BackColor = System.Drawing.Color.Transparent;
            this.tableMain.ColumnCount = 1;
            this.tableMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableMain.Controls.Add(this.tableLayoutPanel2, 0, 6);
            this.tableMain.Controls.Add(this.tableLayoutPanel5, 0, 1);
            this.tableMain.Controls.Add(this.tableLayoutPanel6, 0, 0);
            this.tableMain.Controls.Add(this.tableLayoutPanel3, 0, 3);
            this.tableMain.Controls.Add(this.tableLayoutPanel1, 0, 2);
            this.tableMain.Controls.Add(this.tableLayoutPanel4, 0, 4);
            this.tableMain.Controls.Add(this.tableLayoutPanel7, 0, 5);
            this.tableMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableMain.Location = new System.Drawing.Point(0, 0);
            this.tableMain.Name = "tableMain";
            this.tableMain.RowCount = 7;
            this.tableMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.tableMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableMain.Size = new System.Drawing.Size(1672, 1016);
            this.tableMain.TabIndex = 7;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel2.ColumnCount = 7;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel2.Controls.Add(this.btnStop, 6, 0);
            this.tableLayoutPanel2.Controls.Add(this.tbConsole, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.btnCompute, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.button1, 4, 0);
            this.tableLayoutPanel2.Controls.Add(this.btnStartAcq, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.btnPause, 2, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 968);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1666, 45);
            this.tableLayoutPanel2.TabIndex = 8;
            // 
            // button1
            // 
            this.button1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.button1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(973, 6);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(186, 33);
            this.button1.TabIndex = 1;
            this.button1.Text = "Record";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 10;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel5.Controls.Add(this.lblProject, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.lblMachineUCode, 4, 0);
            this.tableLayoutPanel5.Controls.Add(this.lblTestReference, 8, 0);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(3, 48);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 1;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(1666, 34);
            this.tableLayoutPanel5.TabIndex = 4;
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 3;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel6.Controls.Add(this.lblTime, 2, 0);
            this.tableLayoutPanel6.Controls.Add(this.label1, 1, 0);
            this.tableLayoutPanel6.Controls.Add(this.lblDate, 0, 0);
            this.tableLayoutPanel6.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 1;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 39F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(1666, 39);
            this.tableLayoutPanel6.TabIndex = 5;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel3.ColumnCount = 11;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 5F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 5F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tableLayoutPanel3.Controls.Add(this.label19, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.label20, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.label21, 1, 2);
            this.tableLayoutPanel3.Controls.Add(this.label22, 1, 3);
            this.tableLayoutPanel3.Controls.Add(this.label23, 1, 4);
            this.tableLayoutPanel3.Controls.Add(this.label24, 1, 5);
            this.tableLayoutPanel3.Controls.Add(this.label25, 1, 6);
            this.tableLayoutPanel3.Controls.Add(this.label26, 1, 7);
            this.tableLayoutPanel3.Controls.Add(this.label27, 1, 8);
            this.tableLayoutPanel3.Controls.Add(this.label28, 1, 9);
            this.tableLayoutPanel3.Controls.Add(this.label29, 1, 10);
            this.tableLayoutPanel3.Controls.Add(this.label30, 1, 11);
            this.tableLayoutPanel3.Controls.Add(this.label31, 6, 0);
            this.tableLayoutPanel3.Controls.Add(this.label32, 6, 1);
            this.tableLayoutPanel3.Controls.Add(this.label33, 6, 2);
            this.tableLayoutPanel3.Controls.Add(this.label34, 6, 3);
            this.tableLayoutPanel3.Controls.Add(this.label35, 6, 4);
            this.tableLayoutPanel3.Controls.Add(this.label36, 6, 5);
            this.tableLayoutPanel3.Controls.Add(this.label37, 6, 6);
            this.tableLayoutPanel3.Controls.Add(this.label38, 6, 7);
            this.tableLayoutPanel3.Controls.Add(this.label39, 6, 8);
            this.tableLayoutPanel3.Controls.Add(this.label40, 6, 9);
            this.tableLayoutPanel3.Controls.Add(this.label41, 6, 10);
            this.tableLayoutPanel3.Controls.Add(this.label42, 6, 11);
            this.tableLayoutPanel3.Controls.Add(this.label48, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.label49, 2, 1);
            this.tableLayoutPanel3.Controls.Add(this.label50, 2, 2);
            this.tableLayoutPanel3.Controls.Add(this.label51, 2, 3);
            this.tableLayoutPanel3.Controls.Add(this.label52, 2, 4);
            this.tableLayoutPanel3.Controls.Add(this.label53, 2, 5);
            this.tableLayoutPanel3.Controls.Add(this.label54, 2, 6);
            this.tableLayoutPanel3.Controls.Add(this.label55, 2, 7);
            this.tableLayoutPanel3.Controls.Add(this.label56, 2, 8);
            this.tableLayoutPanel3.Controls.Add(this.label57, 2, 9);
            this.tableLayoutPanel3.Controls.Add(this.label58, 2, 10);
            this.tableLayoutPanel3.Controls.Add(this.label59, 2, 11);
            this.tableLayoutPanel3.Controls.Add(this.label60, 3, 0);
            this.tableLayoutPanel3.Controls.Add(this.label61, 3, 1);
            this.tableLayoutPanel3.Controls.Add(this.label62, 3, 2);
            this.tableLayoutPanel3.Controls.Add(this.label63, 3, 3);
            this.tableLayoutPanel3.Controls.Add(this.label64, 3, 4);
            this.tableLayoutPanel3.Controls.Add(this.label65, 3, 5);
            this.tableLayoutPanel3.Controls.Add(this.label66, 3, 6);
            this.tableLayoutPanel3.Controls.Add(this.label67, 3, 7);
            this.tableLayoutPanel3.Controls.Add(this.label68, 3, 8);
            this.tableLayoutPanel3.Controls.Add(this.label69, 3, 9);
            this.tableLayoutPanel3.Controls.Add(this.label70, 3, 10);
            this.tableLayoutPanel3.Controls.Add(this.label71, 3, 11);
            this.tableLayoutPanel3.Controls.Add(this.label72, 4, 0);
            this.tableLayoutPanel3.Controls.Add(this.label73, 4, 1);
            this.tableLayoutPanel3.Controls.Add(this.label74, 4, 2);
            this.tableLayoutPanel3.Controls.Add(this.label75, 4, 3);
            this.tableLayoutPanel3.Controls.Add(this.label76, 4, 4);
            this.tableLayoutPanel3.Controls.Add(this.label77, 4, 5);
            this.tableLayoutPanel3.Controls.Add(this.label78, 4, 6);
            this.tableLayoutPanel3.Controls.Add(this.label79, 4, 7);
            this.tableLayoutPanel3.Controls.Add(this.label80, 4, 8);
            this.tableLayoutPanel3.Controls.Add(this.label81, 4, 9);
            this.tableLayoutPanel3.Controls.Add(this.label82, 4, 10);
            this.tableLayoutPanel3.Controls.Add(this.label83, 4, 11);
            this.tableLayoutPanel3.Controls.Add(this.label84, 7, 0);
            this.tableLayoutPanel3.Controls.Add(this.label85, 8, 0);
            this.tableLayoutPanel3.Controls.Add(this.label86, 9, 0);
            this.tableLayoutPanel3.Controls.Add(this.label87, 7, 1);
            this.tableLayoutPanel3.Controls.Add(this.label88, 7, 2);
            this.tableLayoutPanel3.Controls.Add(this.label89, 7, 3);
            this.tableLayoutPanel3.Controls.Add(this.label90, 7, 4);
            this.tableLayoutPanel3.Controls.Add(this.label91, 7, 5);
            this.tableLayoutPanel3.Controls.Add(this.label92, 7, 6);
            this.tableLayoutPanel3.Controls.Add(this.label93, 7, 7);
            this.tableLayoutPanel3.Controls.Add(this.label94, 7, 8);
            this.tableLayoutPanel3.Controls.Add(this.label95, 7, 9);
            this.tableLayoutPanel3.Controls.Add(this.label96, 7, 10);
            this.tableLayoutPanel3.Controls.Add(this.label97, 7, 11);
            this.tableLayoutPanel3.Controls.Add(this.label98, 8, 1);
            this.tableLayoutPanel3.Controls.Add(this.label99, 8, 2);
            this.tableLayoutPanel3.Controls.Add(this.label100, 8, 3);
            this.tableLayoutPanel3.Controls.Add(this.label101, 8, 4);
            this.tableLayoutPanel3.Controls.Add(this.label102, 8, 5);
            this.tableLayoutPanel3.Controls.Add(this.label103, 8, 6);
            this.tableLayoutPanel3.Controls.Add(this.label104, 8, 7);
            this.tableLayoutPanel3.Controls.Add(this.label105, 8, 8);
            this.tableLayoutPanel3.Controls.Add(this.label106, 8, 9);
            this.tableLayoutPanel3.Controls.Add(this.label107, 8, 10);
            this.tableLayoutPanel3.Controls.Add(this.label108, 8, 11);
            this.tableLayoutPanel3.Controls.Add(this.label109, 9, 1);
            this.tableLayoutPanel3.Controls.Add(this.label110, 9, 2);
            this.tableLayoutPanel3.Controls.Add(this.label111, 9, 3);
            this.tableLayoutPanel3.Controls.Add(this.label112, 9, 4);
            this.tableLayoutPanel3.Controls.Add(this.label113, 9, 5);
            this.tableLayoutPanel3.Controls.Add(this.label114, 9, 6);
            this.tableLayoutPanel3.Controls.Add(this.label115, 9, 7);
            this.tableLayoutPanel3.Controls.Add(this.label116, 9, 8);
            this.tableLayoutPanel3.Controls.Add(this.label117, 9, 9);
            this.tableLayoutPanel3.Controls.Add(this.label118, 9, 10);
            this.tableLayoutPanel3.Controls.Add(this.label119, 9, 11);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 128);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 12;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.336329F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333061F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333061F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333061F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333061F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333061F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333061F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333061F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333061F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333061F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333061F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333061F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(1666, 554);
            this.tableLayoutPanel3.TabIndex = 9;
            // 
            // label19
            // 
            this.label19.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label19.AutoSize = true;
            this.label19.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.label19.Location = new System.Drawing.Point(10, 11);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(73, 25);
            this.label19.TabIndex = 0;
            this.label19.Text = "label19";
            // 
            // label20
            // 
            this.label20.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label20.AutoSize = true;
            this.label20.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.label20.Location = new System.Drawing.Point(10, 57);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(73, 25);
            this.label20.TabIndex = 0;
            this.label20.Text = "label19";
            // 
            // label21
            // 
            this.label21.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label21.AutoSize = true;
            this.label21.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.label21.Location = new System.Drawing.Point(10, 103);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(73, 25);
            this.label21.TabIndex = 0;
            this.label21.Text = "label19";
            // 
            // label22
            // 
            this.label22.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label22.AutoSize = true;
            this.label22.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.label22.Location = new System.Drawing.Point(10, 149);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(73, 25);
            this.label22.TabIndex = 0;
            this.label22.Text = "label19";
            // 
            // label23
            // 
            this.label23.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label23.AutoSize = true;
            this.label23.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.label23.Location = new System.Drawing.Point(10, 195);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(73, 25);
            this.label23.TabIndex = 0;
            this.label23.Text = "label19";
            // 
            // label24
            // 
            this.label24.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label24.AutoSize = true;
            this.label24.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.label24.Location = new System.Drawing.Point(10, 241);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(73, 25);
            this.label24.TabIndex = 0;
            this.label24.Text = "label19";
            // 
            // label25
            // 
            this.label25.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label25.AutoSize = true;
            this.label25.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.label25.Location = new System.Drawing.Point(10, 287);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(73, 25);
            this.label25.TabIndex = 0;
            this.label25.Text = "label19";
            // 
            // label26
            // 
            this.label26.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label26.AutoSize = true;
            this.label26.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.label26.Location = new System.Drawing.Point(10, 333);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(73, 25);
            this.label26.TabIndex = 0;
            this.label26.Text = "label19";
            // 
            // label27
            // 
            this.label27.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label27.AutoSize = true;
            this.label27.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.label27.Location = new System.Drawing.Point(10, 379);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(73, 25);
            this.label27.TabIndex = 0;
            this.label27.Text = "label19";
            // 
            // label28
            // 
            this.label28.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label28.AutoSize = true;
            this.label28.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.label28.Location = new System.Drawing.Point(10, 425);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(73, 25);
            this.label28.TabIndex = 0;
            this.label28.Text = "label19";
            // 
            // label29
            // 
            this.label29.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label29.AutoSize = true;
            this.label29.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.label29.Location = new System.Drawing.Point(10, 471);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(73, 25);
            this.label29.TabIndex = 0;
            this.label29.Text = "label19";
            // 
            // label30
            // 
            this.label30.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label30.AutoSize = true;
            this.label30.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.label30.Location = new System.Drawing.Point(10, 517);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(73, 25);
            this.label30.TabIndex = 0;
            this.label30.Text = "label19";
            // 
            // label31
            // 
            this.label31.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label31.AutoSize = true;
            this.label31.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.label31.Location = new System.Drawing.Point(827, 11);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(73, 25);
            this.label31.TabIndex = 0;
            this.label31.Text = "label19";
            // 
            // label32
            // 
            this.label32.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label32.AutoSize = true;
            this.label32.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.label32.Location = new System.Drawing.Point(827, 57);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(73, 25);
            this.label32.TabIndex = 0;
            this.label32.Text = "label19";
            // 
            // label33
            // 
            this.label33.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label33.AutoSize = true;
            this.label33.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.label33.Location = new System.Drawing.Point(827, 103);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(73, 25);
            this.label33.TabIndex = 0;
            this.label33.Text = "label19";
            // 
            // label34
            // 
            this.label34.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label34.AutoSize = true;
            this.label34.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.label34.Location = new System.Drawing.Point(827, 149);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(73, 25);
            this.label34.TabIndex = 0;
            this.label34.Text = "label19";
            // 
            // label35
            // 
            this.label35.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label35.AutoSize = true;
            this.label35.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.label35.Location = new System.Drawing.Point(827, 195);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(73, 25);
            this.label35.TabIndex = 0;
            this.label35.Text = "label19";
            // 
            // label36
            // 
            this.label36.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label36.AutoSize = true;
            this.label36.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.label36.Location = new System.Drawing.Point(827, 241);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(73, 25);
            this.label36.TabIndex = 0;
            this.label36.Text = "label19";
            // 
            // label37
            // 
            this.label37.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label37.AutoSize = true;
            this.label37.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.label37.Location = new System.Drawing.Point(827, 287);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(73, 25);
            this.label37.TabIndex = 0;
            this.label37.Text = "label19";
            // 
            // label38
            // 
            this.label38.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label38.AutoSize = true;
            this.label38.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.label38.Location = new System.Drawing.Point(827, 333);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(73, 25);
            this.label38.TabIndex = 0;
            this.label38.Text = "label19";
            // 
            // label39
            // 
            this.label39.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label39.AutoSize = true;
            this.label39.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.label39.Location = new System.Drawing.Point(827, 379);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(73, 25);
            this.label39.TabIndex = 0;
            this.label39.Text = "label19";
            // 
            // label40
            // 
            this.label40.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label40.AutoSize = true;
            this.label40.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.label40.Location = new System.Drawing.Point(827, 425);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(73, 25);
            this.label40.TabIndex = 0;
            this.label40.Text = "label19";
            // 
            // label41
            // 
            this.label41.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label41.AutoSize = true;
            this.label41.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.label41.Location = new System.Drawing.Point(827, 471);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(73, 25);
            this.label41.TabIndex = 0;
            this.label41.Text = "label19";
            // 
            // label42
            // 
            this.label42.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label42.AutoSize = true;
            this.label42.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.label42.Location = new System.Drawing.Point(827, 517);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(73, 25);
            this.label42.TabIndex = 0;
            this.label42.Text = "label19";
            // 
            // label48
            // 
            this.label48.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label48.AutoSize = true;
            this.label48.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.label48.Location = new System.Drawing.Point(528, 11);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(73, 25);
            this.label48.TabIndex = 0;
            this.label48.Text = "label19";
            // 
            // label49
            // 
            this.label49.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label49.AutoSize = true;
            this.label49.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.label49.Location = new System.Drawing.Point(528, 57);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(73, 25);
            this.label49.TabIndex = 0;
            this.label49.Text = "label19";
            // 
            // label50
            // 
            this.label50.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label50.AutoSize = true;
            this.label50.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.label50.Location = new System.Drawing.Point(528, 103);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(73, 25);
            this.label50.TabIndex = 0;
            this.label50.Text = "label19";
            // 
            // label51
            // 
            this.label51.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label51.AutoSize = true;
            this.label51.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.label51.Location = new System.Drawing.Point(528, 149);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(73, 25);
            this.label51.TabIndex = 0;
            this.label51.Text = "label19";
            // 
            // label52
            // 
            this.label52.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label52.AutoSize = true;
            this.label52.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.label52.Location = new System.Drawing.Point(528, 195);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(73, 25);
            this.label52.TabIndex = 0;
            this.label52.Text = "label19";
            // 
            // label53
            // 
            this.label53.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label53.AutoSize = true;
            this.label53.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.label53.Location = new System.Drawing.Point(528, 241);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(73, 25);
            this.label53.TabIndex = 0;
            this.label53.Text = "label19";
            // 
            // label54
            // 
            this.label54.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label54.AutoSize = true;
            this.label54.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.label54.Location = new System.Drawing.Point(528, 287);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(73, 25);
            this.label54.TabIndex = 0;
            this.label54.Text = "label19";
            // 
            // label55
            // 
            this.label55.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label55.AutoSize = true;
            this.label55.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.label55.Location = new System.Drawing.Point(528, 333);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(73, 25);
            this.label55.TabIndex = 0;
            this.label55.Text = "label19";
            // 
            // label56
            // 
            this.label56.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label56.AutoSize = true;
            this.label56.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.label56.Location = new System.Drawing.Point(528, 379);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(73, 25);
            this.label56.TabIndex = 0;
            this.label56.Text = "label19";
            // 
            // label57
            // 
            this.label57.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label57.AutoSize = true;
            this.label57.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.label57.Location = new System.Drawing.Point(528, 425);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(73, 25);
            this.label57.TabIndex = 0;
            this.label57.Text = "label19";
            // 
            // label58
            // 
            this.label58.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label58.AutoSize = true;
            this.label58.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.label58.Location = new System.Drawing.Point(528, 471);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(73, 25);
            this.label58.TabIndex = 0;
            this.label58.Text = "label19";
            // 
            // label59
            // 
            this.label59.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label59.AutoSize = true;
            this.label59.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.label59.Location = new System.Drawing.Point(528, 517);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(73, 25);
            this.label59.TabIndex = 0;
            this.label59.Text = "label19";
            // 
            // label60
            // 
            this.label60.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label60.AutoSize = true;
            this.label60.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label60.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label60.Location = new System.Drawing.Point(633, 13);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(66, 21);
            this.label60.TabIndex = 0;
            this.label60.Text = "label19";
            // 
            // label61
            // 
            this.label61.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label61.AutoSize = true;
            this.label61.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label61.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label61.Location = new System.Drawing.Point(633, 59);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(66, 21);
            this.label61.TabIndex = 0;
            this.label61.Text = "label19";
            // 
            // label62
            // 
            this.label62.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label62.AutoSize = true;
            this.label62.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label62.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label62.Location = new System.Drawing.Point(633, 105);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(66, 21);
            this.label62.TabIndex = 0;
            this.label62.Text = "label19";
            // 
            // label63
            // 
            this.label63.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label63.AutoSize = true;
            this.label63.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label63.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label63.Location = new System.Drawing.Point(633, 151);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(66, 21);
            this.label63.TabIndex = 0;
            this.label63.Text = "label19";
            // 
            // label64
            // 
            this.label64.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label64.AutoSize = true;
            this.label64.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label64.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label64.Location = new System.Drawing.Point(633, 197);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(66, 21);
            this.label64.TabIndex = 0;
            this.label64.Text = "label19";
            // 
            // label65
            // 
            this.label65.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label65.AutoSize = true;
            this.label65.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label65.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label65.Location = new System.Drawing.Point(633, 243);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(66, 21);
            this.label65.TabIndex = 0;
            this.label65.Text = "label19";
            // 
            // label66
            // 
            this.label66.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label66.AutoSize = true;
            this.label66.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label66.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label66.Location = new System.Drawing.Point(633, 289);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(66, 21);
            this.label66.TabIndex = 0;
            this.label66.Text = "label19";
            // 
            // label67
            // 
            this.label67.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label67.AutoSize = true;
            this.label67.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label67.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label67.Location = new System.Drawing.Point(633, 335);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(66, 21);
            this.label67.TabIndex = 0;
            this.label67.Text = "label19";
            // 
            // label68
            // 
            this.label68.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label68.AutoSize = true;
            this.label68.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label68.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label68.Location = new System.Drawing.Point(633, 381);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(66, 21);
            this.label68.TabIndex = 0;
            this.label68.Text = "label19";
            // 
            // label69
            // 
            this.label69.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label69.AutoSize = true;
            this.label69.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label69.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label69.Location = new System.Drawing.Point(633, 427);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(66, 21);
            this.label69.TabIndex = 0;
            this.label69.Text = "label19";
            // 
            // label70
            // 
            this.label70.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label70.AutoSize = true;
            this.label70.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label70.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label70.Location = new System.Drawing.Point(633, 473);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(66, 21);
            this.label70.TabIndex = 0;
            this.label70.Text = "label19";
            // 
            // label71
            // 
            this.label71.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label71.AutoSize = true;
            this.label71.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label71.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label71.Location = new System.Drawing.Point(633, 519);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(66, 21);
            this.label71.TabIndex = 0;
            this.label71.Text = "label19";
            // 
            // label72
            // 
            this.label72.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label72.AutoSize = true;
            this.label72.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label72.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label72.Location = new System.Drawing.Point(734, 13);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(66, 21);
            this.label72.TabIndex = 0;
            this.label72.Text = "label19";
            // 
            // label73
            // 
            this.label73.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label73.AutoSize = true;
            this.label73.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label73.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label73.Location = new System.Drawing.Point(734, 59);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(66, 21);
            this.label73.TabIndex = 0;
            this.label73.Text = "label19";
            // 
            // label74
            // 
            this.label74.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label74.AutoSize = true;
            this.label74.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label74.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label74.Location = new System.Drawing.Point(734, 105);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(66, 21);
            this.label74.TabIndex = 0;
            this.label74.Text = "label19";
            // 
            // label75
            // 
            this.label75.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label75.AutoSize = true;
            this.label75.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label75.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label75.Location = new System.Drawing.Point(734, 151);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(66, 21);
            this.label75.TabIndex = 0;
            this.label75.Text = "label19";
            // 
            // label76
            // 
            this.label76.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label76.AutoSize = true;
            this.label76.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label76.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label76.Location = new System.Drawing.Point(734, 197);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(66, 21);
            this.label76.TabIndex = 0;
            this.label76.Text = "label19";
            // 
            // label77
            // 
            this.label77.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label77.AutoSize = true;
            this.label77.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label77.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label77.Location = new System.Drawing.Point(734, 243);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(66, 21);
            this.label77.TabIndex = 0;
            this.label77.Text = "label19";
            // 
            // label78
            // 
            this.label78.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label78.AutoSize = true;
            this.label78.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label78.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label78.Location = new System.Drawing.Point(734, 289);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(66, 21);
            this.label78.TabIndex = 0;
            this.label78.Text = "label19";
            // 
            // label79
            // 
            this.label79.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label79.AutoSize = true;
            this.label79.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label79.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label79.Location = new System.Drawing.Point(734, 335);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(66, 21);
            this.label79.TabIndex = 0;
            this.label79.Text = "label19";
            // 
            // label80
            // 
            this.label80.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label80.AutoSize = true;
            this.label80.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label80.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label80.Location = new System.Drawing.Point(734, 381);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(66, 21);
            this.label80.TabIndex = 0;
            this.label80.Text = "label19";
            // 
            // label81
            // 
            this.label81.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label81.AutoSize = true;
            this.label81.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label81.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label81.Location = new System.Drawing.Point(734, 427);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(66, 21);
            this.label81.TabIndex = 0;
            this.label81.Text = "label19";
            // 
            // label82
            // 
            this.label82.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label82.AutoSize = true;
            this.label82.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label82.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label82.Location = new System.Drawing.Point(734, 473);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(66, 21);
            this.label82.TabIndex = 0;
            this.label82.Text = "label19";
            // 
            // label83
            // 
            this.label83.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label83.AutoSize = true;
            this.label83.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label83.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label83.Location = new System.Drawing.Point(734, 519);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(66, 21);
            this.label83.TabIndex = 0;
            this.label83.Text = "label19";
            // 
            // label84
            // 
            this.label84.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label84.AutoSize = true;
            this.label84.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.label84.Location = new System.Drawing.Point(1345, 11);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(73, 25);
            this.label84.TabIndex = 0;
            this.label84.Text = "label19";
            // 
            // label85
            // 
            this.label85.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label85.AutoSize = true;
            this.label85.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label85.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label85.Location = new System.Drawing.Point(1450, 13);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(66, 21);
            this.label85.TabIndex = 0;
            this.label85.Text = "label19";
            // 
            // label86
            // 
            this.label86.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label86.AutoSize = true;
            this.label86.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label86.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label86.Location = new System.Drawing.Point(1551, 13);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(66, 21);
            this.label86.TabIndex = 0;
            this.label86.Text = "label19";
            // 
            // label87
            // 
            this.label87.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label87.AutoSize = true;
            this.label87.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.label87.Location = new System.Drawing.Point(1345, 57);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(73, 25);
            this.label87.TabIndex = 0;
            this.label87.Text = "label19";
            // 
            // label88
            // 
            this.label88.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label88.AutoSize = true;
            this.label88.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.label88.Location = new System.Drawing.Point(1345, 103);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(73, 25);
            this.label88.TabIndex = 0;
            this.label88.Text = "label19";
            // 
            // label89
            // 
            this.label89.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label89.AutoSize = true;
            this.label89.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.label89.Location = new System.Drawing.Point(1345, 149);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(73, 25);
            this.label89.TabIndex = 0;
            this.label89.Text = "label19";
            // 
            // label90
            // 
            this.label90.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label90.AutoSize = true;
            this.label90.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.label90.Location = new System.Drawing.Point(1345, 195);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(73, 25);
            this.label90.TabIndex = 0;
            this.label90.Text = "label19";
            // 
            // label91
            // 
            this.label91.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label91.AutoSize = true;
            this.label91.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.label91.Location = new System.Drawing.Point(1345, 241);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(73, 25);
            this.label91.TabIndex = 0;
            this.label91.Text = "label19";
            // 
            // label92
            // 
            this.label92.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label92.AutoSize = true;
            this.label92.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.label92.Location = new System.Drawing.Point(1345, 287);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(73, 25);
            this.label92.TabIndex = 0;
            this.label92.Text = "label19";
            // 
            // label93
            // 
            this.label93.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label93.AutoSize = true;
            this.label93.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.label93.Location = new System.Drawing.Point(1345, 333);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(73, 25);
            this.label93.TabIndex = 0;
            this.label93.Text = "label19";
            // 
            // label94
            // 
            this.label94.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label94.AutoSize = true;
            this.label94.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.label94.Location = new System.Drawing.Point(1345, 379);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(73, 25);
            this.label94.TabIndex = 0;
            this.label94.Text = "label19";
            // 
            // label95
            // 
            this.label95.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label95.AutoSize = true;
            this.label95.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.label95.Location = new System.Drawing.Point(1345, 425);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(73, 25);
            this.label95.TabIndex = 0;
            this.label95.Text = "label19";
            // 
            // label96
            // 
            this.label96.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label96.AutoSize = true;
            this.label96.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.label96.Location = new System.Drawing.Point(1345, 471);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(73, 25);
            this.label96.TabIndex = 0;
            this.label96.Text = "label19";
            // 
            // label97
            // 
            this.label97.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label97.AutoSize = true;
            this.label97.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.label97.Location = new System.Drawing.Point(1345, 517);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(73, 25);
            this.label97.TabIndex = 0;
            this.label97.Text = "label19";
            // 
            // label98
            // 
            this.label98.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label98.AutoSize = true;
            this.label98.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label98.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label98.Location = new System.Drawing.Point(1450, 59);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(66, 21);
            this.label98.TabIndex = 0;
            this.label98.Text = "label19";
            // 
            // label99
            // 
            this.label99.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label99.AutoSize = true;
            this.label99.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label99.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label99.Location = new System.Drawing.Point(1450, 105);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(66, 21);
            this.label99.TabIndex = 0;
            this.label99.Text = "label19";
            // 
            // label100
            // 
            this.label100.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label100.AutoSize = true;
            this.label100.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label100.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label100.Location = new System.Drawing.Point(1450, 151);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(66, 21);
            this.label100.TabIndex = 0;
            this.label100.Text = "label19";
            // 
            // label101
            // 
            this.label101.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label101.AutoSize = true;
            this.label101.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label101.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label101.Location = new System.Drawing.Point(1450, 197);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(66, 21);
            this.label101.TabIndex = 0;
            this.label101.Text = "label19";
            // 
            // label102
            // 
            this.label102.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label102.AutoSize = true;
            this.label102.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label102.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label102.Location = new System.Drawing.Point(1450, 243);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(66, 21);
            this.label102.TabIndex = 0;
            this.label102.Text = "label19";
            // 
            // label103
            // 
            this.label103.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label103.AutoSize = true;
            this.label103.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label103.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label103.Location = new System.Drawing.Point(1450, 289);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(66, 21);
            this.label103.TabIndex = 0;
            this.label103.Text = "label19";
            // 
            // label104
            // 
            this.label104.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label104.AutoSize = true;
            this.label104.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label104.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label104.Location = new System.Drawing.Point(1450, 335);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(66, 21);
            this.label104.TabIndex = 0;
            this.label104.Text = "label19";
            // 
            // label105
            // 
            this.label105.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label105.AutoSize = true;
            this.label105.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label105.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label105.Location = new System.Drawing.Point(1450, 381);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(66, 21);
            this.label105.TabIndex = 0;
            this.label105.Text = "label19";
            // 
            // label106
            // 
            this.label106.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label106.AutoSize = true;
            this.label106.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label106.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label106.Location = new System.Drawing.Point(1450, 427);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(66, 21);
            this.label106.TabIndex = 0;
            this.label106.Text = "label19";
            // 
            // label107
            // 
            this.label107.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label107.AutoSize = true;
            this.label107.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label107.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label107.Location = new System.Drawing.Point(1450, 473);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(66, 21);
            this.label107.TabIndex = 0;
            this.label107.Text = "label19";
            // 
            // label108
            // 
            this.label108.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label108.AutoSize = true;
            this.label108.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label108.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label108.Location = new System.Drawing.Point(1450, 519);
            this.label108.Name = "label108";
            this.label108.Size = new System.Drawing.Size(66, 21);
            this.label108.TabIndex = 0;
            this.label108.Text = "label19";
            // 
            // label109
            // 
            this.label109.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label109.AutoSize = true;
            this.label109.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label109.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label109.Location = new System.Drawing.Point(1551, 59);
            this.label109.Name = "label109";
            this.label109.Size = new System.Drawing.Size(66, 21);
            this.label109.TabIndex = 0;
            this.label109.Text = "label19";
            // 
            // label110
            // 
            this.label110.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label110.AutoSize = true;
            this.label110.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label110.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label110.Location = new System.Drawing.Point(1551, 105);
            this.label110.Name = "label110";
            this.label110.Size = new System.Drawing.Size(66, 21);
            this.label110.TabIndex = 0;
            this.label110.Text = "label19";
            // 
            // label111
            // 
            this.label111.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label111.AutoSize = true;
            this.label111.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label111.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label111.Location = new System.Drawing.Point(1551, 151);
            this.label111.Name = "label111";
            this.label111.Size = new System.Drawing.Size(66, 21);
            this.label111.TabIndex = 0;
            this.label111.Text = "label19";
            // 
            // label112
            // 
            this.label112.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label112.AutoSize = true;
            this.label112.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label112.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label112.Location = new System.Drawing.Point(1551, 197);
            this.label112.Name = "label112";
            this.label112.Size = new System.Drawing.Size(66, 21);
            this.label112.TabIndex = 0;
            this.label112.Text = "label19";
            // 
            // label113
            // 
            this.label113.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label113.AutoSize = true;
            this.label113.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label113.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label113.Location = new System.Drawing.Point(1551, 243);
            this.label113.Name = "label113";
            this.label113.Size = new System.Drawing.Size(66, 21);
            this.label113.TabIndex = 0;
            this.label113.Text = "label19";
            // 
            // label114
            // 
            this.label114.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label114.AutoSize = true;
            this.label114.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label114.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label114.Location = new System.Drawing.Point(1551, 289);
            this.label114.Name = "label114";
            this.label114.Size = new System.Drawing.Size(66, 21);
            this.label114.TabIndex = 0;
            this.label114.Text = "label19";
            // 
            // label115
            // 
            this.label115.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label115.AutoSize = true;
            this.label115.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label115.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label115.Location = new System.Drawing.Point(1551, 335);
            this.label115.Name = "label115";
            this.label115.Size = new System.Drawing.Size(66, 21);
            this.label115.TabIndex = 0;
            this.label115.Text = "label19";
            // 
            // label116
            // 
            this.label116.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label116.AutoSize = true;
            this.label116.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label116.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label116.Location = new System.Drawing.Point(1551, 381);
            this.label116.Name = "label116";
            this.label116.Size = new System.Drawing.Size(66, 21);
            this.label116.TabIndex = 0;
            this.label116.Text = "label19";
            // 
            // label117
            // 
            this.label117.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label117.AutoSize = true;
            this.label117.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label117.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label117.Location = new System.Drawing.Point(1551, 427);
            this.label117.Name = "label117";
            this.label117.Size = new System.Drawing.Size(66, 21);
            this.label117.TabIndex = 0;
            this.label117.Text = "label19";
            // 
            // label118
            // 
            this.label118.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label118.AutoSize = true;
            this.label118.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label118.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label118.Location = new System.Drawing.Point(1551, 473);
            this.label118.Name = "label118";
            this.label118.Size = new System.Drawing.Size(66, 21);
            this.label118.TabIndex = 0;
            this.label118.Text = "label19";
            // 
            // label119
            // 
            this.label119.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label119.AutoSize = true;
            this.label119.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label119.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label119.Location = new System.Drawing.Point(1551, 519);
            this.label119.Name = "label119";
            this.label119.Size = new System.Drawing.Size(66, 21);
            this.label119.TabIndex = 0;
            this.label119.Text = "label19";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel1.ColumnCount = 11;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 5F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 5F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tableLayoutPanel1.Controls.Add(this.label2, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label3, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.label4, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.label5, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.label6, 6, 0);
            this.tableLayoutPanel1.Controls.Add(this.label7, 8, 0);
            this.tableLayoutPanel1.Controls.Add(this.label8, 7, 0);
            this.tableLayoutPanel1.Controls.Add(this.label9, 9, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 88);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1666, 34);
            this.tableLayoutPanel1.TabIndex = 10;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Yellow;
            this.label2.Location = new System.Drawing.Point(10, 4);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(203, 25);
            this.label2.TabIndex = 0;
            this.label2.Text = "Hydraulic Parameters";
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Yellow;
            this.label3.Location = new System.Drawing.Point(518, 4);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 25);
            this.label3.TabIndex = 0;
            this.label3.Text = "Units";
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Yellow;
            this.label4.Location = new System.Drawing.Point(619, 1);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(62, 32);
            this.label4.TabIndex = 0;
            this.label4.Text = "Avg. Value";
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Yellow;
            this.label5.Location = new System.Drawing.Point(720, 1);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(62, 32);
            this.label5.TabIndex = 0;
            this.label5.Text = "Real Value";
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Yellow;
            this.label6.Location = new System.Drawing.Point(827, 4);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(216, 25);
            this.label6.TabIndex = 0;
            this.label6.Text = "Mechanical Parameters";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Yellow;
            this.label7.Location = new System.Drawing.Point(1436, 1);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(62, 32);
            this.label7.TabIndex = 0;
            this.label7.Text = "Avg. Value";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Yellow;
            this.label8.Location = new System.Drawing.Point(1335, 1);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(58, 25);
            this.label8.TabIndex = 0;
            this.label8.Text = "Units";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Yellow;
            this.label9.Location = new System.Drawing.Point(1537, 1);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(62, 32);
            this.label9.TabIndex = 0;
            this.label9.Text = "Real Value";
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tableLayoutPanel4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.tableLayoutPanel4.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel4.ColumnCount = 11;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 5F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 5F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tableLayoutPanel4.Controls.Add(this.label17, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.label10, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.label11, 2, 0);
            this.tableLayoutPanel4.Controls.Add(this.label12, 3, 0);
            this.tableLayoutPanel4.Controls.Add(this.label18, 6, 0);
            this.tableLayoutPanel4.Controls.Add(this.label13, 4, 0);
            this.tableLayoutPanel4.Controls.Add(this.label14, 7, 0);
            this.tableLayoutPanel4.Controls.Add(this.label15, 8, 0);
            this.tableLayoutPanel4.Controls.Add(this.label16, 9, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 688);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(1666, 34);
            this.tableLayoutPanel4.TabIndex = 10;
            // 
            // label17
            // 
            this.label17.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Yellow;
            this.label17.Location = new System.Drawing.Point(10, 4);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(195, 25);
            this.label17.TabIndex = 0;
            this.label17.Text = "Electrical Parameters";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.label10.Location = new System.Drawing.Point(4, 1);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(1, 30);
            this.label10.TabIndex = 0;
            this.label10.Text = "Real Value";
            // 
            // label11
            // 
            this.label11.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Yellow;
            this.label11.Location = new System.Drawing.Point(518, 4);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(58, 25);
            this.label11.TabIndex = 0;
            this.label11.Text = "Units";
            // 
            // label12
            // 
            this.label12.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Yellow;
            this.label12.Location = new System.Drawing.Point(619, 1);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(62, 32);
            this.label12.TabIndex = 0;
            this.label12.Text = "Avg. Value";
            // 
            // label18
            // 
            this.label18.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Yellow;
            this.label18.Location = new System.Drawing.Point(827, 4);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(201, 25);
            this.label18.TabIndex = 0;
            this.label18.Text = "Vibration Parameters";
            // 
            // label13
            // 
            this.label13.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Yellow;
            this.label13.Location = new System.Drawing.Point(720, 1);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(62, 32);
            this.label13.TabIndex = 0;
            this.label13.Text = "Real Value";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Yellow;
            this.label14.Location = new System.Drawing.Point(1335, 1);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(58, 25);
            this.label14.TabIndex = 0;
            this.label14.Text = "Units";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Yellow;
            this.label15.Location = new System.Drawing.Point(1436, 1);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(62, 32);
            this.label15.TabIndex = 0;
            this.label15.Text = "Avg. Value";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Yellow;
            this.label16.Location = new System.Drawing.Point(1537, 1);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(62, 32);
            this.label16.TabIndex = 0;
            this.label16.Text = "Real Value";
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel7.ColumnCount = 11;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 5F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 5F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tableLayoutPanel7.Controls.Add(this.label43, 1, 0);
            this.tableLayoutPanel7.Controls.Add(this.label44, 1, 1);
            this.tableLayoutPanel7.Controls.Add(this.label45, 1, 2);
            this.tableLayoutPanel7.Controls.Add(this.label46, 1, 3);
            this.tableLayoutPanel7.Controls.Add(this.label47, 1, 4);
            this.tableLayoutPanel7.Controls.Add(this.label120, 2, 0);
            this.tableLayoutPanel7.Controls.Add(this.label121, 2, 1);
            this.tableLayoutPanel7.Controls.Add(this.label122, 2, 2);
            this.tableLayoutPanel7.Controls.Add(this.label123, 2, 3);
            this.tableLayoutPanel7.Controls.Add(this.label124, 2, 4);
            this.tableLayoutPanel7.Controls.Add(this.label125, 3, 0);
            this.tableLayoutPanel7.Controls.Add(this.label126, 3, 1);
            this.tableLayoutPanel7.Controls.Add(this.label127, 3, 2);
            this.tableLayoutPanel7.Controls.Add(this.label128, 3, 3);
            this.tableLayoutPanel7.Controls.Add(this.label129, 3, 4);
            this.tableLayoutPanel7.Controls.Add(this.label130, 4, 0);
            this.tableLayoutPanel7.Controls.Add(this.label131, 4, 1);
            this.tableLayoutPanel7.Controls.Add(this.label132, 4, 2);
            this.tableLayoutPanel7.Controls.Add(this.label133, 4, 3);
            this.tableLayoutPanel7.Controls.Add(this.label134, 4, 4);
            this.tableLayoutPanel7.Controls.Add(this.label135, 7, 0);
            this.tableLayoutPanel7.Controls.Add(this.label136, 7, 1);
            this.tableLayoutPanel7.Controls.Add(this.label137, 7, 2);
            this.tableLayoutPanel7.Controls.Add(this.label150, 6, 0);
            this.tableLayoutPanel7.Controls.Add(this.label138, 7, 3);
            this.tableLayoutPanel7.Controls.Add(this.label139, 7, 4);
            this.tableLayoutPanel7.Controls.Add(this.label140, 8, 0);
            this.tableLayoutPanel7.Controls.Add(this.label141, 8, 1);
            this.tableLayoutPanel7.Controls.Add(this.label142, 8, 2);
            this.tableLayoutPanel7.Controls.Add(this.label143, 8, 3);
            this.tableLayoutPanel7.Controls.Add(this.label144, 8, 4);
            this.tableLayoutPanel7.Controls.Add(this.label145, 9, 0);
            this.tableLayoutPanel7.Controls.Add(this.label146, 9, 1);
            this.tableLayoutPanel7.Controls.Add(this.label147, 9, 2);
            this.tableLayoutPanel7.Controls.Add(this.label148, 9, 3);
            this.tableLayoutPanel7.Controls.Add(this.label149, 9, 4);
            this.tableLayoutPanel7.Controls.Add(this.label151, 6, 1);
            this.tableLayoutPanel7.Controls.Add(this.label152, 6, 2);
            this.tableLayoutPanel7.Controls.Add(this.label153, 6, 3);
            this.tableLayoutPanel7.Controls.Add(this.label154, 6, 4);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel7.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tableLayoutPanel7.Location = new System.Drawing.Point(3, 728);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 5;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(1666, 234);
            this.tableLayoutPanel7.TabIndex = 10;
            // 
            // label43
            // 
            this.label43.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label43.AutoSize = true;
            this.label43.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.label43.Location = new System.Drawing.Point(10, 11);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(73, 25);
            this.label43.TabIndex = 0;
            this.label43.Text = "label19";
            // 
            // label44
            // 
            this.label44.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label44.AutoSize = true;
            this.label44.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.label44.Location = new System.Drawing.Point(10, 57);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(73, 25);
            this.label44.TabIndex = 0;
            this.label44.Text = "label19";
            // 
            // label45
            // 
            this.label45.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label45.AutoSize = true;
            this.label45.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.label45.Location = new System.Drawing.Point(10, 103);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(73, 25);
            this.label45.TabIndex = 0;
            this.label45.Text = "label19";
            // 
            // label46
            // 
            this.label46.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label46.AutoSize = true;
            this.label46.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.label46.Location = new System.Drawing.Point(10, 149);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(73, 25);
            this.label46.TabIndex = 0;
            this.label46.Text = "label19";
            // 
            // label47
            // 
            this.label47.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label47.AutoSize = true;
            this.label47.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.label47.Location = new System.Drawing.Point(10, 196);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(73, 25);
            this.label47.TabIndex = 0;
            this.label47.Text = "label19";
            // 
            // label120
            // 
            this.label120.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label120.AutoSize = true;
            this.label120.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.label120.Location = new System.Drawing.Point(528, 11);
            this.label120.Name = "label120";
            this.label120.Size = new System.Drawing.Size(73, 25);
            this.label120.TabIndex = 0;
            this.label120.Text = "label19";
            // 
            // label121
            // 
            this.label121.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label121.AutoSize = true;
            this.label121.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.label121.Location = new System.Drawing.Point(528, 57);
            this.label121.Name = "label121";
            this.label121.Size = new System.Drawing.Size(73, 25);
            this.label121.TabIndex = 0;
            this.label121.Text = "label19";
            // 
            // label122
            // 
            this.label122.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label122.AutoSize = true;
            this.label122.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.label122.Location = new System.Drawing.Point(528, 103);
            this.label122.Name = "label122";
            this.label122.Size = new System.Drawing.Size(73, 25);
            this.label122.TabIndex = 0;
            this.label122.Text = "label19";
            // 
            // label123
            // 
            this.label123.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label123.AutoSize = true;
            this.label123.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.label123.Location = new System.Drawing.Point(528, 149);
            this.label123.Name = "label123";
            this.label123.Size = new System.Drawing.Size(73, 25);
            this.label123.TabIndex = 0;
            this.label123.Text = "label19";
            // 
            // label124
            // 
            this.label124.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label124.AutoSize = true;
            this.label124.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.label124.Location = new System.Drawing.Point(528, 196);
            this.label124.Name = "label124";
            this.label124.Size = new System.Drawing.Size(73, 25);
            this.label124.TabIndex = 0;
            this.label124.Text = "label19";
            // 
            // label125
            // 
            this.label125.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label125.AutoSize = true;
            this.label125.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label125.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label125.Location = new System.Drawing.Point(633, 13);
            this.label125.Name = "label125";
            this.label125.Size = new System.Drawing.Size(66, 21);
            this.label125.TabIndex = 0;
            this.label125.Text = "label19";
            // 
            // label126
            // 
            this.label126.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label126.AutoSize = true;
            this.label126.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label126.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label126.Location = new System.Drawing.Point(633, 59);
            this.label126.Name = "label126";
            this.label126.Size = new System.Drawing.Size(66, 21);
            this.label126.TabIndex = 0;
            this.label126.Text = "label19";
            // 
            // label127
            // 
            this.label127.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label127.AutoSize = true;
            this.label127.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label127.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label127.Location = new System.Drawing.Point(633, 105);
            this.label127.Name = "label127";
            this.label127.Size = new System.Drawing.Size(66, 21);
            this.label127.TabIndex = 0;
            this.label127.Text = "label19";
            // 
            // label128
            // 
            this.label128.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label128.AutoSize = true;
            this.label128.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label128.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label128.Location = new System.Drawing.Point(633, 151);
            this.label128.Name = "label128";
            this.label128.Size = new System.Drawing.Size(66, 21);
            this.label128.TabIndex = 0;
            this.label128.Text = "label19";
            // 
            // label129
            // 
            this.label129.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label129.AutoSize = true;
            this.label129.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label129.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label129.Location = new System.Drawing.Point(633, 198);
            this.label129.Name = "label129";
            this.label129.Size = new System.Drawing.Size(66, 21);
            this.label129.TabIndex = 0;
            this.label129.Text = "label19";
            // 
            // label130
            // 
            this.label130.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label130.AutoSize = true;
            this.label130.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label130.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label130.Location = new System.Drawing.Point(734, 13);
            this.label130.Name = "label130";
            this.label130.Size = new System.Drawing.Size(66, 21);
            this.label130.TabIndex = 0;
            this.label130.Text = "label19";
            // 
            // label131
            // 
            this.label131.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label131.AutoSize = true;
            this.label131.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label131.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label131.Location = new System.Drawing.Point(734, 59);
            this.label131.Name = "label131";
            this.label131.Size = new System.Drawing.Size(66, 21);
            this.label131.TabIndex = 0;
            this.label131.Text = "label19";
            // 
            // label132
            // 
            this.label132.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label132.AutoSize = true;
            this.label132.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label132.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label132.Location = new System.Drawing.Point(734, 105);
            this.label132.Name = "label132";
            this.label132.Size = new System.Drawing.Size(66, 21);
            this.label132.TabIndex = 0;
            this.label132.Text = "label19";
            // 
            // label133
            // 
            this.label133.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label133.AutoSize = true;
            this.label133.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label133.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label133.Location = new System.Drawing.Point(734, 151);
            this.label133.Name = "label133";
            this.label133.Size = new System.Drawing.Size(66, 21);
            this.label133.TabIndex = 0;
            this.label133.Text = "label19";
            // 
            // label134
            // 
            this.label134.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label134.AutoSize = true;
            this.label134.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label134.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label134.Location = new System.Drawing.Point(734, 198);
            this.label134.Name = "label134";
            this.label134.Size = new System.Drawing.Size(66, 21);
            this.label134.TabIndex = 0;
            this.label134.Text = "label19";
            // 
            // label135
            // 
            this.label135.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label135.AutoSize = true;
            this.label135.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.label135.Location = new System.Drawing.Point(1345, 11);
            this.label135.Name = "label135";
            this.label135.Size = new System.Drawing.Size(73, 25);
            this.label135.TabIndex = 0;
            this.label135.Text = "label19";
            // 
            // label136
            // 
            this.label136.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label136.AutoSize = true;
            this.label136.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.label136.Location = new System.Drawing.Point(1345, 57);
            this.label136.Name = "label136";
            this.label136.Size = new System.Drawing.Size(73, 25);
            this.label136.TabIndex = 0;
            this.label136.Text = "label19";
            // 
            // label137
            // 
            this.label137.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label137.AutoSize = true;
            this.label137.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.label137.Location = new System.Drawing.Point(1345, 103);
            this.label137.Name = "label137";
            this.label137.Size = new System.Drawing.Size(73, 25);
            this.label137.TabIndex = 0;
            this.label137.Text = "label19";
            // 
            // label150
            // 
            this.label150.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label150.AutoSize = true;
            this.label150.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.label150.Location = new System.Drawing.Point(827, 11);
            this.label150.Name = "label150";
            this.label150.Size = new System.Drawing.Size(73, 25);
            this.label150.TabIndex = 0;
            this.label150.Text = "label19";
            // 
            // label138
            // 
            this.label138.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label138.AutoSize = true;
            this.label138.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.label138.Location = new System.Drawing.Point(1345, 149);
            this.label138.Name = "label138";
            this.label138.Size = new System.Drawing.Size(73, 25);
            this.label138.TabIndex = 0;
            this.label138.Text = "label19";
            // 
            // label139
            // 
            this.label139.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label139.AutoSize = true;
            this.label139.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.label139.Location = new System.Drawing.Point(1345, 196);
            this.label139.Name = "label139";
            this.label139.Size = new System.Drawing.Size(73, 25);
            this.label139.TabIndex = 0;
            this.label139.Text = "label19";
            // 
            // label140
            // 
            this.label140.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label140.AutoSize = true;
            this.label140.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label140.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label140.Location = new System.Drawing.Point(1450, 13);
            this.label140.Name = "label140";
            this.label140.Size = new System.Drawing.Size(66, 21);
            this.label140.TabIndex = 0;
            this.label140.Text = "label19";
            // 
            // label141
            // 
            this.label141.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label141.AutoSize = true;
            this.label141.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label141.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label141.Location = new System.Drawing.Point(1450, 59);
            this.label141.Name = "label141";
            this.label141.Size = new System.Drawing.Size(66, 21);
            this.label141.TabIndex = 0;
            this.label141.Text = "label19";
            // 
            // label142
            // 
            this.label142.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label142.AutoSize = true;
            this.label142.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label142.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label142.Location = new System.Drawing.Point(1450, 105);
            this.label142.Name = "label142";
            this.label142.Size = new System.Drawing.Size(66, 21);
            this.label142.TabIndex = 0;
            this.label142.Text = "label19";
            // 
            // label143
            // 
            this.label143.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label143.AutoSize = true;
            this.label143.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label143.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label143.Location = new System.Drawing.Point(1450, 151);
            this.label143.Name = "label143";
            this.label143.Size = new System.Drawing.Size(66, 21);
            this.label143.TabIndex = 0;
            this.label143.Text = "label19";
            // 
            // label144
            // 
            this.label144.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label144.AutoSize = true;
            this.label144.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label144.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label144.Location = new System.Drawing.Point(1450, 198);
            this.label144.Name = "label144";
            this.label144.Size = new System.Drawing.Size(66, 21);
            this.label144.TabIndex = 0;
            this.label144.Text = "label19";
            // 
            // label145
            // 
            this.label145.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label145.AutoSize = true;
            this.label145.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label145.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label145.Location = new System.Drawing.Point(1551, 13);
            this.label145.Name = "label145";
            this.label145.Size = new System.Drawing.Size(66, 21);
            this.label145.TabIndex = 0;
            this.label145.Text = "label19";
            // 
            // label146
            // 
            this.label146.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label146.AutoSize = true;
            this.label146.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label146.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label146.Location = new System.Drawing.Point(1551, 59);
            this.label146.Name = "label146";
            this.label146.Size = new System.Drawing.Size(66, 21);
            this.label146.TabIndex = 0;
            this.label146.Text = "label19";
            // 
            // label147
            // 
            this.label147.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label147.AutoSize = true;
            this.label147.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label147.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label147.Location = new System.Drawing.Point(1551, 105);
            this.label147.Name = "label147";
            this.label147.Size = new System.Drawing.Size(66, 21);
            this.label147.TabIndex = 0;
            this.label147.Text = "label19";
            // 
            // label148
            // 
            this.label148.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label148.AutoSize = true;
            this.label148.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label148.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label148.Location = new System.Drawing.Point(1551, 151);
            this.label148.Name = "label148";
            this.label148.Size = new System.Drawing.Size(66, 21);
            this.label148.TabIndex = 0;
            this.label148.Text = "label19";
            // 
            // label149
            // 
            this.label149.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label149.AutoSize = true;
            this.label149.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label149.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label149.Location = new System.Drawing.Point(1551, 198);
            this.label149.Name = "label149";
            this.label149.Size = new System.Drawing.Size(66, 21);
            this.label149.TabIndex = 0;
            this.label149.Text = "label19";
            // 
            // label151
            // 
            this.label151.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label151.AutoSize = true;
            this.label151.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.label151.Location = new System.Drawing.Point(827, 57);
            this.label151.Name = "label151";
            this.label151.Size = new System.Drawing.Size(73, 25);
            this.label151.TabIndex = 0;
            this.label151.Text = "label19";
            // 
            // label152
            // 
            this.label152.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label152.AutoSize = true;
            this.label152.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.label152.Location = new System.Drawing.Point(827, 103);
            this.label152.Name = "label152";
            this.label152.Size = new System.Drawing.Size(73, 25);
            this.label152.TabIndex = 0;
            this.label152.Text = "label19";
            // 
            // label153
            // 
            this.label153.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label153.AutoSize = true;
            this.label153.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.label153.Location = new System.Drawing.Point(827, 149);
            this.label153.Name = "label153";
            this.label153.Size = new System.Drawing.Size(73, 25);
            this.label153.TabIndex = 0;
            this.label153.Text = "label19";
            // 
            // label154
            // 
            this.label154.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label154.AutoSize = true;
            this.label154.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.label154.Location = new System.Drawing.Point(827, 196);
            this.label154.Name = "label154";
            this.label154.Size = new System.Drawing.Size(73, 25);
            this.label154.TabIndex = 0;
            this.label154.Text = "label19";
            // 
            // DASWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;

            this.ClientSize = new System.Drawing.Size(1672, 1016);
            this.Controls.Add(this.tableMain);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "DASWindow";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "DASWindow";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.DASWindow_Load);
            this.tableMain.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel7.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox tbConsole;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Button btnCompute;
        private System.Windows.Forms.Button btnPause;
        private System.Windows.Forms.Button btnStartAcq;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblTime;
        private System.Windows.Forms.Label lblProject;
        private System.Windows.Forms.Label lblMachineUCode;
        private System.Windows.Forms.Label lblTestReference;
        private System.Windows.Forms.Label lblDate;
        private System.Windows.Forms.TableLayoutPanel tableMain;
        private System.Windows.Forms.Button button1;
        //private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        //private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        //private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        //private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        //private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        //private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        //private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private MyTableLayoutPanel tableLayoutPanel5;
        private MyTableLayoutPanel tableLayoutPanel6;
        private MyTableLayoutPanel tableLayoutPanel2;
        private MyTableLayoutPanel tableLayoutPanel3;
        private MyTableLayoutPanel tableLayoutPanel1;
        private MyTableLayoutPanel tableLayoutPanel4;
        private MyTableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.Label label90;
        private System.Windows.Forms.Label label91;
        private System.Windows.Forms.Label label92;
        private System.Windows.Forms.Label label93;
        private System.Windows.Forms.Label label94;
        private System.Windows.Forms.Label label95;
        private System.Windows.Forms.Label label96;
        private System.Windows.Forms.Label label97;
        private System.Windows.Forms.Label label98;
        private System.Windows.Forms.Label label99;
        private System.Windows.Forms.Label label100;
        private System.Windows.Forms.Label label101;
        private System.Windows.Forms.Label label102;
        private System.Windows.Forms.Label label103;
        private System.Windows.Forms.Label label104;
        private System.Windows.Forms.Label label105;
        private System.Windows.Forms.Label label106;
        private System.Windows.Forms.Label label107;
        private System.Windows.Forms.Label label108;
        private System.Windows.Forms.Label label109;
        private System.Windows.Forms.Label label110;
        private System.Windows.Forms.Label label111;
        private System.Windows.Forms.Label label112;
        private System.Windows.Forms.Label label113;
        private System.Windows.Forms.Label label114;
        private System.Windows.Forms.Label label115;
        private System.Windows.Forms.Label label116;
        private System.Windows.Forms.Label label117;
        private System.Windows.Forms.Label label118;
        private System.Windows.Forms.Label label119;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label120;
        private System.Windows.Forms.Label label121;
        private System.Windows.Forms.Label label122;
        private System.Windows.Forms.Label label123;
        private System.Windows.Forms.Label label124;
        private System.Windows.Forms.Label label125;
        private System.Windows.Forms.Label label126;
        private System.Windows.Forms.Label label127;
        private System.Windows.Forms.Label label128;
        private System.Windows.Forms.Label label129;
        private System.Windows.Forms.Label label130;
        private System.Windows.Forms.Label label131;
        private System.Windows.Forms.Label label132;
        private System.Windows.Forms.Label label133;
        private System.Windows.Forms.Label label134;
        private System.Windows.Forms.Label label135;
        private System.Windows.Forms.Label label136;
        private System.Windows.Forms.Label label137;
        private System.Windows.Forms.Label label138;
        private System.Windows.Forms.Label label139;
        private System.Windows.Forms.Label label150;
        private System.Windows.Forms.Label label140;
        private System.Windows.Forms.Label label141;
        private System.Windows.Forms.Label label142;
        private System.Windows.Forms.Label label143;
        private System.Windows.Forms.Label label144;
        private System.Windows.Forms.Label label145;
        private System.Windows.Forms.Label label146;
        private System.Windows.Forms.Label label147;
        private System.Windows.Forms.Label label148;
        private System.Windows.Forms.Label label149;
        private System.Windows.Forms.Label label151;
        private System.Windows.Forms.Label label152;
        private System.Windows.Forms.Label label153;
        private System.Windows.Forms.Label label154;
    }
}