﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DasCoreLib;

namespace WinFormDasCoreClient
{
    public partial class CommonLookupTablesWindow : Form
    {
        //DASCORE API
        DasCoreLib.DasCore dc;
        BindingSource lookup_bs;
        List<LookupTableDetails> lookuptable;
        LookupTableDetails sel = new LookupTableDetails();
        
        private LookupDataWindow LDwin;

        public CommonLookupTablesWindow(DasCoreLib.DasCore dc)
        {
            InitializeComponent();
            this.dc = dc;
            lookup_bs = new BindingSource();
            this.RefreshLookupTableList();
            sel.tableName = "";                       
        }

        private void CommonLookupTablesWindow_Load(object sender, EventArgs e)
        {
            // SETTING FOR LOOKUP TABLE LIST DATA GRID VIEW.

            this.dgvLookupTableList.AutoGenerateColumns = false;
            this.dgvLookupTableList.AutoSize = true;
            this.dgvLookupTableList.DataSource = lookup_bs;
            DataGridViewColumn nameCol = new DataGridViewTextBoxColumn();
            nameCol.DataPropertyName = "TableName";
            nameCol.Name = "Lookup Table Name";
            nameCol.Width = 275;
            
            this.dgvLookupTableList.Columns.Add(nameCol);
        }

        private void btnNewLookupTable_Click(object sender, EventArgs e)
        {
            string tablename = this.txtTableName.Text;
            if (tablename == "")
            {
                MessageBox.Show("Please enter the table name that is to be created.");

                return;
            }
            //DASCORE API
            if (dc.DoesLookupTableExist(tablename))
            {
                MessageBox.Show("Error : Table name already exists");
                return;
            }
            else
            {
                try
                {
                    //DASCORE API
                    dc.CreateNewLookupTable(tablename);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }

            RefreshLookupTableList();
        }

        private void dgvLookupTableList_CellClick(object sender, DataGridViewCellEventArgs e)
        {            
            if (this.dgvLookupTableList.SelectedRows.Count > 0)
                sel = (LookupTableDetails)this.dgvLookupTableList.SelectedRows[0].DataBoundItem;

            this.txtTableName.Text = sel.TableName;
        }

        private void RefreshLookupTableList()
        {
            lookup_bs.Clear();

            //DASCORE API
            lookuptable = dc.GetAvailableCommonLookupTableNames();

            foreach (LookupTableDetails lt in lookuptable)
            {
                lookup_bs.Add(lt);
            }

            this.Invalidate();
        }

        private void btnModifyLookupTable_Click(object sender, EventArgs e)
        {
            if (sel.TableName != "")
            {
                LDwin = new LookupDataWindow(dc, sel.TableName);
                LDwin.ShowDialog();
            }
            else MessageBox.Show("No lookup table selected!", "Error",MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

    }
}
