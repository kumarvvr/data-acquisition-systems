﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WinFormDasCoreClient
{
    public partial class TimedWaitWindow : Form
    {

        int completedTicks = 0;
        int totalTicks = 0;
        string[] messages;
        

        public TimedWaitWindow(string[] messages, int totalMilliseconds)
        {
            InitializeComponent();
            timer1.Interval = totalMilliseconds / messages.Length;
            this.messages = messages;
            totalTicks = messages.Length;
            this.btnContinue.Enabled = false;
            progressBar1.Style = ProgressBarStyle.Marquee;
            progressBar1.MarqueeAnimationSpeed = 20;
            timer1.Start();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            completedTicks++;

            //progressBar1.Value = (int)(100 * completedTicks / totalTicks) ;

            this.tbConsole.Text += messages[completedTicks - 1] + Environment.NewLine;

            if (completedTicks == totalTicks)
            {
                timer1.Stop();
                //this.Close();
                progressBar1.Style = ProgressBarStyle.Continuous;
                progressBar1.Value = 100;
                this.btnContinue.Enabled = true;
            }
        }

        private void btnContinue_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
