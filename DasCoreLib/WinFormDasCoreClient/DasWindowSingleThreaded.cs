﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DasCoreLib;

namespace WinFormDasCoreClient
{
    public partial class DasWindowSingleThreaded : DevExpress.XtraEditors.XtraForm
    {
        DasCoreLib.DasCore dascore;
        List<ChannelData> chData;

        public DasWindowSingleThreaded(DasCore dc)
        {
            InitializeComponent();
            this.dascore = dc;

            
        }

        private void DasTimer_Tick(object sender, EventArgs e)
        {
            chData = dascore.ScanHardware();
            updateUI();
        }

        private void updateUI()
        {
            this.tbConsole.Text = "";

            for (int i = 0; i < this.chData.Count; i++)
            {
                tbConsole.Text += chData[i].pname + " : " + chData[i].computedValue.ToString() + Environment.NewLine;
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            DasTimer.Start();
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            DasTimer.Stop();
        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            if (DasTimer.Enabled)
                DasTimer.Stop();
            this.Close();
        }
    }
}