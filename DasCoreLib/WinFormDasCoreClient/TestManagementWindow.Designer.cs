﻿namespace WinFormDasCoreClient
{
    partial class TestManagementWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TestManagementWindow));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            this.statusbar = new System.Windows.Forms.StatusStrip();
            this.statusMUcode = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusProj = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel4 = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusMcNo = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel6 = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusTestref = new System.Windows.Forms.ToolStripStatusLabel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.tblMachineBtns = new System.Windows.Forms.TableLayoutPanel();
            this.btnAddProjMc = new System.Windows.Forms.Button();
            this.btnNewTest = new System.Windows.Forms.Button();
            this.btnUpdtProjMc = new System.Windows.Forms.Button();
            this.btnDelProjMc = new System.Windows.Forms.Button();
            this.dgvMachines = new System.Windows.Forms.DataGridView();
            this.tblTestbtns = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.button1 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.btnViewRpt = new System.Windows.Forms.Button();
            this.btnGenRpt = new System.Windows.Forms.Button();
            this.btnConductTest = new System.Windows.Forms.Button();
            this.btnContinueTest = new System.Windows.Forms.Button();
            this.btnDelTest = new System.Windows.Forms.Button();
            this.btnUpdtTest = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dgvTests = new System.Windows.Forms.DataGridView();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.dgvProject = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.tblprojBtns = new System.Windows.Forms.TableLayoutPanel();
            this.btnAddProject = new System.Windows.Forms.Button();
            this.btnUpdtSpParams = new System.Windows.Forms.Button();
            this.btnUpdateProj = new System.Windows.Forms.Button();
            this.statusbar.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tblMachineBtns.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMachines)).BeginInit();
            this.tblTestbtns.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTests)).BeginInit();
            this.tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProject)).BeginInit();
            this.tblprojBtns.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusbar
            // 
            this.statusbar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusMUcode,
            this.toolStripStatusLabel2,
            this.statusProj,
            this.toolStripStatusLabel4,
            this.statusMcNo,
            this.toolStripStatusLabel6,
            this.statusTestref});
            this.statusbar.Location = new System.Drawing.Point(0, 808);
            this.statusbar.Name = "statusbar";
            this.statusbar.Size = new System.Drawing.Size(1141, 22);
            this.statusbar.TabIndex = 0;
            this.statusbar.Text = "statusStrip1";
            // 
            // statusMUcode
            // 
            this.statusMUcode.BackColor = System.Drawing.SystemColors.Control;
            this.statusMUcode.Name = "statusMUcode";
            this.statusMUcode.Size = new System.Drawing.Size(109, 17);
            this.statusMUcode.Text = "toolStripStatusLabel1";
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.BackColor = System.Drawing.SystemColors.Control;
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(23, 17);
            this.toolStripStatusLabel2.Text = ">>";
            // 
            // statusProj
            // 
            this.statusProj.BackColor = System.Drawing.SystemColors.Control;
            this.statusProj.Name = "statusProj";
            this.statusProj.Size = new System.Drawing.Size(109, 17);
            this.statusProj.Text = "toolStripStatusLabel3";
            // 
            // toolStripStatusLabel4
            // 
            this.toolStripStatusLabel4.BackColor = System.Drawing.SystemColors.Control;
            this.toolStripStatusLabel4.Name = "toolStripStatusLabel4";
            this.toolStripStatusLabel4.Size = new System.Drawing.Size(23, 17);
            this.toolStripStatusLabel4.Text = ">>";
            // 
            // statusMcNo
            // 
            this.statusMcNo.BackColor = System.Drawing.SystemColors.Control;
            this.statusMcNo.Name = "statusMcNo";
            this.statusMcNo.Size = new System.Drawing.Size(109, 17);
            this.statusMcNo.Text = "toolStripStatusLabel5";
            // 
            // toolStripStatusLabel6
            // 
            this.toolStripStatusLabel6.BackColor = System.Drawing.SystemColors.Control;
            this.toolStripStatusLabel6.Name = "toolStripStatusLabel6";
            this.toolStripStatusLabel6.Size = new System.Drawing.Size(23, 17);
            this.toolStripStatusLabel6.Text = ">>";
            // 
            // statusTestref
            // 
            this.statusTestref.BackColor = System.Drawing.SystemColors.Control;
            this.statusTestref.Name = "statusTestref";
            this.statusTestref.Size = new System.Drawing.Size(109, 17);
            this.statusTestref.Text = "toolStripStatusLabel7";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 2F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 39.77173F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60.22827F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 2F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 1, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 5F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 5F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1141, 808);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel2.Controls.Add(this.tblMachineBtns, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.dgvMachines, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.tblTestbtns, 1, 3);
            this.tableLayoutPanel2.Controls.Add(this.label3, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.label2, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.dgvTests, 0, 3);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(457, 8);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 4;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(678, 792);
            this.tableLayoutPanel2.TabIndex = 7;
            // 
            // tblMachineBtns
            // 
            this.tblMachineBtns.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tblMachineBtns.ColumnCount = 1;
            this.tblMachineBtns.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tblMachineBtns.Controls.Add(this.btnAddProjMc, 0, 0);
            this.tblMachineBtns.Controls.Add(this.btnNewTest, 0, 1);
            this.tblMachineBtns.Controls.Add(this.btnUpdtProjMc, 0, 2);
            this.tblMachineBtns.Controls.Add(this.btnDelProjMc, 0, 3);
            this.tblMachineBtns.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tblMachineBtns.Location = new System.Drawing.Point(481, 53);
            this.tblMachineBtns.Name = "tblMachineBtns";
            this.tblMachineBtns.RowCount = 5;
            this.tblMachineBtns.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tblMachineBtns.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tblMachineBtns.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tblMachineBtns.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tblMachineBtns.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tblMachineBtns.Size = new System.Drawing.Size(194, 340);
            this.tblMachineBtns.TabIndex = 4;
            // 
            // btnAddProjMc
            // 
            this.btnAddProjMc.AutoEllipsis = true;
            this.btnAddProjMc.BackColor = System.Drawing.Color.Transparent;
            this.btnAddProjMc.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnAddProjMc.BackgroundImage")));
            this.btnAddProjMc.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAddProjMc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnAddProjMc.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnAddProjMc.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddProjMc.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnAddProjMc.Location = new System.Drawing.Point(2, 2);
            this.btnAddProjMc.Margin = new System.Windows.Forms.Padding(1);
            this.btnAddProjMc.Name = "btnAddProjMc";
            this.btnAddProjMc.Size = new System.Drawing.Size(190, 38);
            this.btnAddProjMc.TabIndex = 6;
            this.btnAddProjMc.Text = "Add New &Pump";
            this.btnAddProjMc.UseVisualStyleBackColor = false;
            this.btnAddProjMc.Click += new System.EventHandler(this.btnAddProjMc_Click);
            // 
            // btnNewTest
            // 
            this.btnNewTest.BackColor = System.Drawing.Color.Transparent;
            this.btnNewTest.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnNewTest.BackgroundImage")));
            this.btnNewTest.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnNewTest.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnNewTest.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnNewTest.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNewTest.ForeColor = System.Drawing.Color.Crimson;
            this.btnNewTest.Location = new System.Drawing.Point(2, 43);
            this.btnNewTest.Margin = new System.Windows.Forms.Padding(1);
            this.btnNewTest.Name = "btnNewTest";
            this.btnNewTest.Size = new System.Drawing.Size(190, 38);
            this.btnNewTest.TabIndex = 1;
            this.btnNewTest.Text = "Start &New Test";
            this.btnNewTest.UseVisualStyleBackColor = false;
            this.btnNewTest.Click += new System.EventHandler(this.btnNewTest_Click);
            // 
            // btnUpdtProjMc
            // 
            this.btnUpdtProjMc.BackColor = System.Drawing.Color.Transparent;
            this.btnUpdtProjMc.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnUpdtProjMc.BackgroundImage")));
            this.btnUpdtProjMc.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnUpdtProjMc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnUpdtProjMc.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnUpdtProjMc.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdtProjMc.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnUpdtProjMc.Location = new System.Drawing.Point(2, 84);
            this.btnUpdtProjMc.Margin = new System.Windows.Forms.Padding(1);
            this.btnUpdtProjMc.Name = "btnUpdtProjMc";
            this.btnUpdtProjMc.Size = new System.Drawing.Size(190, 38);
            this.btnUpdtProjMc.TabIndex = 7;
            this.btnUpdtProjMc.Text = "Edit Selected P&ump";
            this.btnUpdtProjMc.UseVisualStyleBackColor = false;
            this.btnUpdtProjMc.Click += new System.EventHandler(this.btnUpdtProjMc_Click);
            // 
            // btnDelProjMc
            // 
            this.btnDelProjMc.BackColor = System.Drawing.Color.Transparent;
            this.btnDelProjMc.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnDelProjMc.BackgroundImage")));
            this.btnDelProjMc.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnDelProjMc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnDelProjMc.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnDelProjMc.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelProjMc.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnDelProjMc.Location = new System.Drawing.Point(2, 125);
            this.btnDelProjMc.Margin = new System.Windows.Forms.Padding(1);
            this.btnDelProjMc.Name = "btnDelProjMc";
            this.btnDelProjMc.Size = new System.Drawing.Size(190, 38);
            this.btnDelProjMc.TabIndex = 8;
            this.btnDelProjMc.Text = "D&elete Selected Pump";
            this.btnDelProjMc.UseVisualStyleBackColor = false;
            this.btnDelProjMc.Click += new System.EventHandler(this.btnDelProjMc_Click);
            // 
            // dgvMachines
            // 
            this.dgvMachines.AllowUserToAddRows = false;
            this.dgvMachines.AllowUserToDeleteRows = false;
            this.dgvMachines.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvMachines.BackgroundColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvMachines.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dgvMachines.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvMachines.DefaultCellStyle = dataGridViewCellStyle8;
            this.dgvMachines.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvMachines.Location = new System.Drawing.Point(3, 53);
            this.dgvMachines.Name = "dgvMachines";
            this.dgvMachines.RowHeadersVisible = false;
            this.dgvMachines.RowTemplate.Height = 30;
            this.dgvMachines.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvMachines.Size = new System.Drawing.Size(472, 340);
            this.dgvMachines.TabIndex = 5;
            this.dgvMachines.SelectionChanged += new System.EventHandler(this.dgvMachines_SelectionChanged);
            // 
            // tblTestbtns
            // 
            this.tblTestbtns.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tblTestbtns.ColumnCount = 1;
            this.tblTestbtns.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tblTestbtns.Controls.Add(this.tableLayoutPanel4, 0, 4);
            this.tblTestbtns.Controls.Add(this.btnConductTest, 0, 1);
            this.tblTestbtns.Controls.Add(this.btnContinueTest, 0, 0);
            this.tblTestbtns.Controls.Add(this.btnDelTest, 0, 3);
            this.tblTestbtns.Controls.Add(this.btnUpdtTest, 0, 2);
            this.tblTestbtns.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tblTestbtns.Location = new System.Drawing.Point(481, 449);
            this.tblTestbtns.Name = "tblTestbtns";
            this.tblTestbtns.RowCount = 5;
            this.tblTestbtns.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tblTestbtns.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tblTestbtns.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tblTestbtns.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tblTestbtns.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tblTestbtns.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tblTestbtns.Size = new System.Drawing.Size(194, 340);
            this.tblTestbtns.TabIndex = 4;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Outset;
            this.tableLayoutPanel4.ColumnCount = 1;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Controls.Add(this.button1, 0, 3);
            this.tableLayoutPanel4.Controls.Add(this.label4, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.btnViewRpt, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.btnGenRpt, 0, 2);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 167);
            this.tableLayoutPanel4.Margin = new System.Windows.Forms.Padding(2);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 4;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(188, 170);
            this.tableLayoutPanel4.TabIndex = 11;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Transparent;
            this.button1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button1.BackgroundImage")));
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.button1.Location = new System.Drawing.Point(3, 129);
            this.button1.Margin = new System.Windows.Forms.Padding(1);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(182, 38);
            this.button1.TabIndex = 8;
            this.button1.Text = "Open Test Folder";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label4.Location = new System.Drawing.Point(5, 11);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 21);
            this.label4.TabIndex = 7;
            this.label4.Text = "Reports";
            // 
            // btnViewRpt
            // 
            this.btnViewRpt.BackColor = System.Drawing.Color.Transparent;
            this.btnViewRpt.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnViewRpt.BackgroundImage")));
            this.btnViewRpt.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnViewRpt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnViewRpt.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnViewRpt.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnViewRpt.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnViewRpt.Location = new System.Drawing.Point(3, 45);
            this.btnViewRpt.Margin = new System.Windows.Forms.Padding(1);
            this.btnViewRpt.Name = "btnViewRpt";
            this.btnViewRpt.Size = new System.Drawing.Size(182, 38);
            this.btnViewRpt.TabIndex = 4;
            this.btnViewRpt.Text = "&View Test Report";
            this.btnViewRpt.UseVisualStyleBackColor = false;
            this.btnViewRpt.Click += new System.EventHandler(this.btnViewRpt_Click);
            // 
            // btnGenRpt
            // 
            this.btnGenRpt.BackColor = System.Drawing.Color.Transparent;
            this.btnGenRpt.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnGenRpt.BackgroundImage")));
            this.btnGenRpt.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnGenRpt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnGenRpt.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnGenRpt.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGenRpt.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnGenRpt.Location = new System.Drawing.Point(3, 87);
            this.btnGenRpt.Margin = new System.Windows.Forms.Padding(1);
            this.btnGenRpt.Name = "btnGenRpt";
            this.btnGenRpt.Size = new System.Drawing.Size(182, 38);
            this.btnGenRpt.TabIndex = 3;
            this.btnGenRpt.Text = "Re-&Generate Report";
            this.btnGenRpt.UseVisualStyleBackColor = false;
            this.btnGenRpt.Click += new System.EventHandler(this.btnGenRpt_Click);
            // 
            // btnConductTest
            // 
            this.btnConductTest.BackColor = System.Drawing.Color.Transparent;
            this.btnConductTest.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnConductTest.BackgroundImage")));
            this.btnConductTest.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnConductTest.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnConductTest.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnConductTest.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConductTest.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnConductTest.Location = new System.Drawing.Point(2, 43);
            this.btnConductTest.Margin = new System.Windows.Forms.Padding(1);
            this.btnConductTest.Name = "btnConductTest";
            this.btnConductTest.Size = new System.Drawing.Size(190, 38);
            this.btnConductTest.TabIndex = 2;
            this.btnConductTest.Text = "&Continue Selected Test";
            this.btnConductTest.UseVisualStyleBackColor = false;
            this.btnConductTest.Click += new System.EventHandler(this.btnConductTest_Click);
            // 
            // btnContinueTest
            // 
            this.btnContinueTest.BackColor = System.Drawing.Color.Transparent;
            this.btnContinueTest.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnContinueTest.BackgroundImage")));
            this.btnContinueTest.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnContinueTest.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnContinueTest.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnContinueTest.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnContinueTest.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnContinueTest.Location = new System.Drawing.Point(2, 2);
            this.btnContinueTest.Margin = new System.Windows.Forms.Padding(1);
            this.btnContinueTest.Name = "btnContinueTest";
            this.btnContinueTest.Size = new System.Drawing.Size(190, 38);
            this.btnContinueTest.TabIndex = 1;
            this.btnContinueTest.Text = "Add New &Test";
            this.btnContinueTest.UseVisualStyleBackColor = false;
            this.btnContinueTest.Click += new System.EventHandler(this.btnNewTest_Click);
            // 
            // btnDelTest
            // 
            this.btnDelTest.BackColor = System.Drawing.Color.Transparent;
            this.btnDelTest.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnDelTest.BackgroundImage")));
            this.btnDelTest.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnDelTest.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnDelTest.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnDelTest.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelTest.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnDelTest.Location = new System.Drawing.Point(2, 125);
            this.btnDelTest.Margin = new System.Windows.Forms.Padding(1);
            this.btnDelTest.Name = "btnDelTest";
            this.btnDelTest.Size = new System.Drawing.Size(190, 38);
            this.btnDelTest.TabIndex = 10;
            this.btnDelTest.Text = "&Delete Selected Test";
            this.btnDelTest.UseVisualStyleBackColor = false;
            this.btnDelTest.Click += new System.EventHandler(this.btnDelTest_Click);
            // 
            // btnUpdtTest
            // 
            this.btnUpdtTest.BackColor = System.Drawing.Color.Transparent;
            this.btnUpdtTest.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnUpdtTest.BackgroundImage")));
            this.btnUpdtTest.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnUpdtTest.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnUpdtTest.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnUpdtTest.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdtTest.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnUpdtTest.Location = new System.Drawing.Point(2, 84);
            this.btnUpdtTest.Margin = new System.Windows.Forms.Padding(1);
            this.btnUpdtTest.Name = "btnUpdtTest";
            this.btnUpdtTest.Size = new System.Drawing.Size(190, 38);
            this.btnUpdtTest.TabIndex = 9;
            this.btnUpdtTest.Text = "M&odify Logsheet";
            this.btnUpdtTest.UseVisualStyleBackColor = false;
            this.btnUpdtTest.Click += new System.EventHandler(this.btnUpdtTest_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label3.Location = new System.Drawing.Point(3, 409);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(472, 37);
            this.label3.TabIndex = 6;
            this.label3.Text = "Tests";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label2.Location = new System.Drawing.Point(3, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(472, 37);
            this.label2.TabIndex = 6;
            this.label2.Text = "Pumps";
            // 
            // dgvTests
            // 
            this.dgvTests.AllowUserToAddRows = false;
            this.dgvTests.AllowUserToDeleteRows = false;
            this.dgvTests.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvTests.BackgroundColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvTests.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.dgvTests.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvTests.DefaultCellStyle = dataGridViewCellStyle10;
            this.dgvTests.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvTests.Location = new System.Drawing.Point(3, 449);
            this.dgvTests.Name = "dgvTests";
            this.dgvTests.RowHeadersVisible = false;
            this.dgvTests.RowTemplate.Height = 30;
            this.dgvTests.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvTests.Size = new System.Drawing.Size(472, 340);
            this.dgvTests.TabIndex = 5;
            this.dgvTests.SelectionChanged += new System.EventHandler(this.dgvTests_SelectionChanged);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Controls.Add(this.dgvProject, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.tblprojBtns, 0, 2);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(5, 8);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 3;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(446, 792);
            this.tableLayoutPanel3.TabIndex = 8;
            // 
            // dgvProject
            // 
            this.dgvProject.AllowUserToAddRows = false;
            this.dgvProject.AllowUserToDeleteRows = false;
            this.dgvProject.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvProject.BackgroundColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvProject.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle11;
            this.dgvProject.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvProject.DefaultCellStyle = dataGridViewCellStyle12;
            this.dgvProject.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvProject.Location = new System.Drawing.Point(3, 53);
            this.dgvProject.Name = "dgvProject";
            this.dgvProject.RowHeadersVisible = false;
            this.dgvProject.RowTemplate.Height = 30;
            this.dgvProject.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvProject.Size = new System.Drawing.Size(440, 696);
            this.dgvProject.TabIndex = 8;
            this.dgvProject.SelectionChanged += new System.EventHandler(this.dgvProject_SelectionChanged_1);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label1.Location = new System.Drawing.Point(3, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(440, 37);
            this.label1.TabIndex = 7;
            this.label1.Text = "Projects";
            // 
            // tblprojBtns
            // 
            this.tblprojBtns.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tblprojBtns.ColumnCount = 3;
            this.tblprojBtns.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33332F));
            this.tblprojBtns.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tblprojBtns.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tblprojBtns.Controls.Add(this.btnAddProject, 0, 0);
            this.tblprojBtns.Controls.Add(this.btnUpdtSpParams, 2, 0);
            this.tblprojBtns.Controls.Add(this.btnUpdateProj, 1, 0);
            this.tblprojBtns.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tblprojBtns.Location = new System.Drawing.Point(0, 752);
            this.tblprojBtns.Margin = new System.Windows.Forms.Padding(0);
            this.tblprojBtns.Name = "tblprojBtns";
            this.tblprojBtns.RowCount = 1;
            this.tblprojBtns.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tblprojBtns.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tblprojBtns.Size = new System.Drawing.Size(446, 40);
            this.tblprojBtns.TabIndex = 3;
            // 
            // btnAddProject
            // 
            this.btnAddProject.BackColor = System.Drawing.Color.Transparent;
            this.btnAddProject.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnAddProject.BackgroundImage")));
            this.btnAddProject.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAddProject.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnAddProject.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnAddProject.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddProject.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnAddProject.Location = new System.Drawing.Point(2, 2);
            this.btnAddProject.Margin = new System.Windows.Forms.Padding(1);
            this.btnAddProject.Name = "btnAddProject";
            this.btnAddProject.Size = new System.Drawing.Size(145, 36);
            this.btnAddProject.TabIndex = 3;
            this.btnAddProject.Text = "&Add Project";
            this.btnAddProject.UseVisualStyleBackColor = false;
            this.btnAddProject.Click += new System.EventHandler(this.btnAddProject_Click);
            // 
            // btnUpdtSpParams
            // 
            this.btnUpdtSpParams.BackColor = System.Drawing.Color.Transparent;
            this.btnUpdtSpParams.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnUpdtSpParams.BackgroundImage")));
            this.btnUpdtSpParams.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnUpdtSpParams.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnUpdtSpParams.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnUpdtSpParams.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdtSpParams.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnUpdtSpParams.Location = new System.Drawing.Point(298, 2);
            this.btnUpdtSpParams.Margin = new System.Windows.Forms.Padding(1);
            this.btnUpdtSpParams.Name = "btnUpdtSpParams";
            this.btnUpdtSpParams.Size = new System.Drawing.Size(146, 36);
            this.btnUpdtSpParams.TabIndex = 5;
            this.btnUpdtSpParams.Text = "&Modify Specs.";
            this.btnUpdtSpParams.UseVisualStyleBackColor = false;
            this.btnUpdtSpParams.Click += new System.EventHandler(this.btnUpdtSpParams_Click);
            // 
            // btnUpdateProj
            // 
            this.btnUpdateProj.BackColor = System.Drawing.Color.Transparent;
            this.btnUpdateProj.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnUpdateProj.BackgroundImage")));
            this.btnUpdateProj.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnUpdateProj.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnUpdateProj.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnUpdateProj.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdateProj.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnUpdateProj.Location = new System.Drawing.Point(150, 2);
            this.btnUpdateProj.Margin = new System.Windows.Forms.Padding(1);
            this.btnUpdateProj.Name = "btnUpdateProj";
            this.btnUpdateProj.Size = new System.Drawing.Size(145, 36);
            this.btnUpdateProj.TabIndex = 4;
            this.btnUpdateProj.Text = "M&odify Project";
            this.btnUpdateProj.UseVisualStyleBackColor = false;
            this.btnUpdateProj.Click += new System.EventHandler(this.btnUpdateProj_Click);
            // 
            // TestManagementWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DimGray;
            this.ClientSize = new System.Drawing.Size(1141, 830);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.statusbar);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimizeBox = false;
            this.Name = "TestManagementWindow";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Test Manager";
            this.statusbar.ResumeLayout(false);
            this.statusbar.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tblMachineBtns.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvMachines)).EndInit();
            this.tblTestbtns.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTests)).EndInit();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProject)).EndInit();
            this.tblprojBtns.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusbar;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tblMachineBtns;
        private System.Windows.Forms.ToolStripStatusLabel statusMUcode;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.ToolStripStatusLabel statusProj;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel4;
        private System.Windows.Forms.ToolStripStatusLabel statusMcNo;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel6;
        private System.Windows.Forms.ToolStripStatusLabel statusTestref;
        private System.Windows.Forms.TableLayoutPanel tblprojBtns;
        private System.Windows.Forms.Button btnAddProject;
        private System.Windows.Forms.Button btnUpdtSpParams;
        private System.Windows.Forms.Button btnUpdateProj;
        private System.Windows.Forms.Button btnAddProjMc;
        private System.Windows.Forms.Button btnDelProjMc;
        private System.Windows.Forms.Button btnUpdtProjMc;
        private System.Windows.Forms.DataGridView dgvMachines;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TableLayoutPanel tblTestbtns;
        private System.Windows.Forms.Button btnUpdtTest;
        private System.Windows.Forms.Button btnDelTest;
        private System.Windows.Forms.Button btnConductTest;
        private System.Windows.Forms.DataGridView dgvTests;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnViewRpt;
        private System.Windows.Forms.Button btnGenRpt;
        private System.Windows.Forms.DataGridView dgvProject;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        public System.Windows.Forms.Button btnContinueTest;
        public System.Windows.Forms.Button btnNewTest;


    }
}