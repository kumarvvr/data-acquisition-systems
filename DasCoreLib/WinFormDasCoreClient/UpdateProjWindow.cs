﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DasCoreLib;
using BHEL.PUMPSDAS.Datatypes;
using DasCoreLib;
using DasCoreUtilities;

namespace WinFormDasCoreClient
{
    public partial class UpdateProjWindow : Form
    {
        ProjectDetails project;
        InstalledMachine mc;
        DasCore dc;
        List<SpecifiedParamsDetails> spDetails;
        List<Point> SPlookupLocations;
        Font linkFont, defaultFont;
        Color linkcolor, defaultcolor;
        ProjectLookupManagerWindow plmw;
        bool projectlookupChangeStatus;
        string updatedLookupName;

        /*
        public UpdateProjWindow(DasCore dc, InstalledMachine mc)
        {
            InitializeComponent();
            this.dc = dc;
            this.DialogResult = DialogResult.Cancel;
            this.Text = "Add New Project - " + mc.machineuniquecode;
            this.mc = mc;
            isNewProject = true;
            UpdateProjSPGrid();
        }
        */

        public UpdateProjWindow(DasCore dc, InstalledMachine mc, ProjectDetails proj)
        {
            InitializeComponent();
            this.dc = dc;
            this.DialogResult = DialogResult.Cancel;
            this.Text = "Modify Project - " + mc.machineuniquecode + " -> " + proj.projectname;
            this.mc = mc;
            this.project = proj;
            tbProjName.Text = proj.projectname;
            tbProjDesc.Text = proj.projectdescription;
            UpdateProjSPGrid();
        }

        private void UpdateProjSPGrid()
        {
            SPlookupLocations = new List<Point>();

            defaultFont = dgvSP.DefaultCellStyle.Font;
            defaultcolor = dgvSP.DefaultCellStyle.ForeColor;

            linkFont = new Font(defaultFont.FontFamily, defaultFont.Size, FontStyle.Underline);
            linkcolor = new Color();
            linkcolor = Color.Blue;

            spDetails = dc.GetProjectSpecifiedParameters(mc, project);
            for (int i = 0; i < spDetails.Count; i++)
            {
                dgvSP.Rows.Add(new object[] { spDetails[i].paramdescription, spDetails[i].paramtype, spDetails[i].paramunits, spDetails[i].defaultdata });
                if (spDetails[i].paramtype == "lookup")
                {
                    Point p = new Point(i, 3);
                    SPlookupLocations.Add(p);

                    dgvSP.Rows[p.X].Cells[p.Y].Style.ForeColor = linkcolor;
                    dgvSP.Rows[p.X].Cells[p.Y].Style.Font = linkFont;
                }
            }

        }


        private void LookupCellChangeOnEnter(DataGridView dgv, Point p)
        {            
            dgv.Cursor = Cursors.Hand;
        }
        private void LookupCellChangeOnLeave(DataGridView dgv, Point p)
        {            
            dgv.Cursor = Cursors.Default;
        }
        private void OpenLookupDataWindow(DataGridView sender, Point p)
        {
            string tablename = (sender.Rows[p.X].Cells[p.Y].Value.ToString());
            
            LookupTableDetails details = dc.GetLookupTableByTableName(tablename);
            // Open project lookup table selection window....
            plmw = new ProjectLookupManagerWindow(this.dc, this.mc, project);
            plmw.FormClosing += new FormClosingEventHandler(plmw_FormClosing);
            plmw.ShowDialog();
            if (projectlookupChangeStatus)
            {
                sender.Rows[p.X].Cells[p.Y].Value = this.updatedLookupName;
            }
        }

        void plmw_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (plmw.DialogResult == System.Windows.Forms.DialogResult.OK)
            {
                projectlookupChangeStatus = true;
                updatedLookupName = plmw.SelectedLookup.tablename;
            }
            else
            {
                projectlookupChangeStatus = false;
                updatedLookupName = "";
            }
            //throw new NotImplementedException();
        }

        private void dgvSP_CellMouseEnter(object sender, DataGridViewCellEventArgs e)
        {
            // Change the color of the cells when the mouse enters...

            Point p = new Point(e.RowIndex, e.ColumnIndex);

            for (int i = 0; i < SPlookupLocations.Count; i++)
            {
                if (p == SPlookupLocations[i])
                {
                    LookupCellChangeOnEnter(((DataGridView)sender), p);
                    break;
                }
            }
        }
        private void dgvSP_CellMouseLeave(object sender, DataGridViewCellEventArgs e)
        {
            Point p = new Point(e.RowIndex, e.ColumnIndex);

            for (int i = 0; i < SPlookupLocations.Count; i++)
            {
                if (p == SPlookupLocations[i])
                {
                    LookupCellChangeOnLeave((DataGridView)sender, p);
                    break;
                }
            }
        }
        private void dgvSP_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            Point p = new Point(e.RowIndex, e.ColumnIndex);


            for (int i = 0; i < SPlookupLocations.Count; i++)
            {
                if (p == SPlookupLocations[i])
                {
                    dgvSP.ClearSelection();
                    OpenLookupDataWindow((DataGridView)sender, p);
                    break;
                }
            }
        }

        private List<SpecifiedParamsDetails> ExtractProjectSpecifiedParametersFromGrid()
        {
            SpecifiedParamsDetails row;
            List<SpecifiedParamsDetails> data = new List<SpecifiedParamsDetails>();
            DataGridView target = this.dgvSP;
            DataGridViewCellCollection cells;
            for (int i = 0; i < target.Rows.Count; i++)
            {
                row = spDetails[i];

                cells = target.Rows[i].Cells;
                for (int j = 0; j < cells.Count; j++)
                {
                    if (cells[j].Value == null)
                        cells[j].Value = "";
                }                
                row.paramdescription = DefaultConversion.GetStringValue(cells["paramdescription"].Value.ToString());
                row.paramunits = DefaultConversion.GetStringValue(cells["paramunits"].Value.ToString());
                row.defaultdata = DefaultConversion.GetStringValue(cells["defaultdata"].Value.ToString());
                data.Add(row);
            }

            return data;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (tbProjDesc.Text == "" || tbProjName.Text == "")
            {
                MessageBox.Show("Invalid Data Entered!");
                return;
            }
            try
            {
                dc.UpdateExistingProject(mc,project);
                dc.UpdateProjectSpecifiedParameters(mc, project, ExtractProjectSpecifiedParametersFromGrid());
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                this.Close();
                //MessageBox.Show("Failed to Add New Project! ");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        public ProjectDetails GetNewProjectDetails()
        {
            return this.project;
        }

    }
}
