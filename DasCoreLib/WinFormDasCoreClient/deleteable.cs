﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ExcelManager;



namespace WinFormDasCoreClient
{
    public partial class deleteable : Form
    {
        public deleteable()
        {
            InitializeComponent();
        }

        private void toolStripSplitButton1_ButtonClick(object sender, EventArgs e)
        {
            MessageBox.Show("Clicked");
        }

        private void somethingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Something");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ExcelFile xlfile = new ExcelFile("c:\\LinePrintTemplate.xlsx");
            xlfile.Open(false);
            xlfile.SelectWorksheet("lineprint");
            Dictionary<string, string> entry = new Dictionary<string, string>();
            entry.Add("<##projectname##>", "Some project");
            entry.Add("<##date##>", DateTime.Now.ToLongDateString());
            xlfile.FindAndReplaceMarkers(entry);

            List<string> colData = new List<string>();

            for (int i = 0; i < 20; i++)
            {
                colData.Add("Data : " + i);
            }

            int row, col;

            if (xlfile.FindMarkedCell(out row, out col, "<##paramdesc##>"))
            {
                xlfile.FillDataInColumn(row, col, colData);
            }
            else
            {
                MessageBox.Show("Could not find cell");
            }
            xlfile.SaveFile();
            xlfile.CloseFile();
        }
    }
}
