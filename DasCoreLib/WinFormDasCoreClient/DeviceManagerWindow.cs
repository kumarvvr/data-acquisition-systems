﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DasCoreLib;
using DasCoreUtilities;
using BHEL.PUMPSDAS.Datatypes;

namespace WinFormDasCoreClient
{
    public partial class DeviceManagerWindow : Form
    {       
        DasCore dc;
        DataGridViewColumn didCol, dNameCol, dDescCol, dCstringCol, dTypeCol;
        DataGridViewCellStyle disabledColStyle;
        List<DeviceDetails> devices;

        DeviceConfigurationWindow devconfWin;

        public DeviceManagerWindow(DasCore dc)
        {
            InitializeComponent();
            this.dc = dc;

            disabledColStyle = new DataGridViewCellStyle();
            disabledColStyle.BackColor = SystemColors.Control;

            UpdateGridColumns();
            RefreshGridData();
        }
        private void UpdateGridColumns()
        {
            didCol = new DataGridViewTextBoxColumn();
            didCol.Name = "deviceid";
            didCol.HeaderText = "Device ID";
            didCol.Width = 75;
            didCol.ReadOnly = true;
            
            //didCol.DefaultCellStyle = disabledColStyle;

            dNameCol = new DataGridViewTextBoxColumn();
            dNameCol.Name = "devicename";
            dNameCol.HeaderText = "Device Name";
            dNameCol.Width = 100;
            dNameCol.ReadOnly = true;
            //dNameCol.DefaultCellStyle = disabledColStyle;

            dDescCol = new DataGridViewTextBoxColumn();
            dDescCol.Name = "devicedesc";
            dDescCol.HeaderText = "Device Description";
            dDescCol.Width = 500;
            dDescCol.ReadOnly = true;
            //dDescCol.DefaultCellStyle = disabledColStyle;

            dCstringCol = new DataGridViewTextBoxColumn();
            dCstringCol.Name = "connstring";
            dCstringCol.HeaderText = "Connection String";
            dCstringCol.Width = 317;
            dCstringCol.ReadOnly = true;
            //dCstringCol.DefaultCellStyle = disabledColStyle;

            dgvDevices.Columns.Add(didCol);
            dgvDevices.Columns.Add(dNameCol);
            dgvDevices.Columns.Add(dDescCol);
            dgvDevices.Columns.Add(dCstringCol);
        }
        private void RefreshGridData()
        {
            this.dgvDevices.Rows.Clear();
            try
            {
                this.devices = dc.GetAllDevices();
                for(int i=0;i<devices.Count;i++)
                {
                    dgvDevices.Rows.Add(new object[] {  devices[i].deviceid, 
                                                        devices[i].devicename, 
                                                        devices[i].devicedesc, 
                                                        devices[i].connstring });
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void btnAddDevice_Click(object sender, EventArgs e)
        {
            devconfWin = new DeviceConfigurationWindow(dc);
            devconfWin.FormClosing += new FormClosingEventHandler(devconfWin_FormClosing);
            devconfWin.ShowDialog();
        }
        void devconfWin_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (devconfWin.DialogResult == System.Windows.Forms.DialogResult.OK)
            {
                RefreshGridData();
            }
            devconfWin = null;
        }
        private void btnModifyDevice_Click(object sender, EventArgs e)
        {
            devconfWin = new DeviceConfigurationWindow(dc,this.GetSelectedDevice(this.dgvDevices.SelectedRows[0]));
            devconfWin.FormClosing +=new FormClosingEventHandler(devconfWin_FormClosing);
            devconfWin.ShowDialog();
        }
        private DeviceDetails GetSelectedDevice(DataGridViewRow row)
        {
            DeviceDetails result = new DeviceDetails();
            int did = DefaultConversion.GetIntValue(row.Cells["deviceid"].Value.ToString());

            result.connstring = DefaultConversion.GetStringValue(row.Cells["connstring"].Value.ToString());
            result.devicedesc = DefaultConversion.GetStringValue(row.Cells["devicedesc"].Value.ToString());
            result.devicename = DefaultConversion.GetStringValue(row.Cells["devicename"].Value.ToString());
            result.deviceid = did;

            return result;

        }
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
