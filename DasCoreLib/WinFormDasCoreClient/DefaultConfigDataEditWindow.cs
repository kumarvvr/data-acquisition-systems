﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DasCoreLib;
using System.Diagnostics;
using BHEL.PUMPSDAS.Datatypes;
using ExcelManager;


namespace WinFormDasCoreClient
{
    // TODO : Add machine information header to this window...
    public partial class DefaultConfigDataEditWindow : Form
    {
        DasCore dc;
        InstalledMachine machine;
        Dictionary<string, List<DefaultConfigurationData>> ConfigData;
        bool isDirty = true;
        DataGridViewCellStyle disabledTextStyle;
        DataGridViewCellStyle disabledButtonStyle;
        DataGridViewColumn pnamecol, ptype, pdesc, pbtn, punits, pdefaultdata, pdispindex;
        CommonLookupManagerWindow lmw;
        ExcelWorkBookDataSet xlWorkBk;

        string lookupparamname, lookuptablename,lookuptabledesc;
        string titleTemplate;
                
        public DefaultConfigDataEditWindow(DasCore dc, InstalledMachine machine)
        {
            InitializeComponent();
            this.dc = dc;
            this.machine = machine;

            disabledTextStyle = new DataGridViewCellStyle();
            disabledButtonStyle = new DataGridViewCellStyle();

            disabledTextStyle.BackColor = SystemColors.Control;
            disabledButtonStyle.ForeColor = SystemColors.ControlDark;
            disabledButtonStyle.BackColor = SystemColors.InactiveCaptionText;

            this.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            this.SetStyle(ControlStyles.UserPaint, true);
            this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);

            titleTemplate = this.lblTitle.Text;

            this.lblTitle.Text = string.Format(titleTemplate, machine.dllversiondesc);
            
        }
        private void EditTableDataWaindow_Load(object sender, EventArgs e)
        {
            try
            {
                ConfigData = dc.GetDefaultConfigurationData(this.machine);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            // Set the Datagrid Columns
            
            GenerateGridColumnObjects(out pnamecol, out ptype, out pdesc, out pbtn, out punits, out pdefaultdata, out pdispindex);
            InitializeDatagrid(dgvMD, ConfigData[DatabaseManager.default_md_tablename]);
            dgvMD.RowHeadersVisible = false;

            GenerateGridColumnObjects(out pnamecol, out ptype, out pdesc, out pbtn, out punits, out pdefaultdata, out pdispindex);
            InitializeDatagrid(dgvSP,ConfigData[DatabaseManager.default_sp_tablename]);
            dgvSP.RowHeadersVisible = false;

            GenerateGridColumnObjects(out pnamecol, out ptype, out pdesc, out pbtn, out punits, out pdefaultdata, out pdispindex);
            InitializeDatagrid(dgvMP, ConfigData[DatabaseManager.default_mp_tablename]);
            dgvMP.RowHeadersVisible = false;

            GenerateGridColumnObjects(out pnamecol, out ptype, out pdesc, out pbtn, out punits, out pdefaultdata, out pdispindex);
            InitializeDatagrid(dgvTSP,ConfigData[DatabaseManager.default_tsp_tablename]);
            dgvTSP.RowHeadersVisible = false;

            GenerateGridColumnObjects(out pnamecol, out ptype, out pdesc, out pbtn, out punits, out pdefaultdata, out pdispindex);
            InitializeDatagrid(dgvRP,ConfigData[DatabaseManager.default_rp_tablename]);
            dgvRP.RowHeadersVisible = false;
            
        }        
        private void GenerateGridColumnObjects( out DataGridViewColumn pname,
                                                out DataGridViewColumn ptype,
                                                out DataGridViewColumn pdesc,
                                                out DataGridViewColumn pbtn,
                                                out DataGridViewColumn punits,
                                                out DataGridViewColumn pdefaultdata,
                                                out DataGridViewColumn pdispindex)
        {
            pname = new DataGridViewTextBoxColumn();
            pname.HeaderText = "Parameter Name";
            pname.Name = "paramname";
            pname.Width = 200;
            pname.ReadOnly = true;
            //pname.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;            

            ptype = new DataGridViewTextBoxColumn();
            ptype.HeaderText = "Parameter Type";
            ptype.Name = "paramtype";
            ptype.Width = 80;
            ptype.ReadOnly = true;

            //ptype.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;

            pdesc = new DataGridViewTextBoxColumn();
            pdesc.HeaderText = "Parameter Description";
            pdesc.Name = "paramdescription";
            pdesc.Width = 200;
            //pdesc.ReadOnly = true;

            pbtn = new DataGridViewButtonColumn();
            pbtn.HeaderText = "Lookup Select";
            pbtn.Name = "lookupbutton";
            pbtn.Width = 80;

            punits = new DataGridViewTextBoxColumn();
            punits.HeaderText = "Parameter Units";
            punits.Name = "paramunits";
            punits.Width = 80;
            //punits.ReadOnly = true;

            pdefaultdata = new DataGridViewTextBoxColumn();
            pdefaultdata.HeaderText = "Default Data";
            pdefaultdata.Name = "defaultdata";
            pdefaultdata.Width = 200;
            //pdefaultdata.ReadOnly = true;

            pdispindex = new DataGridViewTextBoxColumn();
            pdispindex.HeaderText = "Display Index";
            pdispindex.Name = "displayindex";
            pdispindex.Width = 60;
        }
        private void InitializeDatagrid(DataGridView target,List<DefaultConfigurationData> data )
        {
            target.Columns.Add(pnamecol);
            target.Columns.Add(ptype);
            target.Columns.Add(pdesc);
            target.Columns.Add(pbtn);
            target.Columns.Add(punits);
            target.Columns.Add(pdefaultdata);
            target.Columns.Add(pdispindex);
            
            for (int i = 0; i < data.Count; i++)
            {
                target.Rows.Add(new object[]{
                    data[i].paramname,
                    data[i].paramtype,
                    data[i].paramdescription,
                    "Select",
                    data[i].paramunits,
                    data[i].defaultdata,
                    data[i].displayindex
                });
            }

            for (int i = 0; i < data.Count; i++)
            {
                target.Rows[i].Cells[0].Style = disabledTextStyle;
                target.Rows[i].Cells[1].Style = disabledTextStyle;

                if (target.Rows[i].Cells["paramtype"].Value.ToString() != "lookup")
                {
                    target.Rows[i].Cells["lookupbutton"].Style = disabledButtonStyle;
                }
            }
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            TestReportGenerator trg = new TestReportGenerator(this.dc);
            
            isDirty = true;
            bool isDone = false;
            try
            {
                if (dc.UpdateDefaultCongigurationData(this.machine, GenerateUpdatedData()))
                {
                    // Generate and write the new default template file....

                    xlWorkBk = trg.GenerateReportTemplateWorkBook(DasCoreSettings.GetMachineTestReportTemplateFileFullName(machine), machine);
                    xlWorkBk.WriteData();
                    isDone = true;
                    isDirty = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            if (isDone)
                MessageBox.Show("Updated Successfully");
        }
        private void btnBack_Click(object sender, EventArgs e)
        {
            if (isDirty)
            {
                if (MessageBox.Show("Do you want to close this windows??", "Cancel?", MessageBoxButtons.OKCancel) == System.Windows.Forms.DialogResult.OK)
                    this.Close();
                else
                    return;
            }
            this.Close();
        }
        private Dictionary<string, List<DefaultConfigurationData>> GenerateUpdatedData()
        {
            Dictionary<string, List<DefaultConfigurationData>> result = new Dictionary<string, List<DefaultConfigurationData>>(0);
            List<DefaultConfigurationData> data;

            ExtractConfigDataFromGrid(this.dgvMD, out data);
            result.Add(DatabaseManager.default_md_tablename, data);

            ExtractConfigDataFromGrid(this.dgvMP, out data);
            result.Add(DatabaseManager.default_mp_tablename, data);

            ExtractConfigDataFromGrid(this.dgvRP, out data);
            result.Add(DatabaseManager.default_rp_tablename, data);

            ExtractConfigDataFromGrid(this.dgvSP, out data);
            result.Add(DatabaseManager.default_sp_tablename, data);

            ExtractConfigDataFromGrid(this.dgvTSP, out data);
            result.Add(DatabaseManager.default_tsp_tablename, data);

            return result;
        }
        private void ExtractConfigDataFromGrid(DataGridView target, out List<DefaultConfigurationData> data)
        {
            DefaultConfigurationData row;
            data = new List<DefaultConfigurationData>();
            DataGridViewCellCollection cells;
            for (int i = 0; i < target.Rows.Count; i++)
            {
                row = new DefaultConfigurationData();
                cells = target.Rows[i].Cells;
                for (int j = 0; j < cells.Count; j++)
                {
                    if (cells[j].Value == null)
                        cells[j].Value = "";
                }
                row.fk_machineCode = machine.machinecode.ToString();
                row.paramname = target.Rows[i].Cells["paramname"].Value.ToString();
                row.paramtype = target.Rows[i].Cells["paramtype"].Value.ToString();
                row.paramdescription = target.Rows[i].Cells["paramdescription"].Value.ToString();
                row.paramunits = target.Rows[i].Cells["paramunits"].Value.ToString();
                row.defaultdata = target.Rows[i].Cells["defaultdata"].Value.ToString();
                row.displayindex = int.Parse(target.Rows[i].Cells["displayindex"].Value.ToString());

                data.Add(row);
            }
        }
        private void dgvALL_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView target = (DataGridView)sender;
            if (e.RowIndex == -1)
                return;
            if (target.Rows[e.RowIndex].Cells["paramtype"].Value.ToString() == "lookup" && e.ColumnIndex == 3/*target.Rows[e.RowIndex].Cells["lookupbutton"].ColumnIndex*/)
            {
                //MessageBox.Show(dgvTSP.Rows[e.RowIndex].Cells[0].Value.ToString());
                lookupparamname = target.Rows[e.RowIndex].Cells["paramname"].Value.ToString();


                lmw = new CommonLookupManagerWindow(this.dc);
                lmw.FormClosing += new FormClosingEventHandler(lmw_FormClosing);
                lmw.ShowDialog();

                target.Rows[e.RowIndex].Cells["defaultdata"].Value = this.lookuptablename;
                //target.Rows[e.RowIndex].Cells["paramdescription"].Value = this.lookuptabledesc;
                this.lookuptablename = "";
                this.lookuptabledesc = "";
            }
        }
        void lmw_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.lookuptablename = lmw.SelectedLookup.tablename;
            this.lookuptabledesc = lmw.SelectedLookup.lookupdesc;
            //throw new NotImplementedException();
        }
        private void dgvAll_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }
        private void dgvAll_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            //MessageBox.Show("Begin Edit");
            DataGridView target = (DataGridView)sender;
            if (target.Rows[e.RowIndex].Cells["paramtype"].Value.ToString() == "lookup" && (/*e.ColumnIndex == 2 ||*/ e.ColumnIndex == 5))
            {
                e.Cancel = true;
                return;
            }
        }     
    }
}
