﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BHEL.PUMPSDAS.Datatypes;

namespace WinFormDasCoreClient
{
    public partial class ObjectViewWindow : Form
    {

        // Note this windows is supposed to be created once and then shown and hidden
        // multiple times with only the recorded data being changed...

        // NOTE : Used for both Line Print and results...
        string projectName,frameName,machineNumber,testReference;
        List<DataGridViewColumn> additionalColumns;
        DataGridViewColumn valuecol;

        public string noise1, noise2;

        

        public ObjectViewWindow(
            string projectName,
            string frameName,
            string machineNumber,
            string testReference
            )
        {
            InitializeComponent();

            this.projectName = projectName;
            this.frameName = frameName;
            this.machineNumber = machineNumber;
            this.testReference = testReference;

            this.lblProject.Text+=this.projectName;
            this.lblFrame.Text += this.frameName;
            this.lblMachineNumber.Text += this.machineNumber;
            this.lblTestReference.Text += this.testReference;

            valuecol = dgvLinePrint.Columns["value"];

            this.tbNoise1.Text = "";

            this.tbNoise2.Text = "";
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        public void UpdateData(string date, string time, string point,List<LinePrintData> data,bool isComputeDisplay)
        {
            ClearData();
            SetUIStatus(isComputeDisplay);
            this.lblDate.Text = date+" " + time;
            this.lblPoint.Text += point;

            for(int i =0;i<data.Count;i++)
            {
                dgvLinePrint.Rows.Add(new object[]{data[i].paramdesc,data[i].paramunits,data[i].value});
            }

        }

        private void SetUIStatus(bool isComputeDisplay)
        {
            if (isComputeDisplay)
            {
                this.tbNoise1.Enabled = false;
                this.tbNoise2.Enabled = false;
                this.btnRecord.Enabled = false;
                this.btnCancel.Text = "Close";

            }
            else
            {
                this.tbNoise1.Enabled = true;
                this.tbNoise2.Enabled = true;
                this.btnRecord.Enabled = true;
                this.btnCancel.Text = "Cancel";
            }
        }

        public void UpdateMultiValueData(List<string> dates, List<string> times, List<string> points, List<List<LinePrintData>> dataList,bool isComputeDisplay)
        {
            ClearData();
            SetUIStatus(isComputeDisplay);
            if (!(dates.Count == times.Count && times.Count == points.Count && points.Count == dataList.Count))
            {
                MessageBox.Show("Invalid Data...");
                this.Close();
            }

            // Initialise the column....
            additionalColumns = new List<DataGridViewColumn>();
            List<LinePrintData> metaLinePrint,currentList;

            metaLinePrint = dataList[0];


            int count = dates.Count();
            int rows = dataList[0].Count;

            // The first value column should be made as a value holder too...
            valuecol.HeaderText = "Point : "+points[0] + Environment.NewLine + dates[0] + Environment.NewLine + times[0];
            
            // Adding count - 1 columns
            for (int i = 1; i < count; i++)
            {
                DataGridViewColumn col = new DataGridViewColumn(valuecol.CellTemplate);
                col.HeaderText = "Point : " + points[i] + Environment.NewLine + dates[i] + Environment.NewLine + times[i];
                col.Name = "value" + i;
                dgvLinePrint.Columns.Add(col);
                additionalColumns.Add(col);
            }

            // Add the required number of rows to the data grid view....

            for (int i = 0; i < rows; i++)
            {
                dgvLinePrint.Rows.Add();
            }
                // Update the rows data....
                for (int row = 0; row < rows; row++)
                {
                    // Column 0 is always the Param description
                    // Column 1 is always the Param Units.
                    // values always start from value 2.

                    // Count is the number of columns.....

                    dgvLinePrint.Rows[row].Cells[0].Value = metaLinePrint[row].paramdesc;

                    dgvLinePrint.Rows[row].Cells[1].Value = metaLinePrint[row].paramunits;

                    for (int jj = 0; jj < count; jj++)
                    {
                        // Update the required cells....
                        dgvLinePrint.Rows[row].Cells[jj + 2].Value = dataList[jj][row].value;
                    }
                }


        }

        private void ClearData()
        {
            this.lblDate.Text = "";
            this.lblPoint.Text = "Point : ";
            this.dgvLinePrint.Rows.Clear();

            // check for existance of additional columns and remove them...

            if (additionalColumns != null && additionalColumns.Count > 0)
            {
                for (int i = 0; i < additionalColumns.Count; i++)
                {
                    dgvLinePrint.Columns.Remove(additionalColumns[i]);
                }
            }
            if(additionalColumns != null)
            additionalColumns.Clear();

            valuecol.HeaderText = "Value";
        }

        private void RecordPoint()
        {
            this.noise1 = tbNoise1.Text;
            this.noise2 = tbNoise2.Text;

            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }

        private void CancelRecord()
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        private void ObjectViewWindow_SizeChanged(object sender, EventArgs e)
        {
            ResetDataGridViewMaximumSize(this.Width,this.Height);
        }

        private void ResetDataGridViewMaximumSize(int width, int height)
        {
            this.dgvLinePrint.MaximumSize = new Size(width, height);
            dgvLinePrint.Invalidate(true);
        }



        private void btnRecord_Click_1(object sender, EventArgs e)
        {
            RecordPoint();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            CancelRecord();
        }
    }
}
