﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BHEL.PUMPSDAS.Datatypes;


namespace WinFormDasCoreClient
{
    public partial class LookupViewWindow : Form
    {
        
        List<LookupDataRow> data;

        public LookupViewWindow(LookupTableDetails details, List<LookupDataRow> data)
        {
            InitializeComponent();

            lbllookupTableDesc.Text = details.tablename + " : " + details.lookupdesc;
            dgvLookup.Columns["key"].HeaderText = details.keytext;
            dgvLookup.Columns["value"].HeaderText = details.valuetext;
            this.data = data;
            UpdateGrid();
        }

        private void UpdateGrid()
        {
            for (int i = 0; i < data.Count; i++)
            {
                dgvLookup.Rows.Add(new object[] { data[i].key.ToString(), data[i].value.ToString() });
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
