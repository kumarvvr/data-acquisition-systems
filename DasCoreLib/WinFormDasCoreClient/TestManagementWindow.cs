﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DasCoreLib;
using DasCoreUtilities;
using BHEL.PUMPSDAS.Datatypes;
using FileManager;
using ExcelManager;
using System.Diagnostics;

namespace WinFormDasCoreClient
{
    public partial class TestManagementWindow : Form
    {
       #region Global Objects

        DasCore dc;
        public InstalledMachine mc;
        
        DataGridViewTextBoxColumn pCodeCol, pNameCol, pDescCol;
        DataGridViewTextBoxColumn mNumberCol, mDescCol;
        DataGridViewTextBoxColumn tRefNumberCol, tDateCol, tRemarksCol;

        DataGridViewCellStyle disabledStyle;
        DataGridStyles styles;

        List<ProjectDetails> projectList;
        List<ProjectMachineDetails> projmclist;
        List<MachineTestDetails> testlist;

        public ProjectDetails selectedProject, newProject;
        public ProjectMachineDetails selectedProjMachine, newProjMachine;
        public MachineTestDetails selectedTest, newTest;
        //SpecifiedParamsDetails 

        UpdateProjWindow updtProjWin;
        ModifyProjWindow mdprjWin;
        bool UpdateProjWinCancelStatus;
        UpdateProjMachineWindow updtProjMcWin;
        bool UpdateMcWinCancelStatus;
        UpdateMachineTestWindow updtTestWin;
        bool UpdateTestMcWinCancelStatus;
        ProjectSpecifiedParamsWindow pSpWin;
        TestSummaryWindow tstSumWin;
        TestReportGenerator reportGenerator;


        public bool isTestRequested = false;

       #endregion
        
        public TestManagementWindow(DasCore dc,InstalledMachine mc)
        {
            InitializeComponent();
            this.dc = dc;
            this.mc = mc;

            styles = new DataGridStyles();
            disabledStyle = styles.GetDisabledTextStyle();

            projectList = new List<ProjectDetails>(0);
            projmclist = new List<ProjectMachineDetails>(0);
            testlist = new List<MachineTestDetails>(0);
            selectedProject = new ProjectDetails();
            selectedProjMachine = new ProjectMachineDetails();
            selectedTest = new MachineTestDetails();

            if (!dc.CheckForAdminRights())
            {
                btnDelProjMc.Visible = false;
                btnDelTest.Visible = false;
                tblTestbtns.Enabled = false;
                tblMachineBtns.Enabled = false;
            }

            //dc.UpdateCurrentMachine(mc);
            InitializeProjectGrid();
            InitializeMachineGrid();
            InitializeTestGrid();
            statusMUcode.Text = mc.machineuniquecode;
            statusProj.Text = "";
            statusMcNo.Text = "";
            statusTestref.Text = "";
            btnConductTest.Enabled = false;
            RefreshProjectGridData();


            reportGenerator = new TestReportGenerator(this.dc);
        }

       #region Project

        private void InitializeProjectGrid()
        {
            pCodeCol = new DataGridViewTextBoxColumn() { Name = "projectcode", HeaderText = "Project Code", Width = 100, ReadOnly = true };
            //pCodeCol.DefaultCellStyle = disabledStyle;

            pNameCol = new DataGridViewTextBoxColumn() { Name = "projectname", HeaderText = "Project Name", Width = 100, ReadOnly = true };
            //pNameCol.DefaultCellStyle = disabledStyle;

            pDescCol = new DataGridViewTextBoxColumn() { Name = "projectdescription", HeaderText = "Project Description", Width = 100, ReadOnly = true };
            //pDescCol.DefaultCellStyle = disabledStyle;

            dgvProject.Columns.Add(pCodeCol);
            dgvProject.Columns.Add(pNameCol);
            dgvProject.Columns.Add(pDescCol);
        }

        private void RefreshProjectGridData()
        {
            dgvProject.Rows.Clear();
            try
            {
                projectList = dc.GetProjectListByInstalledMachine(this.mc);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                this.Close();
            }

            for (int i = 0; i < projectList.Count; i++)
            {
                dgvProject.Rows.Add(new object[] { projectList[i].projectcode, projectList[i].projectname, projectList[i].projectdescription });
            }

        }

        private ProjectDetails GetProjectDetailsFromSelectedRow(DataGridViewRow row)
        {
            ProjectDetails result = new ProjectDetails();
            result.projectcode = DefaultConversion.GetIntValue(row.Cells["projectcode"].Value.ToString());
            int index = -1;

            for (int i = 0; i < projectList.Count; i++)
            {
                if (projectList[i].projectcode == result.projectcode)
                {
                    index = i;
                    break;
                }
            }
            if (index != -1)
                result = projectList[index];
            else MessageBox.Show("Select A Valid Project from the list!");
            
            return result;
        }
        
        private void AddProject()
        {
            mdprjWin = new ModifyProjWindow(this.mc);

            mdprjWin.FormClosing += new FormClosingEventHandler(mdprjWin_FormClosing);

            mdprjWin.ShowDialog();

            RefreshProjectGridData();
        }

        void mdprjWin_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (mdprjWin.DialogResult == System.Windows.Forms.DialogResult.OK)
            {
                newProject = mdprjWin.GetNewProjectDetails();

                // Create the project in the system..

                newProject = dc.CreateNewProject(mc, newProject);

                updtProjWin = new UpdateProjWindow(dc, mc, newProject);

                updtProjWin.ShowDialog();

                RefreshProjectGridData();
            }
            //throw new NotImplementedException();
        }

        private void ModifyProject()
        {            
            updtProjWin = new UpdateProjWindow(dc, mc,selectedProject);
            //updtProjWin.FormClosing += new FormClosingEventHandler(newProjWin_FormClosing);
            updtProjWin.ShowDialog();
            if (UpdateProjWinCancelStatus == false)
            {
                try
                {
                    dc.UpdateExistingProject(mc, newProject);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                RefreshProjectGridData();
            }
        }

        private void ModifySpecifiedParameters()
        {
            pSpWin = new ProjectSpecifiedParamsWindow(dc, mc, selectedProject);
            pSpWin.ShowDialog();
        }

        /*
        private void  newProjWin_FormClosing(object sender, FormClosingEventArgs e)
        {
 	    if (updtProjWin.DialogResult == System.Windows.Forms.DialogResult.OK)
            {
                newProject = updtProjWin.GetNewProjectDetails();
                UpdateProjWinCancelStatus = false;
            }
        else UpdateProjWinCancelStatus = true;
        updtProjWin = null;
        }
        */

        private void btnAddProject_Click(object sender, EventArgs e)
        {
            AddProject();
        }

        private void btnUpdateProj_Click(object sender, EventArgs e)
        {
            ModifyProject();
        }

        private void btnUpdtSpParams_Click(object sender, EventArgs e)
        {
            ModifySpecifiedParameters();
        }

        private void dgvProject_SelectionChanged(object sender, EventArgs e)
        {
            

        }

        #endregion

       #region Machine

        private void InitializeMachineGrid()
        {
            mNumberCol = new DataGridViewTextBoxColumn() { Name = "machinenumber", HeaderText = "Machine Number", Width = 100, ReadOnly = true };
            //mNumberCol.DefaultCellStyle = disabledStyle;

            mDescCol = new DataGridViewTextBoxColumn() { Name = "machinedescription", HeaderText = "Machine Description", Width = 100, ReadOnly = true };
            //mDescCol.DefaultCellStyle = disabledStyle;

            dgvMachines.Columns.Add(mNumberCol);
            dgvMachines.Columns.Add(mDescCol);
        }

        private void RefreshMachineGridData()
        {
            dgvMachines.Rows.Clear();
            try
            {
                projmclist = dc.GetProjectMachines(selectedProject);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                this.Close();
            }

            for (int i = 0; i < projmclist.Count; i++)
            {
                dgvMachines.Rows.Add(new object[] { projmclist[i].machinenumber, projmclist[i].machinedescription });
            }            
        }

        private ProjectMachineDetails GetProjectMachineDetailsFromSelectedRow(DataGridViewRow row)
        {
            ProjectMachineDetails result = new ProjectMachineDetails();
            result.machinenumber = DefaultConversion.GetStringValue(row.Cells["machinenumber"].Value.ToString());
            int index = -1;

            for (int i = 0; i < projmclist.Count; i++)
            {
                if (projmclist[i].machinenumber == result.machinenumber)
                {
                    index = i;
                    break;
                }
            }
            if (index != -1)
                result = projmclist[index];
            else MessageBox.Show("Select A Valid Machine from the list!");

            return result;
        }

        private void AddProjectMachine()
        {
            updtProjMcWin = new UpdateProjMachineWindow(selectedProject);
            updtProjMcWin.FormClosing +=new FormClosingEventHandler(updtProjMcWin_FormClosing);
            updtProjMcWin.ShowDialog();
            if (UpdateMcWinCancelStatus == false)
            {
                try
                {
                    dc.CreateProjectMachine(mc, selectedProject, newProjMachine.machinenumber,newProjMachine.machinedescription);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                RefreshMachineGridData();
            }
        }

        private void UpdateProjectMachine()
        {
            updtProjMcWin = new UpdateProjMachineWindow(selectedProject,selectedProjMachine);
            updtProjMcWin.FormClosing += new FormClosingEventHandler(updtProjMcWin_FormClosing);
            updtProjMcWin.ShowDialog();
            if (UpdateMcWinCancelStatus == false)
            {
                try
                {
                    dc.UpdateProjectMachine(mc, selectedProject, newProjMachine);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                RefreshMachineGridData();
            }
        }

        private void DeleteProjectMachine()
        {
            try
            {
                dc.DeleteProjectMachine(mc, selectedProject, selectedProjMachine);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Cannot Delete Pump! Test data must be deleted first!");
            }
            RefreshMachineGridData();
        }

        void updtProjMcWin_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (updtProjMcWin.DialogResult == DialogResult.OK)
            {
                newProjMachine = updtProjMcWin.GetNewProjectMachineDetails();
                UpdateMcWinCancelStatus = false;
            }
            else UpdateMcWinCancelStatus = true;
            updtProjMcWin = null;
        }

        private void btnAddProjMc_Click(object sender, EventArgs e)
        {
            AddProjectMachine();
        }

        private void btnUpdtProjMc_Click(object sender, EventArgs e)
        {
            UpdateProjectMachine();
        }

        private void btnDelProjMc_Click(object sender, EventArgs e)
        {
            DeleteProjectMachine();
        }

        private void dgvMachines_SelectionChanged(object sender, EventArgs e)
        {
            if (dgvMachines.SelectedRows.Count > 0)
            {
                selectedProjMachine = GetProjectMachineDetailsFromSelectedRow(dgvMachines.SelectedRows[0]);
                statusMcNo.Text = selectedProjMachine.machinenumber;
                //dc.UpdateCurrentProjectMachine(selectedProjMachine);
                btnConductTest.Enabled = false;
                statusTestref.Text = "";
                tblTestbtns.Enabled = true;
                RefreshTestGridData();
            }
            else
            {
                dgvTests.Rows.Clear();
                tblTestbtns.Enabled = false;
            }
        }

#endregion

       #region Tests
               
        private void InitializeTestGrid()
        {
            tRefNumberCol = new DataGridViewTextBoxColumn() { Name = "testreference", HeaderText = "Test Reference", Width = 100, ReadOnly = true };
            //tRefNumberCol.DefaultCellStyle = disabledStyle;

            tDateCol = new DataGridViewTextBoxColumn() { Name = "testdate", HeaderText = "Test Date", Width = 100, ReadOnly = true };
            //tDateCol.DefaultCellStyle = disabledStyle;

            tRemarksCol = new DataGridViewTextBoxColumn() { Name = "testremarks", HeaderText = "Test Remarks", Width = 100, ReadOnly = true };
            //tRemarksCol.DefaultCellStyle = disabledStyle;

            this.dgvTests.Columns.Add(tRefNumberCol);
            this.dgvTests.Columns.Add(tDateCol);
            this.dgvTests.Columns.Add(tRemarksCol);
        }

        private void RefreshTestGridData()
        {
            dgvTests.Rows.Clear();
            try
            {
                testlist = dc.GetMachineTests(selectedProjMachine);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                this.Close();
            }

            for (int i = 0; i < testlist.Count; i++)
            {
                dgvTests.Rows.Add(new object[] { testlist[i].testreference, testlist[i].testdate, testlist[i].testremarks });
            }

        }

        private MachineTestDetails GetTestDetailsFromSelectedRow(DataGridViewRow row)
        {
            MachineTestDetails result = new MachineTestDetails();
            result.testreference = DefaultConversion.GetStringValue(row.Cells["testreference"].Value.ToString());
            int index = -1;

            for (int i = 0; i < testlist.Count; i++)
            {
                if (testlist[i].testreference == result.testreference)
                {
                    index = i;
                    break;
                }
            }
            if (index != -1)
                result = testlist[index];
            else MessageBox.Show("Select A Valid Test from the list!");

            return result;
        }

        private void AddNewTest()
        {
            updtTestWin = new UpdateMachineTestWindow(selectedProjMachine);
            updtTestWin.FormClosing += new FormClosingEventHandler(updtTestWin_FormClosing);

            updtTestWin.ShowDialog();
            if (UpdateTestMcWinCancelStatus == false)
            {
                try
                {
                    this.selectedTest = dc.CreateMachineTest(mc, selectedProject, selectedProjMachine, newTest.testreference, newTest.testdate, newTest.testremarks);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                RefreshTestGridData();
            }
        }

        private void ConductTest()
        {
            tstSumWin = new TestSummaryWindow(dc, mc, selectedProject, selectedProjMachine, selectedTest);
            tstSumWin.FormClosing += new FormClosingEventHandler(tstSumWin_FormClosing);
            tstSumWin.ShowDialog();
        }

        private void ViewTestReport()
        {
            string testfilename = DasCoreSettings.GetMachineTestReportFileFullName(mc, selectedProject, selectedProjMachine, selectedTest);

            ProcessHelper.OpenProcess("\""+testfilename+"\"", "", false, System.Diagnostics.ProcessWindowStyle.Maximized);
            //throw new NotImplementedException();
        }

        private void RegenerateTestReport()
        {
            string sf, df;

            sf = DasCoreSettings.GetMachineTestReportTemplateFileFullName(mc);

            df = DasCoreSettings.GetMachineTestReportFileFullName(mc,selectedProject,selectedProjMachine,selectedTest);

            FileSystemHandler.CopyFile(sf, df, true);

            ExcelWorkBookDataSet excelworkbook = reportGenerator.GenerateReportWorkBook(df, mc, selectedProject, selectedProjMachine, selectedTest, false);

            //Create a modal window to write and open the new file...

            ExcelWriterWindow ewWin = new ExcelWriterWindow(excelworkbook);

            ewWin.ShowDialog();

            ewWin.Dispose();

            ProcessHelper.OpenProcess("\"" + df + "\"", "", false, ProcessWindowStyle.Normal);
        }

        private void OpenTestFolder()
        {
            string testpath = DasCoreSettings.GetMachineTestReportsPath(mc, selectedProject, selectedProjMachine, selectedTest);

            ProcessHelper.OpenProcess(
                "explorer.exe ",
                "\"" + testpath + "\"",
                false,
                ProcessWindowStyle.Normal
                );
        }

        private void UpdateTestDetails()
        {
            updtTestWin = new UpdateMachineTestWindow(selectedProjMachine,selectedTest);
            updtTestWin.FormClosing += new FormClosingEventHandler(updtTestWin_FormClosing);

            updtTestWin.ShowDialog();
            if (UpdateTestMcWinCancelStatus == false)
            {
                try
                {
                    dc.UpdateMachineTest(newTest);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                RefreshTestGridData();
            }
        }

        private void DeleteTestDetails()
        {
            try
            {
                dc.DeleteMachineTest(selectedTest);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Source + " : " + ex.Message);
            }
            RefreshTestGridData();
        }

        void updtTestWin_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (updtTestWin.DialogResult == System.Windows.Forms.DialogResult.OK)
            {
                newTest = updtTestWin.GetNewMachineTestDetails();
                UpdateTestMcWinCancelStatus = false;
            }
            else UpdateTestMcWinCancelStatus = true;
            updtTestWin = null;
        }

        private void btnNewTest_Click(object sender, EventArgs e)
        {
            AddNewTest();
            ConductTest();
        }

        private void btnUpdtTest_Click(object sender, EventArgs e)
        {
            UpdateTestDetails();
        }

        private void btnDelTest_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure you want to delete this test? All saved data will be erased permanently!", "Delete test", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1) == System.Windows.Forms.DialogResult.Yes)
            {
                DeleteTestDetails();
            }
        }

        private void dgvTests_SelectionChanged(object sender, EventArgs e)
        {
            if (dgvTests.SelectedRows.Count > 0)
            {
                selectedTest = GetTestDetailsFromSelectedRow(dgvTests.SelectedRows[0]);
                statusTestref.Text = selectedTest.testreference;
                //dc.UpdateCurrentTest(selectedTest);
                btnConductTest.Enabled = true;
            }
            
        }    

#endregion

        private void btnConductTest_Click(object sender, EventArgs e)
        {

            ConductTest();

        }


        void tstSumWin_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (tstSumWin.DialogResult == DialogResult.OK)
            {
                isTestRequested = true;
                this.Close();                
            }
            else
            {
                //this.Show();
            }
        }

        private void btnGenRpt_Click(object sender, EventArgs e)
        {
            RegenerateTestReport();
        }



        private void dgvProject_SelectionChanged_1(object sender, EventArgs e)
        {
            if (dgvProject.SelectedRows.Count > 0)
            {
                selectedProject = GetProjectDetailsFromSelectedRow(dgvProject.SelectedRows[0]);


                statusProj.Text = selectedProject.projectname;
                //dc.UpdateCurrentProject(selectedProject);
                btnConductTest.Enabled = false;
                statusMcNo.Text = "";
                statusTestref.Text = "";
                tblMachineBtns.Enabled = true;
                RefreshMachineGridData();
            }
            else
            {
                dgvMachines.Rows.Clear();
                dgvTests.Rows.Clear();
                tblTestbtns.Enabled = false;
                tblMachineBtns.Enabled = false;
            }
        }

        private void btnViewRpt_Click(object sender, EventArgs e)
        {
            ViewTestReport();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenTestFolder();
        }



        
            

    }


}
