﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using DasCoreLib;
using BHEL.PUMPSDAS.Datatypes;
using System.Diagnostics;
using System.Threading;
using System.Reflection;
using CodeMonkey;
using System.Drawing.Imaging;
using System.Drawing;
using FileManager;
using ExcelManager;
using DasCoreUtilities;


namespace WinFormDasCoreClient
{
    public partial class DXAcqWindow : Form
    {
       #region Global Objects

        DasCore dascore;

        delegate void HardwareWorkerThreadMethod();
        delegate void WindowUIUpdateDelegate();
        delegate void DasDataExtractDelegate();

        HardwareWorkerThreadMethod workerMethod;
        IAsyncResult asyncResult;
        AsyncCallback asyncReturn;
        static bool isAcquisitionRunning = false, acqpause = false;
        List<MeasurementProfileDetails> mTemplate;

        ResultParamsBase rp, rpAvg;
        MeasurementProfileBase mp, mpAvg;
        SpecifiedParamsBase sp;
        TestSetupParamsBase tsp;
        MachineDetailsBase md;
        MachineDescriptor mcd;

        WindowUIUpdateDelegate WindowUIUpdateMethod;
        DasDataExtractDelegate DasExtractMethod;

        Stopwatch sw;
        bool continueScan = true;

        ProjectDetails selectedProject;
        ProjectMachineDetails selectedProjMachine;
        MachineTestDetails selectedTest;
        InstalledMachine selectedMachine;
        List<MeasurementProfileDetails> mpDetails;
        List<ResultParamsDetails> rpDetails;

        UIConfigurator uiconfig;
        Compiler scriptCompiler;
        Assembly  UIScriptAssembly;
        LinePrinterBase linePrinter;
        Assembly LinePrinterScriptAssembly;

        AlarmColors alColors;
        List<AlarmStatus> alarm;

        int currentPoint;
        int currentScan;
        bool isAverageUpdated;
        string currentPointDate;
        string currentPointTime;

        ObjectViewWindow lpWindow, computeWindow;
        ActivityCommentsWindow acWin;

        ScanMode scMode;

        // Activity Log...

        List<String> activities;
        List<string> comments;

        TestReportGenerator reportGenerator;
        List<LinePrintData> currentLinePrintData;
        bool isDasInitializationSuccess = false;

        
       #endregion

        public DXAcqWindow(DasCore dc)
        {

            InitializeComponent();
            activities = new List<string>(0);
            comments = new List<string>(0);
            this.dascore = dc;            
            this.selectedMachine = dc.GetCurrentMachine();
            this.selectedProject = dc.GetCurrentProject();
            this.selectedProjMachine = dc.GetCurrentProjectMachine();
            this.selectedTest = dc.GetCurrentTest();
            AddActivity("------------------------------------------------------------");
            AddActivity("Machine Selected : " + selectedMachine.machineuniquecode);
            AddActivity("Project Selected : " + selectedProject.projectname);
            AddActivity("Pump Selected : " + selectedProjMachine.machinenumber);
            AddActivity("Test Selected : " + selectedTest.testreference);

            AddActivity("  ");
            
            sw = new Stopwatch();

            workerMethod = new HardwareWorkerThreadMethod(DoWork);
            asyncReturn = new AsyncCallback(WorkerCompleted);

            AddActivity("Attempting to Initialize Das Core Objects...");

            try
            {
                this.WindowUIUpdateMethod = this.UpdateWindowUI;
                this.DasExtractMethod = this.DasExtract;

                // Hardware Initialization
                try
                {
                    dascore.InitializeAndConfigureHardware();
                }
                catch (Exception ex)
                {
                    throw new Exception("Failed to establish communication with DAQ Hardware : " + ex.Message + "\nStack Trace :\n" + ex.StackTrace);
                    AddActivity("Hardware Initialization failed.." + Environment.NewLine + ex.Message);
                    this.Close();                    
                }

                
                AddActivity("Hardware Configured and Initialized..");
                // Update pointers to the main objects.
                tsp = dascore.GetCurrentTestSetupParams();
                AddActivity("Test Setup Parameters updated..");
                md = dascore.GetCurrentMachineDetails();
                AddActivity("Machine Details updated..");
                mcd = dascore.GetCurrentMachineDescriptor();
                AddActivity("Machine Descriptor updated..");
                sp = dascore.GetCurrentSpecifiedParams();
                AddActivity("Specified Parameters updated..");
                rp = dascore.GetCurrentResultParams();
                AddActivity("Result Parameters updated..");
                mp = dascore.GetCurrentMeasurementProfile();
                mpAvg = dascore.GetCurrentMeasurementProfileAverage();
                rpAvg = dascore.GetCurrentResultParamsAverage();
                mpDetails = dascore.GetMeasurementProfileDetails(selectedMachine);
                rpDetails = dascore.GetResultParamsDetails(selectedMachine);
                timer1.Start();

                alColors = DasCoreSettings.GetAlarmColors();


                AddActivity("Attempting to Compile UI script");
                if (CompileUIScript())
                {
                    AddActivity("UI Script Successfull Compiled..");
                    InitializeUI();
                }
                else
                {
                    MessageBox.Show("Error processing UI Script !");
                    this.Close();
                }

                AddActivity("Attempting to compile Line Print Script");
                if (CompileLinePrinterScript())
                {
                    AddActivity("Line Print Script successfully compiled");
                    GenerateLinePrinter();
                }
                else
                {
                    MessageBox.Show("Error processing Line Printer Script !");
                    this.Close();
                }
                scanProgress.Style = ProgressBarStyle.Continuous;
                scanProgress.MarqueeAnimationSpeed = 0;

                // Create and hold the line print window....
                lpWindow = new ObjectViewWindow(selectedProject.projectname,
                    selectedProjMachine.machinedescription,
                    selectedProjMachine.machinenumber,
                    selectedTest.testreference);
                lpWindow.FormClosing += new FormClosingEventHandler(lpWindow_FormClosing);

                // Create and hold the line Compute View....
                computeWindow = new ObjectViewWindow(selectedProject.projectname,
                    selectedProjMachine.machinedescription,
                    selectedProjMachine.machinenumber,
                    selectedTest.testreference);
                computeWindow.FormClosing += new FormClosingEventHandler(computeWindow_FormClosing);

                acWin = new ActivityCommentsWindow();

                acWin.FormClosing += new FormClosingEventHandler(acWin_FormClosing);
                
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
                this.Close();
            }
            reportGenerator = new TestReportGenerator(this.dascore);
            AddActivity("Das Core Objects successfully Initialized");            
        }

        private bool CompileUIScript()
        {
            bool isSuccessful;
            string compileErrors = "";

            scriptCompiler = null;
            scriptCompiler = new Compiler("", "");

            string scriptpath = DasCoreSettings.GetMachineUIConfigScriptFileFullName(selectedMachine);
            if (UIScriptAssembly != null)
                UIScriptAssembly = null;
            //MessageBox.Show("UI Script : " + scriptpath);

            try
            {
                //errors = scriptCompiler.Compile(scriptpath, false);
                UIScriptAssembly = scriptCompiler.CompileInMemory(scriptpath,out isSuccessful,out compileErrors);
                if ( ! isSuccessful )
                {
                    MessageBox.Show("UI Script Compilation failed : " + compileErrors);
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("UI Script Compilation failed : " + ex.Message + " : " + compileErrors + " \n Stack Trace :\n " + ex.Source);
                return false;
            }
        }
        private void GenerateUIConfigurator()
        {
            List<Label> labels = new List<Label>();
            labels.Add(lblProject);
            labels.Add(lblMachineUCode);
            labels.Add(lblTestReference);

            labels.Add(label5);
            labels.Add(label6);
            labels.Add(label7);

            List<DataGridView> grids = new List<DataGridView>();
            grids.Add(dgvOne);
            grids.Add(dgvTwo);
            grids.Add(dgvThree);
            grids.Add(dgvFour);


            if (uiconfig != null)
                uiconfig = null;


            try
            {               
                Type uiconfigtype = UIScriptAssembly.GetType(DasCoreSettings.GetMachineUIScriptObjectName(selectedMachine));
                uiconfig = (UIConfigurator)Activator.CreateInstance(uiconfigtype);

                uiconfig.InitializeUIConfig(ref labels, ref grids, ref rp, ref rpAvg, ref mp, ref mpAvg, ref sp, ref tsp, ref md, ref mcd, mpDetails, rpDetails, alColors);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Source + " : " + ex.Message);
            }
        }
        private void InitializeUI()
        {
            try
            {
                GenerateUIConfigurator();
                uiconfig.UpdateStaticUIControls();
                this.lblDate.Text = "";
                this.currentPoint = dascore.GetCurrentPoint();
                UpdateCurrentPointLabel(currentPoint);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            this.Invalidate();
        }

        public bool CompileLinePrinterScript()
        {
            bool isSuccessful;
            string compileErrors = "";

            scriptCompiler = null;
            scriptCompiler = new Compiler("", "");

            string scriptpath = DasCoreSettings.GetMachineLinePrinterScriptFileFullName(selectedMachine);
            if (LinePrinterScriptAssembly != null)
                LinePrinterScriptAssembly = null;
            //MessageBox.Show("Line Print Script : " + scriptpath);

            try
            {
                //errors = scriptCompiler.Compile(scriptpath, false);
                LinePrinterScriptAssembly = scriptCompiler.CompileInMemory(scriptpath, out isSuccessful, out compileErrors);
                if (!isSuccessful)
                {
                    MessageBox.Show("Line Printer Script Compilation failed! Process aborted ! : " + compileErrors);
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Line Printer Script Compilation failed! Process aborted ! : " + ex.Source + " : " + ex.Message + " : " + compileErrors);
                return false;
            }
        }
        private void GenerateLinePrinter()
        {
            try
            {
                // TODO : Remidn Deb
                Type lpType = this.LinePrinterScriptAssembly.GetType(DasCoreSettings.GetMachineLinePrinterObjectName(selectedMachine));
                linePrinter = (LinePrinterBase)Activator.CreateInstance(lpType);


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Source + " : " + ex.Message);
            }
        }

        #region ASync thread functions

        void DoWork()
        {
            #region backup
            // Do not access UI elements from here....
            // Donot access variables from this object....
            // This method runs asynchronously...
            // Very unsafe to access UI elements or members of this class from this method.
            // Send complete information to this class and let it do it's work.
            //Random rr = new Random(DateTime.Now.Second);
            //List<ChannelData> result = new List<ChannelData>(0);
            //ChannelData data;
            //for (int i = 0; i < input.Count; i++)
            //{
            //    data = input[i];
            //    data.rawvalue = rr.NextDouble();
            //    data.computedValue = data.rawvalue * 100;
            //    result.Add(data);
            //}
            //// Simulate a long running thread.
            //Thread.Sleep(1500);
            //return result;
            #endregion

            try
            {
                //sw.Start();
                alarm = dascore.ScanHardware(out isAverageUpdated, scMode);
                //Thread.Sleep(100);
                //sw.Stop();

            }
            catch (Exception ex)
            {
                string err = "ERROR : " + ex.Source + " : " + ex.Message;
                Thread.Sleep(1000);
            }
        }
        void WorkerCompleted(IAsyncResult res)
        {

            // Access all variables in this object here itself.....
            if (isAcquisitionRunning == true && acqpause == false)
            {
                
                workerMethod.EndInvoke(asyncResult);

                // make use of the results here....

                // Manipulation.

                // Auto computation.

                // Actual Data saving.

                // Update the UI
                ExtractDasInfo();

                UpdateUI();



                // Since the command for pausing the Acquisition is not true....
                // Start acquiring data again..

                asyncResult = workerMethod.BeginInvoke(asyncReturn, null);
            }

            else
            {

                workerMethod.EndInvoke(asyncResult);

                isAcquisitionRunning = false;
            }


        }
        void WorkerStart()
        {
            if (isAcquisitionRunning == false)
            {
                isAcquisitionRunning = true;

                acqpause = false;

                asyncResult = workerMethod.BeginInvoke(asyncReturn, null);
            }
            else
            {
                MessageBox.Show("Acquisition is in progress...");
            }
        }
        void WorkerStop()
        {
            workerMethod.EndInvoke(asyncResult);
        }

        #endregion
        
        private void UpdateUI()
        {
            Invoke(WindowUIUpdateMethod);
        }
        private void ExtractDasInfo()
        {
            this.Invoke(DasExtractMethod);
        }
        private void DasExtract()
        {
            //this.actRes = dascore.GetActualResult();
            //this.mp = dascore.GetActualMeasurementProfile();
        }
        void UpdateWindowUI()
        {
            //List<AlarmStatus> alarm = new List<AlarmStatus>(0);
            currentPoint = dascore.GetCurrentPoint();
            currentScan = dascore.GetCurrentScan();
            UpdateCurrentPointLabel(currentPoint);
            scanProgress.Value = currentScan * 10;
   
            uiconfig.UpdateDynamicUIControls(alarm,this.isAverageUpdated);
            this.lblRecentActivity.Text = activities[activities.Count - 1];
            this.Invalidate();
        }
        

        private void StartAcquisition()
        {
            continueScan = true;
            WorkerStart();
            this.btnStop.Enabled = false;
        }
        private void PauseAcquisition()
        {
            acqpause = true;
            continueScan = false;
            this.btnStop.Enabled = true;
        }
        private void StopAcquisition()
        {
            //WorkerStop();
            continueScan = false;
            isAcquisitionRunning = false;

            // Report Generation....

            string sf, df;

            sf = DasCoreSettings.GetMachineTestReportTemplateFileFullName(selectedMachine);

            df = DasCoreSettings.GetMachineTestReportFileFullName(selectedMachine, selectedProject, selectedProjMachine, selectedTest);

            FileSystemHandler.CopyFile(sf, df, true);

            ExcelWorkBookDataSet excelworkbook = reportGenerator.GenerateReportWorkBook(df, selectedMachine, selectedProject, selectedProjMachine, selectedTest, false);
            
            //Create a modal window to write and open the new file...

            ExcelWriterWindow ewWin = new ExcelWriterWindow(excelworkbook);

            ewWin.ShowDialog();

            ewWin.Dispose();

            ProcessHelper.OpenProcess( "\"" + df + "\"","", false, ProcessWindowStyle.Normal);

            this.Close();
        }

        private void EditUiConfigurationScript()
        {
            // Pause the acquisition if it's already running..
            PauseAcquisition();
            AddActivity("UI Configuration script editing started...");


            string sf, df;

            sf = DasCoreSettings.GetMachineUIConfigScriptFileFullName(selectedMachine);
            df = sf + ".bkp";
            FileSystemHandler.CopyFile(sf, df, true);

            // Open an NPP window to show the script...
            ProcessHelper.OpenProcess(DasCoreSettings.GetScriptEditorFileFullName(), DasCoreSettings.GetMachineEditUIConfigScriptProcessArguments(selectedMachine), true, ProcessWindowStyle.Normal);

            AddActivity("UI Configuration script editing ended...");
            AddActivity("Compiling UI configuration script...");


            // Compile the newly updated script..
            if (this.CompileUIScript())
            {
                AddActivity("UI configuration script successfully compiled..");
                // Script Compilation successful...
                InitializeUI();
            }
            else
            {
                MessageBox.Show("Error compiling UI Configuration script. Reverting back to old script..");
                AddActivity("UI Configuration script compilation failed...");
                AddActivity("Restoring file from backup..");
                FileSystemHandler.CopyFile(df, sf, true);
                this.CompileUIScript();
                InitializeUI();               
            }

            AddActivity("Acquisition re-started");

            //StartAcquisition();

        }
        private void EditLinePrintScript()
        {
            // Pause the acquisition if it's already running..
            PauseAcquisition();
            AddActivity("Line Print script editing started...");

            string sf, df;

            sf = DasCoreSettings.GetMachineLinePrinterScriptFileFullName(selectedMachine);
            df = sf + ".bkp";
            FileSystemHandler.CopyFile(sf, df, true);
            // Open an NPP window to show the script...
            ProcessHelper.OpenProcess(
                DasCoreSettings.GetScriptEditorFileFullName(),
                DasCoreSettings.GetMachineEditLinePrintScriptProcessArguments(selectedMachine),
                true,
                ProcessWindowStyle.Normal
                );


            AddActivity("Line Print script editing ended...");
            AddActivity("Compiling Line Print script...");
            // Compile the newly updated script..

            if (this.CompileLinePrinterScript())
            {
                // Script Compilation successful...
                this.GenerateLinePrinter();
                AddActivity("Line Print script successfully compiled..");
            }
            else
            {
                MessageBox.Show("Error compiling Line Print script. Reverting back to old script..");
                AddActivity("Line Print script compilation failed...");
                AddActivity("Restoring file from backup..");
                FileSystemHandler.CopyFile(df, sf, true);
                this.CompileLinePrinterScript();
                this.GenerateLinePrinter();                
            }

            AddActivity("Acquisition re-started");
            //StartAcquisition();
        }
        private void EditErrorCorrectorScript()
        {
            // Pause the acquisition if it's already running..
            PauseAcquisition();
            AddActivity("Error Corrector script editing started...");
            // Open an NPP window to show the script...

            string sf, df;

            sf = DasCoreSettings.GetProjectMachineErrCorrScriptFileFullName(selectedMachine, selectedProject, selectedProjMachine);
            df = sf + ".bkp";
            FileSystemHandler.CopyFile(sf, df, true);


            ProcessHelper.OpenProcess(
                DasCoreSettings.GetScriptEditorFileFullName(),
                DasCoreSettings.GetProjectMachineEditErrorCorrectorScriptProcessArguments(selectedMachine,selectedProject,selectedProjMachine),
                true,
                ProcessWindowStyle.Normal);

            AddActivity("Error Corrector script editing ended...");
            AddActivity("Compiling Error Corrector script...");

            try
            {
                // Compile the newly updated script..
                if (dascore.ReLoadErrorCorrector())
                {
                    // Script Compilation successful...                    
                    AddActivity("Error Corrector script successfully compiled..");
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error compiling script. Reverting back to old script..");
                AddActivity("Error Corrector script compilation failed...");
                AddActivity("Restoring file from backup..");
                FileSystemHandler.CopyFile(df, sf, true);
                dascore.ReLoadErrorCorrector();
            }
            AddActivity("Acquisition re-started");
            //StartAcquisition();
        }
                

        private void UpdateCurrentPointLabel(int point)
        {
            lblCurrentPoint.Text = "Current Point : " + point;
        }
        private void RecordPoint()
        {
            PauseAcquisition();
            AddActivity("Point No. : " + this.currentPoint + " Record process Initiated");
            // Show the recorded point before adding to database...
            currentLinePrintData = linePrinter.GenerateLinePrintData(this.mpDetails, this.rpDetails, this.mpAvg, this.rpAvg);
            currentPointDate = GetDateStamp(DateTime.Now,false);
            currentPointTime = GetTimeStamp(DateTime.Now, false);
            lpWindow.UpdateData(currentPointDate,currentPointTime , this.currentPoint.ToString(), currentLinePrintData,false);

            lpWindow.ShowDialog();


            AddActivity("Data Acquisition continued..");
            StartAcquisition();
        }
        private void RecordPointData(string n1, string n2)
        {
            AddActivity("Point No. : " + this.currentPoint + " Data insertion in database started..");
            dascore.RecordCurrentPoint(n1, n2);
            AddActivity("Point No. : " + this.currentPoint + " Point recorded. Data insertion in database completed..");
            // Do the line printing here..... before current point is updated...
            

            FileSystemHandler.CopyFile(DasCoreSettings.GetLinePrintTemplateFileFullName(), DasCoreSettings.GetMachineLinePrintFileName(selectedMachine, selectedProject, selectedProjMachine, selectedTest, currentPoint), true);
            string filename = DasCoreSettings.GetMachineLinePrintFileName(selectedMachine, selectedProject, selectedProjMachine, selectedTest, currentPoint);
            ExcelWorkBookDataSet excelworkbook = reportGenerator.GenerateLinePrintWorkBook(
                currentPointDate,
                currentPointTime,
                currentPoint,
                currentLinePrintData,
                n1,
                n2,
                filename,
                selectedMachine, selectedProject, selectedProjMachine, selectedTest
                );


            // Create a modal window to write and open the new file...
            
            ExcelWriterWindow ewWin = new ExcelWriterWindow(excelworkbook);

            ewWin.ShowDialog();

            ewWin.Dispose();
            // Open the file...for display...
            if(MessageBox.Show("Do you want to print this logsheet?","Logsheet Printing",MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
            {
                //OpenProcess("excel.exe", " \"" + filename + "\"", false, ProcessWindowStyle.Normal);

                try
                {
                    AddActivity("Printing line print for point : " + currentPoint);

                    ExcelFile xlFile = new ExcelFile(filename);

                    xlFile.Open(false);

                    xlFile.PrintWorkSheet(DasCoreLib.DasCoreSettings.GetLinePrintSheetName(),false,1);

                    xlFile.CloseFile();

                    AddActivity("Printing completed for point : " + currentPoint);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    AddActivity("Printing line print failed for point : " + currentPoint);
                }
            }

            this.currentPoint = dascore.GetCurrentPoint();
            
        }
        private void ComputePoint()
        {
            PauseAcquisition();
            
            List<LinePrintData> data = new List<LinePrintData>();
            LinePrintData lp;

            for (int i = 0; i < rpDetails.Count; i++)
            {
                lp = new LinePrintData();
                lp.paramdesc = rpDetails[i].paramdescription;
                lp.paramunits = rpDetails[i].paramunits;
                lp.value = this.rpAvg.GetValue(rpDetails[i].paramname).ToString();

                data.Add(lp);
            }
            // Todo update date and time...
            computeWindow.UpdateData(GetDateStamp(DateTime.Now,false), GetTimeStamp(DateTime.Now,false), this.currentPoint.ToString(), data,true);
            computeWindow.ShowDialog();
            
            StartAcquisition();
        }
        private void DisplayAllRecordedPoints()
        {
            if (currentPoint == 1)
            {
                MessageBox.Show("There are no recorded points !");
                return;
            }
            PauseAcquisition();

            List<List<LinePrintData>> dataList = new List<List<LinePrintData>>();
            List<LinePrintData> data;
            List<ObjectData> mp, rp;
            DateTime dt;
            string noise1, noise2;
            List<string> dates, times, points;

            dates = new List<string>();
            times = new List<string>();
            points = new List<string>();

            int nPoints = currentPoint - 1;


            /*public bool GetMeasurementDataPoint(
              * InstalledMachine mc, 
              * ProjectDetails projc, 
              * ProjectMachineDetails pmd, 
              * MachineTestDetails mtd, 
              * int pointnum, 
              * bool isActual,
              * out List<ObjectData> measPt,
              * out List<ObjectData> resultPt,
              * out DateTime dt,
              * out string noise1,
              * out string noise2)*/

            /* UpdateMultiValueData(
             * List<string> dates, 
             * List<string> times, 
             * List<string> points, 
             * List<List<LinePrintData>> dataList) */
        
            // We are only showing measurement profile data 

            for (int i = 0; i < nPoints; i++)
            {
                dascore.GetMeasurementDataPoint(selectedMachine, selectedProject, selectedProjMachine, selectedTest,
                    i+1, false, out mp, out rp, out dt, out noise1, out noise2);
                dates.Add(dt.Day + "/" + dt.Month + "/" + dt.Year);
                times.Add(dt.Hour + ":" + dt.Minute + ":" + dt.Second);
                points.Add(i.ToString());
                data = new List<LinePrintData>();
                for (int j = 0; j < mpDetails.Count; j++)
                {
                    LinePrintData lp = new LinePrintData();
                    lp.paramdesc = mpDetails[j].paramdescription;
                    lp.paramunits = mpDetails[j].paramunits;
                    for (int k = 0; i < mp.Count; k++)
                    {
                        if (mp[k].pindex == mpDetails[j].displayindex)
                        {
                            lp.value = mp[k].pvalue;
                            break;
                        }
                    }

                    data.Add(lp);
                }

                dataList.Add(data);
            }

            computeWindow.UpdateMultiValueData(dates, times, points, dataList,true);
            computeWindow.ShowDialog();
            computeWindow.FormClosing+=new FormClosingEventHandler(computeWindow_FormClosing);
            StartAcquisition();
        }
        private void TakeScreenshot()
        {
            this.tsbtnTakeScreenShot.Visible = false;
            this.Invalidate();
            Rectangle bounds = Screen.PrimaryScreen.Bounds;
            DateTime Now = DateTime.Now;
            string datetime = Now.Day + "-" + Now.Month + "-" + Now.Year + "_" + Now.Hour + "-" + Now.Minute + "-" + Now.Second;
            string fileName = DasCoreSettings.GetTestScreenshotFullName(selectedMachine,selectedProject,selectedProjMachine,selectedTest,datetime);
            
            using (Bitmap bmp = new Bitmap(bounds.Width, bounds.Height))
            {
                using (Graphics g = Graphics.FromImage(bmp))
                {
                    g.CopyFromScreen(new Point(bounds.Left, bounds.Top), Point.Empty, bounds.Size);                    
                    bmp.Save(fileName, ImageFormat.Png);
                }
            }

            this.tsbtnTakeScreenShot.Visible = true;
            this.Invalidate();
        }
        private void ShowCommentsWindow()
        {
            acWin.UpdateActivityComments(this.activities, this.comments);
            acWin.ShowDialog();
        }
        private void OpenScreenShotsFolder()
        {
            ProcessHelper.OpenProcess(
                "explorer.exe ",
                "\""+DasCoreSettings.GetMachineTestScreenshotsPath(selectedMachine, selectedProject, selectedProjMachine, selectedTest)+"\"",
                false,
                ProcessWindowStyle.Normal
                );
        }
        private void OpenLinePrintsFolder()
        {
            ProcessHelper.OpenProcess(
                "explorer.exe ",
                "\"" + DasCoreSettings.GetMachineTestLinePrintsPath(selectedMachine, selectedProject, selectedProjMachine, selectedTest) + "\"",
                false,
                ProcessWindowStyle.Normal
                );
        }
        private string GetDateTimeStamp(bool isForFileName)
        {
            DateTime now = DateTime.Now;

            return GetDateStamp(now,isForFileName) + " " + GetTimeStamp(now,isForFileName);

            
        }
        private string GetDateStamp(DateTime now,bool isForFileName)
        {
            int date, month, year;
            string d, m, y;

            date = now.Day;
            month = now.Month;
            year = now.Year;

            d = date.ToString();
            m = month.ToString();
            y = year.ToString();

            if (date < 10)
                d = "0" + d;

            if (month < 10)
                m = "0" + m;
            if (isForFileName)
                return d + "_" + m + "_" + y;
            return d + "/" + m + "/" + y ;
        }
        private string GetTimeStamp(DateTime now,bool isForFileName)
        {
            int hour, minute, second;
            string h, mn, sec;

            

            hour = now.Hour;
            minute = now.Minute;
            second = now.Second;

            

            h = hour.ToString();
            mn = minute.ToString();
            sec = second.ToString();
            

            if (hour < 10)
                h = "0" + h;

            if (minute < 10)
                mn = "0" + mn;

            if (second < 10)
                sec = "0" + sec;
            if (isForFileName)
                return h + "-" + mn + "-" + sec;
            return h + ":" + mn + ":" + sec;
        }
        private void ReleaseResources()
        {
            AddActivity("Acquisition Stopped !");

            AddActivity("------------------------------------------------------------");

            string logFileName = DasCoreSettings.GetMachineTestReportsPath(selectedMachine, selectedProject, selectedProjMachine, selectedTest)
                + "\\Data_Acquisition_log_" + GetDateStamp(DateTime.Now, true) + ".txt";
            string logData = "";

            for (int i = 0; i < activities.Count; i++)
            {
                logData += activities[i] + Environment.NewLine;
            }

            logData += "----------------------User Comments-------------------------"+ Environment.NewLine;

            for (int i = 0; i < comments.Count; i++)
            {
                logData += comments[i] + Environment.NewLine;
            }

            logData += "----------------------User Comments-------------------------" + Environment.NewLine;
            FileSystemHandler.WriteToTextFile(logFileName, true, logData);

            // release any resources...

            dascore = null;
            workerMethod = null;
            asyncResult = null;
            asyncReturn = null;
            mTemplate = null;
            rp = null;
            rpAvg = null;
            mp = null;
            mpAvg = null;
            sp = null;
            tsp = null;
            md = null;
            mcd = null;
            WindowUIUpdateMethod = null;
            DasExtractMethod = null;

            mpDetails = null;
            rpDetails = null;
            uiconfig = null;
            scriptCompiler = null;
            UIScriptAssembly = null;
            linePrinter = null;
            LinePrinterScriptAssembly = null;
            lpWindow = null;
            computeWindow = null;
            acWin = null;

            // Activity Log...

            activities = null;
        }
        private void AddActivity(string activity)
        {
            activities.Add(GetDateTimeStamp(false) + " - "+activity);
        }

        private void EditMeasurementConfigurationScript()
        {
            PauseAcquisition();
            ChannelConfigWindow ccw = new ChannelConfigWindow(this.dascore, dascore.GetCurrentMachine());
            ccw.ShowDialog();
            dascore.ReloadCurrentMeasurementProfile();
            //StartAcquisition();
        }

        #region Form event handlers

        private void btnStartAcq_Click_1(object sender, EventArgs e)
        {
            StartAcquisition();
        }

        private void btnPause_Click_1(object sender, EventArgs e)
        {
            PauseAcquisition();
        }

        private void btnStop_Click_1(object sender, EventArgs e)
        {
            StopAcquisition();
            
        }

        private void timer1_Tick_1(object sender, EventArgs e)
        {
            this.lblTime.Text = GetDateTimeStamp(false);
        }

        private void DXAcqWindow_Load(object sender, EventArgs e)
        {
            //dascore.ReConfigureHardware();

            /*TimedWaitWindow tmw = new TimedWaitWindow(new string[] { "Intitializing Devices...",
                "Initializing Scan Lists...",
                "Checking Hardware Configuration..",
                "Completed"}, 10000);

            tmw.Text = "Hardware Communication in Progress...";
            tmw.ShowDialog();
            tmw.Dispose();
            tmw = null;*/
        }

        private void btnCompute_Click(object sender, EventArgs e)
        {
            ComputePoint();
        }

        private void btnRecord_Click(object sender, EventArgs e)
        {
            RecordPoint();
        }

        private void DXAcqWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (isAcquisitionRunning)
            {
                StopAcquisition();
            }

            // Save the activity log.....

            ReleaseResources();
        }

        private void btnRecordedPoints_Click(object sender, EventArgs e)
        {
            DisplayAllRecordedPoints();
        }

        void lpWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (lpWindow.DialogResult == System.Windows.Forms.DialogResult.OK)
            {
                // Command has been recieved to record the point...
                RecordPointData(lpWindow.noise1, lpWindow.noise2);
                e.Cancel = true;
                lpWindow.Hide();
                return;
            }
            else if (lpWindow.DialogResult == System.Windows.Forms.DialogResult.Cancel)
            {
                // Do nothing....do not record any data...
                AddActivity("Point No. : " + this.currentPoint + " Record action cancelled..");
                e.Cancel = true;
                lpWindow.Hide();
                return;
            }
            else // Handles the hide event...
            {
                e.Cancel = true;
            }
            
            //throw new NotImplementedException();
        }

        void computeWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            computeWindow.Hide();
            //throw new NotImplementedException();
        }

        void acWin_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.comments = acWin.GetComments();
            //e.Cancel = false;
            //acWin.Hide();
        }

        private void toolStripSplitButton1_ButtonClick(object sender, EventArgs e)
        {
            TakeScreenshot();
        }

        private void viewScreenshotsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenScreenShotsFolder();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            ShowCommentsWindow();
        }

        private void editMeasurementConfigurationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EditMeasurementConfigurationScript();
        }

        private void editUIConfigurationScriptToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EditUiConfigurationScript();
        }

        private void editLinePrintingScriptToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            EditLinePrintScript();
        }

        private void editErrorScriptToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            EditErrorCorrectorScript();
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            OpenLinePrintsFolder();
        }

        private void tsCbScanMode_TextChanged(object sender, EventArgs e)
        {
            switch (tsCbScanMode.Text)
            {
                case "SAFE_START_MODE" :
                    scMode = ScanMode.SAFE_START_MODE;
                    break;

                case "NORMAL_MODE" :
                    scMode = ScanMode.NORMAL_MODE;
                    break;

                case "MONITORING_MODE" :
                    scMode = ScanMode.MONITORING_MODE;
                    break;

                case "FAST_MONITORING_MODE" :
                    scMode = ScanMode.FAST_MONITORING_MODE;
                    break;
            }
        }

        #endregion

        #region DEPRECATED-BACKUP

        private void InitializeGrids()
        {
            /*
            DataGridViewTextBoxColumn desccol, unitscol, avgcol, realcol;

            desccol = new DataGridViewTextBoxColumn();
            desccol.HeaderText = "Parameter description";
            desccol.Name = "paramdesc";
            desccol.Width = 140;
            desccol.ReadOnly = true;


            unitscol = new DataGridViewTextBoxColumn();
            unitscol.HeaderText = "Units";
            unitscol.Name = "units";
            unitscol.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            unitscol.ReadOnly = true;

            avgcol = new DataGridViewTextBoxColumn();
            avgcol.HeaderText = "Avg. Value";
            avgcol.Name = "avgval";
            avgcol.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            avgcol.ReadOnly = true;

            realcol = new DataGridViewTextBoxColumn();
            realcol.HeaderText = "Real Value";
            realcol.Name = "realval";
            realcol.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            realcol.ReadOnly = true;


            //this.dgvOne.Columns.Add(desccol);
            //this.dgvOne.Columns.Add(unitscol);
            //this.dgvOne.Columns.Add(avgcol);
            //this.dgvOne.Columns.Add(realcol);
            */

            for (int i = 0; i < 17; i++)
            {
                dgvOne.Rows.Add(new object[] { "param" + i, "", "", "" });
            }
            for (int i = 0; i < 5; i++)
            {
                this.dgvTwo.Rows.Add(new object[] { "param" + i, "", "", "" });
            }
            for (int i = 0; i < 17; i++)
            {
                dgvThree.Rows.Add(new object[] { "param" + i, "", "", "" });
            }
            for (int i = 0; i < 5; i++)
            {
                dgvFour.Rows.Add(new object[] { "param" + i, "", "", "" });
            }

            dgvOne.Invalidate();            
        }

        private void RefreshGrids()
        {

        }        

        private void InitializeCellStyles()
        {
            
        }
        
        private void InitializeFixedUIData()
        {
            this.lblDate.Text = DateTime.Now.Date.Day + "-" + DateTime.Now.Date.Month + "-" + DateTime.Now.Year;
            this.lblProject.Text += selectedProject.projectname;
            this.lblMachineUCode.Text += selectedMachine.machineuniquecode;
            this.lblTestReference.Text += selectedTest.testreference;

            /*for (int i = 0; i < mpDetails.Count; i++)
            {
                if (mpDetails[i].paramtype == "Hydraulic")
                {
                    this.dgvOne.Rows[i].Cells[0].Value = DateTime.Now.Ticks.ToString();
                }
                else if (mpDetails[i].paramtype == "Mechanical")
                {
                    this.dgvThree.Rows[i].Cells[0].Value = DateTime.Now.Ticks.ToString();
                }
                else if (mpDetails[i].paramtype == "Electrical")
                {
                    this.dgvTwo.Rows[i].Cells[0].Value = DateTime.Now.Ticks.ToString();
                }
                else if (mpDetails[i].paramtype == "Vibration")
                {
                    this.dgvOne.Rows[i].Cells[0].Value = DateTime.Now.Ticks.ToString();
                }
            }*/
            this.Invalidate();
        }

        private void specifiedParametersToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void btnSpecParams_Click(object sender, EventArgs e)
        {

        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        


        
















        /*
           void InitializeHydraulicGrid()
        {
            pNameCol = new GridColumn();
            pNameCol.Name = "paramname";
            pNameCol.Caption = "Parameter Name";

            pUnitsCol = new GridColumn();
            pUnitsCol.Name = "paramunits";
            pUnitsCol.Caption = "Units";

            pAvgValCol = new GridColumn();
            pAvgValCol.Name = "pavgvalue";
            pAvgValCol.Caption = "Average Value";

            pValueCol = new GridColumn();
            pValueCol.Name = "pvalue";
            pValueCol.Caption = "Value";

            dgvHydraulic.Columns.Add(pNameCol);
            dgvHydraulic.Columns.Add(pUnitsCol);
            dgvHydraulic.Columns.Add(pAvgValCol);
            dgvHydraulic.Columns.Add(pValueCol);
        }

        void InitializeMechanicalGrid()
        {
            pNameCol = new GridColumn();
            pNameCol.Name = "paramname";
            pNameCol.Caption = "Parameter Name";

            pUnitsCol = new GridColumn();
            pUnitsCol.Name = "paramunits";
            pUnitsCol.Caption = "Units";

            pAvgValCol = new GridColumn();
            pAvgValCol.Name = "pavgvalue";
            pAvgValCol.Caption = "Average Value";

            pValueCol = new GridColumn();
            pValueCol.Name = "pvalue";
            pValueCol.Caption = "Value";

            dgvMechanical.Columns.Add(pNameCol);
            dgvMechanical.Columns.Add(pUnitsCol);
            dgvMechanical.Columns.Add(pAvgValCol);
            dgvMechanical.Columns.Add(pValueCol);
        }

        void InitializeElectricalGrid()
        {
            pNameCol = new GridColumn();
            pNameCol.Name = "paramname";
            pNameCol.Caption = "Parameter Name";

            pUnitsCol = new GridColumn();
            pUnitsCol.Name = "paramunits";
            pUnitsCol.Caption = "Units";

            pAvgValCol = new GridColumn();
            pAvgValCol.Name = "pavgvalue";
            pAvgValCol.Caption = "Average Value";

            pValueCol = new GridColumn();
            pValueCol.Name = "pvalue";
            pValueCol.Caption = "Value";

            dgvElectrical.Columns.Add(pNameCol);
            dgvElectrical.Columns.Add(pUnitsCol);
            dgvElectrical.Columns.Add(pAvgValCol);
            dgvElectrical.Columns.Add(pValueCol);
        }

        void InitializeVibrationGrid()
        {
            pNameCol = new GridColumn();
            pNameCol.Name = "paramname";
            pNameCol.Caption = "Parameter Name";

            pUnitsCol = new GridColumn();
            pUnitsCol.Name = "paramunits";
            pUnitsCol.Caption = "Units";

            pAvgValCol = new GridColumn();
            pAvgValCol.Name = "pavgvalue";
            pAvgValCol.Caption = "Average Value";

            pValueCol = new GridColumn();
            pValueCol.Name = "pvalue";
            pValueCol.Caption = "Value";

            dgvVibration.Columns.Add(pNameCol);
            dgvVibration.Columns.Add(pUnitsCol);
            dgvVibration.Columns.Add(pAvgValCol);
            dgvVibration.Columns.Add(pValueCol);
        }

        void RefreshHydraulicGrid()
        {
            dgvHydraulic.GroupPanelText = "Hydraulic Parameters";
            dgvHydraulic.AddNewRow();

        }
        
        List<ChannelData> DoWork(List<ChannelData> input)
        {
            #region backup
            // Do not access UI elements from here....
            // Donot access variables from this object....
            // This method runs asynchronously...
            // Very unsafe to access UI elements or members of this class from this method.
            // Send complete information to this class and let it do it's work.
            //Random rr = new Random(DateTime.Now.Second);
            //List<ChannelData> result = new List<ChannelData>(0);
            //ChannelData data;
            //for (int i = 0; i < input.Count; i++)
            //{
            //    data = input[i];
            //    data.rawvalue = rr.NextDouble();
            //    data.computedValue = data.rawvalue * 100;
            //    result.Add(data);
            //}
            //// Simulate a long running thread.
            //Thread.Sleep(1500);
            //return result;
            #endregion

            dascore.ScanHardware();

            return result;
        }

        void WorkerCompleted(IAsyncResult res)
        {

            // Access all variables in this object here itself.....
            if (isAcquisitionRunning == true && acqpause == false)
            {
                result = workerMethod.EndInvoke(asyncResult);

                // make use of the results here....
                
                // Manipulation.

                // Auto computation.

                // Actual Data saving.

                // Update the UI
                ExtractDasInfo();

                UpdateUI();


                this.result = null;

                // Since the command for pausing the Acquisition is not true....
                // Start acquiring data again..

                asyncResult = workerMethod.BeginInvoke(values,asyncReturn, null);
            }

            else
            {

                workerMethod.EndInvoke(asyncResult);

                isAcquisitionRunning = false;
            }
            
            
        }

        void WorkerStart()
        {
            if (isAcquisitionRunning == false)
            {
                isAcquisitionRunning = true;

                acqpause = false;

                asyncResult = workerMethod.BeginInvoke(values, asyncReturn, null);
            }
            else
            {
                MessageBox.Show("Acquisition is in progress...");
            }
        }

        void WorkerStop()
        {
            workerMethod.EndInvoke(this.asyncResult);
        }

        private void UpdateUI()
        {
            this.Invoke(TextBoxUpdaterMethod, this.result);
        }

        private void ExtractDasInfo()
        {
            this.Invoke(DasExtractMethod);
        }

        private void DasExtract()
        {
            //this.actRes = dascore.GetActualResult();
            //this.mp = dascore.GetActualMeasurementProfile();
        }
        void UpdateTextBox(List<ChannelData> values)

        {
            this.tbConsole.Text = "";
            ChannelData val;
            //for (int i = 0; i < values.Count; i++)
            //{
            //    val = values[i];
            //    this.tbConsole.Text += val.pname + ". rawvalue=" + val.rawvalue.ToString() + Environment.NewLine;
            //    this.tbConsole.Text += val.pname + ". ComputedValue=" + val.computedValue.ToString() + Environment.NewLine;
            //}

            //this.tbConsole.Text += this.actRes.GetDoubleValue("st_efficiency").ToString()+Environment.NewLine;

          //  List<DynamicDataProperty> dpro = this.mp.GetDynamicPropertyList();

          //  for (int i = 0; i < dpro.Count; i++)
         //   {
         //       this.tbConsole.Text += dpro[i].pName+":"+this.mp.GetDoubleValue(dpro[i].pName).ToString() + Environment.NewLine;
         //   }

        }

        private void btnStartAcq_Click(object sender, EventArgs e)
        {
            WorkerStart();
            this.btnStop.Enabled = false;
        }

        private void btnPause_Click(object sender, EventArgs e)
        {
            acqpause = true;
            this.btnStop.Enabled = true;
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            //WorkerStop();
            this.Close();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            this.lblTime.Text = DateTime.Now.Hour + " : " + DateTime.Now.Minute + " : " + DateTime.Now.Second;
        }
        */
        #endregion

    }
}