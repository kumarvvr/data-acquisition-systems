﻿namespace WinFormDasCoreClient
{
    partial class CommonLookupTablesWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvLookupTableList = new System.Windows.Forms.DataGridView();
            this.btnNewLookupTable = new System.Windows.Forms.Button();
            this.btnModifyLookupTable = new System.Windows.Forms.Button();
            this.txtTableName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLookupTableList)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvLookupTableList
            // 
            this.dgvLookupTableList.AllowUserToAddRows = false;
            this.dgvLookupTableList.AllowUserToDeleteRows = false;
            this.dgvLookupTableList.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvLookupTableList.EnableHeadersVisualStyles = false;
            this.dgvLookupTableList.Location = new System.Drawing.Point(17, 41);
            this.dgvLookupTableList.MaximumSize = new System.Drawing.Size(401, 342);
            this.dgvLookupTableList.MinimumSize = new System.Drawing.Size(401, 342);
            this.dgvLookupTableList.MultiSelect = false;
            this.dgvLookupTableList.Name = "dgvLookupTableList";
            this.dgvLookupTableList.ReadOnly = true;
            this.dgvLookupTableList.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvLookupTableList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvLookupTableList.Size = new System.Drawing.Size(401, 342);
            this.dgvLookupTableList.TabIndex = 1;
            this.dgvLookupTableList.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvLookupTableList_CellClick);
            // 
            // btnNewLookupTable
            // 
            this.btnNewLookupTable.Font = new System.Drawing.Font("Calibri", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNewLookupTable.Location = new System.Drawing.Point(249, 424);
            this.btnNewLookupTable.Margin = new System.Windows.Forms.Padding(155, 10, 10, 10);
            this.btnNewLookupTable.Name = "btnNewLookupTable";
            this.btnNewLookupTable.Size = new System.Drawing.Size(169, 38);
            this.btnNewLookupTable.TabIndex = 2;
            this.btnNewLookupTable.Text = "Create New Lookup Table";
            this.btnNewLookupTable.UseVisualStyleBackColor = true;
            this.btnNewLookupTable.Click += new System.EventHandler(this.btnNewLookupTable_Click);
            // 
            // btnModifyLookupTable
            // 
            this.btnModifyLookupTable.AutoSize = true;
            this.btnModifyLookupTable.Font = new System.Drawing.Font("Calibri", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnModifyLookupTable.Location = new System.Drawing.Point(17, 424);
            this.btnModifyLookupTable.Name = "btnModifyLookupTable";
            this.btnModifyLookupTable.Size = new System.Drawing.Size(154, 38);
            this.btnModifyLookupTable.TabIndex = 2;
            this.btnModifyLookupTable.Text = "Modify Lookup Table";
            this.btnModifyLookupTable.UseVisualStyleBackColor = true;
            this.btnModifyLookupTable.Click += new System.EventHandler(this.btnModifyLookupTable_Click);
            // 
            // txtTableName
            // 
            this.txtTableName.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTableName.Location = new System.Drawing.Point(17, 388);
            this.txtTableName.Margin = new System.Windows.Forms.Padding(4);
            this.txtTableName.Name = "txtTableName";
            this.txtTableName.Size = new System.Drawing.Size(400, 27);
            this.txtTableName.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Wheat;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(347, 29);
            this.label1.TabIndex = 5;
            this.label1.Text = "Available Common Lookup Tables";
            // 
            // CommonLookupTablesWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DimGray;
            this.ClientSize = new System.Drawing.Size(430, 474);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnNewLookupTable);
            this.Controls.Add(this.txtTableName);
            this.Controls.Add(this.btnModifyLookupTable);
            this.Controls.Add(this.dgvLookupTableList);
            this.Name = "CommonLookupTablesWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Common Lookup Tables";
            this.Load += new System.EventHandler(this.CommonLookupTablesWindow_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvLookupTableList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvLookupTableList;
        private System.Windows.Forms.Button btnNewLookupTable;
        private System.Windows.Forms.Button btnModifyLookupTable;
        private System.Windows.Forms.TextBox txtTableName;
        private System.Windows.Forms.Label label1;
    }
}