﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace WinFormDasCoreClient
{
    public class DataGridStyles
    {
        DataGridViewCellStyle colHeaderCellStyle, disabledTextCellStyle;

        public DataGridStyles()
        {
            colHeaderCellStyle = new DataGridViewCellStyle();
            colHeaderCellStyle.BackColor = SystemColors.ButtonHighlight;
            colHeaderCellStyle.ForeColor = Color.Wheat;
            colHeaderCellStyle.WrapMode = DataGridViewTriState.True;

            disabledTextCellStyle = new DataGridViewCellStyle();
            disabledTextCellStyle.BackColor = SystemColors.Control;
        }

        public DataGridViewCellStyle GetColumnHeaderStyle()
        {
            return this.colHeaderCellStyle;
        }

        public DataGridViewCellStyle GetDisabledTextStyle()
        {
            return this.disabledTextCellStyle;
        }
        
    }
}
