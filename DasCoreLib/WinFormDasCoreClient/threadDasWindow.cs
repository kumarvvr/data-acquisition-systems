﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DasCoreLib;
using DasCoreUtilities;
using System.Threading;
using BHEL.PUMPSDAS.Datatypes;
using System.Diagnostics;

namespace WinFormDasCoreClient
{
    public partial class threadDasWindow : Form
    {
        DasCore dc;
        HardwareManager hwmgr;
        BackgroundWorker hardwareScanner;
        System.Windows.Forms.Timer hardwareMonitorTimer;

        int timeElapsed=0;

        int maxTime = 3000;

        bool continueScan = false;

        public threadDasWindow(DasCore dc)
        {
            // Das core is already initialized with current test details...

            InitializeComponent();

            this.dc = dc;

            console.Text = "";

            InitializeHardwareScanner();

            hardwareMonitorTimer = new System.Windows.Forms.Timer();
            hardwareMonitorTimer.Interval = 100;
            hardwareMonitorTimer.Tick += new EventHandler(hardwareMonitorTimer_Tick);
        }

        void hardwareScanner_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            // Worker is completed....
            if (continueScan)
            {
                DoScan();
            }
            
            //throw new NotImplementedException();
        }

        private void DoScan()
        {
            List<AlarmStatus> alarms;
            List<ChannelData> data;
            data = hwmgr.ScanDataFromHardware(out alarms, HardwareScanMode.BYPASS_TIME_MEASUREMENTS);
            int c = data.Count;
        }

        void hardwareMonitorTimer_Tick(object sender, EventArgs e)
        {
            if (timeElapsed > maxTime)
            {
                this.console.Text+="Time elapsed is more than given time..."+Environment.NewLine;
                timeElapsed = 0;

                // Stop the thread here....
                if (hardwareScanner.IsBusy)
                {
                    this.console.Text += "Scan Abort command given..." + Environment.NewLine;
                    hardwareScanner.CancelAsync();
                    this.console.Text += "Scan aborted..." + Environment.NewLine;
                    InitializeHardwareScanner();

                    hardwareMonitorTimer.Stop();
                    timeElapsed = 0;
                }
                // Bring window to normal state.....
            }
            else
            {
                this.console.Text += "Time elapsed is "+timeElapsed.ToString() + Environment.NewLine;
                timeElapsed += 100;
            }


            //throw new NotImplementedException();
        }

        private void InitializeHardwareScanner()
        {
            hardwareScanner = new BackgroundWorker();
            hardwareScanner.WorkerReportsProgress = true;
            hardwareScanner.WorkerSupportsCancellation = true;

            hardwareScanner.DoWork += new DoWorkEventHandler(hardwareScanner_DoWork);
            hardwareScanner.RunWorkerCompleted += new RunWorkerCompletedEventHandler(hardwareScanner_RunWorkerCompleted);
        }

        void hardwareScanner_DoWork(object sender, DoWorkEventArgs e)
        {
            DoScan();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!continueScan)
            {
                dc.InitializeAndConfigureHardware();

                this.hwmgr = dc.GetHardwareManager();

                continueScan = true;

                hardwareMonitorTimer.Start();

                hardwareScanner.RunWorkerAsync();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.continueScan = false;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.continueScan = true;
        }
    }
}
