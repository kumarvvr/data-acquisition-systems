﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DasCoreLib;
using DasCoreUtilities;
using BHEL.PUMPSDAS.Datatypes;

namespace WinFormDasCoreClient
{
    public partial class CommonLookupManagerWindow : Form
    {
        DasCore dc;
        string newtbldesc, newtblkeyname, newtblvalname;
        List<LookupTableDetails> lookupTables;
        List<LookupDataRow> lookupDataRows = new List<LookupDataRow>();
        NewLookupTableInfoWindow newltwin;
        public LookupTableDetails SelectedLookup;
        InstalledMachine machine;
        ProjectDetails project;
        LookupTableDetails currentEditedTable;

        public CommonLookupManagerWindow(DasCore dc)
        {
            InitializeComponent();            
            this.dc = dc;
            lookupTables = new List<LookupTableDetails>();
            // Data is obtained at this point.
            RefreshLookupTableList();           
            gbEditLookupData.Enabled = false;
        }
        private void RefreshLookupTableList()
        {
            try
            { 
                lookupTables = dc.GetAvailableCommonLookupTableDetails();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                this.Close();
            }
            
            DataGridViewTextBoxColumn lkid, lkdesc, lkkey, lkval, lktname;
            DataGridViewCheckBoxColumn lkiscommon;

            dgvAvailableLookups.Rows.Clear();
            dgvAvailableLookups.Columns.Clear();

            lkid = new DataGridViewTextBoxColumn();
            lkid.Name = "lookupid";
            lkid.HeaderText = "Lookup ID";
            lkid.Width = 60;
            lkid.ValueType = typeof(int);
            lkid.ReadOnly = true;
            lkid.FillWeight = 10;

            lkdesc = new DataGridViewTextBoxColumn();
            lkdesc.Name = "lookupdesc";
            lkdesc.HeaderText = "Description";
            lkdesc.Width = 120;
            lkdesc.ValueType = typeof(int);
            lkdesc.ReadOnly = true;
            lkdesc.FillWeight = 40;

            lkkey = new DataGridViewTextBoxColumn();
            lkkey.Name = "keytext";
            lkkey.HeaderText = "Key label";
            lkkey.Width = 80;
            lkkey.ValueType = typeof(int);
            lkkey.ReadOnly = true;
            lkkey.FillWeight = 20;

            lkval = new DataGridViewTextBoxColumn();
            lkval.Name = "valuetext";
            lkval.HeaderText = "Value Label";
            lkval.Width = 80;
            lkval.ValueType = typeof(int);
            lkval.ReadOnly = true;
            lkval.FillWeight = 20;

            lktname = new DataGridViewTextBoxColumn();
            lktname.Name = "tablename";
            lktname.HeaderText = "Table Name";
            lktname.Width = 120;
            lktname.ValueType = typeof(int);
            lktname.ReadOnly = true;
            lktname.FillWeight = 10;

            lkiscommon = new DataGridViewCheckBoxColumn();
            lkiscommon.Name = "iscommon";
            lkiscommon.HeaderText = "Is Common";
            lkiscommon.Width = 70;
            lkiscommon.ValueType = typeof(int);
            lkiscommon.ReadOnly = true;
            lkiscommon.FillWeight = 10;

            this.dgvAvailableLookups.Columns.Add(lkid);
            this.dgvAvailableLookups.Columns.Add(lkdesc);
            this.dgvAvailableLookups.Columns.Add(lkkey);
            this.dgvAvailableLookups.Columns.Add(lkval);
            this.dgvAvailableLookups.Columns.Add(lktname);
            this.dgvAvailableLookups.Columns.Add(lkiscommon);

            this.dgvAvailableLookups.RowHeadersVisible = false;
            this.dgvEditLookup.RowHeadersVisible = false;

            for (int i = 0; i < lookupTables.Count; i++)
            {
                this.dgvAvailableLookups.Rows.Add(new object[] { 
                    lookupTables[i].lookupid.ToString(), 
                    lookupTables[i].lookupdesc.ToString(), 
                    lookupTables[i].keytext.ToString(), 
                    lookupTables[i].valuetext.ToString(), 
                    lookupTables[i].tablename.ToString(), 
                    lookupTables[i].iscommon.ToString() });
            }

            this.dgvAvailableLookups.Invalidate();
        }
        private void btnSelect_Click(object sender, EventArgs e)
        {
            SelectedLookup = new LookupTableDetails();

            SelectedLookup = this.GetSelectedLookupTable(this.dgvAvailableLookups.SelectedRows[0]);

            this.Close();
        }
        private void btnAddNew_Click(object sender, EventArgs e)
        {
            newltwin = new NewLookupTableInfoWindow();
            newltwin.FormClosing += new FormClosingEventHandler(newltwin_FormClosing);
            newltwin.ShowDialog();
                       
            try
            {
                dc.CreateNewCommonLookupTable(newtbldesc, newtblkeyname, newtblvalname);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error creating table! ERROR : " + ex.Message);
            }            

            newtblkeyname = "";
            newtblvalname = "";
            newtbldesc = "";

            RefreshLookupTableList();
        }
        void newltwin_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (newltwin.DialogResult == System.Windows.Forms.DialogResult.OK)
            {
                newtblkeyname = newltwin._keyText;
                newtblvalname = newltwin._valueText;
                newtbldesc = newltwin._tabledesc;
            }
        }
        private void btnModify_Click(object sender, EventArgs e)
        {
            LookupTableDetails lookupDetails = new LookupTableDetails();

            lookupDetails = this.GetSelectedLookupTable(this.dgvAvailableLookups.SelectedRows[0]);
            RefreshEditLookupGrid(lookupDetails);
            RefreshEditLookupData(lookupDetails);
            RefreshTableDetailsData(lookupDetails);
            currentEditedTable = lookupDetails;
            this.dgvEditLookup.Invalidate();
        }
        private void RefreshEditLookupGrid(LookupTableDetails lookupDetails)
        {
            DataGridViewColumn keyCol = new DataGridViewTextBoxColumn();
            keyCol.Name = lookupDetails.keytext;
            keyCol.Width = 50;
            keyCol.ValueType = typeof(string);

            DataGridViewColumn valueCol = new DataGridViewTextBoxColumn();
            valueCol.Name = lookupDetails.valuetext;
            valueCol.Width = 100;
            valueCol.ValueType = typeof(string);

            this.lookupDataRows.Clear();
            this.dgvEditLookup.Rows.Clear();
            this.dgvEditLookup.Columns.Clear();

            this.dgvEditLookup.Columns.Add(keyCol);
            this.dgvEditLookup.Columns.Add(valueCol);

            this.gbEditLookupData.Enabled = true;
        }
        private void RefreshEditLookupData(LookupTableDetails lookupDetails)
        {
            try
            {
                lookupDataRows = dc.GetLookupData(lookupDetails);

                for (int i = 0; i < lookupDataRows.Count; i++)
                {
                    this.dgvEditLookup.Rows.Add(new object[] { lookupDataRows[i].key.ToString(), lookupDataRows[i].value.ToString() });
                }
                this.gbAvlLookupTables.Enabled = false;
            }
            catch (Exception ex)
            {
                this.gbEditLookupData.Enabled = false;
                MessageBox.Show("Error retrieving lookup data! ERROR : " + ex.Message);
            }      
        }
        private LookupTableDetails GetSelectedLookupTable(DataGridViewRow row)
        {
            LookupTableDetails lookupDetails = new LookupTableDetails();
            int id = DefaultConversion.GetIntValue(row.Cells["lookupid"].Value.ToString());
            int index = -1;
            for (int i = 0; i < lookupTables.Count; i++)
            {
                if (lookupTables[i].lookupid == id)
                {
                    index = i;
                    break;
                }
            }

            lookupDetails = lookupTables[index];

            return lookupDetails;
        }
        private void RefreshTableDetailsData(LookupTableDetails lookupDetails)
        {
            tbLookupTblDesc.Text = lookupDetails.lookupdesc;
            tbLookupTableKeylabel.Text = lookupDetails.keytext;
            tbLookupTableValueLabel.Text = lookupDetails.valuetext;
        }
        private void btnSave_Click(object sender, EventArgs e)
        {

            DataGridViewRowCollection rowcoll = this.dgvEditLookup.Rows;
            this.lookupDataRows.Clear();
            LookupDataRow row;
            if (rowcoll.Count > 1)
            {
                try
                {
                    for (int i = 0; i < rowcoll.Count - 1; i++)
                    {
                        row = new LookupDataRow();
                        row.key = double.Parse((string)rowcoll[i].Cells[0].Value.ToString());
                        row.value = double.Parse((string)rowcoll[i].Cells[1].Value.ToString());
                        this.lookupDataRows.Add(row);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Failed to save data into lookup table! ERROR : " + ex.Message);
                }
            }

            this.currentEditedTable.lookupdesc = tbLookupTblDesc.Text;
            this.currentEditedTable.keytext = tbLookupTableKeylabel.Text;
            this.currentEditedTable.valuetext = tbLookupTableValueLabel.Text;

            // DASCORE API
            dc.UpdateLookupData(this.currentEditedTable, lookupDataRows);

            RefreshLookupTableList();
            dgvEditLookup.Rows.Clear();
            tbLookupTableValueLabel.Clear();
            tbLookupTableKeylabel.Clear();
            tbLookupTblDesc.Clear();
            gbAvlLookupTables.Enabled = true;
            gbEditLookupData.Enabled = false;
        }
        private void btnDiscard_Click(object sender, EventArgs e)
        {
            RefreshLookupTableList();
            dgvEditLookup.Rows.Clear();
            tbLookupTableValueLabel.Clear();
            tbLookupTableKeylabel.Clear();
            tbLookupTblDesc.Clear();
            gbAvlLookupTables.Enabled = true;
            gbEditLookupData.Enabled = false;
        }      
        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure? Table will be deleted permanently!", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == System.Windows.Forms.DialogResult.Yes)
            {
                try
                {
                    dc.DeleteLookupTable(this.GetSelectedLookupTable(this.dgvAvailableLookups.SelectedRows[0]));
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error deleting table! ERROR : " + ex.Message);
                }
            }
        }
        private void dgvAvailableLookups_SelectionChanged(object sender, EventArgs e)
        {
            gbEditLookupData.Enabled = false;
        }

    }
}
