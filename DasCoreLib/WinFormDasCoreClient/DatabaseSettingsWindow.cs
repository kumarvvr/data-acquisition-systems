﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DasCoreLib;
using BHEL.PUMPSDAS.Datatypes;

namespace WinFormDasCoreClient
{
    public partial class DatabaseSettingsWindow : Form
    {
        DasCore dc;
        DatabaseDetails dbase;

        public DatabaseSettingsWindow(DasCore dc)
        {
            InitializeComponent();
            this.dc = dc;

            try
            {
                dbase = dc.GetDatabaseSettings();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error getting settings! " + ex.Message);
            }

            tbDriver.Text = dbase.driver;
            tbServer.Text = dbase.server;
            tbPort.Text = dbase.port;
            tbDatabaseName.Text = dbase.databaseName;
            tbUserName.Text = dbase.userName;
            tbPassword.Text = dbase.password;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            dbase.driver = tbDriver.Text;
            dbase.server = tbServer.Text;
            dbase.port = tbPort.Text;
            dbase.databaseName = tbDatabaseName.Text;
            dbase.userName = tbUserName.Text;
            dbase.password = tbPassword.Text;

            try
            {
                dc.UpdateDatabaseSettings(dbase);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            this.Close();

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
