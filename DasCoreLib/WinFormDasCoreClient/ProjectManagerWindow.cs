﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DasCoreLib;
using BHEL.PUMPSDAS.Datatypes;

namespace WinFormDasCoreClient
{
    public partial class ProjectManagerWindow : Form
    {
        DasCore dc;
        InstalledMachine mc;
        DataGridViewColumn pCodeCol,pNameCol,pDescCol;
        DataGridViewCellStyle disabledColStyle;

        UpdateProjWindow newProjWin;
        bool newProjWinCancelStatus;
        ModifyProjWindow modifyProjWin;
        ProjectLookupManagerWindow pjWin;
        bool modifyProjWinCancelStatus;
        
        List<ProjectDetails> projectList;
        ProjectDetails newProj,modProj;

        UpdateProjWindow updtPrjWin;
        ModifyProjWindow mdprjWin;


        public ProjectManagerWindow(DasCore dc,InstalledMachine mc)
        {
            InitializeComponent();
            this.dc = dc;
            this.mc = mc;
            this.lblTitle.Text = string.Format(lblTitle.Text, mc.dllversiondesc);

            disabledColStyle = new DataGridViewCellStyle();
            disabledColStyle.BackColor = SystemColors.Control;

            UpdateGrid();
            RefreshGridData();
        }

        private void UpdateGrid()
        {
            pCodeCol = new DataGridViewTextBoxColumn();
            pCodeCol.Name = "projectcode";
            pCodeCol.HeaderText = "Project Code";
            pCodeCol.Width = 100;
            pCodeCol.ReadOnly = true;
            pCodeCol.SortMode = DataGridViewColumnSortMode.NotSortable;
            pCodeCol.Visible = false;
            //pCodeCol.DefaultCellStyle = disabledColStyle;

            pNameCol = new DataGridViewTextBoxColumn();
            pNameCol.Name = "projectname";
            pNameCol.HeaderText = "Project Name";
            pNameCol.Width = 200;
            pNameCol.ReadOnly = true;
            pNameCol.SortMode = DataGridViewColumnSortMode.NotSortable;
            //pNameCol.DefaultCellStyle = disabledColStyle;

            pDescCol = new DataGridViewTextBoxColumn();
            pDescCol.Name = "projectdescription";
            pDescCol.HeaderText = "Project Description";
            pDescCol.Width = 420;
            pDescCol.ReadOnly = true;
            pDescCol.SortMode = DataGridViewColumnSortMode.NotSortable;
            //pDescCol.DefaultCellStyle = disabledColStyle;

            dgvProjectDetails.Columns.Add(pCodeCol);
            dgvProjectDetails.Columns.Add(pNameCol);
            dgvProjectDetails.Columns.Add(pDescCol);

        }

        private void RefreshGridData()
        {
            dgvProjectDetails.Rows.Clear();
            try
            {
                projectList = dc.GetProjectListByInstalledMachine(this.mc);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                this.Close();
            }

            for (int i = 0; i < projectList.Count; i++)
            {
                dgvProjectDetails.Rows.Add(new object[] { projectList[i].projectcode, projectList[i].projectname, projectList[i].projectdescription});
            }
            
        }

        private void btnAddProj_Click(object sender, EventArgs e)
        {
            mdprjWin = new ModifyProjWindow(this.mc);

            mdprjWin.FormClosing += new FormClosingEventHandler(mdprjWin_FormClosing);

            mdprjWin.ShowDialog();
        }

        void mdprjWin_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (mdprjWin.DialogResult == System.Windows.Forms.DialogResult.OK)
            {
                newProj = mdprjWin.GetNewProjectDetails();

                // Create the project in the system..

                newProj = dc.CreateNewProject(mc, newProj);

                updtPrjWin = new UpdateProjWindow(dc, mc, newProj);

                updtPrjWin.ShowDialog();

                RefreshGridData();
            }//throw new NotImplementedException();
        }

        /*
        void newProjWin_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (newProjWin.DialogResult == System.Windows.Forms.DialogResult.OK)
            {
                newProj = newProjWin.GetNewProjectDetails();
                newProjWinCancelStatus = false;
            }
            else newProjWinCancelStatus = true;
        }
        */

        private void btnModifyProj_Click(object sender, EventArgs e)
        {
            if (dgvProjectDetails.SelectedRows.Count != 1)
            {
                MessageBox.Show("Select a project for modification!");
                return;
            }
            
            modProj = new ProjectDetails();
            modProj.fk_machinecode = this.mc.machinecode;
            modProj.projectcode = int.Parse(dgvProjectDetails.SelectedRows[0].Cells["projectcode"].Value.ToString());
            modProj.projectname = dgvProjectDetails.SelectedRows[0].Cells["projectname"].Value.ToString();
            modProj.projectdescription = dgvProjectDetails.SelectedRows[0].Cells["projectdescription"].Value.ToString();

            /*
            modifyProjWin = new ModifyProjWindow(mc, modProj);

            modifyProjWin.FormClosing += new FormClosingEventHandler(modifyProjWin_FormClosing);
            modifyProjWin.ShowDialog();
            if (modifyProjWinCancelStatus == false)
            {
                try
                {
                    dc.UpdateExistingProject(mc, modProj);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                RefreshGridData();
            }
            */

            updtPrjWin = new UpdateProjWindow(dc, mc, modProj);

            updtPrjWin.ShowDialog();

            RefreshGridData();

            modProj = new ProjectDetails();

        }

        void modifyProjWin_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (modifyProjWin.DialogResult == System.Windows.Forms.DialogResult.OK)
            {
                modProj = modifyProjWin.GetNewProjectDetails();
                modifyProjWinCancelStatus = false;
            }
            else modifyProjWinCancelStatus = true;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnModSpecdpars_Click(object sender, EventArgs e)
        {
            /*if (dgvProjectDetails.SelectedRows.Count != 1)
            {
                MessageBox.Show("Select a project for modification!");
                return;
            }

            modProj = new ProjectDetails();
            modProj.fk_machinecode = this.mc.machinecode;
            modProj.projectcode = int.Parse(dgvProjectDetails.SelectedRows[0].Cells["projectcode"].Value.ToString());
            modProj.projectname = dgvProjectDetails.SelectedRows[0].Cells["projectname"].Value.ToString();
            modProj.projectdescription = dgvProjectDetails.SelectedRows[0].Cells["projectdescription"].Value.ToString();

            ProjectSpecifiedParamsWindow spWin = new ProjectSpecifiedParamsWindow(this.dc, this.mc, modProj);
            spWin.ShowDialog();

            modProj = new ProjectDetails();*/
        }

    }
}
