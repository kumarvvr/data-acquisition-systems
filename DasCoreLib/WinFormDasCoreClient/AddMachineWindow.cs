﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DasCoreLib;
using System.IO;
using BHEL.PUMPSDAS.Datatypes;

namespace WinFormDasCoreClient
{
    public partial class AddMachineWindow : Form
    {
        DasCore dc;

        List<InstalledMachine> machines;

        InstalledMachine newMachine;

        public AddMachineWindow(DasCore dc)
        {
            InitializeComponent();

            this.dc = dc;

            machines = dc.GetListofInstalledMachines();
            for (int i = 0; i < machines.Count; i++)
            {
                cbMachineUniqueCode.Items.Add(machines[i].machineuniquecode);
            }
            if(cbMachineUniqueCode.Items.Count>0)
                cbMachineUniqueCode.SelectedIndex = 0;
            DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        private void UpdateFields()
        {
            this.tbDesc.Text = machines[cbMachineUniqueCode.SelectedIndex].dllversiondesc;
            this.tbMajorVer.Text = machines[cbMachineUniqueCode.SelectedIndex].dllmajorversion;
            this.tbMinorVer.Text = machines[cbMachineUniqueCode.SelectedIndex].dllminorversion;
            this.tbScriptpath.Text = machines[cbMachineUniqueCode.SelectedIndex].codefilepath;

        }

        private void ClearFields()
        {
            this.tbDesc.Text = "";
            this.tbMajorVer.Text = "";
            this.tbMinorVer.Text = "";
            this.tbScriptpath.Text = "";

        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();

            ofd.AddExtension = true;
            ofd.DefaultExt = ".cs";
            ofd.Filter = "C# Code File(*.cs)|*.cs";
            ofd.Multiselect = false;
            ofd.ShowDialog(this);
            tbScriptpath.Text = ofd.FileName;

        }

        private void btnInstall_Click(object sender, EventArgs e)
        {
            if (this.cbMachineUniqueCode.Text.ToString() == "" ||
                tbMajorVer.Text == "" ||
                tbMinorVer.Text == "" ||
                tbScriptpath.Text == "")
            {
                MessageBox.Show("Please enter valid data in all the fields!");
                return;
            }

            FileInfo fi = new FileInfo(tbScriptpath.Text);
            if (!fi.Exists)
            {
                MessageBox.Show("Script file not found!");
                return;
            }

            try
            {
                newMachine = dc.InstallNewMachine(tbScriptpath.Text, DasCoreSettings.GetCompiledScriptsRootPath(), cbMachineUniqueCode.Text.ToString(), tbMajorVer.Text, tbMinorVer.Text, tbDesc.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                DialogResult = DialogResult.Abort;
                return;
            }
            
            MessageBox.Show("Successfully installed new machine!");
            DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cbMachineUniqueCode_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateFields();
        }

        private void cbMachineUniqueCode_TextChanged(object sender, EventArgs e)
        {
            ClearFields();
        }

        public InstalledMachine GetNewInstalledMachine()
        {
            return newMachine;
        }
    }
}
