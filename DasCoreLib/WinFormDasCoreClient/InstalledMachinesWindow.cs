﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DasCoreLib;
using BHEL.PUMPSDAS.Datatypes;

namespace WinFormDasCoreClient
{
    public partial class InstalledMachinesWindow : Form
    {
        DasCore dc;
        List<InstalledMachine> installedmachines;
        InstalledMachine selectedMachine;
        
        public InstalledMachinesWindow(DasCore dc)
        {
            this.dc = dc;
            InitializeComponent();
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        private void InstalledMachinesWindow_Load(object sender, EventArgs e)
        {
            //DASCORE API
            installedmachines = dc.GetListofInstalledMachines();

            DataGridViewCellStyle disabledColumnStyle = new DataGridViewCellStyle();
            disabledColumnStyle.BackColor = SystemColors.Control;

            // SETTING FOR LOOKUP TABLE LIST DATA GRID VIEW.
            this.dgvInstalledMachineList.AutoGenerateColumns = false;
            this.dgvInstalledMachineList.AutoSize = true;

            DataGridViewColumn mcUniqueCodeCol = new DataGridViewTextBoxColumn();
            mcUniqueCodeCol.Name = "Frame Name";
            mcUniqueCodeCol.Width = 188;
            mcUniqueCodeCol.ValueType = typeof(string);
            mcUniqueCodeCol.ReadOnly = true;
            mcUniqueCodeCol.DefaultCellStyle = disabledColumnStyle;

            DataGridViewColumn dlldescCol = new DataGridViewTextBoxColumn();
            dlldescCol.Name = "Description";
            dlldescCol.Width = 360;
            dlldescCol.ValueType = typeof(string);
            dlldescCol.ReadOnly = true;
            dlldescCol.DefaultCellStyle = disabledColumnStyle;

            DataGridViewColumn mMachineCodeCol = new DataGridViewTextBoxColumn();
            mMachineCodeCol.Name = "Machine Code";
            mMachineCodeCol.Width = 0;
            mMachineCodeCol.ValueType = typeof(string);
            mMachineCodeCol.ReadOnly = true;
            mMachineCodeCol.DefaultCellStyle = disabledColumnStyle;

            this.dgvInstalledMachineList.Columns.Add(mcUniqueCodeCol);
            this.dgvInstalledMachineList.Columns.Add(dlldescCol);
            this.dgvInstalledMachineList.Columns.Add(mMachineCodeCol);
            

            for (int i = 0; i < installedmachines.Count; i++)
            {
                this.dgvInstalledMachineList.Rows.Add(new object[] { installedmachines[i].machineuniquecode.ToString(),installedmachines[i].dllversiondesc.ToString(),installedmachines[i].machinecode.ToString() });
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if(this.dgvInstalledMachineList.SelectedRows.Count == 1)
            {
                DataGridViewRow row = this.dgvInstalledMachineList.SelectedRows[0];

                int selectedcode = int.Parse(row.Cells[2].Value.ToString());

                int idx = -1;
                for(int i = 0;i<installedmachines.Count;i++)
                {
                    if(installedmachines[i].machinecode == selectedcode)
                    {
                        idx = i;
                        break;
                    }
                }

                if(idx != -1)
                {
                    selectedMachine = installedmachines[idx];                    
                }

                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                this.Close();
            }
            
            else MessageBox.Show("Please Select a Machine!");
        }

        public InstalledMachine GetSelectedMachine()
        {
            return this.selectedMachine;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }



    }
}
