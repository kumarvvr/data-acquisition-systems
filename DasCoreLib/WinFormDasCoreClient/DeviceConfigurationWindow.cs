﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DasCoreLib;
using BHEL.PUMPSDAS.Datatypes;

namespace WinFormDasCoreClient
{
    public partial class DeviceConfigurationWindow : Form
    {        
        DeviceDetails device;
        DasCore dc;
        int deviceid;
        bool IsNewDevice=true;

        DataGridViewColumn mtypeCol, rststrCol, confstrCol, measstrCol, addonCol;
        DataGridViewCellStyle disabledColStyle = new DataGridViewCellStyle();
        
        List<DeviceDetails> devices;
        List<DeviceConfigDetails> deviceConfigs, newConfigs;

        List<string> meastypes,supportedHardwares;
                
        public DeviceConfigurationWindow(DasCore dc, DeviceDetails device)
        {
            InitializeComponent();
            IsNewDevice = false;
            this.dc = dc;
            this.device = device;
            supportedHardwares = HardwareManager.GetSupportedHardwareTypes();

            UpdateWindow();
        }
        public DeviceConfigurationWindow(DasCore dc)
        {
            InitializeComponent();
            IsNewDevice = true;
            device = new DeviceDetails();
            this.dc = dc;
            supportedHardwares = HardwareManager.GetSupportedHardwareTypes();
            device.connstring = "*";
            device.devicedesc = HardwareManager.GetDefaultHardwareType();
            device.devicename = "*";

            device = dc.CreateNewDevice(device);

            UpdateWindow();
        }

        void UpdateWindow()
        {
            
           
            DialogResult = System.Windows.Forms.DialogResult.Cancel;
            meastypes = dc.GetSupportedMeasurementTypes();

            // TODO Provide access to hardwares from dc;
            
            DialogResult = System.Windows.Forms.DialogResult.Cancel;
            meastypes = dc.GetSupportedMeasurementTypes();
            UpdateGrid();
            RefreshData();
        }

        private void UpdateGrid()
        {
            disabledColStyle.BackColor = SystemColors.Control;

            mtypeCol = new DataGridViewTextBoxColumn();
            mtypeCol.Name = "meastype";
            mtypeCol.HeaderText = "Measurement Type";
            mtypeCol.Width = 100;
            mtypeCol.ReadOnly = true;
            mtypeCol.DefaultCellStyle = disabledColStyle;

            rststrCol = new DataGridViewTextBoxColumn();
            rststrCol.Name = "resetstring";
            rststrCol.HeaderText = "Reset String";
            rststrCol.Width = 100;
            rststrCol.ReadOnly = false;

            confstrCol = new DataGridViewTextBoxColumn();
            confstrCol.Name = "configstring";
            confstrCol.HeaderText = "Configuration String";
            confstrCol.Width = 100;
            confstrCol.ReadOnly = false;

            measstrCol = new DataGridViewTextBoxColumn();
            measstrCol.Name = "measstring";
            measstrCol.HeaderText = "Measurement String";
            measstrCol.Width = 100;
            measstrCol.ReadOnly = false;

            addonCol = new DataGridViewTextBoxColumn();
            addonCol.Name = "addoncmd";
            addonCol.HeaderText = "Addon Command";
            addonCol.Width = 100;
            addonCol.ReadOnly = false;

            dgvDevConfig.Columns.Add(mtypeCol);
            dgvDevConfig.Columns.Add(rststrCol);
            dgvDevConfig.Columns.Add(confstrCol);
            dgvDevConfig.Columns.Add(measstrCol);
            dgvDevConfig.Columns.Add(addonCol);

            for (int i = 0; i < meastypes.Count; i++)
            {
                dgvDevConfig.Rows.Add(new object[] { meastypes[i], "", "", "", "", "" });
            }

        }
        private void RefreshData()
        {
            try
            {
                tbName.Text = device.devicename;
                //tbDesc.Text = device.devicedesc;
                this.lbDesc.Items.Clear();

                for (int i = 0; i < supportedHardwares.Count;i++ )
                    lbDesc.Items.Add(supportedHardwares[i]);

                lbDesc.Text = device.devicedesc;
                    // Fill up the list box..
                tbConnStr.Text = device.connstring;

                deviceConfigs = dc.GetDeviceConfigDetails(device);

                for (int i = 0; i < meastypes.Count; i++)
                {
                    for (int j = 0; j < deviceConfigs.Count; j++)
                    {
                        if (deviceConfigs[j].meastype == meastypes[i])
                        {
                            dgvDevConfig.Rows[i].Cells["resetstring"].Value = deviceConfigs[j].resetstring;
                            dgvDevConfig.Rows[i].Cells["configstring"].Value = deviceConfigs[j].configstring;
                            dgvDevConfig.Rows[i].Cells["measstring"].Value = deviceConfigs[j].measstring;
                            dgvDevConfig.Rows[i].Cells["addoncmd"].Value = deviceConfigs[j].addoncmd;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                DialogResult = System.Windows.Forms.DialogResult.Abort;
                this.Close();
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            SaveData();
        }
        private void SaveData()
        {
            if (tbName.Text == "" || tbName.Text == null)
            {
                MessageBox.Show("Enter valid device name!");
            }
            else if (lbDesc.SelectedIndex == -1)
            {
                MessageBox.Show("Please select a Hardware Type");
            }
            else
            {
                device.devicename = tbName.Text;
                device.devicedesc = lbDesc.SelectedItem.ToString();
                device.connstring = tbConnStr.Text;

                newConfigs = new List<DeviceConfigDetails>();
                DeviceConfigDetails config;

                try
                {
                    dc.UpdateExistingDeviceDetails(device);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    DialogResult = System.Windows.Forms.DialogResult.Abort;
                    this.Close();
                }

                for (int i = 0; i < dgvDevConfig.Rows.Count; i++)
                {
                    config = new DeviceConfigDetails();
                    config.fk_deviceid = deviceid;
                    config.meastype = GetDefaultStringValueIfBlank(dgvDevConfig.Rows[i].Cells["meastype"]);
                    config.resetstring = GetDefaultStringValueIfBlank(dgvDevConfig.Rows[i].Cells["resetstring"]);
                    config.configstring = GetDefaultStringValueIfBlank(dgvDevConfig.Rows[i].Cells["configstring"]);
                    config.measstring = GetDefaultStringValueIfBlank(dgvDevConfig.Rows[i].Cells["measstring"]);
                    config.addoncmd = GetDefaultStringValueIfBlank(dgvDevConfig.Rows[i].Cells["addoncmd"]);

                    newConfigs.Add(config);
                }

                try
                {
                    dc.UpdateExistingDeviceConfig(newConfigs, device);
                    MessageBox.Show("Machine Updated Successfully!");
                    DialogResult = System.Windows.Forms.DialogResult.OK;
                    this.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    DialogResult = System.Windows.Forms.DialogResult.Abort;
                    this.Close();
                }
            }
        }
        private string GetDefaultStringValueIfBlank(DataGridViewCell rawdata)
        {
            if (rawdata.Value == null)
            {
                return "...";
            }
            else return rawdata.Value.ToString();
        }
        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }
    }
}
