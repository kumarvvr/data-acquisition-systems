﻿namespace WinFormDasCoreClient
{
    partial class NewLookupTableInfoWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NewLookupTableInfoWindow));
            this.btnSaveTable = new System.Windows.Forms.Button();
            this.btnCancelTable = new System.Windows.Forms.Button();
            this.tbltdesc = new System.Windows.Forms.TextBox();
            this.tbltvname = new System.Windows.Forms.TextBox();
            this.tbltkname = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnSaveTable
            // 
            this.btnSaveTable.BackColor = System.Drawing.Color.Transparent;
            this.btnSaveTable.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnSaveTable.BackgroundImage")));
            this.btnSaveTable.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSaveTable.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnSaveTable.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSaveTable.Location = new System.Drawing.Point(308, 198);
            this.btnSaveTable.Name = "btnSaveTable";
            this.btnSaveTable.Size = new System.Drawing.Size(75, 33);
            this.btnSaveTable.TabIndex = 4;
            this.btnSaveTable.Text = "Save";
            this.btnSaveTable.UseVisualStyleBackColor = false;
            this.btnSaveTable.Click += new System.EventHandler(this.btnSaveTable_Click);
            // 
            // btnCancelTable
            // 
            this.btnCancelTable.BackColor = System.Drawing.Color.Transparent;
            this.btnCancelTable.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnCancelTable.BackgroundImage")));
            this.btnCancelTable.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnCancelTable.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnCancelTable.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancelTable.Location = new System.Drawing.Point(12, 198);
            this.btnCancelTable.Name = "btnCancelTable";
            this.btnCancelTable.Size = new System.Drawing.Size(75, 33);
            this.btnCancelTable.TabIndex = 6;
            this.btnCancelTable.Text = "Cancel";
            this.btnCancelTable.UseVisualStyleBackColor = false;
            this.btnCancelTable.Click += new System.EventHandler(this.btnCancelTable_Click);
            // 
            // tbltdesc
            // 
            this.tbltdesc.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbltdesc.Location = new System.Drawing.Point(12, 142);
            this.tbltdesc.Name = "tbltdesc";
            this.tbltdesc.Size = new System.Drawing.Size(371, 29);
            this.tbltdesc.TabIndex = 3;
            // 
            // tbltvname
            // 
            this.tbltvname.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbltvname.Location = new System.Drawing.Point(12, 86);
            this.tbltvname.Name = "tbltvname";
            this.tbltvname.Size = new System.Drawing.Size(371, 29);
            this.tbltvname.TabIndex = 2;
            // 
            // tbltkname
            // 
            this.tbltkname.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbltkname.Location = new System.Drawing.Point(12, 33);
            this.tbltkname.Name = "tbltkname";
            this.tbltkname.Size = new System.Drawing.Size(371, 29);
            this.tbltkname.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Wheat;
            this.label4.Location = new System.Drawing.Point(8, 118);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(137, 21);
            this.label4.TabIndex = 3;
            this.label4.Text = "Table Description :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Wheat;
            this.label3.Location = new System.Drawing.Point(8, 65);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(145, 21);
            this.label3.TabIndex = 4;
            this.label3.Text = "Value values Label :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Wheat;
            this.label1.Location = new System.Drawing.Point(8, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(131, 21);
            this.label1.TabIndex = 5;
            this.label1.Text = "Key values Label :";
            // 
            // NewLookupTableInfoWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DimGray;
            this.ClientSize = new System.Drawing.Size(395, 243);
            this.Controls.Add(this.btnSaveTable);
            this.Controls.Add(this.btnCancelTable);
            this.Controls.Add(this.tbltdesc);
            this.Controls.Add(this.tbltvname);
            this.Controls.Add(this.tbltkname);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "NewLookupTableInfoWindow";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "NewLookupTableInfoWindow";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSaveTable;
        private System.Windows.Forms.Button btnCancelTable;
        private System.Windows.Forms.TextBox tbltdesc;
        private System.Windows.Forms.TextBox tbltvname;
        private System.Windows.Forms.TextBox tbltkname;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;

    }
}