﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using DasCoreLib;
using System.Threading;
using BHEL.PUMPSDAS.Datatypes;
using System.Diagnostics;

using System.Reflection;

namespace WinFormDasCoreClient
{

    struct RowLabelLink
    {
        public int row;
        public Label dl, ul, al, rl;
    }

    public partial class DASWindow : Form
    {

        // Using Async Result methods.
        DasCore dascore;

        delegate List<ChannelData> HardwareWorkerThreadMethod(List<ChannelData> input);
        delegate void WindowUIUpdateDelegate(List<ChannelData> values);
        delegate void DasDataExtractDelegate();

        HardwareWorkerThreadMethod workerMethod;
        IAsyncResult asyncResult;
        AsyncCallback asyncReturn;
        static bool isAcquisitionRunning = false,acqpause = false;
        List<MeasurementProfileDetails> mTemplate;

        ResultParamsBase rp;
        MeasurementProfileBase mp,mpAvg;
        SpecifiedParamsBase sp;
        TestSetupParamsBase tsp;
        MachineDetailsBase md;
        

        WindowUIUpdateDelegate WindowUIUpdateMethod;
        DasDataExtractDelegate DasExtractMethod;

        List<ChannelData> values;
        List<ChannelData> result;

        RowLabelLink r0;
        List<RowLabelLink> rows;

        long timeperscan;
        Stopwatch sw;

        bool isMultiThread = true;
        bool continueScan = true;
        PropertyInfo pinfo;
            


        public DASWindow(DasCoreLib.DasCore dc)
        {
            InitializeComponent();
            this.dascore = dc;

            sw = new Stopwatch();

            // mTemplate = dc.GetMeasurementProfileDetails(dc.GetCurrentMachine());

            workerMethod = new HardwareWorkerThreadMethod(DoWork);
            asyncReturn = new AsyncCallback(this.WorkerCompleted);


            //dascore.ScanHardware();

            // Initialize the data container.
            values = new List<ChannelData>(0);
            for (int i = 0; i < 100; i++)
            {
                ChannelData data = new ChannelData();
                values.Add(data);
            }

            this.WindowUIUpdateMethod = this.UpdateWindowUI;
            this.DasExtractMethod = this.DasExtract;

            // this.actRes = dascore.GetActualResult();
            //  this.mp = dascore.GetActualMeasurementProfile();

            
            this.lblDate.Text = DateTime.Now.Date.Day + "-" + DateTime.Now.Date.Month + "-" + DateTime.Now.Year;
            this.lblProject.Text = "NTPC BARH 800 MW SUPER CRITICAL POWER PLANT, MAHARASHTRA";
            this.lblMachineUCode.Text = "MDG 405";
            this.lblTestReference.Text = "TR125452244";

            // Update pointers to the main objects.
            this.tsp = dascore.GetCurrentTestSetupParams();
            this.md = dascore.GetCurrentMachineDetails();
            this.sp = dascore.GetCurrentSpecifiedParams();
            this.rp = dascore.GetCurrentResultParamsAverage();
            this.mp = dascore.GetCurrentMeasurementProfile();
            this.mpAvg = dascore.GetCurrentMeasurementProfileAverage();
            
        }


        List<ChannelData> DoWork(List<ChannelData> input)
        {
            #region backup
            // Do not access UI elements from here....
            // Donot access variables from this object....
            // This method runs asynchronously...
            // Very unsafe to access UI elements or members of this class from this method.
            // Send complete information to this class and let it do it's work.
            //Random rr = new Random(DateTime.Now.Second);
            //List<ChannelData> result = new List<ChannelData>(0);
            //ChannelData data;
            //for (int i = 0; i < input.Count; i++)
            //{
            //    data = input[i];
            //    data.rawvalue = rr.NextDouble();
            //    data.computedValue = data.rawvalue * 100;
            //    result.Add(data);
            //}
            //// Simulate a long running thread.
            //Thread.Sleep(1500);
            //return result;
            #endregion
            
            try
            {
                sw.Start();
                //dascore.ScanHardware();
                Thread.Sleep(1000);
                sw.Stop();
                
            }
            catch (Exception ex)
            {
                //MessageBox.Show("ERROR : " + ex.Source + " : " + ex.Message);
                Thread.Sleep(1000);
            }

            return result;
        }

        void WorkerCompleted(IAsyncResult res)
        {

            // Access all variables in this object here itself.....
            if (isAcquisitionRunning == true && acqpause == false)
            {
                result = workerMethod.EndInvoke(asyncResult);

                

                // make use of the results here....
                
                // Manipulation.

                // Auto computation.

                // Actual Data saving.

                // Update the UI
                ExtractDasInfo();

                UpdateUI();


                this.result = null;

                // Since the command for pausing the Acquisition is not true....
                // Start acquiring data again..

                asyncResult = workerMethod.BeginInvoke(values,asyncReturn, null);
            }

            else
            {

                workerMethod.EndInvoke(asyncResult);

                isAcquisitionRunning = false;
            }
            
            
        }

        void WorkerStart()
        {
            if (isAcquisitionRunning == false)
            {
                isAcquisitionRunning = true;

                acqpause = false;

                asyncResult = workerMethod.BeginInvoke(values, asyncReturn, null);
            }
            else
            {
                MessageBox.Show("Acquisition is in progress...");
            }
        }

        void WorkerStop()
        {
            workerMethod.EndInvoke(this.asyncResult);
        }

        private void UpdateUI()
        {
            this.Invoke(this.WindowUIUpdateMethod, this.result);
        }

        private void ExtractDasInfo()
        {
            this.Invoke(DasExtractMethod);
        }

        private void DasExtract()
        {
            //this.actRes = dascore.GetActualResult();
            //this.mp = dascore.GetActualMeasurementProfile();
        }

        void UpdateWindowUI(List<ChannelData> values)

        {
            this.tbConsole.Text = "";
            this.label19.Text = mp.GetDoubleValue("suctionpressure").ToString();           

        }

        private void btnStartAcq_Click(object sender, EventArgs e)
        {
            continueScan = true;
            WorkerStart();
            this.btnStop.Enabled = false;
        }

        private void btnPause_Click(object sender, EventArgs e)
        {
            acqpause = true;
            continueScan = false;
            this.btnStop.Enabled = true;
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            //WorkerStop();
            continueScan = false;
            this.Close();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            this.lblTime.Text = DateTime.Now.Hour + " : " + DateTime.Now.Minute + " : " + DateTime.Now.Second;
        }

        private void DASWindow_Load(object sender, EventArgs e)
        {
            try
            {
                dascore.ReConfigureHardware();
                TimedWaitWindow tmw = new TimedWaitWindow(new string[] { "Intitializing Devices...",
                "Initializing Scan Lists...",
                "Checking Hardware Configuration..",
                "Completed"}, 10000);
                tmw.Text = "Hardware Communication in Progress...";
                tmw.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: Failed to configure DAS : " + ex.Source + " : " + ex.Message);
            }
        }
    }
}
