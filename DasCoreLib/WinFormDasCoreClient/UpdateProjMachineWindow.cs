﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DasCoreLib;
using DasCoreUtilities;
using BHEL.PUMPSDAS.Datatypes;

namespace WinFormDasCoreClient
{
    public partial class UpdateProjMachineWindow : Form
    {
        ProjectMachineDetails projmachine;
        ProjectDetails project;

        public UpdateProjMachineWindow(ProjectDetails project)
        {
            InitializeComponent();
            DialogResult = System.Windows.Forms.DialogResult.Cancel;
            Text = "Add Pump for - " + project.projectname;
            this.project = project;
        }

        public UpdateProjMachineWindow(ProjectDetails project, ProjectMachineDetails projmachine)
        {
            InitializeComponent();
            DialogResult = System.Windows.Forms.DialogResult.Cancel;
            Text = "Modify Pump Details for - " + project.projectname;
            this.project = project;
            this.projmachine = projmachine;
            tbMcNumber.Text = projmachine.machinenumber;
            tbMcDesc.Text = projmachine.machinedescription;
        }              

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (tbMcNumber.Text == "" || tbMcNumber.Text == null)
            {
                MessageBox.Show("Invalid Data Entered!");
                return;
            }
            else
            {
                projmachine.fk_machinecode = project.fk_machinecode;
                projmachine.fk_projectcode = project.projectcode;
                projmachine.machinenumber = tbMcNumber.Text;
                projmachine.machinedescription = DefaultConversion.GetStringValue(tbMcDesc.Text);
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                this.Close();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        public ProjectMachineDetails GetNewProjectMachineDetails()
        {
            return projmachine;
        }

    }
}
