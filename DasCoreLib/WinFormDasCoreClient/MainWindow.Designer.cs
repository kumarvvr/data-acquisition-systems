﻿namespace WinFormDasCoreClient
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newTestToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.installedMachinesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editDefaultConfigurationDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editChannelConfigurationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.commonSystemDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lookupTablesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewEditCommonLookupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.devicesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dAQHardwareToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.commonDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.manageProjectsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tESTSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.AddNewMachineMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tESTSToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.multiThreadedDASWINDOWToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripDropDownButton1 = new System.Windows.Forms.ToolStripDropDownButton();
            this.exitToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripDropDownButton2 = new System.Windows.Forms.ToolStripDropDownButton();
            this.manageProjectsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.manageTestsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripDropDownButton3 = new System.Windows.Forms.ToolStripDropDownButton();
            this.commonDataToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.commonLookupTablesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.retrieveReportsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripDropDownButton4 = new System.Windows.Forms.ToolStripDropDownButton();
            this.addNewMachineToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editDefaultConfigurationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.channelConfigurationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editDefaultUIConfigurationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.manageHardwareDevicesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.systemSettingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.databaseSettingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dataStorePathToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.runModeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonConductTest = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.editDefaultMachineErrorScriptToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(35, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newTestToolStripMenuItem});
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.Size = new System.Drawing.Size(106, 22);
            this.newToolStripMenuItem.Text = "New";
            // 
            // newTestToolStripMenuItem
            // 
            this.newTestToolStripMenuItem.Name = "newTestToolStripMenuItem";
            this.newTestToolStripMenuItem.Size = new System.Drawing.Size(130, 22);
            this.newTestToolStripMenuItem.Text = "New Test";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(106, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            // 
            // viewToolStripMenuItem
            // 
            this.viewToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.installedMachinesToolStripMenuItem,
            this.commonSystemDataToolStripMenuItem,
            this.devicesToolStripMenuItem});
            this.viewToolStripMenuItem.Name = "viewToolStripMenuItem";
            this.viewToolStripMenuItem.Size = new System.Drawing.Size(54, 20);
            this.viewToolStripMenuItem.Text = "System";
            // 
            // installedMachinesToolStripMenuItem
            // 
            this.installedMachinesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.editDefaultConfigurationDataToolStripMenuItem,
            this.editChannelConfigurationToolStripMenuItem});
            this.installedMachinesToolStripMenuItem.Name = "installedMachinesToolStripMenuItem";
            this.installedMachinesToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.installedMachinesToolStripMenuItem.Text = "Installed Machines";
            // 
            // editDefaultConfigurationDataToolStripMenuItem
            // 
            this.editDefaultConfigurationDataToolStripMenuItem.Name = "editDefaultConfigurationDataToolStripMenuItem";
            this.editDefaultConfigurationDataToolStripMenuItem.Size = new System.Drawing.Size(235, 22);
            this.editDefaultConfigurationDataToolStripMenuItem.Text = "Edit Default Configuration Data";
            // 
            // editChannelConfigurationToolStripMenuItem
            // 
            this.editChannelConfigurationToolStripMenuItem.Name = "editChannelConfigurationToolStripMenuItem";
            this.editChannelConfigurationToolStripMenuItem.Size = new System.Drawing.Size(235, 22);
            this.editChannelConfigurationToolStripMenuItem.Text = "Edit Channel Configuration";
            // 
            // commonSystemDataToolStripMenuItem
            // 
            this.commonSystemDataToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lookupTablesToolStripMenuItem});
            this.commonSystemDataToolStripMenuItem.Name = "commonSystemDataToolStripMenuItem";
            this.commonSystemDataToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.commonSystemDataToolStripMenuItem.Text = "Common System Data";
            // 
            // lookupTablesToolStripMenuItem
            // 
            this.lookupTablesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.viewEditCommonLookupToolStripMenuItem});
            this.lookupTablesToolStripMenuItem.Name = "lookupTablesToolStripMenuItem";
            this.lookupTablesToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.lookupTablesToolStripMenuItem.Text = "Lookup Tables";
            // 
            // viewEditCommonLookupToolStripMenuItem
            // 
            this.viewEditCommonLookupToolStripMenuItem.Name = "viewEditCommonLookupToolStripMenuItem";
            this.viewEditCommonLookupToolStripMenuItem.Size = new System.Drawing.Size(236, 22);
            this.viewEditCommonLookupToolStripMenuItem.Text = "View/Edit Common Lookup Data";
            // 
            // devicesToolStripMenuItem
            // 
            this.devicesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dAQHardwareToolStripMenuItem});
            this.devicesToolStripMenuItem.Name = "devicesToolStripMenuItem";
            this.devicesToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.devicesToolStripMenuItem.Text = "Devices";
            // 
            // dAQHardwareToolStripMenuItem
            // 
            this.dAQHardwareToolStripMenuItem.Name = "dAQHardwareToolStripMenuItem";
            this.dAQHardwareToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.dAQHardwareToolStripMenuItem.Text = "DAQ Hardware";
            // 
            // commonDataToolStripMenuItem
            // 
            this.commonDataToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.manageProjectsToolStripMenuItem});
            this.commonDataToolStripMenuItem.Name = "commonDataToolStripMenuItem";
            this.commonDataToolStripMenuItem.Size = new System.Drawing.Size(58, 20);
            this.commonDataToolStripMenuItem.Text = "Projects";
            // 
            // manageProjectsToolStripMenuItem
            // 
            this.manageProjectsToolStripMenuItem.Name = "manageProjectsToolStripMenuItem";
            this.manageProjectsToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.manageProjectsToolStripMenuItem.Text = "Manage Projects";
            // 
            // tESTSToolStripMenuItem
            // 
            this.tESTSToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.AddNewMachineMenuItem});
            this.tESTSToolStripMenuItem.Name = "tESTSToolStripMenuItem";
            this.tESTSToolStripMenuItem.Size = new System.Drawing.Size(58, 20);
            this.tESTSToolStripMenuItem.Text = "Settings";
            // 
            // AddNewMachineMenuItem
            // 
            this.AddNewMachineMenuItem.Name = "AddNewMachineMenuItem";
            this.AddNewMachineMenuItem.Size = new System.Drawing.Size(170, 22);
            this.AddNewMachineMenuItem.Text = "Add New Machine";
            // 
            // tESTSToolStripMenuItem1
            // 
            this.tESTSToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.multiThreadedDASWINDOWToolStripMenuItem});
            this.tESTSToolStripMenuItem1.Name = "tESTSToolStripMenuItem1";
            this.tESTSToolStripMenuItem1.Size = new System.Drawing.Size(49, 20);
            this.tESTSToolStripMenuItem1.Text = "TESTS";
            // 
            // multiThreadedDASWINDOWToolStripMenuItem
            // 
            this.multiThreadedDASWINDOWToolStripMenuItem.Name = "multiThreadedDASWINDOWToolStripMenuItem";
            this.multiThreadedDASWINDOWToolStripMenuItem.Size = new System.Drawing.Size(229, 22);
            this.multiThreadedDASWINDOWToolStripMenuItem.Text = "Multi-Threaded DAS WINDOW";
            // 
            // toolStrip1
            // 
            this.toolStrip1.AutoSize = false;
            this.toolStrip1.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.toolStrip1.BackgroundImage = global::WinFormDasCoreClient.Properties.Resources.Diffused_White3;
            this.toolStrip1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolStrip1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripDropDownButton1,
            this.toolStripSeparator2,
            this.toolStripDropDownButton2,
            this.toolStripSeparator3,
            this.toolStripDropDownButton3,
            this.toolStripSeparator4,
            this.toolStripDropDownButton4,
            this.toolStripSeparator1,
            this.toolStripButtonConductTest,
            this.toolStripSeparator5,
            this.toolStripButton1});
            this.toolStrip1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.toolStrip1.Location = new System.Drawing.Point(1, 1);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStrip1.Size = new System.Drawing.Size(1014, 40);
            this.toolStrip1.TabIndex = 2;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripDropDownButton1
            // 
            this.toolStripDropDownButton1.BackColor = System.Drawing.Color.Transparent;
            this.toolStripDropDownButton1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.toolStripDropDownButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripDropDownButton1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem1});
            this.toolStripDropDownButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton1.Name = "toolStripDropDownButton1";
            this.toolStripDropDownButton1.Size = new System.Drawing.Size(47, 37);
            this.toolStripDropDownButton1.Text = "&File";
            // 
            // exitToolStripMenuItem1
            // 
            this.exitToolStripMenuItem1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.exitToolStripMenuItem1.Name = "exitToolStripMenuItem1";
            this.exitToolStripMenuItem1.Size = new System.Drawing.Size(114, 26);
            this.exitToolStripMenuItem1.Text = "Exit";
            this.exitToolStripMenuItem1.Click += new System.EventHandler(this.exitToolStripMenuItem1_Click_1);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 40);
            // 
            // toolStripDropDownButton2
            // 
            this.toolStripDropDownButton2.BackColor = System.Drawing.Color.Transparent;
            this.toolStripDropDownButton2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.toolStripDropDownButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripDropDownButton2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.manageProjectsToolStripMenuItem1,
            this.manageTestsToolStripMenuItem});
            this.toolStripDropDownButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton2.Name = "toolStripDropDownButton2";
            this.toolStripDropDownButton2.Size = new System.Drawing.Size(78, 37);
            this.toolStripDropDownButton2.Text = "&Projects";
            // 
            // manageProjectsToolStripMenuItem1
            // 
            this.manageProjectsToolStripMenuItem1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.manageProjectsToolStripMenuItem1.Name = "manageProjectsToolStripMenuItem1";
            this.manageProjectsToolStripMenuItem1.Size = new System.Drawing.Size(205, 26);
            this.manageProjectsToolStripMenuItem1.Text = "Manage Projects";
            this.manageProjectsToolStripMenuItem1.Click += new System.EventHandler(this.manageProjectsToolStripMenuItem1_Click);
            // 
            // manageTestsToolStripMenuItem
            // 
            this.manageTestsToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.manageTestsToolStripMenuItem.Name = "manageTestsToolStripMenuItem";
            this.manageTestsToolStripMenuItem.Size = new System.Drawing.Size(205, 26);
            this.manageTestsToolStripMenuItem.Text = "Manage Tests";
            this.manageTestsToolStripMenuItem.Click += new System.EventHandler(this.manageTestsToolStripMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 40);
            // 
            // toolStripDropDownButton3
            // 
            this.toolStripDropDownButton3.BackColor = System.Drawing.Color.Transparent;
            this.toolStripDropDownButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripDropDownButton3.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.commonDataToolStripMenuItem1,
            this.retrieveReportsToolStripMenuItem});
            this.toolStripDropDownButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton3.Name = "toolStripDropDownButton3";
            this.toolStripDropDownButton3.Size = new System.Drawing.Size(55, 37);
            this.toolStripDropDownButton3.Text = "&Data";
            // 
            // commonDataToolStripMenuItem1
            // 
            this.commonDataToolStripMenuItem1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.commonDataToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.commonLookupTablesToolStripMenuItem});
            this.commonDataToolStripMenuItem1.Name = "commonDataToolStripMenuItem1";
            this.commonDataToolStripMenuItem1.Size = new System.Drawing.Size(324, 26);
            this.commonDataToolStripMenuItem1.Text = "View / Edit System Common Data";
            // 
            // commonLookupTablesToolStripMenuItem
            // 
            this.commonLookupTablesToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.commonLookupTablesToolStripMenuItem.Name = "commonLookupTablesToolStripMenuItem";
            this.commonLookupTablesToolStripMenuItem.Size = new System.Drawing.Size(203, 26);
            this.commonLookupTablesToolStripMenuItem.Text = "Common Tables";
            this.commonLookupTablesToolStripMenuItem.Click += new System.EventHandler(this.commonLookupTablesToolStripMenuItem_Click);
            // 
            // retrieveReportsToolStripMenuItem
            // 
            this.retrieveReportsToolStripMenuItem.Name = "retrieveReportsToolStripMenuItem";
            this.retrieveReportsToolStripMenuItem.Size = new System.Drawing.Size(324, 26);
            this.retrieveReportsToolStripMenuItem.Text = "Retrieve Reports";
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 40);
            // 
            // toolStripDropDownButton4
            // 
            this.toolStripDropDownButton4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.toolStripDropDownButton4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripDropDownButton4.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addNewMachineToolStripMenuItem,
            this.editDefaultConfigurationToolStripMenuItem,
            this.channelConfigurationToolStripMenuItem,
            this.editDefaultUIConfigurationToolStripMenuItem,
            this.editDefaultMachineErrorScriptToolStripMenuItem,
            this.manageHardwareDevicesToolStripMenuItem,
            this.systemSettingsToolStripMenuItem});
            this.toolStripDropDownButton4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.toolStripDropDownButton4.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton4.Name = "toolStripDropDownButton4";
            this.toolStripDropDownButton4.Size = new System.Drawing.Size(126, 37);
            this.toolStripDropDownButton4.Text = "&Administration";
            this.toolStripDropDownButton4.Click += new System.EventHandler(this.toolStripDropDownButton4_Click);
            // 
            // addNewMachineToolStripMenuItem
            // 
            this.addNewMachineToolStripMenuItem.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.addNewMachineToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.addNewMachineToolStripMenuItem.Name = "addNewMachineToolStripMenuItem";
            this.addNewMachineToolStripMenuItem.Size = new System.Drawing.Size(376, 26);
            this.addNewMachineToolStripMenuItem.Text = "Add / Update Pump Type";
            this.addNewMachineToolStripMenuItem.Click += new System.EventHandler(this.addNewMachineToolStripMenuItem_Click);
            // 
            // editDefaultConfigurationToolStripMenuItem
            // 
            this.editDefaultConfigurationToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.editDefaultConfigurationToolStripMenuItem.Name = "editDefaultConfigurationToolStripMenuItem";
            this.editDefaultConfigurationToolStripMenuItem.Size = new System.Drawing.Size(376, 26);
            this.editDefaultConfigurationToolStripMenuItem.Text = "Edit Pump Default Data";
            this.editDefaultConfigurationToolStripMenuItem.Click += new System.EventHandler(this.editDefaultConfigurationToolStripMenuItem_Click);
            // 
            // channelConfigurationToolStripMenuItem
            // 
            this.channelConfigurationToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.channelConfigurationToolStripMenuItem.Name = "channelConfigurationToolStripMenuItem";
            this.channelConfigurationToolStripMenuItem.Size = new System.Drawing.Size(376, 26);
            this.channelConfigurationToolStripMenuItem.Text = "Edit Pump Default Channel Configuration";
            this.channelConfigurationToolStripMenuItem.Click += new System.EventHandler(this.channelConfigurationToolStripMenuItem_Click);
            // 
            // editDefaultUIConfigurationToolStripMenuItem
            // 
            this.editDefaultUIConfigurationToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.editDefaultUIConfigurationToolStripMenuItem.Name = "editDefaultUIConfigurationToolStripMenuItem";
            this.editDefaultUIConfigurationToolStripMenuItem.Size = new System.Drawing.Size(376, 26);
            this.editDefaultUIConfigurationToolStripMenuItem.Text = "Edit Pump Default UI Configuration";
            this.editDefaultUIConfigurationToolStripMenuItem.Click += new System.EventHandler(this.editDefaultUIConfigurationToolStripMenuItem_Click);
            // 
            // manageHardwareDevicesToolStripMenuItem
            // 
            this.manageHardwareDevicesToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.manageHardwareDevicesToolStripMenuItem.Name = "manageHardwareDevicesToolStripMenuItem";
            this.manageHardwareDevicesToolStripMenuItem.Size = new System.Drawing.Size(376, 26);
            this.manageHardwareDevicesToolStripMenuItem.Text = "Manage Hardware Devices";
            this.manageHardwareDevicesToolStripMenuItem.Click += new System.EventHandler(this.manageHardwareDevicesToolStripMenuItem_Click);
            // 
            // systemSettingsToolStripMenuItem
            // 
            this.systemSettingsToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.systemSettingsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.databaseSettingsToolStripMenuItem,
            this.dataStorePathToolStripMenuItem,
            this.runModeToolStripMenuItem});
            this.systemSettingsToolStripMenuItem.Name = "systemSettingsToolStripMenuItem";
            this.systemSettingsToolStripMenuItem.Size = new System.Drawing.Size(376, 26);
            this.systemSettingsToolStripMenuItem.Text = "System Settings";
            // 
            // databaseSettingsToolStripMenuItem
            // 
            this.databaseSettingsToolStripMenuItem.Name = "databaseSettingsToolStripMenuItem";
            this.databaseSettingsToolStripMenuItem.Size = new System.Drawing.Size(214, 26);
            this.databaseSettingsToolStripMenuItem.Text = "Database Settings";
            this.databaseSettingsToolStripMenuItem.Click += new System.EventHandler(this.databaseSettingsToolStripMenuItem_Click);
            // 
            // dataStorePathToolStripMenuItem
            // 
            this.dataStorePathToolStripMenuItem.Name = "dataStorePathToolStripMenuItem";
            this.dataStorePathToolStripMenuItem.Size = new System.Drawing.Size(214, 26);
            this.dataStorePathToolStripMenuItem.Text = "Path Settings";
            // 
            // runModeToolStripMenuItem
            // 
            this.runModeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.toolStripMenuItem2});
            this.runModeToolStripMenuItem.Name = "runModeToolStripMenuItem";
            this.runModeToolStripMenuItem.Size = new System.Drawing.Size(214, 26);
            this.runModeToolStripMenuItem.Text = "Run Mode";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Checked = true;
            this.toolStripMenuItem1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(165, 26);
            this.toolStripMenuItem1.Text = "Live";
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(165, 26);
            this.toolStripMenuItem2.Text = "Simulation";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 40);
            // 
            // toolStripButtonConductTest
            // 
            this.toolStripButtonConductTest.BackColor = System.Drawing.Color.LightCoral;
            this.toolStripButtonConductTest.Image = global::WinFormDasCoreClient.Properties.Resources.AQUA_ICONS_SYSTEM_DIAGNOSTICS;
            this.toolStripButtonConductTest.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonConductTest.Name = "toolStripButtonConductTest";
            this.toolStripButtonConductTest.Size = new System.Drawing.Size(136, 37);
            this.toolStripButtonConductTest.Text = "Conduct Test";
            this.toolStripButtonConductTest.Click += new System.EventHandler(this.toolStripButtonConductTest_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 40);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(36, 37);
            this.toolStripButton1.Text = "toolStripButton1";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click_1);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.toolStrip1, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.Padding = new System.Windows.Forms.Padding(1);
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1016, 734);
            this.tableLayoutPanel1.TabIndex = 3;
            // 
            // editDefaultMachineErrorScriptToolStripMenuItem
            // 
            this.editDefaultMachineErrorScriptToolStripMenuItem.Name = "editDefaultMachineErrorScriptToolStripMenuItem";
            this.editDefaultMachineErrorScriptToolStripMenuItem.Size = new System.Drawing.Size(376, 26);
            this.editDefaultMachineErrorScriptToolStripMenuItem.Text = "Edit Pump Default Error Script";
            this.editDefaultMachineErrorScriptToolStripMenuItem.Click += new System.EventHandler(this.editDefaultMachineErrorScriptToolStripMenuItem_Click);
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DimGray;
            this.BackgroundImage = global::WinFormDasCoreClient.Properties.Resources.window_skin11;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1016, 734);
            this.Controls.Add(this.tableLayoutPanel1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Pumps Testing Department - Data Acquisition System";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tESTSToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem AddNewMachineMenuItem;
        private System.Windows.Forms.ToolStripMenuItem commonDataToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem installedMachinesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editDefaultConfigurationDataToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem commonSystemDataToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lookupTablesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewEditCommonLookupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem manageProjectsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newTestToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem devicesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dAQHardwareToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editChannelConfigurationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tESTSToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem multiThreadedDASWINDOWToolStripMenuItem;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton1;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem1;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton2;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton3;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton4;
        private System.Windows.Forms.ToolStripMenuItem manageProjectsToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem commonDataToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem addNewMachineToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem commonLookupTablesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editDefaultConfigurationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem manageHardwareDevicesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem channelConfigurationToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton toolStripButtonConductTest;
        private System.Windows.Forms.ToolStripMenuItem manageTestsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem systemSettingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem databaseSettingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dataStorePathToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem runModeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editDefaultUIConfigurationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.ToolStripMenuItem retrieveReportsToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripMenuItem editDefaultMachineErrorScriptToolStripMenuItem;
    }
}

