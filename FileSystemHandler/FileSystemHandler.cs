﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
//using DataFormats;


namespace FileManager
{    
    public class FileSystemHandler
    {
        #region Event Handling

        /*public event ExistingFileEventHandler ExistingFileEvent;

        public event FileNotFoundEventHandler FileNotFoundEvent;

        public event FileUnreadableEventHandler FileUnreadableEvent;

        public event DirNotFoundEventHandler DirNotFoundEvent;*/

        #endregion
        
        public static void CreateFolder(String folderPath)
        {
            try
            {
                DirectoryInfo diSrc = new DirectoryInfo(folderPath);
                diSrc.Create();
            }
            catch (Exception e)
            {
                throw(new Exception("\nCould not create folder: \n" + folderPath));
            }

        }

        public static void CreateMultipleFolders(List<String> folderList)
        {
            for (int i = 0; i < folderList.Count; i++)
            {
                try
                {
                    CreateFolder(folderList[i]);
                }
                catch (Exception ex)
                {
                    //Do nothing
                }
            }
        }

        public static void DeleteFolder(String folderPath, bool delAllFiles)
        {
            try
            {
                DirectoryInfo diSrc = new DirectoryInfo(folderPath);
                diSrc.Delete(delAllFiles);
            }
            catch (Exception e)
            {
                throw (new Exception("\nCould not delete folder!\n" + e.Message));
            }
        }

        public static bool RenameFolder(string folderPath, string newName)
        {
            try
            {
                DirectoryInfo diSrc = new DirectoryInfo(folderPath);
                if (diSrc.Exists)
                {
                    /*
                    DirectoryInfo newdi = diSrc.Parent;
                    string newDir = newdi.FullName + "\\" + newName;
                    FileSystemHandler.CreateFolder(newDir);
                    FileInfo[] fi = newdi.GetFiles();
                    for (int i = 0; i < fi.Length; i++)
                    {
                        fi[i].MoveTo()
                    }
                    
                     */

                }
                else return false;
            }
            catch (Exception e)
            {
                throw (new Exception("\nCould not rename folder!\n" + e.Message));
            }
            return true;
        }

        public static bool CopyFile(String sourceFileFullName, String destinationFileFullName, bool overwrite)
        {
            try
            {
                FileInfo fi = new FileInfo(sourceFileFullName);
                if (fi.Exists)
                {
                    fi.CopyTo(destinationFileFullName, overwrite);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool CopyFile(String sourcefileFullName, String destinationPath, String destFileName, bool overwrite)
        {
            try
            {
                FileInfo fi = new FileInfo(sourcefileFullName);
                if (fi.Exists)
                {
                    fi.CopyTo(destinationPath + "\\" + destFileName, overwrite);
                    return true;
                }
                else return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static List<String> CopyListOfFiles(List<String> sourceFilePathList, String destDir, bool overWrite)
        {
            List<String> filesNotCopied = new List<String>(0);            

            DirectoryInfo di = new DirectoryInfo(destDir);
            if (!di.Exists)
                CreateFolder(destDir);

            for (int i = 0; i < sourceFilePathList.Count; i++)
            {
                try
                {
                    FileInfo fi = new FileInfo(sourceFilePathList[i]);
                    fi.CopyTo(destDir+fi.Name, overWrite);
                }
                catch (Exception e)
                {
                    filesNotCopied.Add(sourceFilePathList[i]);
                }
            }

            return filesNotCopied;
        }

        public static List<String> CopyListOfFiles(List<String> sourceFilePathList, String destDir, List<String> destFileNameList, bool overWrite)
        {
            List<String> filesNotCopied = new List<String>(0);

            for (int i = 0; i < sourceFilePathList.Count; i++)
            {
                try
                {
                    FileInfo fi = new FileInfo(sourceFilePathList[i]);
                    fi.CopyTo(destDir+destFileNameList[i] , overWrite);
                }
                catch (Exception e)
                {
                    filesNotCopied.Add(sourceFilePathList[i]);
                }
            }

            return filesNotCopied;
        }

        public static List<String> CopyAllFilesInFolder(String sourceDir, String destDir, bool overWrite)
        {
            List<String> filesNotCopied = new List<String>(0);

            DirectoryInfo diSrc = new DirectoryInfo(sourceDir);
            FileInfo[] fi = diSrc.GetFiles();            
            DirectoryInfo diDest = new DirectoryInfo(destDir);

            if (!diDest.Exists)
                CreateFolder(destDir);
                                   
            for (int i = 0; i < fi.Length; i++)
            {
                try
                {                    
                    FileInfo fiDest = fi[i];
                    fiDest.CopyTo(destDir+fi[i].Name, overWrite);
                }

                catch (Exception e)
                {
                    filesNotCopied.Add(fi[i].FullName);
                }
            }

            return filesNotCopied;
        }

        public static List<String> CopyAllFilesInFolder(String sourceDir, String fileExtension, String destDir, bool overWrite)
        {
            List<String> filesNotCopied = new List<String>(0);

            DirectoryInfo diSrc = new DirectoryInfo(sourceDir);
            FileInfo[] fi = diSrc.GetFiles();
            DirectoryInfo diDest = new DirectoryInfo(destDir);

            for (int i = 0; i < fi.Length; i++)
            {
                try
                {
                    FileInfo fiDest = fi[i];

                    if (fi[i].Extension.Equals(fileExtension))
                    {
                        fiDest.CopyTo(diDest.Name + fi[i].Name, overWrite);
                    }
                }

                catch (Exception e)
                {
                    filesNotCopied.Add(fi[i].FullName);
                }
            }

            return filesNotCopied;
        }

        public static List<String> DeleteAllFilesInFolder(String sourceDir, bool delAllFiles)
        {
            List<String> filesNotDeleted = new List<String>(0);
            DirectoryInfo di = new DirectoryInfo(sourceDir);
            FileInfo[] fi = di.GetFiles();

            for (int i = 0; i < fi.Length; i++)
            {
                try
                {
                    fi[i].Delete();
                }

                catch (Exception e)
                {
                    filesNotDeleted.Add(fi[i].Name);
                }
            }

            return filesNotDeleted;
            
        }

        public static bool DeleteFile(string filepath)
        {
            try
            {
                FileInfo fi = new FileInfo(filepath);
                fi.Delete();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            return true;
        }

        public static List<String> DeleteListOfFiles(List<String> sourceFilePathList)
        {
            List<String> filesNotDeleted = new List<String>(0);

            for (int i = 0; i < sourceFilePathList.Count; i++)
            {
                try
                {
                    FileInfo fi = new FileInfo(sourceFilePathList[i]);
                    fi.Delete();
                }

                catch (Exception e)
                {
                    filesNotDeleted.Add(sourceFilePathList[i]);
                }
            }

            return filesNotDeleted;
        }

        public static List<String> GetListOfFilesInFolder(String sourceDir, String searchPattern)
        {
            List<String> fileList = new List<string>(0);

            DirectoryInfo diSrc = new DirectoryInfo(sourceDir);
            FileInfo[] fi = diSrc.GetFiles(searchPattern);

            for (int i = 0; i < fi.Length; i++)
            {
                fileList.Add(fi[i].FullName);
            }

            return fileList;
        }

        public static List<String> GetListOfFilesInFolder(String sourceDir)
        {
            List<String> fileList = new List<String>(0);

            DirectoryInfo diSrc = new DirectoryInfo(sourceDir);
            FileInfo[] fi = diSrc.GetFiles();

            for (int i = 0; i < fi.Length; i++)
            {
                fileList.Add(fi[i].FullName);
            }

            return fileList;
        }

        public static bool CheckIfFileAlreadyExists(String filename, String destDir)
        {            
            DirectoryInfo diDest = new DirectoryInfo(destDir);
            if (diDest.GetFiles(filename).Length > 0)
                return true;
            else return false;
        }

        public static String GetFileNameWithExtension(String filepath)
        {
            FileInfo fi = new FileInfo(filepath);
            return fi.Name;
        }

        public static bool CheckIfFolderExists(string dir)
        {
            DirectoryInfo dinfo = new DirectoryInfo(dir);
            if (dinfo.Exists)
                return true;
            return false;
        }

        public static List<string> FindFiles(string path, string searchCriterion)
        {
            DirectoryInfo dinfo = new DirectoryInfo(path);
            FileInfo[] fi = { };
            List<string> filesFound = new List<string>();

            if (dinfo.Exists)
            {
                fi = dinfo.GetFiles(searchCriterion);

                for (int i = 0; i < fi.Length; i++)
                {
                    filesFound.Add(fi[i].FullName);
                }
            }

            return filesFound;
        }

        public static void CreateTextFile(string fullpath, bool replace)
        {
            FileInfo fi = new FileInfo(fullpath);
            if (fi.Exists)
            {
                if (replace)
                {
                    fi.Delete();
                    fi.Create();
                }
            }
            else fi.Create();
        }

        public static void WriteToTextFile(string fullpath, bool append, string text)
        {
            StreamWriter writer = null;
            try
            {
                FileInfo fi = new FileInfo(fullpath);
                writer = new StreamWriter(fullpath, append, Encoding.Unicode);
                writer.Write(text);
            }
            catch (Exception e)
            {
                throw new Exception("Failed to write to file : " + fullpath + " : " + e.Source + " : " + e.Message);
            }
            finally
            {
                writer.Flush();
                writer.Dispose();
            }           
            
        }

        public static string ReadFileToText(string fullpath)
        {
            StreamReader  reader =  null;
            string result;
            try
            {
                FileInfo fi = new FileInfo(fullpath);

                reader = new StreamReader(fullpath, Encoding.Unicode);

                result =  reader.ReadToEnd();
                                
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to read file : " + fullpath + " : " + ex.Source + " : " + ex.Message);
            }
            finally
            {
                reader.Dispose();
            }

            return result;
        }
    }    
}
